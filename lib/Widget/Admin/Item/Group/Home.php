<?php
class Widget_Admin_Item_Group_Home extends Widget_Site {
	protected $groups;
	public function __construct() {
		parent::__construct();
		$this->groups=Model_Item_Group::Get(NULL,NULL);
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Group/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$group=new Model_Item_Group();
				$group->setName($this->data->name);
				$group->save();
				\Pecee\Router::Refresh();
			}
		}
	}
}