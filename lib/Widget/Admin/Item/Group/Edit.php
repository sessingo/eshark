<?php
class Widget_Admin_Item_Group_Edit extends Widget_Site {
	protected $groups;
	protected $group;
	public function __construct($itemGroupId) {
		parent::__construct();
		$this->groups=Model_Item_Group::Get(NULL,NULL);
		$this->group=Model_Item_Group::GetById($itemGroupId);
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Group/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			
			if(!$this->hasErrors()) {
				$this->group->setName($this->data->name);
				$this->group->update();
				\Pecee\Router::Refresh();
			}
		}
	}
}