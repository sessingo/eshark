<?php
class Widget_Admin_Item_Group_Delete extends Widget_Site {
	public function __construct($itemGroupId) {
		parent::__construct();
		$group=Model_Item_Group::GetById($itemGroupId);
		if($group->hasRow()) {
			$group->delete();
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index'));
	}
}