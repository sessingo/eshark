<?php
class Widget_Admin_Case_Subject_Delete extends Widget_Site {
	public function __construct($caseSubjectId) {
		parent::__construct();
		$subject=Model_Case_Subject::GetById($caseSubjectId);
		if($subject->hasRow()) {
			$subject->delete();
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index'));
	}
}