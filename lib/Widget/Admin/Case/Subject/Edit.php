<?php
class Widget_Admin_Case_Subject_Edit extends Widget_Site {
	protected $subjects;
	protected $subject;
	public function __construct($caseSubjectId) {
		parent::__construct();
		$this->subjects=Model_Case_Subject::Get(NULL, NULL);
		$this->subject=Model_Case_Subject::GetById($caseSubjectId);
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Case/Subject'), 'subject', new UI_Form_Validate_NotNullOrEmpty());
			
			if(!$this->hasErrors()) {
				$this->subject->setTitle($this->data->subject);
				$this->subject->update();
				\Pecee\Router::Refresh();
			}
		}
	}
}