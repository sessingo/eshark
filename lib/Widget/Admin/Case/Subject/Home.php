<?php
class Widget_Admin_Case_Subject_Home extends Widget_Site {
	protected $subjects;
	public function __construct() {
		parent::__construct();
		$this->subjects=Model_Case_Subject::Get(NULL, NULL);
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Case/Subject'), 'subject', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$subject=new Model_Case_Subject();
				$subject->setTitle($this->data->subject);
				$subject->save();
				\Pecee\Router::Refresh();
			}
		}
	}
}