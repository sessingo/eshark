<?php
class Widget_Admin_User_Edit extends Widget_Site {
	protected $user;
	public function __construct($userId) {
		parent::__construct();
		
		$this->user=Model_User::GetByUserID($userId);
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/User/Username'), 'username', new UI_Form_Validate_Username());
			$this->addInputValidation($this->_('Admin/User/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Type'), 'type', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Email'), 'email', new UI_Form_Validate_Email());
				
			if(!$this->hasErrors()) {
				$type=(in_array($this->data->type, Model_User::$TYPES)) ? $this->data->type : Model_User::TYPE_EMPLOYEE;
				$this->user->setUsername($this->data->username);
				if($this->data->password) {
					$this->addInputValidation($this->_('Admin/User/Password'), 'password', new UI_Form_Validate_NotNullOrEmpty());
					if(!$this->hasErrors()) {
						$this->user->setPassword($this->data->password);
					}
				}
				$this->user->setEmail($this->data->email);
				$this->user->setData(array('name' => $this->data->name, 'jobtitle' => $this->data->jobtitle, 'type' => $type));
				$this->user->update();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, ''));
			}
		}
	}
}