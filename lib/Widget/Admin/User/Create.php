<?php
class Widget_Admin_User_Create extends Widget_Site {
	public function __construct() {
		parent::__construct();
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/User/Username'), 'username', new UI_Form_Validate_Username());
			$this->addInputValidation($this->_('Admin/User/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Type'), 'type', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Password'), 'password', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Email'), 'email', new UI_Form_Validate_Email());
			
			if(!$this->hasErrors()) {
				$user=new Model_User();
				$type=(in_array($this->data->type, Model_User::$TYPES)) ? $this->data->type : Model_User::TYPE_EMPLOYEE;
				$user->setUsername($this->data->username);
				$user->setPassword($this->data->password);
				$user->setEmail($this->data->email);
				$user->setData(array('name' => $this->data->name, 'jobtitle' => $this->data->jobtitle, 'type' => $type));
				$user->save();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, ''));
			}	
		}
	}
}