<?php
class Widget_Admin_User_Delete extends Widget_Site {
	public function __construct($userId) {
		parent::__construct();
		$user=Model_User::GetByUserID($userId);
		if($user->hasRow()) {
			$user->delete();
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, ''));
	}
}