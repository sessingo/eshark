<?php
class Widget_Admin_Company_Edit extends Widget_Site {
	public function __construct() {
		parent::__construct();
		$this->addInputValidation($this->_('Admin/Company/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
		$this->addInputValidation($this->_('Admin/Company/Country'), 'country', new UI_Form_Validate_Country());
		$this->addInputValidation($this->_('Admin/Company/Workhours'), 'workhours', new UI_Form_Validate_Float());
		
		$this->addInputValidation($this->_('Admin/Company/Reminder'), 'reminderDays', new UI_Form_Validate_Integer());
		$this->addInputValidation($this->_('Admin/Company/FirstReminder'), 'firstReminderDays', new UI_Form_Validate_Integer());
		$this->addInputValidation($this->_('Admin/Company/SecondReminder'), 'secondReminderDays', new UI_Form_Validate_Integer());
		$this->addInputValidation($this->_('Admin/Company/DebtCollection'), 'debtCollectionDays', new UI_Form_Validate_Integer());
		
		$hasLogo=$this->files->logo && $this->files->logo->tmpName != '';
		if($hasLogo) {
			$this->addInputValidation($this->_('Admin/Company/Logo'), 'logo', array(new UI_Form_Validate_File_NotNullOrEmpty(), new UI_Form_Validate_File_AllowedExtension(array('bmp', 'png', 'jpg', 'gif'))));
		}
		
		if($this->isPostBack() && !$this->hasErrors()) {
			$this->company->setName($this->data->name);
			$this->company->setCountry($this->data->country);
			$this->company->data->vatno=$this->data->vatno;
				
			if($hasLogo) {
				$filename=sprintf('%s.%s', \Pecee\Url::Encode(strtolower($this->data->name)), \Pecee\IO\File::GetExtension($this->files->logo->name));
				$this->files->logo->move(\Pecee\Registry::GetInstance()->get('StoragePath') . DIRECTORY_SEPARATOR . $filename);
				$logo=$this->company->getLogo();
				if($logo instanceof Model_Image && $logo->hasRow()) {
					$logo->setFilename($filename);
					$logo->setOriginalFilename($this->files->logo->name);
					$logo->setType($this->files->logo->type);
					$logo->setBytes($this->files->logo->size);
					$logo->update();
				} else {
					$file=new \Pecee\Model\File\ModelFile($filename);
					$file->setOriginalFilename($this->files->logo->name);
					$file->setType($this->files->logo->type);
					$file->setBytes($this->files->logo->size);
					$file->save();
					$this->company->setLogoFileID($file->getFileID());
				}
			}
			
			$this->company->setVatPercentage($this->data->vatpercentage);
			$this->company->data->workhours=\Pecee\Float::ParseFloat($this->data->workhours);
			
			/* Set invoice data */
			$this->company->data->paymentDays=$this->data->paymentDays;
			$this->company->data->reminderDays=$this->data->reminderDays;
			$this->company->data->firstReminderDays=$this->data->firstReminderDays;
			$this->company->data->secondReminderDays=$this->data->secondReminderDays;
			$this->company->data->debtCollectionDays=$this->data->debtCollectionDays;
			
			/* Contact stuff */
			$this->company->data->address = $this->data->address;
			$this->company->data->postalnumber = $this->data->postalnumber;
			$this->company->data->city = $this->data->city;
			$this->company->data->phone = $this->data->phone;
			$this->company->data->cellphone = $this->data->cellphone;
			$this->company->data->website = $this->data->website;
			$this->company->data->ean = $this->data->ean;
			
			$this->company->update();
			
			\Pecee\Router::Refresh();
		}
	}
}