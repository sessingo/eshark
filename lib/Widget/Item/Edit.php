<?php
class Widget_Item_Edit extends Widget_Item_Home {
	protected $item;
	public function __construct($itemId) {
		parent::__construct();
		$this->item=Model_Item::GetById($itemId);
		if(!$this->item->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index'));
		}
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Item/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Item/BuyPrice'), 'buyPrice', new UI_Form_Validate_Float(TRUE));
			$this->addInputValidation($this->_('Item/SellPrice'), 'salePrice', new UI_Form_Validate_Float(TRUE));
			if(!$this->hasErrors()) {
				$this->item->setUserID(Model_User::Current()->getUserID());
				$this->item->setItemGroupID($this->data->itemGroupId);
				/* Set data */
				$form=$this->data->getArray();
				unset($form['buyprice']);
				unset($form['saleprice']);
				unset($form['submit']);
				$this->item->setData($form);
				
				$this->item->data->buyprice=\Pecee\Float::ParseFloat($this->data->buyPrice);
				$this->item->data->saleprice=\Pecee\Float::ParseFloat($this->data->salePrice);
				
				$this->item->save();
		
				// Redirect
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index', array($this->item->getItemGroupID())));
			}
		}
	}
}