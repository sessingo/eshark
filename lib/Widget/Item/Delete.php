<?php
class Widget_Item_Delete extends Widget_Site {
	public function __construct($itemId) {
		parent::__construct();
		$item=Model_Item::GetById($itemId);
		if($item->hasRow()) {
			$item->delete();
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index'));
	}
}