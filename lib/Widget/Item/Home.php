<?php
class Widget_Item_Home extends Widget_Site {
	protected $groupsMenu;
	protected $groups;
	protected $items;
	protected $itemGroupId;
	public function __construct($itemGroupId=NULL) {
		parent::__construct();
		$this->itemGroupId=$itemGroupId;
		$this->groups=Model_Item_Group::Get();
		$this->items=Model_Item::Get($itemGroupId);
		
		$this->groupsMenu=new \Pecee\UI\Menu\Menu();
		$this->groupsMenu->addAttribute('class', 'sub-nav');
		$this->groupsMenu->addItem($this->_('Item/All'), \Pecee\Router::GetRoute(NULL, '', array()));
		$active=FALSE;
		if($this->groups->hasRows()) {
			foreach($this->groups->getRows() as $group) {
				$item=$this->groupsMenu->addItem($group->getName(), \Pecee\Router::GetRoute(NULL, NULL, array($group->getItemGroupID())));
				if($itemGroupId == $group->getItemGroupID()) {
					$item->addClass('active');
					$active=TRUE;
				}
			}
		}
		if(!$active) {
			$this->groupsMenu->getFirst()->addClass('active');
		}
		
	}
}