<?php
class Widget_Item_Add extends Widget_Item_Home {
	protected $itemGroupId;
	public function __construct($itemGroupId=NULL) {
		parent::__construct();
		$this->itemGroupId=$itemGroupId;
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Item/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Item/BuyPrice'), 'buyPrice', new UI_Form_Validate_Float(TRUE));
			$this->addInputValidation($this->_('Item/SellPrice'), 'salePrice', new UI_Form_Validate_Float(TRUE));
			if(!$this->hasErrors()) {
				$item=new Model_Item();
				$item->setUserID(Model_User::Current()->getUserID());
				$item->setItemGroupID($this->data->itemGroupId);
				/* Set data */
				$form=$this->data->getArray();
				unset($form['submit']);
				$item->data->buyprice=\Pecee\Float::ParseFloat($this->data->buyPrice);
				$item->data->saleprice=\Pecee\Float::ParseFloat($this->data->salePrice);
				unset($form['buyprice']);
				unset($form['saleprice']);
				$item->setData($form);
				$item->save();
				
				// Redirect
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index', NULL, NULL, TRUE));
			}
		}
	}
}