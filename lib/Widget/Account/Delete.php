<?php
class Widget_Account_Delete extends Widget_Site {
	public function __construct($accountId) {
		parent::__construct();
		$account=Model_Account::GetById($accountId);
		if($account->hasRows()) {
			$account->delete();
		}
		\Pecee\Router::GoBack();
	}
}