<?php
class Widget_Account_Home extends Widget_Site {
	protected $accountId;
	protected $accounts;
	protected $accountsMenu;
	protected $transfers;
	protected $balance;
	public function __construct($accountId=NULL) {
		parent::__construct();
		$this->accountId=$accountId;
		$this->accounts=Model_Account::Get();
		$this->transfers=Model_Transfer::GetByAccountId($accountId,FALSE,$this->getParam('from'),$this->getParam('to'));
		$this->accountsMenu=new \Pecee\UI\Menu\Menu();
		$this->accountsMenu->addAttribute('class', 'sub-nav');
		if($this->accounts->hasRows()) {
			foreach($this->accounts->getRows() as $account) {
				$item=$this->accountsMenu->addItem($account->getName(), \Pecee\Router::GetRoute(NULL, 'index', array($account->getAccountID())));
				if($account->getAccountID() == $accountId) {
					$item->addClass('active');
				}
			}
		}
	}
	
	protected function getBalance(Model_Transfer &$transfer) {
		$this->balance += $transfer->getAmount();
		return $this->balance;
	}
	
	public function formatTotal(&$amount) {
		
	}
}