<?php
abstract class Widget_Site extends \Pecee\Widget\Widget {
	protected $userMenu;
	protected $topmenu;
	protected $company;
	public function __construct($authLevel=0) {
		$this->checkSecure();
		$this->setAuthLevel($authLevel);
		parent::__construct();
		$this->getSite()->setTitle('e-shark');

		$this->getSite()->addWrappedJs('jquery-1.7.2.min.js');
		$this->getSite()->addWrappedJs('jquery-ui-1.8.21.custom.min.js');
		$this->getSite()->addWrappedJs('dialog.js');
		$this->getSite()->addWrappedJs('pecee.js');
		$this->getSite()->addWrappedJs('global.js');
		$this->getSite()->addWrappedCss('jquery-ui-1.8.21.custom.css');
		$this->getSite()->addWrappedCss('style.css');
		$this->getSite()->setDebug(TRUE);
		
		/* Init menus */
		$this->topmenu=new \Pecee\UI\Menu\Menu();
		$this->topmenu->addAttribute('class', 'menu');
		
		$this->userMenu=new \Pecee\UI\Menu\Menu();
		$this->userMenu->addAttribute('class', 'user-menu');
		
		/* Logged in stuff */
		if(Model_User::IsLoggedIn()) {
			// Ensures that user is not logged out when working
			Model_User::Current()->setTimeout(Helper::DEFAULT_TIMEOUT);
			$this->company=Model_Company::GetMain();
			if(!$this->company->hasRow()) {
				// TODO: HANDLE THIS ERROR - RUN INSTALL GUIDE
				if(Model_User::IsLoggedIn()) {
					Model_User::Current()->signOut();
				}
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
			}
			
			switch(Model_User::Current()->getType()) {
				case Model_User::TYPE_ADMIN:
					$this->topmenu->addItem($this->_('Menu/Customers'), \Pecee\Router::GetRoute('customer', 'index'));
					$this->topmenu->addItem($this->_('Menu/Activities'), \Pecee\Router::GetRoute('activity', 'index'));
					$this->topmenu->addItem($this->_('Menu/Vouchers'), \Pecee\Router::GetRoute('voucher', 'index'));
					$this->topmenu->addItem($this->_('Menu/Invoices'), \Pecee\Router::GetRoute('invoice', 'index'));
					$this->topmenu->addItem($this->_('Menu/Accounts'), \Pecee\Router::GetRoute('account', 'index'));
					$this->topmenu->addItem($this->_('Menu/Items'), \Pecee\Router::GetRoute('item', 'index'));
					break;
				case Model_User::TYPE_EMPLOYEE:
					$this->topmenu->addItem($this->_('Menu/Activity'), \Pecee\Router::GetRoute('activity', 'index'));
					break;
			}
			
			if(Model_User::Current()->getType() == Model_User::TYPE_ADMIN) {
				$this->userMenu->addItem($this->_('Menu/Administration'), \Pecee\Router::GetRoute('admin', 'index'));
			}
			
			$this->userMenu->addItem($this->_('Menu/Signout'), \Pecee\Router::GetRoute('user', 'signout'));
		}
	}
	
	protected function checkSecure() {
		if(\Pecee\Registry::GetInstance()->get('RequireSSL',FALSE) && !\Pecee\Url::IsSecure(\Pecee\Url::CurrentPageUrl(FALSE))) {
			\Pecee\Router::Redirect(sprintf('https://%s%s', $_SERVER['HTTP_HOST'], \Pecee\Router::GetCurrentRoute()));
		}
	}
	
	public function validationFor($name) {
		if($this->form()->validationFor($name)) {
			$el=new \Pecee\UI\Html\Html('span');
			$el->addAttribute('class','error');
			$el->setInnerHtml($this->form()->validationFor($name));
			return $el;
		}
		return '';
	}
}