<?php
class Widget_Page_Error_403 extends Widget_Site {
	public function __construct($timeout) {
		parent::__construct(NULL);
		if($timeout) {
			$this->setMessage('TimeoutError', 'TimeoutError');
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('',''));
		}
	}
}