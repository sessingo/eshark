<?php
class Widget_Page_Error extends \Pecee\Widget\Widget {
	protected $message;
	public function __construct(Exception $e) {
		parent::__construct(NULL);
		$this->setTemplate('Error.php');
		$this->message=$e->getMessage();
	}
}