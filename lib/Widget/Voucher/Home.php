<?php
class Widget_Voucher_Home extends Widget_Site {
	protected $invoices;
	protected $fromDate;
	protected $toDate;
	protected $total;
	protected $totalVat;
	public function __construct() {
		parent::__construct();
		
		if($this->hasParam('submit')) {
			$this->fromDate=\Pecee\String::GetFirstOrValue($this->getParam('from'));
			$this->toDate=\Pecee\String::GetFirstOrValue($this->getParam('to'));
			
			$from=($this->fromDate) ? \Pecee\Date::ToDate(strtotime($this->fromDate)) : NULL;
			$to=($this->toDate) ? \Pecee\Date::ToDate(strtotime($this->toDate)) : NULL;
			$recorded=$this->getParam('recorded',FALSE);
			$this->invoices=Model_Invoice::Get(NULL, Model_Invoice::TYPE_VOUCHER, $from, $to, $recorded, FALSE, Model_Invoice::ORDER_VOUCHER_PAYMENTDATE, Model_Invoice::ORDER_TYPE_DESC);
			Helper::CalculateTotals($this->invoices, $this->company, $this->total, $this->totalVat);
		}
	}
}