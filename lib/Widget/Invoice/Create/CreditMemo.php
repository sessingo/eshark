<?php
class Widget_Invoice_Create_CreditMemo extends Widget_Site {
	protected $customerId;
	protected $redirectUrl;
	protected $productItems;
	public function __construct() {
		parent::__construct();
		$this->redirectUrl=$this->getParam('redirectUrl');
		$this->productItems=array();
	}
	
	public function render() {
		if($this->isPostBack()) {
			if($this->data->productItem) {
				foreach($this->data->productItem as $item) {
					$item=\Pecee\String\Encoding::Base64Decode($item);
					if($item instanceof Model_Invoice_Item) {
						$this->productItems[]=$item;
					}
				}
			}
			if($this->data->postback != '1') {
				$this->addInputValidation($this->_('Invoice/CreditedInvoice'), 'creditedInvoiceId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Invoice/InvoiceDate'), 'invoiceDate', new UI_Form_Validate_Date());
				if(!$this->hasErrors()) {
					$invoice=new Model_Invoice();
					$invoice->setRecorded(\Pecee\Bool::Parse($this->data->Recorded));
					$invoice->setUserID(Model_User::Current()->getUserID());
					if(!is_null($this->customerId)) {
						$invoice->setCustomerID($this->customerId);
					}
					$invoice->setType(Model_Invoice::TYPE_CREDITMEMO);
					$invoice->data->description=$this->data->description;
					$invoice->data->invoiceDate=$this->data->invoiceDate;
					$invoice->data->creditedInvoiceId=$this->data->creditedInvoiceId;
					$invoice->setExcludeVat(TRUE);
					$invoice->save();
					/*@var $productItem Model_Invoice_Item */
					foreach($this->productItems as $productItem) {
						$productItem->setInvoiceID($invoice->getInvoiceID());
						$productItem->save();
					}
					\Pecee\Router::Redirect($this->redirectUrl);
				}
			}
		}
		return parent::render();
	}
	
	public function setCustomerId($customerId) {
		$this->customerId=$customerId;
	}
}