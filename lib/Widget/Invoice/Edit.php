<?php
class Widget_Invoice_Edit extends Widget_Site {
	protected $redirectUrl;
	protected $invoice;
	protected $productItems;
	public function __construct($invoiceId) {
		parent::__construct();
		$this->invoice=Model_Invoice::GetById($invoiceId);
		if(!$this->invoice->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index'));
		}
		
		$this->redirectUrl=$this->getParam('redirectUrl');
		
		if($this->isPostBack()) {
			if($this->data->productItem) {
				foreach($this->data->productItem as $item) {
					$item=\Pecee\String\Encoding::Base64Decode($item);
					if($item instanceof Model_Invoice_Item) {
						$this->productItems[]=$item;
					}
				}
			}
			if($this->data->postback != '1') {
				$this->addInputValidation($this->_('Invoice/Account'), 'accountId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Invoice/InvoiceDate'), 'invoiceDate', new UI_Form_Validate_Date());
				if(!$this->hasErrors()) {
					$this->invoice->setUserID(Model_User::Current()->getUserID());
					$this->invoice->data->accountid=$this->data->accountid;
					$this->invoice->data->description=$this->data->description;
					$this->invoice->data->dueDate=$this->data->dueDate;
					$this->invoice->data->invoiceDate=$this->data->invoiceDate;
					$this->invoice->data->reminderDate=$this->data->reminderDate;
					$this->invoice->data->firstReminderDate=$this->data->firstReminderDate;
					$this->invoice->data->secondReminderDate=$this->data->secondReminderDate;
					$this->invoice->data->debtCollectionDate=$this->data->debtCollectionDate;
					
					if($this->data->template && Invoice_Template::GetInstance()->exists($this->data->template)) {
						$this->invoice->setTemplate($this->data->template);
					}
					
					$this->invoice->setRecorded($this->data->recorded);
					$this->invoice->setExcludeVat($this->data->excludeVat);
					
					$this->invoice->update();
					
					Model_Invoice_Item::Clear($this->invoice->getInvoiceID());
											
					/*@var $productItem Model_Invoice_Item */
					foreach($this->productItems as $productItem) {
						$productItem->setInvoiceID($this->invoice->getInvoiceID());
						$productItem->save();
					}
					\Pecee\Router::Redirect($this->redirectUrl);
				}
			}
		} else {
			$this->productItems=array();
			$items=$this->invoice->getProductItems();
			if($items->hasRows()) {
				$this->productItems=$items->getRows();
			}
		}
	}
}