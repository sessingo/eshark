<?php
class Widget_Invoice_Delete extends Widget_Site {
	public function __construct($invoiceId) {
		parent::__construct();
		$invoice=Model_Invoice::GetById($invoiceId);
		if($invoice->hasRow()) {
			$invoice->delete();
		}
		\Pecee\Router::Redirect($this->getParam('redirectUrl'));
	}
}