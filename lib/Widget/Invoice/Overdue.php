<?php
class Widget_Invoice_Overdue extends Widget_Site {
	protected $invoices;
	public function __construct() {
		parent::__construct();
		$invoices=Model_Invoice::GetOverdueInvoices($this->company, NULL, NULL, 25, 0);
		$this->invoices=new Widget_Snippet_Invoice_List($invoices, $this->company);
	}	
}