<?php
class Widget_Invoice_Recorded extends Widget_Site {
	public function __construct($invoiceId) {
		parent::__construct();
		$invoice=Model_Invoice::GetById($invoiceId);
		if(!$invoice->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
		}
		
		$recorded=\Pecee\Bool::Parse($invoice->getRecorded(),FALSE);
		$invoice->setRecorded(!$recorded);
		$invoice->update();
		\Pecee\Router::Redirect($this->getParam('redirectUrl'));
	}
}