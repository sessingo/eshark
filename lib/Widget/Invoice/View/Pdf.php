<?php
class Widget_Invoice_View_Pdf extends Widget_Site {
	public function __construct($invoiceId) {
		parent::__construct();
		$pdf=new \Pecee\Service\mPDF();
		$invoice=new Widget_Invoice_View($invoiceId);
		$pdf->WriteHTML($invoice->render());
		$pdf->Output();
		die();
	}
}