<?php
class Widget_Invoice_Edit_CreditMemo extends Widget_Site {
	protected $redirectUrl;
	protected $invoice;
	protected $productItems;
	public function __construct($invoiceId) {
		parent::__construct();
		$this->invoice=Model_Invoice::GetById($invoiceId);
		if(!$this->invoice->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'index'));
		}
		
		$this->redirectUrl=$this->getParam('redirectUrl');
		
		if($this->isPostBack()) {
			if($this->data->productItem) {
				foreach($this->data->productItem as $item) {
					$item=\Pecee\String\Encoding::Base64Decode($item);
					if($item instanceof Model_Invoice_Item) {
						$this->productItems[]=$item;
					}
				}
			}
			if($this->data->postback != '1') {
				$this->addInputValidation($this->_('Invoice/CreditedInvoice'), 'creditedInvoiceId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Invoice/InvoiceDate'), 'invoiceDate', new UI_Form_Validate_Date());
				if(!$this->hasErrors()) {
					$this->invoice->setRecorded(\Pecee\Bool::Parse($this->data->Recorded));
					$this->invoice->setUserID(Model_User::Current()->getUserID());
					$this->invoice->data->description=$this->data->description;
					$this->invoice->data->invoiceDate=$this->data->invoiceDate;
					$this->invoice->data->creditedInvoiceId=$this->data->creditedInvoiceId;
					$this->invoice->setExcludeVat(TRUE);
					$this->invoice->update();
					
					Model_Invoice_Item::Clear($this->invoice->getInvoiceID());
					/*@var $productItem Model_Invoice_Item */
					foreach($this->productItems as $productItem) {
						$productItem->setInvoiceID($this->invoice->getInvoiceID());
						$productItem->save();
					}
					\Pecee\Router::Redirect($this->redirectUrl);
				}
			}
		} else {
			$this->productItems=array();
			$items=$this->invoice->getProductItems();
			if($items->hasRows()) {
				$this->productItems=$items->getRows();
			}
		}
	}
}