<?php
class Widget_Invoice_View extends Widget_Site {
	protected $invoice;
	protected $subTotal;
	protected $vatTotal;
	public function __construct($invoiceId) {
		parent::__construct();
		$this->invoice=Model_Invoice::GetById($invoiceId);
		if(!$this->invoice) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
		}
		
		$this->getSite()->clearCss();
		$this->getSite()->clearJs();
		$this->getSite()->addWrappedCss('invoice.css');
		
		if($this->invoice->getProductItems()->hasRows()) {			
			
			if($this->invoice->getExcludeVat()) {
				$vat=sprintf('0.%s', $this->company->getVatPercentage());
				$this->vatTotal=$this->invoice->getTotal($this->company) * floatval($vat);
			} else {
				$vat=sprintf('1.%s', $this->company->getVatPercentage());
				$this->vatTotal=floatval($this->invoice->getTotal($this->company)-($this->invoice->getTotal($this->company) / floatval($vat)));
			}
			
			$this->subTotal=floatval($this->invoice->getTotal($this->company)-$this->vatTotal);
		}
		
		
		$template=!is_null($this->invoice->getTemplate()) ? $this->invoice->getTemplate() : 'Invoice.php';
		
		$this->setTemplate($template);
	}
}