<?php
class Widget_Invoice_Search extends Widget_Site {
	protected $invoices;
	public function __construct() {
		parent::__construct();
		if($this->hasParam('submit')) {
			$data=new \Pecee\Util\Map();
			$tmp=$this->request->getArray();
			unset($tmp['customerid']);
			unset($tmp['invoiceid']);
			unset($tmp['submit']);
			$data->setData($tmp);
			$invoices=Model_Invoice::GetBySearch($this->request->customerId, $this->request->invoiceId, $data);
			$this->invoices=new Widget_Snippet_Invoice_List($invoices, $this->company);
			$this->invoices->setShowTotal(FALSE);
		}
	}
}