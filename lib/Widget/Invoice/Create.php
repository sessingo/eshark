<?php
class Widget_Invoice_Create extends Widget_Site {
	protected $customerId;
	protected $invoiceType;
	protected $redirectUrl;
	protected $productItems;
	public function __construct($type=NULL) {
		parent::__construct();
		$this->invoiceType=(!is_null($type) && in_array($type, Model_Invoice::$TYPES)) ? $type : Model_Invoice::TYPE_INVOICE;
		$this->redirectUrl=$this->getParam('redirectUrl');
		$this->productItems=array();
	}
	
	public function render() {
		if($this->isPostBack()) {
			if($this->data->productItem) {
				foreach($this->data->productItem as $item) {
					$item=\Pecee\String\Encoding::Base64Decode($item);
					if($item instanceof Model_Invoice_Item) {
						$this->productItems[]=$item;
					}
				}
			}
			if($this->data->postback != '1') {
				$this->addInputValidation($this->_('Invoice/Account'), 'accountId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Invoice/InvoiceDate'), 'invoiceDate', new UI_Form_Validate_Date());
				if(!$this->hasErrors()) {
					$invoice=new Model_Invoice();
					$invoice->setRecorded(\Pecee\Bool::Parse($this->data->Recorded));
					$invoice->setUserID(Model_User::Current()->getUserID());
					if(!is_null($this->customerId)) {
						$invoice->setCustomerID($this->customerId);
					}
					$invoice->setType($this->invoiceType);
					$invoice->data->accountid=$this->data->accountid;
					$invoice->data->description=$this->data->description;
					$invoice->data->dueDate=$this->data->dueDate;
					//$invoice->data->invoiceDate=$this->data->invoiceDate;
					$invoice->data->invoiceDate=$this->data->invoiceDate;
					$invoice->data->reminderDate=$this->data->reminderDate;
					$invoice->data->firstReminderDate=$this->data->firstReminderDate;
					$invoice->data->secondReminderDate=$this->data->secondReminderDate;
					$invoice->data->debtCollectionDate=$this->data->debtCollectionDate;
					
					if($this->data->template && Invoice_Template::GetInstance()->exists($this->data->template)) {
						$invoice->setTemplate($this->data->template);
					}
					
					$invoice->setExcludeVat($this->data->excludeVat);
					
					$invoice->save();
					/*@var $productItem Model_Invoice_Item */
					foreach($this->productItems as $productItem) {
						$productItem->setInvoiceID($invoice->getInvoiceID());
						$productItem->save();
					}
					\Pecee\Router::Redirect($this->redirectUrl);
				}
			}
		}
		return parent::render();
	}
	
	public function setCustomerId($customerId) {
		$this->customerId=$customerId;
	}
}