<?php
class Widget_Invoice_Payed extends Widget_Site {
	protected $invoices;
	protected $fromDate;
	protected $toDate;
	public function __construct() {
		parent::__construct();
		
		if($this->hasParam('submit')) {
			$this->fromDate=\Pecee\String::GetFirstOrValue($this->getParam('from'));
			$this->toDate=\Pecee\String::GetFirstOrValue($this->getParam('to'));
			
			$from=($this->fromDate) ? \Pecee\Date::ToDate(strtotime($this->fromDate)) : NULL;
			$to=($this->toDate) ? \Pecee\Date::ToDate(strtotime($this->toDate)) : NULL;
			$recorded=$this->getParam('recorded',FALSE);
			
			$invoices=Model_Invoice::GetPayedInvoices($this->company, $from, $to, $recorded);
			$this->invoices=new Widget_Snippet_Invoice_List($invoices, $this->company);
		}
	}
}