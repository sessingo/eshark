<?php
class Widget_Home extends Widget_Site {
	public function __construct()  {
		parent::__construct(NULL);
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('User/UsernameLabel'), 'username', new UI_Form_Validate_Username());
			$this->addInputValidation($this->_('User/PasswordLabel'), 'password', new UI_Form_Validate_NotNullOrEmpty());
			
			if(!$this->hasErrors()) {
				$user = Model_User::Authenticate($this->data->username, $this->data->password);
				if(!($user instanceof Model_User)) {
					$this->setError($this->_('Error/IncorrectLogin'));
					sleep(3); // To avoid flooding
				} else {
					$user->setTimeout(Helper::DEFAULT_TIMEOUT);
					\Pecee\Session::Instance()->set('UserID', $user->getUserID());
				}
				\Pecee\Router::Refresh();
			} else {
				sleep(3); // To avoid flooding
			}
		}
	}
}