<?php
class Widget_Snippet_Invoice_List extends Widget_Snippet_Abstract {
	protected $invoices;
	protected $company;
	protected $showCustomerInfo;
	protected $showTotal;
	protected $total;
	protected $totalVat;
	public function __construct(Model_Invoice $invoices, Model_Company $company) {
		parent::__construct();
		$this->invoices=$invoices;
		$this->company=$company;
		$this->showCustomerInfo=TRUE;
		$this->showTotal=TRUE;
	}
	
	public function renderContent() {
		if($this->showTotal) {
			Helper::CalculateTotals($this->invoices, $this->company, $this->total, $this->totalVat);
		}
		parent::renderContent();
	}
	
	public function setShowCustomerInfo($value) {
		$this->showCustomerInfo=$value;
	}
	
	public function getShowCustomerInfo() {
		return $this->showCustomerInfo;
	}
	
	public function setShowTotal($value) {
		$this->showTotal=$value;
	}
	
	public function getShowTotal() {
		return $this->showTotal;
	}
}