<?php
class Widget_Transfer_Delete extends Widget_Site {
	public function __construct($transferId) {
		parent::__construct();
		$transfer=Model_Transfer::GetById($transferId);
		if($transfer->hasRow()) {
			$transfer->delete();
		}
		\Pecee\Router::GoBack();
	}
}