<?php
class Widget_Customer_Detail extends Widget_Site {
	protected $customer;
	protected $cases;
	public function __construct($customerId) {
		parent::__construct();
		$this->getSite()->addWrappedJs('customerdetail.js');
		$this->customer=Model_Customer::GetById($customerId);
		if(!$this->customer->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
		}
		$this->cases=Model_Case::Get($this->customer->getCustomerID(), NULL, FALSE, FALSE, 10, 0);
	}
}