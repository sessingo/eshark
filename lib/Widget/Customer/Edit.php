<?php
class Widget_Customer_Edit extends Widget_Site {
	protected $customer;
	public function __construct($customerId) {
		$this->customerId=$customerId;
		parent::__construct();
		$this->customer=Model_Customer::GetById($this->customerId);
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$hasLogo=$this->files->logo && $this->files->logo->tmpName != '';
			if($hasLogo) {
				$this->addInputValidation($this->_('Customer/Logo'), 'logo', array(new UI_Form_Validate_File_NotNullOrEmpty(), new UI_Form_Validate_File_AllowedExtension(array('bmp', 'png', 'jpg', 'gif'))));
			}
			
			if(!$this->hasErrors()) {
				$this->customer->setUserID(Model_User::Current()->getUserID());
				$data=$this->data->getArray();
				unset($data['submit']);
				
				if($hasLogo) {
					
					$logo=$this->customer->getLogo();
					$filename='';
					if($logo instanceof Model_Image && $logo->hasRow()) {
						$filename=sprintf('%s.%s', $logo->getFileID(), \Pecee\IO\File::GetExtension($this->files->logo->name));
						$logo->setFilename($filename);
						$logo->setOriginalFilename($this->files->logo->name);
						$logo->setType($this->files->logo->type);
						$logo->setBytes($this->files->logo->size);
						$logo->update();
					} else {
						$file=new \Pecee\Model\File\ModelFile();
						$filename=sprintf('%s.%s', $file->getFileID(), \Pecee\IO\File::GetExtension($this->files->logo->name));
						$file->setOriginalFilename($this->files->logo->name);
						$file->setType($this->files->logo->type);
						$file->setBytes($this->files->logo->size);
						$file->setFilename($filename);
						$this->company->setLogoFileID($file->getFileID());
						$file->save();
						$data['LogoFileID']=$file->getFileID();
					}
					$this->files->logo->move(\Pecee\Registry::GetInstance()->get('StoragePath') . DIRECTORY_SEPARATOR . $filename);					
				}
				
				$this->customer->setData($data);
				$this->customer->update();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('customer', 'details', array($this->customer->getCustomerID())));
			}
		}
	}
}