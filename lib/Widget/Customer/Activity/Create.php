<?php
class Widget_Customer_Activity_Create extends Widget_Site {
	protected $projectId;
	protected $project;
	protected $activities;
	public function __construct($customerId, $projectId) {
		$this->customerId=$customerId;
		parent::__construct();
		$this->projectId=$projectId;
		if(!is_null($projectId)) {
			$this->project=Model_Project::GetById($projectId, Model_User::Current()->getUserID());
			if(!$this->project->hasRow()) {
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
			}
		}
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Activity/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$activity=new Model_Project_Activity();
				$activity->setProjectID($projectId);
				$activity->setUserID(Model_User::Current()->getUserID());
				$activity->setName($this->data->name);
				$activity->setDescription($this->data->description);
				$activity->setMaxHours($this->data->maxhours);
				$activity->setPrice($this->data->price);
				$activity->save();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'list', array($customerId, $projectId)));
			}
		}
	}
}