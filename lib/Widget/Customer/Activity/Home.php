<?php
class Widget_Customer_Activity_Home extends Widget_Site {
	protected $projectId;
	protected $customerId;
	protected $project;
	protected $activities;
	public function __construct($customerId,$projectId=NULL) {
		$this->customerId=$customerId;
		parent::__construct();
		$this->projectId=$projectId;
		if(!is_null($projectId)) {
			$this->project=Model_Project::GetById($projectId, Model_User::Current()->getUserID());
			if($this->project->hasRow()) {
				$this->activities=Model_Project_Activity::GetByProjectId($this->project->getProjectID());
			}
		}
	}
}