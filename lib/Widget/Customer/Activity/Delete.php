<?php
class Widget_Customer_Activity_Delete extends Widget_Site {
	public function __construct($activityId) {
		parent::__construct();
		$activity=Model_Project_Activity::GetById($activityId);
		if($activity->hasRow()) {
			$project=$activity->getProject();
			$activity->delete();
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'list', array($project->getCustomerID(), $project->getProjectID())));
		}
	}
}