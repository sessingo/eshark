<?php
class Widget_Customer_Activity_Edit extends Widget_Site {
	protected $projectId;
	protected $project;
	protected $activity;
	public function __construct($customerId,$activityId) {
		$this->activity=Model_Project_Activity::GetById($activityId, Model_User::Current()->getUserID());
		if(!$this->activity->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
		}
		$this->customerId=$this->activity->getProject()->getCustomerID();
		parent::__construct();
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Activity/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$this->activity->setProjectID($this->data->project);
				$this->activity->setUserID(Model_User::Current()->getUserID());
				$this->activity->setName($this->data->name);
				$this->activity->setDescription($this->data->description);
				$this->activity->setMaxHours($this->data->maxhours);
				$this->activity->setPrice($this->data->price);
				$this->activity->update();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'list', array($this->customerId, $this->activity->getProject()->getProjectID())));
			}
		}
	}
}