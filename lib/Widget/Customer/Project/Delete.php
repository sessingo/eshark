<?php
class Widget_Customer_Project_Delete extends Widget_Site {
	public function __construct($projectId) {
		parent::__construct();
		$project=Model_Project::GetById($projectId);
		if($project->hasRow()) {
			$project->delete();
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'list',array($project->getCustomerID())));
	}
}