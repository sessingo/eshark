<?php
class Widget_Customer_Project_Edit extends Widget_Site {
	protected $project;
	public function __construct($customerId, $projectId) {
		parent::__construct();
		$this->project=Model_Project::GetById($projectId, Model_User::Current()->getUserID());
		if(!$this->project->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, 'projects',array($customerId)));
		}

		$this->projects=Model_Project::GetByCustomerID($customerId);
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Project/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$this->project->setUserID(Model_User::Current()->getUserID());
				$this->project->setName($this->data->name);
				$this->project->setDescription($this->data->description);
				$this->project->setActive($this->data->active);
				$this->project->update();
				\Pecee\Router::Refresh();
			}
		}
	}
}