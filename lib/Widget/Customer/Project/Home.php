<?php
class Widget_Customer_Project_Home extends Widget_Site {
	protected $projects;
	public function __construct($customerId) {
		$this->customerId=$customerId;
		$this->projects=Model_Project::GetByCustomerID($customerId);
		parent::__construct();

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Project/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$project=new Model_Project();
				$project->setUserID(Model_User::Current()->getUserID());
				$project->setCustomerID($customerId);
				$project->setName($this->data->name);
				$project->setDescription($this->data->description);
				$project->setActive($this->data->active);
				$project->save();
				\Pecee\Router::Refresh();
			}
		}
		
	}
}