<?php
class Widget_Customer_Case_Home extends Widget_Site {
	protected $cases;
	protected $customerId;
	public function __construct($customerId) {
		parent::__construct();
		$this->customerId=$customerId;
		$this->cases=Model_Case::Get($customerId);
	}
}