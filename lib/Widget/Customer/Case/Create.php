<?php
class Widget_Customer_Case_Create extends Widget_Site {
	protected $customerId;
	public function __construct($customerId, $parentCaseId=NULL) {
		parent::__construct();
		$this->customerId=$customerId;
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Case/Subject'), 'subject', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$case=new Model_Case();
				$case->setCustomerID($customerId);
				$case->setUserID(Model_User::Current()->getUserID());
				$case->setCaseSubjectID($this->data->subject);
				$case->setText($this->data->text);
				$case->save();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL,'view',array($customerId, $case->getCaseID())));
			}
		}
	}
}