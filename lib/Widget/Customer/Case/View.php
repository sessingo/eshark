<?php
class Widget_Customer_Case_View extends Widget_Site {
	protected $case;
	public function __construct($customerId, $caseId) {
		parent::__construct();
		$this->case=Model_Case::GetById($caseId);
		if(!$this->case->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
		}
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Case/Description'), 'text', new UI_Form_Validate_NotNullOrEmpty());
			if(!$this->hasErrors()) {
				$case=new Model_Case();
				$case->setParentCaseID($caseId);
				$case->setCustomerID($this->case->getCustomerID());
				$case->setUserID(Model_User::Current()->getUserID());
				$case->setCaseSubjectID($this->data->subject);
				$case->setText($this->data->text);
				$case->save();
				\Pecee\Router::Refresh();
			}
		}
	}
}