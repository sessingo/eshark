<?php
class Widget_Customer_Invoice_Home extends Widget_Site {
	protected $invoices;
	protected $fromDate;
	protected $toDate;
	protected $customerId;
	public function __construct($customerId) {
		parent::__construct();
		$this->customerId=$customerId;
		$this->fromDate=\Pecee\String::GetFirstOrValue($this->getParam('from'));
		$this->toDate=\Pecee\String::GetFirstOrValue($this->getParam('to'));
		
		$from=($this->fromDate) ? \Pecee\Date::ToDate(strtotime($this->fromDate)) : NULL;
		$to=($this->toDate) ? \Pecee\Date::ToDate(strtotime($this->toDate)) : NULL;
		$recorded=$this->getParam('recorded',NULL);
		$hidePayment = $this->getParam('hidePayment',NULL);
		
		$invoices=Model_Invoice::GetCustomerInvoice($customerId,$from,$to,$hidePayment,$recorded);
		$this->invoices=new Widget_Snippet_Invoice_List($invoices, $this->company);
		$this->invoices->setShowCustomerInfo(FALSE);
	}
}