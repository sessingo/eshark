<?php
class Widget_Customer_Home extends Widget_Site {
	protected $customers;
	public function __construct() {
		parent::__construct();
		if($this->hasParam('submit')) {
			$data=new \Pecee\Util\Map();
			$tmp=$this->request->getArray();
			unset($tmp['customerid']);
			unset($tmp['submit']);
			$data->setData($tmp);
			$this->customers=Model_Customer::GetBySearch($this->request->customerId, $data);			
		}
	}
}