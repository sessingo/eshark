<?php
class Widget_Customer_Create extends Widget_Site {
	public function __construct() {
		parent::__construct();
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Customer/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$hasLogo=$this->files->logo && $this->files->logo->tmpName != '';
			if($hasLogo) {
				$this->addInputValidation($this->_('Customer/Logo'), 'logo', array(new UI_Form_Validate_File_NotNullOrEmpty(), new UI_Form_Validate_File_AllowedExtension(array('bmp', 'png', 'jpg', 'gif'))));
			}
			if(!$this->hasErrors()) {
				$customer=new Model_Customer();
				$customer->setUserID(Model_User::Current()->getUserID());
				$data=$this->data->getArray();
				unset($data['submit']);
				
				if($hasLogo) {
					$file=new \Pecee\Model\File\ModelFile();
					$filename=sprintf('%s.%s', $file->getFileID(), \Pecee\IO\File::GetExtension($this->files->logo->name));
					$this->files->logo->move(\Pecee\Registry::GetInstance()->get('StoragePath') . DIRECTORY_SEPARATOR . $filename);
					
					$file->setFilename($filename);
					$file->setOriginalFilename($this->files->logo->name);
					$file->setType($this->files->logo->type);
					$file->setBytes($this->files->logo->size);
					$file->save();
					$data['LogoFileID']=$file->getFileID();
				}
				
				$customer->setData($data);
				$customer->save();
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('customer', 'details', array($customer->getCustomerID())));
			}
		}
	}
}