<?php
class Widget_Activity_Delete extends Widget_Site {
	public function __construct($activityLogId) {
		parent::__construct();
		$log=Model_Project_Activity_Log::GetById($activityLogId);
		if($log->hasRow()) {
			$log->delete();
			\Pecee\Router::GoBack();
		}
	}
}