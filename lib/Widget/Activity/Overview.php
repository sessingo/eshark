<?php
class Widget_Activity_Overview extends Widget_Site {
	protected $activities;
	protected $fromDate;
	protected $toDate;
	protected $customerId;
	protected $projectId;
	protected $activityId;
	protected $totalMinutes;
	protected $totalPrice;
	public function __construct() {
		parent::__construct();
		
		$this->customerId=\Pecee\String::GetFirstOrValue($this->getParam('customerId'),NULL);
		$this->projectId=\Pecee\String::GetFirstOrValue($this->getParam('projectId'),NULL);
		$this->activityId=\Pecee\String::GetFirstOrValue($this->getParam('activityId'),NULL);
		
		$this->fromDate=\Pecee\String::GetFirstOrValue($this->getParam('from'));
		$this->toDate=\Pecee\String::GetFirstOrValue($this->getParam('to'));
		
		$this->fromDate=($this->fromDate) ? \Pecee\Date::ToDate(strtotime($this->fromDate)) : NULL;
		$this->toDate=($this->toDate) ? \Pecee\Date::ToDate(strtotime($this->toDate)) : NULL;
		if($this->hasParam('submit')) {
			$this->activities=Model_Project_Activity_Log::Get($this->fromDate, $this->toDate, $this->customerId, $this->projectId, $this->activityId);
	
			if($this->activities->hasRows()) {
				/* @var $activity Model_Project_Activity_Log */
				foreach($this->activities->getRows() as $activity) {
					$minutes=$activity->getMinutes();
					$this->totalMinutes+=$minutes;
					if($activity->getActivity()->getPrice()) {
						$this->totalPrice+=floatval(($minutes/60)* $activity->getActivity()->getPrice());
					}
				}
			}
		}
	}
}