<?php
class Widget_Activity_Home extends Widget_Site {
	protected $customerId;
	protected $projectId;
	protected $activityId;
	protected $latestActivities;
	protected $activity;
	protected $activityLog;
	protected $date;
	public function __construct() {
		parent::__construct();		
		$this->getSite()->addWrappedCss('activity.css');
		$this->getSite()->addWrappedJs('pecee.js');
		$this->getSite()->addWrappedJs('jquery.ui.datepicker.js');
		$this->getSite()->addWrappedJs('activitylog.js');
		
		$this->date=date('d-m-Y');
		
		if($this->hasParam('date')) {
			$this->date = date('d-m-Y', strtotime($this->getParam('date')));
		} 
		
		$this->latestActivities=Model_Project_Activity_Log::GetLatest(NULL, NULL, NULL, 8);
		$this->activityLog=Model_Project_Activity_Log::Get($this->getDate(), $this->getDate());
		
		if($this->isPostBack()) {
			$this->setDefaultValues();
			if(!is_null($this->activityId)) {
				$this->activity=Model_Project_Activity::GetById($this->activityId);
			}
			
			if($this->data->postback != '1') {
				$this->addInputValidation($this->_('Customer/Activity/Customer'), 'customerId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Customer/Activity/Project'), 'projectId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Customer/Activity/Activity'), 'activityId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Customer/Activity/Hours'), 'hours', new UI_Form_Validate_Hours(TRUE));
				$this->addInputValidation($this->_('Customer/Activity/Minutes'), 'minutes', new UI_Form_Validate_Minutes(TRUE));
				
				
				if(!$this->data->hours && !$this->data->minutes) {
					$this->setError($this->_('Error/EnterAmountOfHours'));
				}
				
				if(!$this->hasErrors()) {
					if($this->activity->hasRow() && !$this->hasErrors()) {
						$log=new Model_Project_Activity_Log();
						$log->setUserID(Model_User::Current()->getUserID());
						$log->setDescription($this->data->description);
						$log->setProjectActivityID($this->data->activityId);
						
						$minutes=$this->data->minutes;
						if($this->data->hours) {
							$minutes+=floatval($this->data->hours*60);
						}
						
						$log->setMinutes($minutes);
						
						$log->setDate($this->getDate());
						$log->save();
						\Pecee\Router::Refresh();
					}
				}
			}
		}
	}
	
	protected function setDefaultValues() {
		$this->customerId=$this->data->customerId;
		$this->projectId=($this->customerId != '') ? $this->data->projectId : NULL;
		$this->activityId=($this->projectId != '') ? $this->data->activityId : NULL;
		$this->customerId=(!is_null($this->data->pCustomerId)) ? $this->data->pCustomerId : $this->customerId;
		$this->projectId=(!is_null($this->data->pProjectId)) ? $this->data->pProjectId : $this->projectId;
		$this->activityId=(!is_null($this->data->pActivityId)) ? $this->data->pActivityId : $this->activityId;
	}
	
	protected function getDate() {
		return date('Y-m-d', strtotime($this->date));
	}
}