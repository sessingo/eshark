<?php
class Widget_Ajax_Dialog_Activity_Edit extends Widget_Ajax_Dialog_Abstract {
	protected $log;
	protected $activity;
	
	protected $customerId;
	protected $projectId;
	protected $activityId;
	public function __construct() {
		$this->title=$this->_('Activity/LogEdit');
		$this->enableDialogClose=TRUE;
		parent::__construct();
		$this->log=Model_Project_Activity_Log::GetById($this->getParam('activityLogId'));
		if(!$this->log->hasRows()) {
			$this->close();
		}
		
		if($this->isPostBack()) {
			$this->setDefaultValues();
			
			if(!is_null($this->activityId)) {
				$this->activity=Model_Project_Activity::GetById($this->activityId, Model_User::Current()->getUserID());
			}
			
			if($this->data->postback != '1') {
				$this->addInputValidation($this->_('Activity/Customer'), 'customerId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Activity/Project'), 'projectId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Activity/Activity'), 'activityId', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Activity/Hours'), 'hours', new UI_Form_Validate_Hours(TRUE));
				$this->addInputValidation($this->_('Activity/Minutes'), 'minutes', new UI_Form_Validate_Minutes(TRUE));
				
				if(!$this->data->hours && !$this->data->minutes) {
					$this->setError($this->_('Activity/EnterAmountOfHours'));
				}
				if($this->activity && $this->activity->hasRow() && !$this->hasErrors()) {
					$this->log->setUserID(Model_User::Current()->getUserID());
					$this->log->setDescription($this->data->description);
					$this->log->setProjectActivityID($this->data->activityId);
					
					$minutes=$this->data->minutes;
					if($this->data->hours) {
						$minutes+=floatval($this->data->hours*60);
					}
					
					$this->log->setMinutes($minutes);
					
					$this->log->update();
					$this->refresh();
				}
			}
		} else {
			$this->customerId=$this->log->getActivity()->getProject()->getCustomerID();
			$this->projectId=$this->log->getActivity()->getProject()->getProjectID();
			$this->activityId=$this->log->getActivity()->getProjectActivityID();
			$this->activity=$this->log->getActivity();
		}
	}
	
	protected function setDefaultValues() {
		$this->customerId=$this->data->customerId;
		$this->projectId=($this->customerId != '') ? $this->data->projectId : NULL;
		$this->activityId=($this->projectId != '') ? $this->data->activityId : NULL;
	}
}