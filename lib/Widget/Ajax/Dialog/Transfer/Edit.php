<?php
class Widget_Ajax_Dialog_Transfer_Edit extends Widget_Ajax_Dialog_Abstract {
	protected $transfer;
	public function __construct() {
		parent::__construct();
		$this->transfer=Model_Transfer::GetById($this->getParam('transferId'));
		if(!$this->transfer->hasRows()) {
			$this->close();
		}
		$this->title='Rediger overførelse';
		if($this->isPostBack()) {
			$this->addInputValidation('Konto', 'accountId', new UI_Form_Validate_Integer());
			$this->addInputValidation('Beløb', 'amount', new UI_Form_Validate_Float());
			$this->addInputValidation('Dato', 'date', new UI_Form_Validate_Date());
			
			if(!$this->hasErrors()) {
				$this->transfer->setUserID(Model_User::Current()->getUserID());
				$this->transfer->setAccountID($this->data->accountId);
				$this->transfer->setText($this->data->text);
				$this->transfer->setDate(\Pecee\Date::FromToDateTime($this->data->date));
				$this->transfer->setAmount(\Pecee\Float::ParseFloat($this->data->amount));
				$this->transfer->update();
				$this->transfer->updateBalance();
				$this->refresh();
			}
		}
	}
}