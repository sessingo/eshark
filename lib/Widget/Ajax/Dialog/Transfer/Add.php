<?php
class Widget_Ajax_Dialog_Transfer_Add extends Widget_Ajax_Dialog_Abstract {
	public function __construct() {
		parent::__construct();
		$this->title='Ny overførelse';
		if($this->isPostBack()) {
			$this->addInputValidation('Konto', 'accountId', new UI_Form_Validate_Integer());
			$this->addInputValidation('Beløb', 'amount', new UI_Form_Validate_Float());
			$this->addInputValidation('Dato', 'date', new UI_Form_Validate_Date());
			
			if(!$this->hasErrors()) {
				$transfer=new Model_Transfer();
				$transfer->setUserID(Model_User::Current()->getUserID());
				$transfer->setAccountID($this->data->accountId);
				$transfer->setText($this->data->text);
				$transfer->setDate(\Pecee\Date::FromToDateTime($this->data->date));
				$transfer->setAmount(\Pecee\Float::ParseFloat($this->data->amount));
				$transfer->save();
				$transfer->updateBalance();
				$this->refresh();
			}
		}
	}
}