<?php
class Widget_Ajax_Dialog_Admin_Account_Edit extends Widget_Ajax_Dialog_Abstract {
	protected $account;
	public function __construct() {
		parent::__construct();
		$this->account=Model_Account::GetById($this->getParam('accountId'));
		if(!$this->account->hasRows()) {
			$this->close();
		}
		$this->title='Rediger konto';
		if($this->isPostBack()) {
			$this->addInputValidation('Navn', 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation('Bank', 'bank', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation('Registrerings nummer', 'regNo', new UI_Form_Validate_Integer());
			$this->addInputValidation('Konto nummer', 'accountNo', new UI_Form_Validate_Integer());
			if(!$this->hasErrors()) {
				$this->account->setUserID(Model_User::Current()->getUserID());
				$this->account->setName($this->data->name);
				$this->account->setRegistrationNumber($this->data->regNo);
				$this->account->setAccountNumber($this->data->accountNo);
				$this->account->setIBAN($this->data->iban);
				$this->account->setSwift($this->data->swift);
				$this->account->setBank($this->data->bank);
				$this->account->update();
				$this->refresh();
			}
		}
	}
}