<?php
class Widget_Ajax_Dialog_Admin_Account_Add extends Widget_Ajax_Dialog_Abstract {
	public function __construct() {
		parent::__construct();
		$this->title='Tilføj konto';
		if($this->isPostBack()) {
			$this->addInputValidation('Navn', 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation('Bank', 'bank', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation('Registrerings nummer', 'regNo', new UI_Form_Validate_Integer());
			$this->addInputValidation('Konto nummer', 'accountNo', new UI_Form_Validate_Integer());
			if(!$this->hasErrors()) {
				$account=new Model_Account();
				$account->setUserID(Model_User::Current()->getUserID());
				$account->setName($this->data->name);
				$account->setRegistrationNumber($this->data->regNo);
				$account->setAccountNumber($this->data->accountNo);
				$account->setIBAN($this->data->iban);
				$account->setSwift($this->data->swift);
				$account->setBank($this->data->bank);
				$account->save();
				$this->refresh();
			}
		}
	}
}