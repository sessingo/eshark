<?php
abstract class Widget_Ajax_Dialog_Abstract extends Widget_Site {
	protected $title;
	protected $enableDialogClose=TRUE;
	public function __construct() {
		
		if(!\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			$this->redirect(\Pecee\Router::GetRoute('', ''));
		}
		
		$this->enableDialogClose=true;
		parent::__construct();
		if($this->isAjaxRequest()) {
			$this->setTemplate('Dialog.php');
		}
	}
	public function close($javascript='') {
		die('<script type="text/javascript">
				$(document).ready(function() {
				dialog.close(); '.$javascript.'});
				</script>');
	}
	public function refresh() {
		die('<script type="text/javascript">
				$(document).ready(function() {
				dialog.close(); location.reload(true); });
				</script>');
	}
	public function redirect($url) {
		die('<script type="text/javascript">
				$(document).ready(function() {
				dialog.close(); top.location.href=\''.addslashes($url).'\'; });
				</script>');
	}
	public function setTitle($title) {
		$this->title=$title;
	}
	public function setEnableDialogClose($bool) {
		$this->enableDialogClose=$bool;
	}
}