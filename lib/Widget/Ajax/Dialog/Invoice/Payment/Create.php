<?php
class Widget_Ajax_Dialog_Invoice_Payment_Create extends Widget_Ajax_Dialog_Abstract {
	protected $customer;
	public function __construct() {
		parent::__construct();
		$this->title=$this->_('Invoice/NewPayment');
		
		$this->customer=Model_Customer::GetById($this->getParam('customerId'));
		if(!$this->customer->hasRow()) {
			$this->close();
		}
		
		if($this->isPostBack()) {
			
			$this->addInputValidation($this->_('Invoice/InvoiceNumber'), 'payedInvoiceId', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Invoice/PaymentDate'), 'paymentDate', new UI_Form_Validate_Date());
			$this->addInputValidation($this->_('Invoice/Amount'), 'price', new UI_Form_Validate_Float());
			
			if(!$this->hasErrors()) {
				$invoice=new Model_Invoice();
				$invoice->setRecorded(\Pecee\Bool::Parse($this->data->recorded));
				$invoice->setUserID(Model_User::Current()->getUserID());
				$invoice->setCustomerID($this->customer->getCustomerID());
				$invoice->setType(Model_Invoice::TYPE_PAYMENT);
				$invoice->data->description=$this->data->description;
				$invoice->data->paymentDate=$this->data->paymentDate;
				$invoice->data->payedInvoiceId=$this->data->payedInvoiceId;
				$invoice->setExcludeVat(TRUE);
				$invoice->save();
				
				$item=new Model_Item();
				$item->UserID=Model_User::Current()->getUserID();
				$item->AutoCreated=TRUE;
				
				$relation=new Model_Invoice_Item();
				$relation->setItem($item);
				$relation->setInvoiceID($invoice->getInvoiceID());
				
				// Make negative
				$price=\Pecee\Float::ParseFloat($this->data->price);
				$price=($price > 0) ? $price/-1 : $price;
				
				$relation->setPrice($price);
				$relation->save();
				$this->refresh();
			}
		}
	}
}