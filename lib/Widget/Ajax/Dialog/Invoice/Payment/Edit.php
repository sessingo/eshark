<?php
class Widget_Ajax_Dialog_Invoice_Payment_Edit extends Widget_Ajax_Dialog_Abstract {
	protected $invoice;
	protected $item;
	public function __construct() {
		parent::__construct();
		$this->title=$this->_('Invoice/EditPayment');
		
		$this->invoice=Model_Invoice::GetById($this->getParam('invoiceId'));
		if(!$this->invoice->hasRow() || $this->invoice->hasRow() && $this->invoice->getType() != Model_Invoice::TYPE_PAYMENT) {
			$this->close();
		}
		
		$items=$this->invoice->getProductItems();
		$this->item=$items->getRow(0);
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Invoice/InvoiceNumber'), 'payedInvoiceId', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Invoice/PaymentDate'), 'paymentDate', new UI_Form_Validate_Date());
			$this->addInputValidation($this->_('Invoice/Amount'), 'price', new UI_Form_Validate_Float());
			
			if(!$this->hasErrors()) {
				$this->invoice->setRecorded(\Pecee\Bool::Parse($this->data->recorded));
				$this->invoice->setUserID(Model_User::Current()->getUserID());
				$this->invoice->data->description=$this->data->description;
				$this->invoice->data->paymentDate=$this->data->paymentDate;
				$this->invoice->data->payedInvoiceId=$this->data->payedInvoiceId;
				$this->invoice->setExcludeVat(TRUE);
				$this->invoice->update();
				
				// Make negative
				$price=\Pecee\Float::ParseFloat($this->data->price);
				$price=($price > 0) ? $price/-1 : $price;
				$this->item->setPrice($price);
				$this->item->update();
				$this->refresh();
			}
		}
	}
}