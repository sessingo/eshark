<?php
class Widget_Ajax_Dialog_Invoice_Voucher_Edit extends Widget_Ajax_Dialog_Abstract {
	protected $invoice;
	protected $item;
	public function __construct() {
		parent::__construct();
		$this->title=$this->_('Voucher/EditVoucher');
		
		$this->invoice=Model_Invoice::GetById($this->getParam('invoiceId'));
		if(!$this->invoice->hasRow()) {
			$this->close();
		}
		
		$items=$this->invoice->getProductItems();
		$this->item=$items->getRow(0);
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Voucher/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Voucher/PaymentDate'), 'paymentDate', new UI_Form_Validate_Date());
			$this->addInputValidation($this->_('Voucher/Amount'), 'price', new UI_Form_Validate_Float());
				
			if(!$this->hasErrors()) {
				$this->invoice->setRecorded(\Pecee\Bool::Parse($this->data->recorded));
				$this->invoice->setUserID(Model_User::Current()->getUserID());
				$this->invoice->data->name=$this->data->name;
				$this->invoice->data->description=$this->data->description;
				$this->invoice->data->invoiceDate=$this->data->invoiceDate;
				$this->invoice->data->paymentDate=$this->data->paymentDate;
				$this->invoice->data->invoiceNo=$this->data->invoiceNo;
				$this->invoice->setExcludeVat(TRUE);
				$this->invoice->update();
				
				$this->item->setPrice(\Pecee\Float::ParseFloat($this->data->price));
				$this->item->update();
		
				$this->refresh();
			}
		}
	}
}