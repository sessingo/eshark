<?php
class Widget_Ajax_Dialog_Invoice_Voucher_Create extends Widget_Ajax_Dialog_Abstract {
	public function __construct() {
		parent::__construct();
		$this->title=$this->_('Voucher/NewVoucher');
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Voucher/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Voucher/PaymentDate'), 'paymentDate', new UI_Form_Validate_Date());
			$this->addInputValidation($this->_('Voucher/Amount'), 'price', new UI_Form_Validate_Float());
			
			if(!$this->hasErrors()) {
				$invoice=new Model_Invoice();
				$invoice->setRecorded(\Pecee\Bool::Parse($this->data->recorded));
				$invoice->setUserID(Model_User::Current()->getUserID());
				$invoice->setType(Model_Invoice::TYPE_VOUCHER);
				$invoice->data->name=$this->data->name;
				$invoice->data->description=$this->data->description;
				$invoice->data->invoiceDate=$this->data->invoiceDate;
				$invoice->data->paymentDate=$this->data->paymentDate;
				$invoice->data->invoiceNo=$this->data->invoiceNo;
				$invoice->setExcludeVat(TRUE);
				$invoice->save();
				
				$item=new Model_Item();
				$item->UserID=Model_User::Current()->getUserID();
				$item->AutoCreated=TRUE;
				
				$relation=new Model_Invoice_Item();
				$relation->setItem($item);
				$relation->setInvoiceID($invoice->getInvoiceID());
				$relation->setPrice(\Pecee\Float::ParseFloat($this->data->price));
				$relation->save();
				
				$this->refresh();
			}
		}
	}
}