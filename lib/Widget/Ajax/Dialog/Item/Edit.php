<?php
class Widget_Ajax_Dialog_Item_Edit extends Widget_Ajax_Dialog_Abstract {
	protected $productItem;
	protected $debug=FALSE;
	protected $finished;
	protected $index;
	public function __construct() {
		parent::__construct();
		$this->index=$this->data->index;
		$this->productItem=$this->data->item;
		$this->productItem=\Pecee\String\Encoding::Base64Decode($this->productItem);	
		if($this->productItem && $this->productItem instanceof Model_Invoice_Item) {
			$this->title=$this->_('Item/EditItem', $this->productItem->getItem()->getName());
			if($this->isPostBack() && !$this->data->autoPostback) {
				$this->addInputValidation($this->_('Item/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
				$this->addInputValidation($this->_('Item/Price'), 'price', new UI_Form_Validate_Float());
				$this->data->count=($this->data->count) ? $this->data->count : 1;
				$this->addInputValidation($this->_('Item/Amount'), 'count', new UI_Form_Validate_Float());
					
				if(!$this->hasErrors()) {
					//$this->productItem=new Model_Invoice_Item(); 

					$this->productItem->getItem()->UserID=Model_User::Current()->getUserID();
					$this->productItem->getItem()->data->Name=$this->data->Name;
					$this->productItem->getItem()->data->Number=$this->data->Number;
					$this->productItem->getItem()->data->Unit=$this->data->Unit;
					$this->productItem->getItem()->AutoCreated=TRUE;
					
					//$this->productItem->setItem($item);
					$this->productItem->Price=\Pecee\Float::ParseFloat($this->data->Price);
					$this->productItem->Description=$this->data->Description;
					$this->productItem->Count=\Pecee\Float::ParseFloat($this->data->Count);
					$this->productItem->Rebate=$this->data->Rebate;
					$this->productItem->setTotal($this->data->amount);
					$this->finished=TRUE;
				}
			}
		} else {
			$this->close();
		}
	}
}