<?php
class Widget_Ajax_Dialog_Item_Add extends Widget_Ajax_Dialog_Abstract {
	protected $productItem;
	protected $debug=FALSE;
	public function __construct() {
		parent::__construct();
		$this->title=$this->_('Item/AddItem');
		
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Item/Name'), 'name', new UI_Form_Validate_NotNullOrEmpty());
			$this->addInputValidation($this->_('Item/Price'), 'price', new UI_Form_Validate_Float());
			$this->data->count=($this->data->count) ? $this->data->count : 1;
			$this->addInputValidation($this->_('Item/Amount'), 'count', new UI_Form_Validate_Float());
			
			if(!$this->hasErrors()) {
				$this->productItem=new Model_Invoice_Item();
				$item=NULL;
				if($this->data->Number) {
					$item=Model_Item::GetByNumber($this->data->Number);
				}
				
				if(is_null($item) || $item instanceof Model_Item && !$item->hasRow()) {
					$item=new Model_Item();
					$item->UserID=Model_User::Current()->getUserID();
					$item->data->Name=$this->data->Name;
					$item->data->Number=$this->data->Number;
					$item->data->Unit=$this->data->Unit;
					$item->AutoCreated=TRUE;
				}
				
				$this->productItem->setItem($item);
				$this->productItem->Price=\Pecee\Float::ParseFloat($this->data->Price);
				$this->productItem->Description=$this->data->Description;
				$this->productItem->Count=\Pecee\Float::ParseFloat($this->data->Count);
				$this->productItem->Rebate=$this->data->Rebate;
				$this->productItem->setTotal($this->data->amount);
			}
		}
	}
}