<?php
class UI_Form_Validate_Date extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $error;
	protected $allowNull;
	public function __construct($allowNull=FALSE) {
		$this->allowNull=$allowNull;
	}
	
	public function validate() {
		if($this->allowNull && !$this->value) {
			return TRUE;
		}
		return \Pecee\Date::IsValid($this->value);
	}
	
	public function getErrorMessage() {
		return $this->_('Error/InvalidDate', $this->name);
	}
}