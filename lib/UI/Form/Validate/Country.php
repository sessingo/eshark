<?php
class UI_Form_Validate_Country extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $err;
	public function validate(){
		if(empty($this->value)) {
			$this->err=\Pecee\Language::Instance()->_('Error/Country/Choose');
			return false;
		}
		return (isset(\Pecee\Currency::$CURRENCIES[$this->value]));
	}

	public function getErrorMessage(){
		return \Pecee\Language::Instance()->_('Error/Country/Invalid');
	}
}