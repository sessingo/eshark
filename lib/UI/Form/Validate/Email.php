<?php
class UI_Form_Validate_Email extends \Pecee\UI\Form\Validate\ValidateInput {
	public function validate() {
		return (\Pecee\Util::is_email($this->value));
	}
	public function getErrorMessage() {
		return $this->_('Error/InvalidEmail', $this->name);
	}
}