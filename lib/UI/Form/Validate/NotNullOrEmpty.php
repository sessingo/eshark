<?php
class UI_Form_Validate_NotNullOrEmpty extends \Pecee\UI\Form\Validate\ValidateInput {
	public function validate() {
		return (!empty($this->value));
	}
	public function getErrorMessage() {
		return $this->_('Error/CannotBeEmpty', $this->name);
	}
}