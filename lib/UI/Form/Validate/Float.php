<?php
class UI_Form_Validate_Float extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $allowEmpty;
	public function __construct($allowEmpty=FALSE) {
		$this->allowEmpty=$allowEmpty;
	}
	public function validate() {
		return ($this->allowEmpty && empty($this->value) || \Pecee\Float::is_float(\Pecee\Float::ParseFloat($this->value)));
	}
	public function getErrorMessage() {
		return $this->_('Error/InvalidFloat', $this->name);
	}
}