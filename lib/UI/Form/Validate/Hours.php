<?php
class UI_Form_Validate_Hours extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $allowNull;
	public function __construct($allowNull=FALSE) {
		$this->allowNull=$allowNull;
	}
	public function validate() {
		if($this->allowNull && !\Pecee\Integer::is_int($this->value) && empty($this->value)) {
			return TRUE;
		}
		return (\Pecee\Integer::is_int($this->value) && $this->value > 0);
	}
	public function getErrorMessage() {
		return \Pecee\Language::Instance()->_('Error/InvalidHours', $this->name);
	}
}