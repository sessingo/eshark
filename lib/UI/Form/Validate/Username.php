<?php
class UI_Form_Validate_Username extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $errorMessage;
	protected $minLength;
	protected $maxLength;
	
	public function __construct($minLength=2, $maxLength=25) {
		$this->minLength=$minLength;
		$this->maxLength=$maxLength;
	}
	
	public function validate() {
		if(empty($this->value)) {
			$this->errorMessage = $this->_('Error/CannotBeEmpty', $this->name);
		} elseif(strlen($this->value) < $this->minLength) {
			$this->errorMessage = $this->_('Error/Username/TooShort', $this->name);
		} elseif(strlen($this->value) > $this->maxLength) {
			$this->errorMessage = $this->_('Error/Username/TooLong', $this->name);
		} elseif(!preg_match('/^[a-zA-Z0-9\_\-]+$/', $this->value)) {
			$this->errorMessage = $this->_('Error/Username/InvalidCharacters', $this->name);
		}
		return !(isset($this->errorMessage));
	}
	public function getErrorMessage() {
		return $this->errorMessage;
	}
}