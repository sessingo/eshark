<?php
class UI_Form_Validate_File_NotNullOrEmpty extends \Pecee\UI\Form\Validate\ValidateFile {
	public function validate() {
		return (!empty($this->fileName) && $this->fileSize > 0 && $this->fileError == 0);
	}
	public function getErrorMessage() {
		return $this->_('Error/CannotBeEmpty', array($this->name));
	}
}