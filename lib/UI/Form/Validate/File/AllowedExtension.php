<?php
class UI_Form_Validate_File_AllowedExtension extends \Pecee\UI\Form\Validate\ValidateFile {
	protected $extensions;
	public function __construct(array $extensions) {
		$this->extensions=$extensions;
	}
	
	public function validate() {
		$ext=\Pecee\IO\File::GetExtension($this->fileName);
		return (in_array($ext, $this->extensions));
	}
	
	public function getErrorMessage() {
		return $this->_('Error/InvalidFormat', $this->name);
	}
}