<?php
class UI_Form_Validate_Integer extends \Pecee\UI\Form\Validate\ValidateInput {
	public function validate() {
		return (\Pecee\Integer::IsNummeric($this->value));
	}
	public function getErrorMessage() {
		return $this->_('Error/InvalidInteger', $this->name);
	}
}