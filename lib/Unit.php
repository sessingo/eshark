<?php
class Unit {
	public $units;
	public function __construct() {
		$this->units=array();
		$this->addUnit('Item/Units/Piece');
		$this->addUnit('Item/Units/Hours');
		$this->addUnit('Item/Units/Day');
		$this->addUnit('Item/Units/Week');
		$this->addUnit('Item/Units/Package');
	}
	public function addUnit($name) {
		$this->units[]=\Pecee\Language::Instance()->_($name);
	}
	public function getUnits() {
		asort($this->units);
		return $this->units;
	}
	public function getByIndex($index) {
		return isset($this->units[$index]) ? $this->units[$index] : NULL;
	}
}