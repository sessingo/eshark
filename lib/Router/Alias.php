<?php
class Router_Alias extends \Pecee\Router\RouterAlias {
	public function getPath($currentPath){
		$path = @explode('/', $currentPath);
		if(isset($path[0])) {
			switch(strtolower($path[0])) {
				case 'image': 
					if(isset($path[1])) {
						return \Pecee\Router::GetRoute('image', 'show', NULL, NULL, TRUE, TRUE);
					}
					break;
			}
		}
		return $currentPath;
	}
	public function getUrl($currentUrl){
		return $currentUrl;
	}
}