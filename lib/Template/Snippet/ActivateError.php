<? /* @var $this Widget_Activate */ ?>
<h3><?= $this->_('Ups, der er sket en fejl!')?></h3>
<p>
	<?= nl2br($this->_('Vi beklager, men der skete en fejl, da vi forsøgte at bekræfte din konto.
Dette sker typisk, hvis du eksempelvis allerede har aktiveret din konto, eller hvis tidsfristen for din aktivering er udløbet.
				
Få tilsendt en ny bekræftelsesmail og kontakt support hvis fejlen fortsat opstår.'))?>
</p>