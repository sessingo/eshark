<h3><?= $this->_('Customer/Project/ExistingProjects')?></h3>
<? if($this->projects->hasRow()) : ?>
	<table>
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>
		<tbody>
			<? /* @var Model_Project */
			foreach($this->projects->getRows() as $project) : ?>
				<tr>
					<td>
						<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit', array($project->getCustomerID(), $project->getProjectID())); ?>"><?= $project->getName(); ?></a>
					</td>
				</tr>
			<? endforeach;?>
		</tbody>
	</table>
<? endif; ?>