<? /* @var $this Widget_Item_Home */ ?>
<ul>
	<li><a href="<?= \Pecee\Router::GetRoute(NULL, NULL, array()); ?>"><?= $this->_('Item/All'); ?></a></li>
<? if($this->groups->hasRows()) : ?>
	<? /* @var $row Widget_Item_Home */ 
	   foreach($this->groups->getRows() as $row) : ?>
		<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'index', array($row->getItemGroupID()));?>"><?= $row->getName(); ?></a></li>
	<? endforeach;?>
<? endif; ?>
</ul>