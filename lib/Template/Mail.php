<?php /* @var $this Widget_Site */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?= $this->printHeader(); ?>
	</head>
	<body style="margin:0;padding:0;font-family:Arial-sans-serif;font-size:14px;">
		<div style="font-family: Arial,sans-serif;font-size:14px;margin:0;padding:0;color: #333;line-height:18px;width:98%;margin:0 auto;">
			<div style="padding:10px;background-color:#F9F9F9;">
				<?= $this->getContentHtml(); ?>
			</div>
			<div style="font-size:12px;color:#444;background-color:#F1F1F1;padding:10px;">
				<?= sprintf('Vises denne e-mail forkert i din browser, kan du trykke %s for at se den i din browser.', '<a href="'.\Pecee\Router::GetRoute(NULL, NULL, NULL, NULL, TRUE).'">her</a>'); ?><br/>
				<?= sprintf('Denne e-mail er sendt automatisk fra %s og kan ikke besvares.', \Pecee\Server::GetRemoteAddr()); ?>
			</div>
		</div>
	</body>
</html>