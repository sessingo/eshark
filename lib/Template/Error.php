<?php /* @var $this Widget_Site */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
	</head>
	<body>
		<div class="header">
			<div class="wrapper">
				<div class="inner">
					<a href="<?= \Pecee\Router::GetRoute('', ''); ?>"><img src="/img/logo.png" alt="" /></a>
				</div>
			</div>
		</div>
		<div class="content">
			<div class="wrapper">
				<div class="inner">
					<?= $this->getContentHtml(); ?>
				</div>
			</div>
		</div>
		<!-- BUILD: <?= \Pecee\Registry::GetInstance()->get('BuildVersion'); ?> -->
	</body>
</html>