<? /* @var $this Widget_Mail_Confirm */ ?>
<div style="padding:20px;">
	<h2 style="font-weight:bold;color:#3483B5;font-size:17px;"><?= $this->_('Bekræft dig selv!')?></h2>
	<p>
		<?= nl2br($this->_('For at holde din konto sikker, vi har brug for at vide, du er hvem du siger du er. 
Lad os vide ved at trykke på den grønne knap.')); ?>
	</p>
	<div style="padding-top:10px;">
		<a href="http://<?= $_SERVER['HTTP_HOST'] . \Pecee\Router::GetRoute('user', 'activate', NULL, array('token' => $this->token)); ?>" style="padding:5px 10px;background-color:#6ABD3D;color:#FFF;text-decoration:none;"><?= $this->_('Ja, jeg er mig!')?></a>
	</div>
</div>