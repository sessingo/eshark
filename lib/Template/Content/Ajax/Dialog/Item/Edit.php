<? /* @var $this Widget_Ajax_Dialog_Item_Edit */ ?>
<? if($this->finished) : ?>
	<script type="text/javascript">
		$(document).ready(function() {
			var input=$('.js-invoices tr:eq(<?= $this->index; ?>) input:first');
			if(input.length > 0) {
				input.val('<?= \Pecee\String\Encoding::Base64Encode($this->productItem); ?>');
				$p.doPostback(input);
			}
		});
	</script>
<? else: ?>
	<?= $this->form()->start('editItem'); ?>
	<?= $this->showErrors('editItem'); ?>
	<?= $this->form()->input('item', 'hidden', \Pecee\String\Encoding::Base64Encode($this->productItem)); ?>
	<?= $this->form()->input('index', 'hidden', $this->index); ?>
	<fieldset>
		<legend><?= $this->_('Item/FindExisting')?></legend>
		<?= $this->form()->input('search', 'text')->addAttribute('placeholder', $this->_('Item/Search'))->addAttribute('class','js-item-search')?> 
	</fieldset>
	
	<script type="text/javascript">
		$(document).ready(function() {
			var form=$('form[name="editItem"]');
			$('input.js-item-search').autocomplete({
				source: function(request,response) {
					$.getJSON('/item/search.json?q='+$('input.js-item-search').val(), function(r) {
						response($.map(r, function(item) {
							return {
								label: item.Number + ' - ' + item.Name,
								value: item.Number,
								data: item
							};
						}));		
					});
				},
				select: function(e,ui) {
					form.find('input[name="editItem_number"]').val(ui.item.data.Number);
					form.find('input[name="editItem_name"]').val(ui.item.data.Name);
					form.find('select[name="editItem_unit"]').val(ui.item.data.Unit);
					form.find('input[name="editItem_price"]').val(ui.item.data.SalePrice);
				}
			});
	
			var calculatePrice=function() {
				var price=parseFloat($('input[name="editItem_price"]').val());
				var count=parseFloat($('input[name="editItem_count"]').val());
				var rebate=parseFloat($('input[name="editItem_rebate"]').val());
				if(!isNaN(count) && !isNaN(price)) {
					price=parseFloat(count*price);
					if(!isNaN(rebate)) {
						price=parseFloat(price-rebate);
					}				
					$('input[name="editItem_amount"]').val(price);
				}
			};
			
			$('input[name="editItem_count"],input[name="editItem_price"],input[name="editItem_rebate"]').live('change', function() {
				calculatePrice();
			});
			calculatePrice();
		});
	</script>
	
	<fieldset>
		<legend><?= $this->_('Item/New')?></legend>
		<table>
		<tr>
			<td>
				<?= $this->form()->input('number', 'text',$this->productItem->getItem()->getNumber())->addAttribute('placeholder', $this->_('Item/ItemNo'))?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('name', 'text',$this->productItem->getItem()->getName())->addAttribute('placeholder', $this->_('Item/Name'))?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('description', 'text', $this->productItem->getDescription())->addAttribute('placeholder', $this->_('Item/Description'))?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('count','text',$this->productItem->getCount())->addAttribute('placeholder', $this->_('Item/Count')); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->selectStart('unit',new Dataset_Item_Unit($this->_('Item/ChooseUnit')), $this->productItem->getItem()->getUnit()); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('price', 'text', $this->productItem->getPrice())->addAttribute('placeholder', $this->_('Item/Price'))?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('rebate', 'text', $this->productItem->getRebate())->addAttribute('placeholder', $this->_('Item/Rebate'))?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('amount', 'text', $this->productItem->getTotal())->addAttribute('placeholder', $this->_('Item/Amount'))?>
			</td>
		</tr>
		<tr>
			<td style="padding-top:15px;">
				<?= $this->form()->submit('submit', $this->_('Item/Update')); ?>
			</td>
		</tr>
	</table>
	</fieldset>
	<?= $this->form()->end();?>
<? endif; ?>