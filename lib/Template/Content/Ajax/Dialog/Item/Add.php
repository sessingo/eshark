<? /* @var $this Widget_Ajax_Dialog_Item_Add */ ?>
<? if($this->productItem && !$this->debug):?>
	<script type="text/javascript">
		$(document).ready(function() {
			var form=$('form[name="invoice"]');
			if(form.length > 0) {
				form.append('<input type="hidden" name="invoice_productItem[]" value="<?= \Pecee\String\Encoding::Base64Encode($this->productItem); ?>" />');
				$p.doPostback(form);
			}
		});
	</script>
<? else: ?>
<?= $this->form()->start('addItem'); ?>
<?= $this->showErrors('addItem'); ?>
<fieldset>
	<legend><?= $this->_('Item/FindExisting')?></legend>
	<?= $this->form()->input('search', 'text')->addAttribute('placeholder', $this->_('Item/Search'))->addAttribute('class','js-item-search')?> 
</fieldset>

<script type="text/javascript">
	$(document).ready(function() {
		var form=$('form[name="addItem"]');
		$('input.js-item-search').autocomplete({
			source: function(request,response) {
				$.getJSON('/item/search.json?q='+$('input.js-item-search').val(), function(r) {
					response($.map(r, function(item) {
						return {
							label: item.Number + ' - ' + item.Name,
							value: item.Number,
							data: item
						};
					}));		
				});
			},
			select: function(e,ui) {
				form.find('input[name="addItem_number"]').val(ui.item.data.Number);
				form.find('input[name="addItem_name"]').val(ui.item.data.Name);
				form.find('select[name="addItem_unit"]').val(ui.item.data.Unit);
				form.find('input[name="addItem_price"]').val(ui.item.data.SalePrice);
			}
		});

		var calculatePrice=function() {
			var price=parseFloat($('input[name="addItem_price"]').val());
			var count=parseFloat($('input[name="addItem_count"]').val());
			var rebate=parseFloat($('input[name="addItem_rebate"]').val());
			if(!isNaN(count) && !isNaN(price)) {
				price=parseFloat(count*price);
				if(!isNaN(rebate)) {
					price=parseFloat(price-rebate);
				}				
				$('input[name="addItem_amount"]').val(price);
			}
		};
		
		$('input[name="addItem_count"],input[name="addItem_price"],input[name="addItem_rebate"]').live('change', function() {
			calculatePrice();
		});
	});
</script>

<fieldset>
	<legend><?= $this->_('Item/New')?></legend>
	<table>
	<tr>
		<td>
			<?= $this->form()->input('number', 'text')->addAttribute('placeholder', $this->_('Item/ItemNo'))?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->input('name', 'text')->addAttribute('placeholder', $this->_('Item/Name'))?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->input('description', 'text')->addAttribute('placeholder', $this->_('Item/Description'))?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->input('count','text')->addAttribute('placeholder', $this->_('Item/Count')); ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->selectStart('unit',new Dataset_Item_Unit($this->_('Item/ChooseUnit'))); ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->input('price', 'text')->addAttribute('placeholder', $this->_('Item/Price'))?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->input('rebate', 'text')->addAttribute('placeholder', $this->_('Item/Rebate'))?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->form()->input('amount', 'text')->addAttribute('placeholder', $this->_('Item/Amount'))?>
		</td>
	</tr>
	<tr>
		<td style="padding-top:15px;">
			<?= $this->form()->submit('submit', $this->_('Item/Add')); ?>
		</td>
	</tr>
</table>
</fieldset>
<?= $this->form()->end();?>
<? endif; ?>