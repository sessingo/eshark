<? /* @var $this Widget_Ajax_Dialog_Admin_Account_Edit */ ?>
<?= $this->form()->start('editAccount'); ?>
<?= $this->showErrors(); ?>
<table>
	<tr>
		<td style="width:120px;">
			Navn
		</td>
		<td>
			<?= $this->form()->input('name', 'text', $this->account->getName()); ?>
		</td>
	</tr>
	<tr>
		<td>
			Bank
		</td>
		<td>
			<?= $this->form()->input('bank', 'text', $this->account->getBank()); ?>
		</td>
	</tr>
	<tr>
		<td>
			Reg no
		</td>
		<td>
			<?= $this->form()->input('regNo', 'text', $this->account->getRegistrationNumber()); ?>
		</td>
	</tr>
	<tr>
		<td>
			Account no
		</td>
		<td>
			<?= $this->form()->input('accountNo', 'text', $this->account->getAccountNumber()); ?>
		</td>
	</tr>
	<tr>
		<td>
			IBAN
		</td>
		<td>
			<?= $this->form()->input('iban', 'text', $this->account->getIBAN()); ?>
		</td>
	</tr>
	<tr>
		<td>
			Swift
		</td>
		<td>
			<?= $this->form()->input('swift', 'text', $this->account->getSwift()); ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<?= $this->form()->submit('submit', 'Gem'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>