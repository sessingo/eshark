<? /* @var $this Widget_Ajax_Dialog_Admin_Account_Add */ ?>
<?= $this->form()->start('addAccount'); ?>
<?= $this->showErrors(); ?>
<table>
	<tr>
		<td style="width:120px;">
			Navn
		</td>
		<td>
			<?= $this->form()->input('name', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Bank
		</td>
		<td>
			<?= $this->form()->input('bank', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Reg no
		</td>
		<td>
			<?= $this->form()->input('regNo', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Account no
		</td>
		<td>
			<?= $this->form()->input('accountNo', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td>
			IBAN
		</td>
		<td>
			<?= $this->form()->input('iban', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Swift
		</td>
		<td>
			<?= $this->form()->input('swift', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<?= $this->form()->submit('submit', 'Gem'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>