<? /* @var $this Widget_Ajax_Dialog_Transfer_Edit */ ?>
<?= $this->form()->start('editTransfer'); ?>
<?= $this->showErrors(); ?>
<table>
	<tr>
		<td style="width:120px;">
			Konto
		</td>
		<td>
			<?= $this->form()->selectStart('accountId',new Dataset_Account(), $this->transfer->getAccountID()); ?>
		</td>
	</tr>
	<tr>
		<td>
			Tekst
		</td>
		<td>
			<?= $this->form()->input('text', 'text', $this->transfer->getText()); ?>
		</td>
	</tr>
	<tr>
		<td>
			Dato
		</td>
		<td>
			<?= $this->form()->input('date', 'text', $this->transfer->getDate())->addAttribute('class','js-datepicker'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Beløb
		</td>
		<td>
			<?= $this->form()->input('amount', 'text', Helper::FormatMoney($this->transfer->getAmount())); ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<?= $this->form()->submit('submit', 'Gem'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-datepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>