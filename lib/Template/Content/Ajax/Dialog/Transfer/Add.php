<? /* @var $this Widget_Ajax_Dialog_Transfer_Add */ ?>
<?= $this->form()->start('addTransfer'); ?>
<?= $this->showErrors(); ?>
<table>
	<tr>
		<td style="width:120px;">
			Konto
		</td>
		<td>
			<?= $this->form()->selectStart('accountId',new Dataset_Account(), $this->getParam('accountId')); ?>
		</td>
	</tr>
	<tr>
		<td>
			Tekst
		</td>
		<td>
			<?= $this->form()->input('text', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Dato
		</td>
		<td>
			<?= $this->form()->input('date', 'text')->addAttribute('class','js-datepicker'); ?>
		</td>
	</tr>
	<tr>
		<td>
			Beløb
		</td>
		<td>
			<?= $this->form()->input('amount', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<?= $this->form()->submit('submit', 'Gem'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-datepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>