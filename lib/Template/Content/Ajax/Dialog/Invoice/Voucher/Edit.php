<? /* @var $this Widget_Ajax_Dialog_Invoice_Voucher_Edit */ ?>
<?= $this->form()->start('editVoucher'); ?>
<?= $this->showErrors(); ?>
	<table>
		<tr>
			<td style="width:120px;">
				<?= $this->_('Voucher/Name')?>
			</td>
			<td>
				<?= $this->form()->input('name', 'text', $this->invoice->getName()); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/Description')?>
			</td>
			<td>
				<?= $this->form()->textarea('description', 5, 15, $this->invoice->getDescription())->addAttribute('style','height:80px;width:360px;')?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/InvoiceNumber')?>
			</td>
			<td>
				<?= $this->form()->input('invoiceNo', 'text', $this->invoice->getInvoiceNo()); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/InvoiceDate')?>
			</td>
			<td>
				<?= $this->form()->input('invoiceDate', 'text', $this->invoice->getInvoiceDate())->addAttribute('class', 'js-datepicker'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/PaymentDate')?>
			</td>
			<td>
				<?= $this->form()->input('paymentDate', 'text', $this->invoice->getPaymentDate())->addAttribute('class', 'js-datepicker'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/Amount')?>
			</td>
			<td>
				<?= $this->form()->input('price', 'text', $this->item->getPrice()); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/Recorded')?>
			</td>
			<td>
				<?= $this->form()->bool('recorded', $this->invoice->getRecorded()); ?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right;">
				<?= $this->form()->submit('submit', $this->_('Voucher/Update')) ?>
			</td>
		</tr>
	</table>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-datepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>