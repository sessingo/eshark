<? /* @var $this Widget_Ajax_Dialog_Invoice_Voucher_Create */ ?>
<?= $this->form()->start('createVoucher'); ?>
<?= $this->showErrors(); ?>
	<table>
		<tr>
			<td style="width:120px;">
				<?= $this->_('Voucher/Name')?>
			</td>
			<td>
				<?= $this->form()->input('name', 'text'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/Description')?>
			</td>
			<td>
				<?= $this->form()->textarea('description', 5, 15)->addAttribute('style','height:80px;width:360px;')?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/InvoiceNumber')?>
			</td>
			<td>
				<?= $this->form()->input('invoiceNo', 'text'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/InvoiceDate')?>
			</td>
			<td>
				<?= $this->form()->input('invoiceDate', 'text', date('d-m-Y'))->addAttribute('class', 'js-datepicker'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/PaymentDate')?>
			</td>
			<td>
				<?= $this->form()->input('paymentDate', 'text')->addAttribute('class', 'js-datepicker'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/Amount')?>
			</td>
			<td>
				<?= $this->form()->input('price', 'text'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Voucher/Recorded')?>
			</td>
			<td>
				<?= $this->form()->bool('recorded')?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right;">
				<?= $this->form()->submit('submit', $this->_('Voucher/Create')) ?>
			</td>
		</tr>
	</table>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-datepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>