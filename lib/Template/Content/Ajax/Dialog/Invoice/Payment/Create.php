<? /* @var $this Widget_Ajax_Dialog_Invoice_Payment_Create */ ?>
<?= $this->form()->start('createPayment'); ?>
<?= $this->showErrors(); ?>
	<table>
		<tr>
			<td style="width:120px;">
				<?= $this->_('Invoice/Customer')?>
			</td>
			<td>
				<?= $this->customer->getName(); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/InvoiceNumber')?>
			</td>
			<td>
				<?= $this->form()->selectStart('payedInvoiceId', new Dataset_Invoice_Payment_List($this->company, $this->customer->getCustomerID(), $this->_('Invoice/ChooseInvoice'))) ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/PaymentDate')?>
			</td>
			<td>
				<?= $this->form()->input('paymentDate', 'text')->addAttribute('class','js-datepicker'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/Description')?>
			</td>
			<td>
				<?= $this->form()->textarea('description', 5, 20)->addAttribute('style','width:350px;height:100px;'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/Amount')?>
			</td>
			<td>
				<?= $this->form()->input('price', 'text'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/Record')?>
			</td>
			<td>
				<?= $this->form()->bool('recorded');?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right;">
				<?= $this->form()->submit('submit', $this->_('Invoice/Add')) ?>
			</td>
		</tr>
	</table>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-datepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>