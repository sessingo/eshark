<? /* @var $this Widget_Ajax_Dialog_Invoice_Payment_Edit */ ?>

<?= $this->form()->start('editPayment'); ?>
<?= $this->showErrors(); ?>
	<table>
		<tr>
			<td style="width:120px;">
				<?= $this->_('Invoice/Customer')?>
			</td>
			<td>
				<?= $this->invoice->getCustomer()->getName(); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/InvoiceNumber')?>
			</td>
			<td>
				<?= $this->form()->selectStart('payedInvoiceId', new Dataset_Invoice_Payment_List($this->company, $this->invoice->getCustomerID(), $this->_('Invoice/ChooseInvoice')), $this->invoice->getPayedInvoiceId(), TRUE) ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/PaymentDate')?>
			</td>
			<td>
				<?= $this->form()->input('paymentDate', 'text', $this->invoice->getPaymentdate(), TRUE)->addAttribute('class','js-datepicker'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/Description')?>
			</td>
			<td>
				<?= $this->form()->textarea('description', 5, 20, $this->invoice->getDescription(), TRUE)->addAttribute('style','width:350px;height:100px;'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/Amount')?>
			</td>
			<td>
				<?= $this->form()->input('price', 'text', Helper::FormatMoney($this->item->getPrice()/-1), TRUE); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->_('Invoice/Record')?>
			</td>
			<td>
				<?= $this->form()->bool('recorded', $this->invoice->getRecorded(), TRUE);?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right;">
				<?= $this->form()->submit('submit', $this->_('Invoice/Edit')) ?>
			</td>
		</tr>
	</table>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-datepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>