<? /* @var $this Widget_Ajax_Dialog_Activity_Edit */ ?>
<?= $this->form()->start('editLog'); ?>
<?= $this->showErrors(); ?>
<table>
	<tr>
		<td style="width:120px;">
			<?= $this->_('Activity/Customer')?>
		</td>
		<td>
			<?= $this->form()->selectStart('customerId', new Dataset_Customer($this->_('Activity/Choose')), $this->customerId)->addAttribute('onchange', '$(\'#projectId\').val(\'\');$p.doPostback(this);')->addAttribute('id', 'customerId'); ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->_('Activity/Project')?>
		</td>
		<td>
			<?= $this->form()->selectStart('projectId', new Dataset_Customer_Project($this->customerId, $this->_('Activity/Choose')), $this->projectId)->addAttribute('onchange', '$p.doPostback(this);')->addAttribute('id', 'projectId'); ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->_('Activity/Activity')?>
		</td>
		<td>
			<?= $this->form()->selectStart('activityId', new Dataset_Customer_Project_Activity($this->projectId, $this->_('Activity/Choose')), $this->activityId)->addAttribute('onchange', '$p.doPostback(this);')->addAttribute('id', 'activityId') ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->_('Activity/Hours')?>
		</td>
		<td>
			<?= $this->form()->input('hours', 'text', $this->log->getHoursLog(),TRUE)->addAttribute('maxlength', '3')->addAttribute('style','width:50px;')->addAttribute('placeholder', $this->_('Activity/Hours')); ?> &nbsp;
			<?= $this->form()->input('minutes', 'text', $this->log->getMinutesLog(),TRUE)->addAttribute('maxlength', '2')->addAttribute('style','width:50px;')->addAttribute('placeholder', $this->_('Activity/Minutes')); ?>
		</td>
	</tr>
	<? if($this->activity && $this->activity->hasRows() && $this->activity->getMaxHours()) : ?>
	<tr>
		<td></td>
		<td>
			<?= $this->_('Activity/ActivityInfo', $this->activity->getMaxHours(), 0); ?>
		</td>
	</tr>
	<? endif; ?>
	<tr>
		<td style="vertical-align:top;">
			<?= $this->_('Activity/Description')?>
		</td>
		<td>
			<?= $this->form()->textarea('description', 6, 50, $this->log->getDescription())->addAttribute('style', 'width:420px;height:180px;'); ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:right;">
			<?= $this->form()->submit('submit', $this->_('Activity/LogHours')) ?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>

<script type="text/javascript">
$p.live('onPostback', function(data) {
	if($('.dialog').is(':visible')) {
		var f=$('.dialog form:first');
		var c=$('#dialog_content');
		var d=$('.dialog:last');
		c.find('.inner').html('');
		d.addClass('loading');
		dialog.center();
		$.ajax({ url: '<?= \Pecee\Router::GetRoute(NULL, NULL, NULL, NULL, TRUE); ?>', 
				 cache: false, 
				 type: 'POST', 
				 data: f.serialize(), 
				 success: function(r) {
					d.removeClass('loading');
					c.html(r);
					dialog.center();
				 }
		});
		return false;
	}
});
</script>