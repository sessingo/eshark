<? /* @var $this Widget_Account_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute('ajax','dialog',array('transfer','add'), array('accountId' => $this->accountId));?>" rel="dialog">Ny overførelse</a></li>
	</ul>
</div>

<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->accountsMenu; ?>
		</div>
		<div class="col2">
			<? if(!$this->accountId) : ?>
			Vælg konto
			<? else: ?>
				<?= $this->form()->start('search', 'get');?>
				<fieldset style="margin-bottom:15px;">
					<legend>Kontobevægelser</legend>
					<div class="input">
						<span class="label">Periode</span>
						<div style="float:left;margin-left:10px;">
							<?= $this->form()->input('from', 'text', $this->getParam('from'))
								->addAttribute('placeholder', $this->_('Invoice/From'))
								->addAttribute('id', 'fromDatepicker')
								->addAttribute('autocomplete', 'off'); ?> 
							<span style="padding:0px 15px;">til</span> 
							<?= $this->form()->input('to', 'text', $this->getParam('to'))
								->addAttribute('placeholder', $this->_('Invoice/To'))
								->addAttribute('id', 'toDatepicker')
								->addAttribute('autocomplete', 'off'); ?> 	
							<?= $this->form()->submit('submit', $this->_('Invoice/Update'))
							->addAttribute('class', 'small'); ?>
						</div>
						<div class="clear"></div>
					</div>
				</fieldset>
				<?= $this->form()->end(); ?>
				
				<? if(!$this->transfers->hasRows()) : ?>
				<div style="text-align:center;">
					Ingen kontobevægelser fundet
				</div>
				<? else: ?>
				<table>
					<thead>
						<tr>
							<th style="width:16px;"></th>
							<th>
								Bogført
							</th>
							<th>
								Tekst
							</th>
							<th style="text-align:right;">
								Beløb
							</th>
							<th style="text-align:right;">
								Saldo
							</th>
							<th>
							
							</th>
						</tr>
					</thead>
					<tbody>
						<? /* @var $transfer Model_Transfer */ 
							foreach($this->transfers->getRows() as $transfer) : ?>
						<? if($transfer->getInvoiceID()) : ?>
						<tr>
							<td>
								<img src="/img/ico/16x16/payment.png" alt="" />
							</td>
							<td>
								<?= Helper::FormatDate($transfer->getDate()); ?>
							</td>
							<td>
								<?= $transfer->getText(); ?>
							</td>
							<td style="text-align:right;">
								<?= Helper::FormatMoney($transfer->getAmount()); ?>
							</td>
							<td style="text-align:right;">
								<?= Helper::FormatMoney($this->getBalance($transfer)); ?>
							</td>
							<td></td>
						</tr>
						<? else: ?>
						<tr>
							<td>
								<img src="/img/ico/16x16/money.png" alt="" />
							</td>
							<td>
								<?= Helper::FormatDate($transfer->getDate()); ?>
							</td>
							<td>
								<?= $transfer->getText(); ?>
							</td>
							<td style="text-align:right;">
								<?= Helper::FormatMoney($transfer->getAmount()); ?>
							</td>
							<td style="text-align:right;">
								<?= Helper::FormatMoney($transfer->getBalance()); ?>
							</td>
							<td style="text-align:right;">
								<a href="<?= \Pecee\Router::GetRoute('transfer','delete',array($transfer->getTransferID()));?>" onclick="return confirm('Er du sikker på, at du vil slette denne transfer?');">Slet</a> - 
								<a href="<?= \Pecee\Router::GetRoute('ajax','dialog',array('transfer','edit'),array('transferId' => $transfer->getTransferID())); ?>" rel="dialog">Rediger</a>
							</td>
						</tr>
						<? endif; ?>
						<? endforeach; ?>
					</tbody>
					<tfoot>
						<tr style="font-weight:bold;text-transform:uppercase;">
							<td colspan="4">
								
							</td>
							<td style="text-align:right;">
								<?= Helper::FormatMoney($this->balance); ?>
							</td>
							<td></td>
						</tr>
					</tfoot>
				</table>
				<? endif; ?>
			<? endif; ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {	
	$('#fromDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#toDatepicker').datepicker('option', 'minDate', selectedDate);
		}
	});

	$('#toDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#fromDatepicker').datepicker('option', 'maxDate', selectedDate);
		}
	});
});
</script>