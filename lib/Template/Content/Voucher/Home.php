<? /* @var $this Widget_Voucher_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute('ajax', 'dialog', array('invoice', 'voucher', 'create')); ?>" rel="dialog"><?= $this->_('Voucher/NewVoucher')?></a></li>
	</ul>
</div>
<div class="ctn">
	<?= $this->form()->start('search', 'get');?>
	<fieldset style="margin-bottom:15px;">
		<legend><?= $this->_('Voucher/Filter'); ?></legend>
		<div class="input">
			<span class="label"><?= $this->_('Voucher/Period')?></span>
			<div style="float:left;margin-left:10px;">
				<?= $this->form()->input('from', 'text', $this->getParam('from'))
					->addAttribute('placeholder', $this->_('Voucher/From'))
					->addAttribute('id', 'fromDatepicker')
					->addAttribute('autocomplete', 'off'); ?> 
				<span style="padding:0px 15px;"><?= $this->_('Voucher/To'); ?></span> 
				<?= $this->form()->input('to', 'text', $this->getParam('to'))
					->addAttribute('placeholder', $this->_('Voucher/To'))
					->addAttribute('id', 'toDatepicker')
					->addAttribute('autocomplete', 'off'); ?> 	
				<?= $this->form()->submit('submit', $this->_('Voucher/Update'))
				->addAttribute('class', 'small'); ?>
			</div>
			<div class="input">
				<span class="label"></span>
				<div style="float:left;margin-left:10px;">
					<?= $this->form()->bool('recorded', $this->getParam('recorded'))
						->addAttribute('ID', 'recorded');?> 
					<?= $this->form()->label($this->_('Voucher/RecordedOnly'), 'recorded'); ?> 
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</fieldset>
	<?= $this->form()->end(); ?>
	
	<? if($this->hasParam('submit')) : ?>
		<? if(!$this->invoices->hasRows()) : ?>
		<div style="text-align:center;">
			<?= $this->_('Voucher/NoVouchersFound')?>
		</div>
		<? else: ?>
			<table>
				<thead>
					<tr>
						<th></th>
						<th>
							<?= $this->_('Voucher/Name'); ?>
						</th>
						<th>
							<?= $this->_('Voucher/Description')?>
						</th>
						<th>
							<?= $this->_('Voucher/InvoiceDate')?>
						</th>
						<th>
							<?= $this->_('Voucher/PaymentDate')?>
						</th>
						<th style="text-align:right;">
							<?= $this->_('Voucher/Vat')?> (<?= $this->company->getVatPercentage(); ?>%)
						</th>
						<th style="text-align:right;">
							<?= $this->_('Voucher/AmountWithVat')?>
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
				<tbody>
					<? /* @var $invoice Model_Invoice */
					   foreach($this->invoices->getRows() as $invoice) : ?>
					<tr>
						<td>
							<img src="<?= $invoice->getIcon(); ?>" alt="" />
						</td>
						<td>
							<?= $invoice->getName(); ?>
						</td>
						<td>
							<?= ($invoice->getDescription()) ? $invoice->getDescription() : $this->_('Voucher/NoDescription'); ?>
						</td>
						<td>
							<?= $invoice->getInvoiceDate(); ?>
						</td>
						<td>
							<?= $invoice->getPaymentDate(); ?>
						</td>
						<td style="text-align:right;">
							<?= Helper::FormatMoney($invoice->getVatTotal($this->company)); ?>
						</td>
						<td style="text-align:right;">
							<?= Helper::FormatMoney($invoice->getTotal($this->company)); ?>
						</td>
						<td style="text-align:right;">
							<? if(!$invoice->getRecorded()) : ?>
							<a href="<?= \Pecee\Router::GetRoute(NULL,'recorded',array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Voucher/ConfirmRecord');?>');"><?= $this->_('Voucher/Record')?></a> - 
							<? endif; ?> 
							<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Voucher/ConfirmDelete');?>');"><?= $this->_('Voucher/Delete'); ?></a> - 
							<a href="<?= \Pecee\Router::GetRoute('ajax','dialog', array('invoice', 'voucher', 'edit'), array('invoiceId' => $invoice->getInvoiceID(), 'redirectUrl' => \Pecee\Router::GetCurrentRoute())); ?>" rel="dialog"><?= $this->_('Voucher/Edit')?></a>
						</td>
					</tr>
					<? endforeach; ?>
				</tbody>
				<tfoot>
					<tr style="font-weight:bold;text-transform:uppercase;">
						<td colspan="5" style="text-align:right;">
							<?= $this->_('Voucher/TotalWithVat', $this->company->getCurrency())?>
						</td>
						<td style="text-align:right;">
							<?= Helper::FormatMoney($this->totalVat); ?>
						</td>
						<td style="text-align:right;">
							<?= Helper::FormatMoney($this->total); ?>
						</td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		<? endif; ?>
	<? endif; ?>
	<script type="text/javascript">
	$(document).ready(function() {	
		$('#fromDatepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
				$('#toDatepicker').datepicker('option', 'minDate', selectedDate);
			}
		});
	
		$('#toDatepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
				$('#fromDatepicker').datepicker('option', 'maxDate', selectedDate);
			}
		});
	});
	</script>
</div>