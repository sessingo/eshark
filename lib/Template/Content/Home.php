<? /* @var $this Widget_Home */ ?>
<? if($this->getMessage('TimeoutError')) : ?>
	<ul class="msg error">
		<li><?= $this->_('Error/TimeoutOccured'); ?></li>
	</ul>
<? endif; ?>
<?= $this->form()->start('login'); ?>
<?= $this->showErrors('login'); ?>
	<div class="input">
		<span class="label">
			<label for="username"><?= $this->_('User/UsernameLabel')?></label>
		</span>
		<?= $this->form()->input('username', 'text')->addAttribute('ID','username'); ?>
	</div>
	<div class="input">
		<span class="label">
			<label for="password"><?= $this->_('User/PasswordLabel')?></label>
		</span>
		<?= $this->form()->input('password', 'password', '')->addAttribute('ID','password'); ?> 
	</div>
	<div class="input btn">
		<?= $this->form()->submit('submit', $this->_('User/SubmitLabel')); ?>
	</div>
<?= $this->form()->end(); ?>