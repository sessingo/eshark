<? /* @var $this Widget_Customer_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<?= $this->form()->start('customer'); ?>
	<?= $this->showErrors('customer'); ?>
	<table>
		<tr>
			<th>
				<?= $this->_('Customer/Name')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('name', 'text', $this->customer->getName()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Address')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->textarea('address', 5, 50, $this->customer->getAddress()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/PostalCode')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('postalnumber', 'text', $this->customer->getPostalnumber()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/City')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('city', 'text', $this->customer->getCity()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<label for="country"><?= $this->_('Customer/Country'); ?></label>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->selectStart('country', new Dataset_Country($this->_('Customer/ChooseCountry')), $this->company->getCountry())->addAttribute('ID','country'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/ContactPerson')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('contact', 'text', $this->customer->getContact()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/DeliveryAddress')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->textarea('deliveryAddress', 5, 50, $this->customer->getDeliveryAddress()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Notes')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->textarea('notes', 5, 50, $this->customer->getNotes()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Telephone')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('phone', 'text', $this->customer->getPhone()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Cellphone')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('cellphone', 'text', $this->customer->getCellphone()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/VATNo')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('vat', 'text', $this->customer->getVat()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Website'); ?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('website', 'text', $this->customer->getWebsite()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Bank')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('bank', 'text', $this->customer->getBank()); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/EAN')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('ean', 'text', $this->customer->getEan()); ?>
			</td>
		</tr>
		<tr>
			<th valign="top">
				<label for="file"><?= $this->_('Customer/Logo'); ?></label>
			</th>
		</tr>
		<tr>
			<td>
				<? if($this->customer->getLogo()) : ?>
					<div style="margin-bottom:15px;">
						<img src="<?= $this->customer->getLogo()->getInternalUrl(); ?>" alt="" />
					</div>
				<? endif; ?>
				<?= $this->form()->input('logo', 'file')->addAttribute('ID','file')->addAttribute('style','float:none;'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->submit('submit', $this->_('Customer/Save')); ?>
			</td>
		</tr>
	</table>
	<?= $this->form()->end(); ?>
</div>