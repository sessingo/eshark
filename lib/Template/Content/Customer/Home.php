<? /* @var $this Widget_Customer_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">

		<?= $this->form()->start('search','get'); ?>
			<div class="input">
				<span class="label">
					<label for="customerId"><?= $this->_('Customer/CustomerNumber')?></label>
				</span>
				<?= $this->form()->input('customerId', 'text', $this->getParam('customerId'))->addAttribute('ID','customerId'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="name"><?= $this->_('Customer/Name')?></label>
				</span>
				<?= $this->form()->input('name', 'text', $this->getParam('name'))->addAttribute('ID','name'); ?> 
			</div>
			<div class="input">
				<span class="label">
					<label for="address"><?= $this->_('Customer/Address')?></label>
				</span>
				<?= $this->form()->input('address', 'text', $this->getParam('address'))->addAttribute('ID','address'); ?> 
			</div>
			<div class="input">
				<span class="label">
					<label for="postalnumber"><?= $this->_('Customer/PostalCodeAndCity')?></label>
				</span>
				<?= $this->form()->input('postalnumber', 'text', $this->getParam('postalnumber'))->addAttribute('ID','postalnumber'); ?> 
				<?= $this->form()->input('city', 'text', $this->getParam('city'))->addAttribute('ID','city'); ?> 
			</div>
			<div class="input">
				<span class="label">
					<label for="telephone"><?= $this->_('Customer/Telephone')?></label>
				</span>
				<?= $this->form()->input('telephone', 'text', $this->getParam('telephone'))->addAttribute('ID','telephone'); ?> 
			</div>
			<div class="input">
				<span class="label">
					<label for="cellphone"><?= $this->_('Customer/Cellphone')?></label>
				</span>
				<?= $this->form()->input('cellphone', 'text', $this->getParam('cellphone'))->addAttribute('ID','cellphone'); ?> 
			</div>
			<div class="input">
				<span class="label">
					<label for="email"><?= $this->_('Customer/Email')?></label>
				</span>
				<?= $this->form()->input('email', 'text', $this->getParam('email'))->addAttribute('ID','email'); ?> 
			</div>
			<div class="input">
				<span class="label">
					<label for="vat"><?= $this->_('Customer/VATNo')?></label>
				</span>
				<?= $this->form()->input('vat', 'text', $this->getParam('vat'))->addAttribute('ID','vat'); ?> 
			</div>
			<div class="input btn">
				<?= $this->form()->submit('submit', $this->_('Customer/FindCustomer')); ?>
			</div>
		<?= $this->form()->end(); ?>

	
	<? if($this->hasParam('submit') && $this->customers->hasRows()) : ?>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>
					<?= $this->_('Customer/CustomerNo'); ?>
				</th>
				<th>
					<?= $this->_('Customer/Name'); ?>
				</th>
				<th>
					<?= $this->_('Customer/Address'); ?>
				</th>
				<th>
					<?= $this->_('Customer/PostalCodeAndCity'); ?>
				</th>
				<th>
					<?= $this->_('Customer/VATNo'); ?>
				</th>
			</tr>
		</thead>
		<? /* @var $customer Model_Customer */
		foreach($this->customers->getRows() as $customer) : ?>
		<tr>
			<td>
				<img src="/img/ico/16x16/customer.png" alt="" />
			</td>
			<td>
				<a href="<?= \Pecee\Router::GetRoute('customer','details', array($customer->getCustomerID())); ?>"><?= $customer->getCustomerID(); ?></a>
			</td>
			<td>
				<a href="<?= \Pecee\Router::GetRoute('customer','details', array($customer->getCustomerID())); ?>"><?= $customer->getName(); ?></a>
			</td>
			<td>
				<?= $customer->getAddress(); ?>
			</td>
			<td>
				<?= $customer->getPostalNumber(); ?> <?= $customer->getCity(); ?>
			</td>
			<td>
				<?= $customer->getVat(); ?>
			</td>
		</tr>
		<? endforeach; ?>
	</table>
	<? endif; ?>
</div>