<? /* @var $this Widget_Customer_Invoice_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute('ajax','dialog',array('invoice','payment','create'), array('customerId' => $this->customerId));?>" rel="dialog"><?= $this->_('Invoice/NewPayment')?></a></li>
		<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'createMemo', array($this->customerId), array('redirectUrl' => \Pecee\Router::GetCurrentRoute())); ?>"><?= $this->_('Invoice/NewCreditMemo')?></a></li>
		<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'create', array($this->customerId), array('redirectUrl' => \Pecee\Router::GetCurrentRoute())); ?>"><?= $this->_('Invoice/NewInvoice')?></a></li>
	</ul>
</div>
<div class="ctn">
	<?= $this->form()->start('search', 'get');?>
	<fieldset style="margin-bottom:15px;">
		<legend><?= $this->_('Invoice/Filter'); ?></legend>
		<div class="input">
			<span class="label"><?= $this->_('Invoice/Period')?></span>
			<div style="float:left;margin-left:10px;">
				<?= $this->form()->input('from', 'text', $this->getParam('from'))
					->addAttribute('placeholder', $this->_('Invoice/From'))
					->addAttribute('id', 'fromDatepicker')
					->addAttribute('autocomplete', 'off'); ?> 
				<span style="padding:0px 15px;"><?= $this->_('Invoice/To'); ?></span> 
				<?= $this->form()->input('to', 'text', $this->getParam('to'))
					->addAttribute('placeholder', $this->_('Invoice/To'))
					->addAttribute('id', 'toDatepicker')
					->addAttribute('autocomplete', 'off'); ?> 	
				<?= $this->form()->submit('submit', $this->_('Invoice/Update'))
				->addAttribute('class', 'small'); ?>
			</div>
			<div class="input">
				<span class="label"></span>
				<div style="float:left;margin-left:10px;">
					<?= $this->form()->bool('hidePayment',$this->getParam('hidePayment'))
						->addAttribute('ID','hidePayment')?> 
					<?= $this->form()->label($this->_('Invoice/HidePayment'), 'hidePayment'); ?> 
					<?= $this->form()->bool('recorded', $this->getParam('recorded'))
						->addAttribute('ID', 'recorded')
						->addAttribute('style','margin-left:20px;');?> 
					<?= $this->form()->label($this->_('Invoice/RecordedOnly'), 'recorded'); ?> 
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</fieldset>
	<?= $this->form()->end(); ?>
	<?= $this->invoices; ?>
	<script type="text/javascript">
	$(document).ready(function() {	
		$('#fromDatepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
				$('#toDatepicker').datepicker('option', 'minDate', selectedDate);
			}
		});
	
		$('#toDatepicker').datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
				$('#fromDatepicker').datepicker('option', 'maxDate', selectedDate);
			}
		});
	});
	</script>
</div>