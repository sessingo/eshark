<? /* @var $this Widget_Customer_Activity_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<?= $this->form()->start('updateActivity'); ?>
	<?= $this->showErrors('updateActivity')?>
		<table>
			<tr>
				<td style="width:120px;">
					<?= $this->_('Customer/Activity/Project'); ?>
				</td>
				<td>
					<?= $this->form()->selectStart('project', new Dataset_Customer_Project($this->activity->getProject()->getCustomerID(), $this->_('Customer/Project/ChooseProject')), $this->activity->getProject()->getProjectID()); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/Name'); ?>
				</td>
				<td>
					<?= $this->form()->input('name', 'text', $this->activity->getName()); ?>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top;">
					<?= $this->_('Customer/Activity/Description'); ?>
				</td>
				<td>
					<?= $this->form()->textarea('description', 5, 35, $this->activity->getDescription()); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/MaxHours'); ?>
				</td>
				<td>
					<?= $this->form()->input('maxhours', 'text', $this->activity->getMaxHours()); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/HourPrice'); ?>
				</td>
				<td>
					<?= $this->form()->input('price', 'text', $this->activity->getPrice()); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/Active'); ?>
				</td>
				<td>
					<?= $this->form()->bool('active', $this->activity->getActive()); ?>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<?= $this->form()->submit('button', $this->_('Customer/Activity/Update')); ?>
				</td>
			</tr>
		</table>
	
	<?= $this->form()->end(); ?>
</div>