<? /* @var $this Widget_Customer_Activity_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<? if($this->project && $this->project->hasRow()) : ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'create', array($this->customerId, $this->project->getProjectID())); ?>" class="btn" style="float:right;"><?= $this->_('Customer/Activity/New')?></a></li>
	</ul>
	<? endif; ?>
</div>
<div class="ctn">
	<?= $this->form()->selectStart('project', new Dataset_Customer_Project($this->customerId, $this->_('Customer/Project/ChooseProject')), $this->projectId)->addAttribute('onchange', 'top.location=\''.\Pecee\Router::GetRoute(NULL, NULL, array($this->customerId)).'\' + this.value;'); ?>
	<? if($this->project && $this->project->hasRow()): ?>
		
		<h2><?= $this->_('Customer/Menu/Activities')?></h2>
		<? if(!$this->activities->hasRows()) : ?>
			<div>
				<?= $this->_('Customer/Activity/NoActivities')?>
			</div>
		<? else: ?>
		<table>
			<thead>
				<tr>
					<th style="text-align:left;">
						<?= $this->_('Customer/Activity/Name'); ?>
					</th>
					<th style="width:100px;text-align:right;">
						<?= $this->_('Customer/Activity/Functions')?>
					</th>
				</tr>
			</thead>
			<tbody>
			<? /* @var $activity Model_Project_Activity */
			foreach($this->activities->getRows() as $activity) : ?>
			<tr>
				<td>
					<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit',array($this->customerId, $activity->getProjectActivityID())); ?>"><?= $activity->getName(); ?></a>
				</td>
				<td style="text-align:right;">
					<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($activity->getProjectActivityID())); ?>" onclick="return confirm('<?= $this->_('Customer/Activity/ConfirmDelete'); ?>');"><?= $this->_('Customer/Activity/Delete');?></a>
				</td>
			</tr>
			<? endforeach; ?>
			</tbody>
		</table>
		<? endif; ?>
	<? endif; ?>
</div>