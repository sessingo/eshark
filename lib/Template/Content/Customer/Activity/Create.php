<? /* @var $this Widget_Customer_Activity_Create */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<?= $this->form()->start('createActivity'); ?>
	<?= $this->showErrors('createActivity'); ?>
		<table>
			<tr>
				<td style="width:120px;">
					<?= $this->_('Customer/Activity/Name'); ?>
				</td>
				<td>
					<?= $this->form()->input('name', 'text'); ?>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top;">
					<?= $this->_('Customer/Activity/Description'); ?>
				</td>
				<td>
					<?= $this->form()->textarea('description', 5, 35); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/MaxHours'); ?>
				</td>
				<td>
					<?= $this->form()->input('maxhours', 'text'); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/HourPrice'); ?>
				</td>
				<td>
					<?= $this->form()->input('price', 'text'); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= $this->_('Customer/Activity/Active'); ?>
				</td>
				<td>
					<?= $this->form()->bool('active', TRUE); ?>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<?= $this->form()->submit('button', $this->_('Customer/Activity/Save')); ?>
				</td>
			</tr>
		</table>
	
	<?= $this->form()->end(); ?>
</div>