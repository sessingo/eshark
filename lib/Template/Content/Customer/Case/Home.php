<? /* @var $this Widget_Customer_Case_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute(NULL,'create',array($this->customerId)); ?>" class="btn small"><?= $this->_('Customer/Case/CreateCase'); ?></a></li>
	</ul>
</div>
<div class="ctn">
	<? if($this->cases->hasRows()) : ?>
	<table>
		<thead>
			<tr>
				<th>
					<?= $this->_('Customer/Case/Cases')?>
				</th>
				<th>
					<?= $this->_('Customer/Case/User')?>
				</th>
				<th>
					<?= $this->_('Customer/Case/Date')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<? /* @var $case Widget_Case_Home */
			foreach($this->cases->getRows() as $case) : ?>
			<tr>
				<td>
					<a href="<?= \Pecee\Router::GetRoute(NULL, 'view', array($this->customerId,$case->getCaseID())); ?>"><?= $case->getSubject()->getTitle(); ?></a>
				</td>
				<td>
					<?= Model_User::GetByUserID($case->getUserID())->data->name; ?>
				</td>
				<td>
					<?= $case->getCreatedDate(); ?>
				</td>
			</tr>
			<? endforeach; ?>
		</tbody>
	</table>
	<? endif; ?>
</div>