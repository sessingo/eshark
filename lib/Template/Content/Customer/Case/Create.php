<? /* @var $this Widget_Customer_Case_Create */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<h3><?= $this->_('Customer/Case/CreateCase'); ?></h3>
	
	<?= $this->form()->start('createCase'); ?>
	<?= $this->showErrors('createCase'); ?>
	<div>
		<?= $this->form()->selectStart('subject', new Dataset_Case_Subject(), 0)->addAttribute('style','width:400px;font-size:19px;');?>
		<?= $this->form()->submit('submit', $this->_('Customer/Case/Save'))->addAttribute('class','small'); ?>
	</div>
	<div>
		<?= $this->form()->textarea('text', 20, 120); ?>
	</div>
	<?= $this->form()->end(); ?>
</div>