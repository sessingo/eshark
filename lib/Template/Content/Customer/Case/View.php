<? /* @var $this Widget_Customer_Case_View */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<h3>Svar på sag</h3>
	<?= $this->form()->start('createCase'); ?>
	<?= $this->showErrors('createCase');?>
	<div>
		<?= $this->form()->submit('submit', $this->_('Customer/Case/Save'))->addAttribute('class','small'); ?>
	</div>
	<div>
		<?= $this->form()->textarea('text', 20, 120); ?>
	</div>
	<?= $this->form()->end(); ?>
	
	<h3><?= $this->case->getSubject()->getTitle(); ?></h3>
	<table>
		<thead>
			<tr>
				<th style="text-align:left;">
					Svar
				</th>
				<th style="text-align:left;">
					Bruger
				</th>
				<th style="text-align:left;">
					Dato
				</th>
			</tr>
		</thead>
		<tbody>
			<? if($this->case->getChildren()->hasRows()) : ?>
			<? /* @var $case Widget_Case_Home */
			foreach($this->case->getChildren()->getRows() as $case) : ?>
			<tr>
				<td>
					<p>
						<?= $case->getText();?>
					</p>
				</td>
				<td>
					<?= Model_User::GetByUserID($case->getUserID())->data->name; ?>
				</td>
				<td>
					<?= $case->getCreatedDate(); ?>
				</td>
			</tr>
			<? endforeach; ?>
			<? endif; ?>
			<tr>
				<td>
					<?= $this->case->getText();?>
				</td>
				<td>
					<?= Model_User::GetByUserID($this->case->getUserID())->data->name; ?>
				</td>
				<td>
					<?= $this->case->getCreatedDate(); ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>