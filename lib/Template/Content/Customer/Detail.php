<? /* @var $this Widget_Customer_Detail */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute('customer','edit',array($this->customer->getCustomerID())); ?>"><?= $this->_('Customer/Edit')?></a></li>
	</ul>
</div>
<div class="ctn">
	<h1><?= $this->customer->getName(); ?></h1>
	
	<div style="float:right;">
		<h3><?= $this->_('Customer/Notes')?></h3>
		<?= $this->form()->textarea('notes', 10, 5, $this->customer->getNotes())
			->addAttribute('style','width:300px;height:100px;')
			->addAttribute('ID','notes')?>
	</div>
	
	<p>
		<?= $this->customer->getAddress(); ?><br/>
		<?= $this->customer->getPostalNumber(); ?>, <?= $this->customer->getCity(); ?><br/>
		<?= $this->customer->getCountry(); ?><br/>
		Tlf: <?= $this->customer->getPhone(); ?><br/>
		Mobil: <?= $this->customer->getCellphone(); ?><br/><br/>
		CVR: <?= $this->customer->getVat(); ?><br/>
		Bank: <?= $this->customer->getBank(); ?><br/>
		EAN: <?= $this->customer->getEan(); ?><br/>
		Kontaktperson: <?= $this->customer->getContact(); ?><br/><br/>
	</p>
	
	<h3><?= $this->_('Customer/DeliveryAddress'); ?></h3>
	<p>
		<?= nl2br($this->customer->getDeliveryAddress());?>
	</p>
	
	<h2><?= $this->_('Customer/Case/Latest')?></h2>
	<? if(!$this->cases->hasRows()) : ?>
		<?= $this->_('Customer/Case/NoneOpen')?>
	<? else: ?>
		<table>
			<thead>
				<tr>
					<th>
						Navn
					</th>
					<th>
						Brugernavn
					</th>
					<th>
						Dato
					</th>
				</tr>
			</thead>
			<tbody>
			<? /* @var $case Model_Case */
			foreach($this->cases->getRows() as $case) : ?>
			<tr>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('customercase','view',array($this->customer->getCustomerID(), $case->getCaseID())); ?>"><?= $case->getSubject()->getTitle(); ?></a>
				</td>
				<td>
					<?= Model_User::GetByUserID($case->getUserID())->data->name; ?>
				</td>
				<td>
					<?= $case->getCreatedDate(); ?>
				</td>
			</tr>
			<? endforeach; ?>
			</tbody>
		</table>
	<? endif; ?>
</div>
<script type="text/javascript">
	var customer=new customerdetail();
	customer.construct({ customerId: '<?= $this->customer->getCustomerID(); ?>' });
</script>