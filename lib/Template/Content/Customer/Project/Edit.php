<? /* @var $this Widget_Customer_Project_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<table>
		<tr>
			<td style="width:50%;vertical-align:top;">
				<?= $this->form()->start('updateProject'); ?>
					<table>
						<tr>
							<td style="width:100px;">
								<?= $this->form()->label($this->_('Customer/Project/Name'), 'name');?>
							</td>
							<td>
								<?= $this->form()->input('name', 'text', $this->project->getName())->addAttribute('id','name'); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->form()->label($this->_('Customer/Project/Description'), 'description');?>
							</td>
							<td>
								<?= $this->form()->input('description', 'text', $this->project->getDescription())->addAttribute('id','description'); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->form()->label($this->_('Customer/Project/Active'), 'active');?>
							</td>
							<td>
								<?= $this->form()->bool('active', $this->project->getActive())->addAttribute('id','active'); ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<?= $this->form()->submit('submit', $this->_('Customer/Project/Update')); ?> 
								<?= $this->form()->input('delete', 'button', $this->_('Customer/Project/Delete'))->addAttribute('ID','deleteProject'); ?>
							</td>
						</tr>
					</table>
					<script type="text/javascript">
						$(document).ready(function() {
							$('#deleteProject').live('click',function() {
								if(confirm('<?= $this->_('Customer/Project/ConfirmDelete'); ?>')) {
									top.location.href='<?= \Pecee\Router::GetRoute(NULL, 'delete',array($this->project->getProjectID())) ?>';
								}
							});
						});
					</script>
				<?= $this->form()->end(); ?>
			</td>
			<td style="vertical-align:top;">
				<?= $this->snippet('Customer/Project/Right.php'); ?>
			</td>
		</tr>
	</table>
</div>