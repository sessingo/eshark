<? /* @var $this Widget_Customer_Project_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<table>
		<tr>
			<td style="width:50%;vertical-align:top;">
				<?= $this->form()->start('createProject'); ?>
				<?= $this->showErrors('createProject'); ?>
					<table>
						<tr>
							<td style="width:100px;">
								<?= $this->form()->label($this->_('Customer/Project/Name'), 'name');?>
							</td>
							<td>
								<?= $this->form()->input('name', 'text')->addAttribute('id','name'); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->form()->label($this->_('Customer/Project/Description'), 'description');?>
							</td>
							<td>
								<?= $this->form()->input('description', 'text')->addAttribute('id','description'); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->form()->label($this->_('Customer/Project/Active'), 'active');?>
							</td>
							<td>
								<?= $this->form()->bool('active',TRUE)->addAttribute('id','active'); ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<?= $this->form()->submit('submit', $this->_('Customer/Project/Save')); ?>
							</td>
						</tr>
					</table>
				<?= $this->form()->end(); ?>
			</td>
			<td style="vertical-align:top;">
				<?= $this->snippet('Customer/Project/Right.php'); ?>
			</td>
		</tr>
	</table>
</div>