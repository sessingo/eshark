<? /* @var $this Widget_Customer_Create */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<?= $this->form()->start('customer'); ?>
	<?= $this->showErrors('customer')?>
	<table>
		<tr>
			<th>
				<?= $this->_('Customer/Name')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('name', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Address')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->textarea('address', 5, 50); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/PostalCode')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('postalnumber', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/City')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('city', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<label for="country"><?= $this->_('Customer/Country'); ?></label>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->selectStart('country', new Dataset_Country($this->_('Customer/ChooseCountry')), $this->company->getCountry())->addAttribute('ID','country'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/ContactPerson')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('contact', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/DeliveryAddress')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->textarea('deliveryAddress', 5, 50); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Notes')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->textarea('notes', 5, 50); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Telephone')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('phone', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Cellphone')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('cellphone', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/VATNo')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('vat', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Website')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('website', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/Bank')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('bank', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th>
				<?= $this->_('Customer/EAN')?>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('ean', 'text'); ?>
			</td>
		</tr>
		<tr>
			<th valign="top">
				<label for="file"><?= $this->_('Customer/Logo'); ?></label>
			</th>
		</tr>
		<tr>
			<td>
				<?= $this->form()->input('logo', 'file')->addAttribute('ID','file'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?= $this->form()->submit('submit', $this->_('Customer/Save')); ?>
			</td>
		</tr>
	</table>
	<?= $this->form()->end(); ?>
</div>