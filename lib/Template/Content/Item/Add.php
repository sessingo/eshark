<? /* @var $this Widget_Item_Add */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>

<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->groupsMenu; ?>
		</div>
		<div class="col2">
			<?= $this->form()->start('addItem'); ?>
			<?= $this->showErrors('addItem'); ?>
			<table>
				<tr>
					<td style="width:160px;">
						<?= $this->_('Item/Category'); ?>
					</td>
					<td>
						<?= $this->form()->selectStart('itemGroupId', new Dataset_Item_Group($this->_('Item/SelectChoose')), $this->itemGroupId); ?>
					</td>
				</tr>
				<tr>
					<td style="width:160px;">
						<?= $this->_('Item/Unit'); ?>
					</td>
					<td>
						<?= $this->form()->selectStart('unit', new Dataset_Item_Unit($this->_('Item/SelectChoose'))); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?= $this->_('Item/ItemNumber'); ?>
					</td>
					<td>
						<?= $this->form()->input('number', 'text');?>
					</td>
				</tr>
				<tr>
					<td>
						<?= $this->_('Item/Name'); ?>
					</td>
					<td>
						<?= $this->form()->input('name', 'text'); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?= $this->_('Item/BuyPrice'); ?>
					</td>
					<td>
						<?= $this->form()->input('buyPrice', 'text'); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?= $this->_('Item/SellPrice'); ?>
					</td>
					<td>
						<?= $this->form()->input('salePrice', 'text'); ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<?= $this->form()->submit('submit', $this->_('Item/Save'))?>
					</td>
				</tr>
			</table>
			<?= $this->form()->end(); ?>
		</div>
		<div class="clear"></div>
	</div>
</div>