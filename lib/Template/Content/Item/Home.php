<? /* @var $this Widget_Item_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'add', array($this->itemGroupId)); ?>"><?= $this->_('Item/AddItem'); ?></a></li>
	</ul>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->groupsMenu; ?>
		</div>
		<div class="col2">
			<? if($this->items->hasRows()) : ?>
			<table>
				<thead>
					<tr>
						<th></th>
						<th>
							<?= $this->_('Item/ItemNo')?>
						</th>
						<th>
							<?= $this->_('Item/Name'); ?>
						</th>
						<th>
							<?= $this->_('Item/Unit')?>
						</th>
						<th>
							<?= $this->_('Item/BuyPrice')?>
						</th>
						<th>
							<?= $this->_('Item/SellPrice')?>
						</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<? /* @var $item Model_Item */
					   foreach($this->items->getRows() as $item) : ?>
					<tr>
						<td>
							<img src="/img/ico/16x16/item.png" alt="" />
						</td>
						<td>
							<?= $item->getNumber(); ?>
						</td>
						<td>
							<?= $item->getName();?>
						</td>
						<td>
							<?= Helper::ShowUnit($item->getUnit()); ?>
						</td>
						<td>
							<?= Helper::FormatMoney($item->getBuyPrice()); ?>
						</td>
						<td>
							<?= Helper::FormatMoney($item->getSalePrice()); ?>
						</td>
						<td style="text-align:right;">
							<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit', array($item->getItemID()));?>"><?= $this->_('Item/Edit'); ?></a> - 
							<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($item->getItemID()));?>" onclick="return confirm('<?= $this->_('Item/ConfirmDelete');?>');"><?= $this->_('Item/Delete'); ?></a>
						</td>
					</tr>
					<? endforeach; ?>
				</tbody>
			</table>
			<? endif; ?>
		</div>
		<div class="clear"></div>
	</div>
</div>