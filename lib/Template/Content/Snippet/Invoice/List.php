<? /* @var $this Widget_Snippet_Invoice_List */ ?>
<? if(!$this->invoices->hasRows()) : ?>
<div style="text-align:center;">
	<?= $this->_('Invoice/NoInvoices')?>
</div>
<? else: ?>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>
					<?= $this->_('Invoice/InvoiceNo')?>
				</th>
				<? if($this->showCustomerInfo) : ?>
				<th>
					<?= $this->_('Invoice/Customer')?>
				</th>
				<? endif; ?>
				<th>
					<?= $this->_('Invoice/Description')?>
				</th>
				<th>
					<?= $this->_('Invoice/InvoiceDate')?>
				</th>
				<th>
					<?= $this->_('Invoice/PaymentDate')?>
				</th>
				<th style="text-align:right;">
					<?= $this->_('Invoice/Vat')?> (<?= $this->company->getVatPercentage(); ?>%)
				</th>
				<th style="text-align:right;">
					<?= $this->_('Invoice/AmountWithVat')?>
				</th>
				<th>
					
				</th>
			</tr>
		</thead>
		<tbody>
			<? /* @var $invoice Model_Invoice */
			   foreach($this->invoices->getRows() as $invoice) : ?>
			<? if($invoice->getType() == Model_Invoice::TYPE_PAYMENT) : ?>
			<tr class="payment">
				<td>
					<img src="<?= $invoice->getIcon(); ?>" alt="" />
				</td>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('invoice','view',array($invoice->getInvoiceID()));?>" rel="new"><?= $invoice->getInvoiceID(); ?></a>
				</td>
				<? if($this->showCustomerInfo) : ?>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('customer','details',array($invoice->getCustomerID())); ?>"><?= $invoice->getCustomer()->getName(); ?></a>
				</td>
				<? endif; ?>
				<td>
					<?= $this->_('Invoice/PaymentInvoice')?> 
					<a href="<?= \Pecee\Router::GetRoute('invoice','view',array($invoice->getPayedInvoiceID()));?>" rel="new"><?= $invoice->getPayedInvoiceID(); ?></a> - 
					<?= $invoice->getDescription(); ?>
				</td>
				<td>
					<?= Helper::FormatDate($invoice->getPaymentDate()); ?>
				</td>
				<td></td>
				<td></td>
				<td style="text-align:right;">
					<?= Helper::FormatMoney($invoice->getTotal($this->company)); ?>
				</td>
				<td style="text-align:right;">
					<? if(!$invoice->getRecorded()) : ?>
					<a href="<?= \Pecee\Router::GetRoute(NULL,'recorded',array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Invoice/ConfirmRecord');?>');"><?= $this->_('Invoice/Record')?></a> - 
					<? endif; ?> 
					<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Invoice/ConfirmDelete');?>');"><?= $this->_('Invoice/Delete'); ?></a> - 
					<a href="<?= \Pecee\Router::GetRoute('ajax','dialog', array('invoice', 'payment', 'edit'), array('invoiceId' => $invoice->getInvoiceID())); ?>" rel="dialog"><?= $this->_('Invoice/Edit')?></a>
				</td>
			</tr>
			<? elseif($invoice->getType() == Model_Invoice::TYPE_CREDITMEMO) : ?>
			<tr class="payment">
				<td>
					<img src="<?= $invoice->getIcon(); ?>" alt="" />
				</td>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('invoice','view',array($invoice->getInvoiceID()));?>" rel="new"><?= $invoice->getInvoiceID(); ?></a>
				</td>
				<? if($this->showCustomerInfo) : ?>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('customer','details',array($invoice->getCustomerID())); ?>"><?= $invoice->getCustomer()->getName(); ?></a>
				</td>
				<? endif; ?>
				<td>
					<?= $this->_('Invoice/CreditedInvoice')?> 
					<a href="<?= \Pecee\Router::GetRoute('invoice','view',array($invoice->getCreditedInvoiceID()));?>" rel="new"><?= $invoice->getCreditedInvoiceID(); ?></a> - 
					<?= $invoice->getDescription(); ?>
				</td>
				<td>
					<?= Helper::FormatDate($invoice->getInvoiceDate()); ?>
				</td>
				<td></td>
				<td></td>
				<td style="text-align:right;">
					<?= Helper::FormatMoney($invoice->getTotal($this->company)); ?>
				</td>
				<td style="text-align:right;">
					<? if(!$invoice->getRecorded()) : ?>
					<a href="<?= \Pecee\Router::GetRoute(NULL,'recorded',array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Invoice/ConfirmRecord');?>');"><?= $this->_('Invoice/Record')?></a> - 
					<? endif; ?> 
					<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Invoice/ConfirmDelete');?>');"><?= $this->_('Invoice/Delete'); ?></a> - 
					<a href="<?= \Pecee\Router::GetRoute(NULL,'editCreditMemo', array($invoice->getInvoiceID()), array('customerId' => $invoice->getCustomerID(), 'redirectUrl' => \Pecee\Router::GetCurrentRoute())); ?>"><?= $this->_('Invoice/Edit')?></a>
				</td>
			</tr>
			<? else: ?>
			<tr>
				<td>
					<img src="<?= $invoice->getIcon(); ?>" alt="" />
				</td>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('invoice','view',array($invoice->getInvoiceID()));?>" rel="new"><?= $invoice->getInvoiceID(); ?></a>
				</td>
				<? if($this->showCustomerInfo) : ?>
				<td>
					<a href="<?= \Pecee\Router::GetRoute('customer','details',array($invoice->getCustomerID())); ?>"><?= $invoice->getCustomer()->getName(); ?></a>
				</td>
				<? endif; ?>
				<td>
					<?= ($invoice->getDescription()) ? $invoice->getDescription() : $this->_('Invoice/NoDescription'); ?>
				</td>
				<td>
					<?= Helper::FormatDate($invoice->getInvoiceDate()); ?>
				</td>
				<td>
					<?= Helper::FormatDate($invoice->getDueDate()); ?>
				</td>
				<td style="text-align:right;">
					<?= Helper::FormatMoney($invoice->getVatTotal($this->company)); ?>
				</td>
				<td style="text-align:right;">
					<?= Helper::FormatMoney($invoice->getTotal($this->company)); ?>
				</td>
				<td style="text-align:right;">
					<? if(!$invoice->getRecorded()) : ?>
					<a href="<?= \Pecee\Router::GetRoute(NULL,'recorded',array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Invoice/ConfirmRecord');?>');"><?= $this->_('Invoice/Record')?></a> - 
					<? endif; ?> 
					<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($invoice->getInvoiceID()), array('redirectUrl' => \Pecee\Router::GetCurrentRoute()));?>" onclick="return confirm('<?= $this->_('Invoice/ConfirmDelete');?>');"><?= $this->_('Invoice/Delete'); ?></a> - 
					<a href="<?= \Pecee\Router::GetRoute(NULL,'edit', array($invoice->getInvoiceID()), array('customerId' => $invoice->getCustomerID(), 'redirectUrl' => \Pecee\Router::GetCurrentRoute())); ?>"><?= $this->_('Invoice/Edit')?></a>
				</td>
			</tr>
			<? endif; ?>
			<? endforeach; ?>
		</tbody>
		<? if($this->showTotal) : ?>
		<tfoot>
			<tr style="font-weight:bold;text-transform:uppercase;">
				<td colspan="<?= ($this->showCustomerInfo) ? '6' : '5'; ?>" style="text-align:right;">
					<?= $this->_('Invoice/TotalWithVat', $this->company->getCurrency())?>
				</td>
				<td style="text-align:right;">
					<?= Helper::FormatMoney($this->totalVat); ?>
				</td>
				<td style="text-align:right;">
					<?= Helper::FormatMoney($this->total); ?>
				</td>
				<td></td>
			</tr>
		</tfoot>
		<? endif; ?>
	</table>
<? endif; ?>