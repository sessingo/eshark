<? /* @var $this Widget_Admin_Account_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute('ajax', 'dialog', array('admin','account','add')); ?>" rel="dialog">Tilføj</a></li>
	</ul>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<? if(!$this->accounts->hasRows()) : ?>
				<div>
					Ingen kontoer tilføjet
				</div>
			<? else: ?>
				<table>
					<thead>
						<tr>
							<th>
								Navn
							</th>
							<th>
								Bank
							</th>
							<th>
								Reg no
							</th>
							<th>
								Konto no
							</th>
							<th>
								IBAN
							</th>
							<th>
								Swift
							</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<? /* @var $account Model_Account */
						foreach($this->accounts->getRows() as $account) : ?>
						<tr>
							<td>
								<?= $account->getName(); ?>
							</td>
							<td>
								<?= $account->getBank(); ?>
							</td>
							<td>
								<?= $account->getRegistrationNumber(); ?>
							</td>
							<td>
								<?= $account->getAccountNumber(); ?>
							</td>
							<td>
								<?= $account->getIBAN(); ?>
							</td>
							<td>
								<?= $account->getSwift(); ?>
							</td>
							<td style="text-align:right;">
								<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($account->getAccountID()));?>" onclick="return confirm('Er du sikker på, at du vil slette denne konto?');">Slet</a> - 
								<a href="<?= \Pecee\Router::GetRoute('ajax', 'dialog',array('admin','account','edit'), array('accountId' => $account->getAccountID())); ?>" rel="dialog">Rediger</a>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif; ?>
		</div>
		<div class="clear"></div>
	</div>
</div>