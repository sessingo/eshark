<? /* @var $this Widget_Admin_Case_Subject_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<table>
				<tr>
					<td style="width:70%;vertical-align:top;">
						<?= $this->form()->start('createSubject'); ?>
						<?= $this->showErrors('createSubject'); ?>
						<table>
							<tr>
								<td style="width:120px;">
									<?= $this->_('Admin/Case/Subject')?>
								</td>
								<td>
									<?= $this->form()->input('subject', 'text'); ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<?= $this->form()->submit('submit', $this->_('Admin/Case/Save')); ?>
								</td>
							</tr>
						</table>
						<?= $this->form()->end(); ?>
					</td>
					<td>
						<? if($this->subjects->hasRows()) :  ?>
						<ul>
							<? /* @var $subject Model_Case_Subject */
							   foreach($this->subjects->getRows() as $subject) : ?>
							<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'edit',array($subject->getCaseSubjectID())); ?>"><?= $subject->getTitle(); ?></a></li>
							<? endforeach;?>
						</ul>
						<? endif; ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
	</div>
</div>