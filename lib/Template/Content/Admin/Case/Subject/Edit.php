<? /* @var $this Widget_Admin_Case_Subject_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<table>
				<tr>
					<td style="width:70%;vertical-align:top;">
						<?= $this->form()->start('editSubject'); ?>
						<?= $this->showErrors('editSubject')?>
						<table>
							<tr>
								<td style="width:120px;">
									<?= $this->_('Admin/Case/Subject')?>
								</td>
								<td>
									<?= $this->form()->input('subject', 'text', $this->subject->getTitle()); ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<?= $this->form()->submit('submit', $this->_('Admin/Case/Update')); ?> 
									<?= $this->form()->input('delete', 'button', $this->_('Admin/Case/Delete'))->addAttribute('ID','deleteSubject'); ?> 
								</td>
							</tr>
						</table>
						<?= $this->form()->end(); ?>
						
						<script type="text/javascript">
							$(document).ready(function() {
								$('#deleteSubject').live('click',function() {
									if(confirm('<?= $this->_('Admin/Case/ConfirmDelete'); ?>')) {
										top.location.href='<?= \Pecee\Router::GetRoute(NULL, 'delete',array($this->subject->getCaseSubjectID()));?>';
									}
								});
							});
						</script>
					</td>
					<td>
						<? if($this->subjects->hasRows()) :  ?>
						<ul>
							<? /* @var $subject Model_Case_Subject */
							   foreach($this->subjects->getRows() as $subject) : ?>
							<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'edit',array($subject->getCaseSubjectID())); ?>"><?= $subject->getTitle(); ?></a></li>
							<? endforeach;?>
						</ul>
						<? endif; ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
	</div>
</div>