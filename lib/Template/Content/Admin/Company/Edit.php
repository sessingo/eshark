<? /* @var $this Widget_Admin_Company_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<?= $this->form()->start('editCompany', 'post', NULL, \Pecee\UI\Form\Form::FORM_ENCTYPE_FORM_DATA); ?>
			<?= $this->showErrors('editCompany'); ?>
			
			<fieldset>
				<legend><?= $this->_('Admin/Company/Generel')?></legend>
				<table>
					<tr>
						<th>
							<label for="name"><?= $this->_('Admin/Company/Name'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('name', 'text', $this->company->getName())->addAttribute('ID','name'); ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="vatPercentage"><?= $this->_('Admin/Company/VATPercentage'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('vatpercentage', 'text', $this->company->getVatPercentage())->addAttribute('ID','vatPercentage'); ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="vatno"><?= $this->_('Admin/Company/WorkHours'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('workhours', 'text', $this->company->getWorkhours())->addAttribute('ID','workhours'); ?>
						</td>
					</tr>
					<tr>
						<th valign="top">
							<label for="file"><?= $this->_('Admin/Company/Logo'); ?></label>
						</th>
						<td>
							<? if($this->company->getLogo()) : ?>
								<div style="margin-bottom:15px;">
									<img src="<?= $this->company->getLogo()->getInternalUrl(); ?>" alt="" />
								</div>
							<? endif; ?>
							<?= $this->form()->input('logo', 'file')->addAttribute('ID','file')->addAttribute('style','float:none;'); ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?= $this->form()->submit('submit', $this->_('Admin/Company/Update')); ?>
						</td>
					</tr>
				</table>
			</fieldset>
			
			<fieldset class="margin-t">
				<legend><?= $this->_('Admin/Company/ContactInfo')?></legend>
				
				<table>
					<tr>
						<th>
							<?= $this->_('Admin/Company/Address')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->textarea('address', 5, 50, $this->company->getAddress()); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/PostalCode')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('postalnumber', 'text', $this->company->getPostalNumber()); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/City')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('city', 'text', $this->company->getCity()); ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="country"><?= $this->_('Admin/Company/Country'); ?></label>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->selectStart('country', new Dataset_Country($this->_('Admin/Company/CountrySelect')), $this->company->getCountry())->addAttribute('ID','country'); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/Telephone')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('phone', 'text', $this->company->getPhone()); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/Cellphone')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('cellphone', 'text', $this->company->getCellphone()); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/VATNo')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('vatno', 'text', $this->company->getVatno())->addAttribute('ID','vatno'); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/Website')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('website', 'text', $this->company->getWebsite()); ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $this->_('Admin/Company/EAN')?>
						</th>
					</tr>
					<tr>
						<td>
							<?= $this->form()->input('ean', 'text', $this->company->getEan()); ?>
						</td>
					</tr>
					<tr>
						<td>
							<?= $this->form()->submit('submit', $this->_('Admin/Company/Update')); ?>
						</td>
					</tr>
				</table> 
			</fieldset>
			
			<fieldset class="margin-t">
				<legend><?= $this->_('Admin/Company/Invoice')?></legend>
				<table>
					<tr>
						<th>
							<label for="paymentDays"><?= $this->_('Admin/Company/DaysUntilPayment')?></label>
						</th>
						<td>
							<?= $this->form()->input('paymentDays', 'text', $this->company->getPaymentDays())->addAttribute('ID','paymentDays'); ?> 
							<?= $this->_('Admin/Company/DaysUntilPaymentDescription')?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="reminderDays"><?= $this->_('Admin/Company/Notification'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('reminderDays', 'text', $this->company->getReminderDays())->addAttribute('ID','reminderDays'); ?> 
							<?= $this->_('Admin/Company/NotificationDescription')?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="firstReminderDays"><?= $this->_('Admin/Company/FirstReminder'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('firstReminderDays', 'text', $this->company->getFirstReminderDays())->addAttribute('ID','firstReminderDays'); ?> 
							<?= $this->_('Admin/Company/FirstReminderDescription')?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="secondReminderDays"><?= $this->_('Admin/Company/SecondReminder'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('secondReminderDays', 'text', $this->company->getSecondReminderDays())->addAttribute('ID','secondReminderDays'); ?> 
							<?= $this->_('Admin/Company/SecondReminderDescription')?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="debtCollectionDays"><?= $this->_('Admin/Company/DebtCollection'); ?></label>
						</th>
						<td>
							<?= $this->form()->input('debtCollectionDays', 'text', $this->company->getDebtCollectionDays())->addAttribute('ID','debtCollectionDays'); ?> 
							<?= $this->_('Admin/Company/DebtCollectionDescription')?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?= $this->form()->submit('submit', $this->_('Admin/Company/Update')); ?>
						</td>
					</tr>
				</table>
			</fieldset>
			<?= $this->form()->end(); ?>
		</div>
		<div class="clear"></div>
	</div>
</div>