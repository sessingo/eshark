<? /* @var $this Widget_Admin_User_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
	<ul class="options">
		<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'create'); ?>"><?= $this->_('Admin/User/Create')?></a></li>
	</ul>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<? if(!$this->users->hasRows()) : ?>
				<div>
					<?= $this->_('Admin/User/NoUsersFound')?>
				</div>
			<? else: ?>
				<table>
					<thead>
						<tr>
							<th>
								<?= $this->_('Admin/User/Name'); ?>
							</th>
							<th>
								<?= $this->_('Admin/User/Username')?>
							</th>
							<th>
								<?= $this->_('Admin/User/Type')?>
							</th>
							<th>
								<?= $this->_('Admin/User/LastActive')?>
							</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<? /* @var $user Model_User */
						foreach($this->users->getRows() as $user) : ?>
						<tr>
							<td>
								<?= $user->getUsername();?>
							</td>
							<td>
								<?= $user->getName(); ?>
							</td>
							<td>
								<?= Helper::ShowType($user->getType()); ?>
							</td>
							<td>
								<?= $user->getLastActivity(); ?>
							</td>
							<td style="text-align:right;">
								<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit', array($user->getUserID())); ?>"><?= $this->_('Admin/User/Edit')?></a> - 
								<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($user->getUserID())); ?>" onclick="return confirm('<?= $this->_('Admin/User/ConfirmDelete'); ?>');"><?= $this->_('Admin/User/Delete')?></a>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif; ?>
		</div>
		<div class="clear"></div>
	</div>
</div>