<? /* @var $this Widget_Admin_User_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<?= $this->form()->start('editUser'); ?>
				<?= $this->showErrors('editUser'); ?>
				<div class="input">
					<span class="label">
						<label for="type"><?= $this->_('Admin/User/Type')?></label>
					</span>
					<?= $this->form()->selectStart('type', new Dataset_User_Type(), $this->user->getType()); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="username"><?= $this->_('Admin/User/Username')?></label>
					</span>
					<?= $this->form()->input('username','text', $this->user->getUsername()); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="password"><?= $this->_('Admin/User/Password')?></label>
					</span>
					<?= $this->form()->input('password','password'); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="name"><?= $this->_('Admin/User/Name')?></label>
					</span>
					<?= $this->form()->input('name','text', $this->user->getName()); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="email"><?= $this->_('Admin/User/Email')?></label>
					</span>
					<?= $this->form()->input('email','text', $this->user->getEmail()); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="jobtitle"><?= $this->_('Admin/User/JobTitle')?></label>
					</span>
					<?= $this->form()->input('jobtitle','text', $this->user->getJobtitle()); ?>
				</div>
				<div class="input btn">
					<?= $this->form()->submit('submit', $this->_('Admin/User/Edit'))?>
				</div>
			<?= $this->form()->end(); ?>
		</div>
		<div class="clear"></div>
	</div>
</div>