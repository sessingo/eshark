<? /* @var $this Widget_Admin_Item_Group_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<table>
				<tr>
					<td style="width:70%;vertical-align:top;">
						<?= $this->form()->start('editGroup'); ?>
						<?= $this->showErrors('editGroup')?>
						<table>
							<tr>
								<td style="width:120px;">
									<?= $this->_('Admin/Item/Name')?>
								</td>
								<td>
									<?= $this->form()->input('name', 'text', $this->group->getName()); ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<?= $this->form()->submit('submit', $this->_('Admin/Item/Update')); ?> 
									<?= $this->form()->input('delete', 'button', $this->_('Admin/Item/Delete'))->addAttribute('ID','deleteGroup'); ?> 
								</td>
							</tr>
						</table>
						<?= $this->form()->end(); ?>
						
						<script type="text/javascript">
							$(document).ready(function() {
								$('#deleteGroup').live('click',function() {
									if(confirm('<?= $this->_('Admin/Item/ConfirmDelete'); ?>')) {
										top.location.href='<?= \Pecee\Router::GetRoute(NULL, 'delete',array($this->group->getItemGroupID()));?>';
									}
								});
							});
						</script>
					</td>
					<td>
						<? if($this->groups->hasRows()) :  ?>
						<ul>
							<? /* @var $subject Model_Item_Group */
							   foreach($this->groups->getRows() as $group) : ?>
							<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'edit',array($group->getItemGroupID())); ?>"><?= $group->getName(); ?></a></li>
							<? endforeach;?>
						</ul>
						<? endif; ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
	</div>
</div>