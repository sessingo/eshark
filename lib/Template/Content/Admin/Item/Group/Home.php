<? /* @var $this Widget_Admin_Item_Group_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<table>
				<tr>
					<td style="width:70%;vertical-align:top;">
						<?= $this->form()->start('createGroup'); ?>
						<?= $this->showErrors('createGroup'); ?>
						<table>
							<tr>
								<td style="width:120px;">
									<?= $this->_('Admin/Item/Name')?>
								</td>
								<td>
									<?= $this->form()->input('name', 'text'); ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<?= $this->form()->submit('submit', $this->_('Admin/Item/Save')); ?>
								</td>
							</tr>
						</table>
						<?= $this->form()->end(); ?>
					</td>
					<td>
						<? if($this->groups->hasRows()) :  ?>
						<ul>
							<? /* @var $subject Model_Item_Group */
							   foreach($this->groups->getRows() as $group) : ?>
							<li><a href="<?= \Pecee\Router::GetRoute(NULL, 'edit',array($group->getItemGroupID())); ?>"><?= $group->getName(); ?></a></li>
							<? endforeach;?>
						</ul>
						<? endif; ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
	</div>
</div>