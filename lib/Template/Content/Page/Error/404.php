<? /* @var $this Widget_Page_Error_404 */ ?>

<h1><?= $this->_('Page/ErrorPage404/Title')?></h1>
<p>
	<?= $this->_('Page/ErrorPage404/Description', $_SERVER['REQUEST_URI']);?>
</p>