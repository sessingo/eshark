<? /* @var $this Widget_Activity_Home */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<table>
		<tr>
			<td style="width:50%;vertical-align:top;padding:0px;">
				<?= $this->form()->start('createLog'); ?>
				<?= $this->showErrors(); ?>
				<div class="input first">
					<span class="label">
						<label for="customerId"><?= $this->_('Activity/Customer')?></label>
					</span>
					<?= $this->form()->selectStart('customerId', new Dataset_Customer($this->_('Activity/Choose')), $this->customerId)->addAttribute('onchange', '$(\'#projectId\').val(\'\');$p.doPostback(this);')->addAttribute('id', 'customerId'); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="projectId"><?= $this->_('Activity/Project')?></label>
					</span>
					<?= $this->form()->selectStart('projectId', new Dataset_Customer_Project($this->customerId, $this->_('Activity/Choose')), $this->projectId)->addAttribute('onchange', '$p.doPostback(this);')->addAttribute('id', 'projectId'); ?>
				</div>
				<div class="input">
					<span class="label">
						<label for="activityId"><?= $this->_('Activity/Activity')?></label>
					</span>
					<?= $this->form()->selectStart('activityId', new Dataset_Customer_Project_Activity($this->projectId, $this->_('Activity/Choose')), $this->activityId)->addAttribute('onchange', '$p.doPostback(this);')->addAttribute('id', 'activityId') ?>
				</div>
				<div class="input">
					<span class="label">
						<?= $this->_('Activity/Hours')?>
					</span> 
					<?= $this->form()->input('hours', 'text')->addAttribute('maxlength', '3')->addAttribute('style','width:50px;')->addAttribute('placeholder', $this->_('Activity/Hours')); ?> 
					<?= $this->form()->input('minutes', 'text')->addAttribute('maxlength', '2')->addAttribute('style','width:50px;')->addAttribute('placeholder', $this->_('Activity/Minutes')); ?>
				</div>
				
				<? if($this->activity && $this->activity->hasRow() && $this->activity->getMaxHours()) : ?>
				<div>
					<?= $this->_('Activity/ActivityInfo', $this->activity->getMaxHours(), 0); ?>
				</div>
				<? endif; ?>
				
				<div style="clear:both;margin-top:20px;">
					<div style="margin-bottom:10px;">
						<label for="description"><?= $this->_('Activity/Description')?></label>
					</div>
					<?= $this->form()->textarea('description', 6, 50)->addAttribute('ID','description')->addAttribute('style','width: 390px;height: 160px;'); ?>
				</div>
				
				<div style="padding-top:15px;margin-bottom:10px;">
					<?= $this->form()->submit('submit', $this->_('Activity/LogHours'))?>
				</div>
				<table>
					<tr>
						<td>
							<?= $this->_('Activity/PreviouslyActivity')?>
						</td>
						<td>
							<? if($this->latestActivities->getRows()) : ?>
								<ul class="js-previousLog">
									<? /* @var $log Model_Project_Activity_Log */
									foreach($this->latestActivities->getRows() as $log) : ?>
									<li>
										<a href="#" data-val="<?= sprintf('[%s,%s,%s]', $log->getActivity()->getProject()->getCustomer()->getCustomerID(), $log->getActivity()->getProject()->getProjectID(), $log->getActivity()->getProjectActivityID()) ?>"><?= $log->getActivity()->getProject()->getCustomer()->getName(); ?> - 
										<?= $log->getActivity()->getProject()->getName(); ?> - 
										<?= $log->getActivity()->getName(); ?></a>
									</li>
									<? endforeach; ?>
								</ul>
							<? endif; ?>
						</td>
					</tr>
				</table>
				<?= $this->form()->end(); ?>
			</td>
			<td style="vertical-align:top;padding:0px;">			
				<div id="datepicker"></div>
				<? if($this->activityLog->hasRows()) : ?>
					<h3><?= $this->_('Activity/ActivityByDate', $this->date)?></h3>
					<table style="margin-top:15px;">
						<thead>
							<tr>
								<th>
									<?= $this->_('Activity/Customer')?>
								</th>
								<th>
									<?= $this->_('Activity/Project')?>
								</th>
								<th>
									<?= $this->_('Activity/Activity')?>
								</th>
								<th>
									<?= $this->_('Activity/HoursMinutesShort')?>
								</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<? /* @var $this Model_Project_Activity_Log */
						foreach($this->activityLog->getRows() as $activity) : ?>
						<tr>
							<td>
								<a href="<?= \Pecee\Router::GetRoute('customer','details',array($activity->getActivity()->getProject()->getCustomer()->getCustomerID()));?>"><?= $activity->getActivity()->getProject()->getCustomer()->getName(); ?></a>
							</td>
							<td>
								<?= $activity->getActivity()->getProject()->getName(); ?>
							</td>
							<td>
								<?= $activity->getActivity()->getName(); ?>
							</td>
							<td>
								<?= Helper::MinutesToHour($activity->getMinutes()); ?>
							</td>
							<td style="text-align:right;">
								<a href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($activity->getProjectActivityLogID())); ?>" onclick="return confirm('<?= $this->_('Activity/ConfirmDelete');?>');"><?= $this->_('Activity/Delete')?></a> &nbsp;
								<a href="<?= \Pecee\Router::GetRoute('ajax', 'dialog', array('activity', 'edit'), array('activityLogId' => $activity->getProjectActivityLogID()));?>" rel="dialog"><?= $this->_('Activity/Edit')?></a>
							</td>
						</tr>
						<? endforeach;?>
						</tbody>
					</table>
				<? endif; ?>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	$(document).ready(function() {	
		var a=new activitylog();
		a.construct({
			'SelectedDate': '<?= $this->date; ?>',
			'WorkHours': '<?= $this->company->getWorkhours(); ?>'
		});
	});
	</script>
</div>