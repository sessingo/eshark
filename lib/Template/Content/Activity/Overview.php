<? /* @var $this Widget_Activity_Overview */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<?= $this->form()->start('filter','get');?>
	<fieldset>
		<legend><?= $this->_('Activity/Period')?></legend>
		<?= $this->form()->input('from', 'text', $this->fromDate)->addAttribute('placeholder', $this->_('Activity/From'))->addAttribute('ID','fromDatepicker'); ?> 
		<?= $this->form()->input('to', 'text', $this->toDate)->addAttribute('placeholder', $this->_('Activity/To'))->addAttribute('ID','toDatepicker')?> 
		<?= $this->form()->submit('submit', $this->_('Activity/Update')); ?>
	</fieldset>
		
	<fieldset>
		<legend><?= $this->_('Activity/Customer');?></legend>
		<?= $this->form()->selectStart('customerId', new Dataset_Customer($this->_('Activity/ChooseCustomer')), $this->customerId)->addAttribute('ID', 'customerId'); ?> 
		
		<? if($this->customerId) : ?>
			<?= $this->form()->selectStart('projectId', new Dataset_Customer_Project($this->customerId, $this->_('Activity/ChooseProject')), $this->projectId)->addAttribute('ID', 'projectId'); ?> 
		<? else: ?>
			<select name="projectId" id="projectId"><option value=""><?= $this->_('Activity/ChooseProject')?></option></select> 
		<? endif; ?>
		
		<? if($this->projectId) : ?>
			<?= $this->form()->selectStart('activityId', new Dataset_Customer_Project_Activity($this->projectId, $this->_('Activity/ChooseActivity')), $this->activityId)->addAttribute('ID', 'activityId'); ?> 
		<? else: ?>
			<select name="activityId" id="activityId"><option value=""><?= $this->_('Activity/ChooseActivity')?></option></select> 
		<? endif; ?>
		<?= $this->form()->submit('submit', $this->_('Activity/Update')); ?>
	</fieldset>
	<?= $this->form()->end(); ?>
	
	<? if($this->hasParam('submit') && $this->activities->hasRows()) : ?>
		<table style="margin-top:20px;">
			<thead>
				<tr>
					<th>
						<?= $this->_('Activity/Customer')?>
					</th>
					<th>
						<?= $this->_('Activity/Project')?>
					</th>
					<th>
						<?= $this->_('Activity/Activity')?>
					</th>
					<th>
						<?= $this->_('Activity/Description')?>
					</th>
					<th>
						<?= $this->_('Activity/User')?>
					</th>
					<th>
						<?= $this->_('Activity/Date')?>
					</th>
					<th style="text-align:right;">
						<?= $this->_('Activity/Hours')?>
					</th>
					<th style="text-align:right;">
						<?= $this->_('Activity/Price')?>
					</th>
				</tr>
			</thead>
			<? /* @var $activity Model_Project_Activity_Log */
			foreach($this->activities->getRows() as $activity):?>
			<tbody>
				<tr>
					<td>
						<?= $activity->getActivity()->getProject()->getCustomer()->getName(); ?>
					</td>
					<td>
						<?= $activity->getActivity()->getProject()->getName(); ?>
					</td>
					<td>
						<?= $activity->getActivity()->getName(); ?>
					</td>
					<td>
						<?= nl2br($activity->getDescription()); ?>
					</td>
					<td>
						<?= $activity->getUser()->data->name; ?>
					</td>
					<td>
						<?= $activity->getDate(); ?>
					</td>
					<td style="text-align:right;">
						<?= Helper::MinutesToHour($activity->getMinutes()); ?>
					</td>
					<td style="text-align:right;">
						<?= Helper::FormatMoney((($activity->getMinutes()/60)*$activity->getActivity()->getPrice())); ?>
					</td>
				</tr>
			</tbody>
			<? endforeach; ?>
			<tfoot style="border-top:1px solid #CCC;font-weight:bold;">
				<tr>
					<td colspan="6" style="text-align:right;text-transform:uppercase;">
						<?= $this->_('Activity/TotalWithVat', $this->company->getCurrency())?>
					</td>
					<td style="text-align:right;">
						<?= Helper::MinutesToHour($this->totalMinutes); ?>
					</td>
					<td style="text-align:right;">
						<?= Helper::FormatMoney($this->totalPrice); ?>
					</td>
				</tr>
			</tfoot>
		</table>
	<? endif; ?>
</div>
<script type="text/javascript">
$(document).ready(function() {	
	$('#fromDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#toDatepicker').datepicker('option', 'minDate', selectedDate);
		}
	});

	$('#toDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#fromDatepicker').datepicker('option', 'maxDate', selectedDate);
		}
	});

	$('#customerId').live('change',function() {
		$('#projectId option[value!=""], #activityId option[value!=""]').remove();
		var p=$('#projectId');
		$.getJSON('<?= \Pecee\Router::GetRoute('project', 'get.json', NULL, array('customerId' => ''))?>' + $(this).val(), function(r) {
			$.each(r.rows, function(item) {
				p.append('<option value="'+this.ProjectID+'">'+this.Name+'</option>');
			});
		});
	});

	$('#projectId').live('change',function() {
		$('#activityId option[value!=""]').remove();
		var p=$('#activityId');
		$.getJSON('<?= \Pecee\Router::GetRoute('activity', 'get.json', NULL, array('projectId' => ''))?>' + $(this).val(), function(r) {
			$.each(r.rows, function(item) {
				p.append('<option value="'+this.ProjectActivityID+'">'+this.Name+'</option>');
			});
		});
	});
});
</script>