<? /* @var $this Widget_Invoice_Overdue */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>

<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<h3><?= $this->_('Invoice/PaymentOverdue')?></h3>
			<?= $this->invoices; ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
 
<script type="text/javascript">
$(document).ready(function() {	
	$('#fromDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#toDatepicker').datepicker('option', 'minDate', selectedDate);
		}
	});

	$('#toDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#fromDatepicker').datepicker('option', 'maxDate', selectedDate);
		}
	});
});
</script>