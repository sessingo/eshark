<? /* @var $this Widget_Invoice_Search */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>

<div class="ctn">
	<fieldset>
		<legend><?= $this->_('Invoice/SearchForInvoice')?></legend>
		<?= $this->form()->start('search','get'); ?>
			<div class="input">
				<span class="label">
					<label for="invoiceId"><?= $this->_('Invoice/InvoiceNumber')?></label>
				</span>
				<?= $this->form()->input('invoiceId', 'text', $this->getParam('invoiceId'))->addAttribute('ID','invoiceId'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="customerId"><?= $this->_('Invoice/CustomerNumber')?></label>
				</span>
				<?= $this->form()->input('customerId', 'text', $this->getParam('customerId'))->addAttribute('ID','customerId'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="invoiceDate"><?= $this->_('Invoice/InvoiceDate')?></label>
				</span>
				<?= $this->form()->input('invoiceDate', 'text', $this->getParam('invoiceDate'))->addAttribute('ID','invoiceDate'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="dueDate"><?= $this->_('Invoice/PaymentDate')?></label>
				</span>
				<?= $this->form()->input('dueDate', 'text', $this->getParam('dueDate'))->addAttribute('ID','dueDate'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="payedDate"><?= $this->_('Invoice/PayedDate')?></label>
				</span>
				<?= $this->form()->input('payedDate', 'text', $this->getParam('payedDate'))->addAttribute('ID','payedDate'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="reminderDate"><?= $this->_('Invoice/ReminderDate')?></label>
				</span>
				<?= $this->form()->input('reminderDate', 'text', $this->getParam('reminderDate'))->addAttribute('ID','reminderDate'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="firstReminderDate"><?= $this->_('Invoice/FirstReminderDate')?></label>
				</span>
				<?= $this->form()->input('firstReminderDate', 'text', $this->getParam('firstReminderDate'))->addAttribute('ID','firstReminderDate'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="secondReminderDate"><?= $this->_('Invoice/SecondReminderDate')?></label>
				</span>
				<?= $this->form()->input('secondReminderDate', 'text', $this->getParam('secondReminderDate'))->addAttribute('ID','secondReminderDate'); ?>
			</div>
			<div class="input">
				<span class="label">
					<label for="debtCollectionDate"><?= $this->_('Invoice/DebtCollectionDate')?></label>
				</span>
				<?= $this->form()->input('debtCollectionDate', 'text', $this->getParam('debtCollectionDate'))->addAttribute('ID','debtCollectionDate'); ?>
			</div>
			<div class="input btn">
				<?= $this->form()->submit('submit', $this->_('Invoice/Search')); ?>
			</div>
		<?= $this->form()->end(); ?>
	</fieldset>
	
	<? if($this->hasParam('submit')) : ?>
		<?= $this->invoices; ?>
	<? endif; ?>
</div>

<script type="text/javascript">
$(document).ready(function() {	
	$('#invoiceDate,#dueDate,#payedDate,#reminderDate,#firstReminderDate,#secondReminderDate,#debtCollectionDate').datepicker({ 
		dateFormat: 'dd-mm-yy'
	});
});
</script>