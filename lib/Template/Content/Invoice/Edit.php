<? /* @var $this Widget_Invoice_Edit */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>
<div class="ctn">
	<h3><?= $this->_('Invoice/EditInvoice');?></h3>
	<?= $this->form()->start('invoice'); ?>
	<?= $this->showErrors('invoice'); ?>
		<table>
			<tr>
				<td class="top" style="width:500px;">
					<table>
						<tr>
							<td style="width:120px;">
								<?= $this->_('Invoice/Account'); ?>
							</td>
							<td>
								<?= $this->form()->selectStart('accountId', new Dataset_Account($this->_('Invoice/Choose')), $this->invoice->getAccountID(), TRUE);?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/Description'); ?>
							</td>
							<td>
								<?= $this->form()->textarea('description', 6, 60, $this->invoice->getDescription(), TRUE);?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/Template'); ?>
							</td>
							<td>
								<?= $this->form()->selectStart('template', new Dataset_Invoice_Template(), $this->invoice->getTemplate(), TRUE)?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/ExcludeVat')?>
							</td>
							<td>
								<?= $this->form()->bool('excludeVat', $this->invoice->getExcludeVat(), TRUE);?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/Record'); ?>
							</td>
							<td>
								<?= $this->form()->bool('recorded', $this->invoice->getRecorded(), TRUE);?>
							</td>
						</tr>
					</table>
				</td>
				<td class="top">
					<table>
						<tr>
							<td>
								<?= $this->_('Invoice/InvoiceDate'); ?>
							</td>
							<td>
								<?= $this->form()->input('invoiceDate', 'text', $this->invoice->getInvoiceDate(), TRUE)
									->addAttribute('class','js-datepicker');?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/PaymentDate'); ?>
							</td>
							<td>
								<?= $this->form()->input('dueDate', 'text', $this->invoice->getDueDate(), TRUE)
									->addAttribute('class','js-datepicker');?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/ReminderDate'); ?>
							</td>
							<td>
								<?= $this->form()->input('reminderDate', 'text', $this->invoice->getReminderDate(), TRUE)
									->addAttribute('class','js-datepicker');?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/FirstReminderDate'); ?>
							</td>
							<td>
								<?= $this->form()->input('firstReminderDate','text', $this->invoice->getFirstReminderDate(), TRUE)
									->addAttribute('class','js-datepicker');?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/SecondReminderDate'); ?>
							</td>
							<td>
								<?= $this->form()->input('secondReminderDate','text', $this->invoice->getSecondReminderDate(), TRUE)
									->addAttribute('class','js-datepicker');?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/DebtCollectionDate'); ?>
							</td>
							<td>
								<?= $this->form()->input('debtCollectionDate','text', $this->invoice->getDebtCollectionDate(),TRUE)
									->addAttribute('class','js-datepicker');?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="<?= \Pecee\Router::GetRoute('ajax', 'dialog', array('item','add'));?>" rel="dialog" style="float:right;margin-top:15px;"><?= $this->_('Invoice/AddItem')?></a>
					<h3><?= $this->_('Invoice/Items')?></h3>
					
					<table>
						<thead>
							<tr>
								<th>
									<?= $this->_('Item/ItemNo')?>
								</th>
								<th>
									<?= $this->_('Item/Name')?>
								</th>
								<th>
									<?= $this->_('Item/Description')?>
								</th>
								<th>
									<?= $this->_('Item/Count')?>
								</th>
								<th>
									<?= $this->_('Item/Unit')?>
								</th>
								<th>
									<?= $this->_('Item/Price')?>
								</th>
								<th>
									<?= $this->_('Item/Rebate')?>
								</th>
								<th>
									<?= $this->_('Item/Amount')?>
								</th>
								<th></th>
							</tr>
						</thead>
						<tbody class="js-invoices">
						<? /* @var $productItem as Model_Invoice_Item */
						   foreach($this->productItems as $i=>$productItem) : ?>
							<tr>
								<td>
									<?= $productItem->getItem()->getNumber(); ?>
									<?= $this->form()->input('productItem[]', 'hidden', \Pecee\String\Encoding::Base64Encode($productItem),TRUE)?>
								</td>
								<td>
									<?= $productItem->getItem()->getName(); ?>
								</td>
								<td>
									<?= $productItem->getDescription(); ?>
								</td>
								<td>
									<?= $productItem->getCount(); ?>
								</td>
								<td>
									<?= Helper::ShowUnit($productItem->getItem()->getUnit()); ?>
								</td>
								<td>
									<?= Helper::FormatMoney($productItem->getPrice()); ?>
								</td>
								<td>
									<?= ($productItem->getRebate()) ? '-'.Helper::FormatMoney($productItem->getRebate()) : ''; ?>
								</td>
								<td>
									<?= Helper::FormatMoney($productItem->getTotal()); ?>
								</td>
								<td style="text-align:right;">
									<a href="<?= \Pecee\Router::GetRoute('ajax','dialog',array('item','edit'));?>" data-item="<?= \Pecee\String\Encoding::Base64Encode($productItem); ?>" data-index="<?= $i; ?>" rel="post-dialog"><?= $this->_('Item/Edit'); ?></a> - 
									<a href="#" class="js-delete-productItem"><?= $this->_('Item/Remove'); ?></a>
								</td>
							</tr>
						<? endforeach;?>
						</tbody>
					</table>
					
					<script type="text/javascript">
						$(document).ready(function() {
							$('a.js-delete-productItem').live('click',function(e) {
								e.preventDefault();
								$(this).parents('tr:first').remove();
							});
							$('.js-datepicker').datepicker({dateFormat: 'dd-mm-yy'});
						});
					</script>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:20px;">
					<?= $this->form()->submit('submit', $this->_('Invoice/SaveChanges'))?>
				</td>
			</tr>
		</table>
	<?= $this->form()->end(); ?>
</div>