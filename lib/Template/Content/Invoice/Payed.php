<? /* @var $this Widget_Invoice_Payed */ ?>
<div class="sub-nav">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP); ?>
</div>

<div class="ctn">
	<div class="columns_150">
		<div class="col1">
			<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_LEFT); ?>
		</div>
		<div class="col2">
			<?= $this->form()->start('search', 'get');?>
			<fieldset style="margin-bottom:15px;">
				<legend><?= $this->_('Invoice/Filter'); ?></legend>
				<div class="input">
					<span class="label"><?= $this->_('Invoice/Period')?></span>
					<div style="float:left;margin-left:10px;">
						<?= $this->form()->input('from', 'text', $this->getParam('from'))
							->addAttribute('placeholder', $this->_('Invoice/From'))
							->addAttribute('id', 'fromDatepicker')
							->addAttribute('autocomplete', 'off'); ?> 
						<span style="padding:0px 15px;"><?= $this->_('Invoice/To'); ?></span> 
						<?= $this->form()->input('to', 'text', $this->getParam('to'))
							->addAttribute('placeholder', $this->_('Invoice/To'))
							->addAttribute('id', 'toDatepicker')
							->addAttribute('autocomplete', 'off'); ?> 	
						<?= $this->form()->submit('submit', $this->_('Invoice/Update'))
						->addAttribute('class', 'small'); ?>
					</div>
					<div class="input">
						<span class="label"></span>
						<div style="float:left;margin-left:10px;">
							<?= $this->form()->bool('recorded', $this->getParam('recorded'))
								->addAttribute('ID', 'recorded');?> 
							<?= $this->form()->label($this->_('Invoice/RecordedOnly'), 'recorded'); ?> 
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</fieldset>
			<?= $this->form()->end(); ?>
			
			<? if($this->hasParam('submit')) : ?>
				<?= $this->invoices; ?>
			<? endif; ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
 
<script type="text/javascript">
$(document).ready(function() {	
	$('#fromDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#toDatepicker').datepicker('option', 'minDate', selectedDate);
		}
	});

	$('#toDatepicker').datepicker({ 
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#fromDatepicker').datepicker('option', 'maxDate', selectedDate);
		}
	});
});
</script>