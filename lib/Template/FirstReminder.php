<? /* @var $this Widget_Invoice_View */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
	</head>
	<body>
		<table style="width:800px;" align="center">
			<tr>
				<td style="padding-top:25px;">
					<span class="bold"><?= $this->invoice->getCustomer()->getName(); ?></span><br/>
					<?= $this->invoice->getCustomer()->getAddress(); ?><br/>
					<?= $this->invoice->getCustomer()->getPostalNumber() ?> <?= $this->invoice->getCustomer()->getCity() ?><br/>
					<?= $this->invoice->getCustomer()->getCountry();?>
				</td>
				<td style="width:290px;text-align:right;padding:15px;">
					<? if($this->company->getLogo()) : ?>
						<img src="<?= $this->company->getLogo()->getInternalUrl(); ?>" alt=""  />
					<? endif; ?>
				</td>
			</tr>
			<tr>
				<td>
					<table style="width:230px;margin-top:5px;">
						<tr>
							<td style="width:90px;">
								<?= $this->_('Invoice/View/Bank')?>
							</td>
							<td class="right">
								<?= $this->invoice->getAccount()->getBank();?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/View/BankAccountRegNo')?>
							</td>
							<td class="right">
								<?= $this->invoice->getAccount()->getRegistrationNumber();?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/View/BankAccountNo')?>
							</td>
							<td class="right">
								<?= $this->invoice->getAccount()->getAccountNumber(); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/View/IBAN')?>
							</td>
							<td class="right">
								<?= $this->invoice->getAccount()->getIBAN(); ?>
							</td>
						</tr>
					</table>
				</td>
				<td style="padding-bottom:30px;">
					<h3>Rykkergebyr</h3>
					<table style="width:230px;">
						<tr>
							<td style="width:150px;">
								<?= $this->_('Invoice/View/InvoiceNo')?>
							</td>
							<td class="right">
								<?= $this->invoice->getInvoiceID(); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/View/CustomerNo')?>
							</td>
							<td class="right">
								<?= $this->invoice->getCustomerID(); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/View/InvoiceDate')?>
							</td>
							<td class="right">
								<?= date('d-m-Y', strtotime($this->invoice->getInvoiceDate())); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/View/PaymentDate')?>
							</td>
							<td class="right">
								<?= date('d-m-Y', strtotime($this->invoice->getDueDate())); ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-bottom:20px;">
					Denne rykker er et varsel i henhold til inkassolovens § 10. 
					Det skyldige beløb skal betales senest <?= \Pecee\Date::DaysUntil(strtotime($this->invoice->getDueDate())); ?> dage fra dags dato. Hvis det ikke sker, 
					vil det kunne medføre yderligere omkostninger for Dem, idet sagen uden yderligere 
					varsel vil blive overgivet til inkasso.
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<? if($this->invoice->getProductItems()->hasRows()) : ?>
					<table class="invoice">
						<thead>
							<tr>
								<th>
									<?= $this->_('Item/ItemNo')?>
								</th>
								<th>
									<?= $this->_('Item/Name')?>
								</th>
								<th>
									<?= $this->_('Item/Description')?>
								</th>
								<th class="center">
									<?= $this->_('Item/Count')?>
								</th>
								<th class="center">
									<?= $this->_('Item/Unit')?>
								</th>
								<th class="right">
									<?= $this->_('Item/Price')?>
								</th>
								<th class="right">
									<?= $this->_('Item/Amount')?>
								</th>
							</tr>
						</thead>
						<tbody>		
						<? /* @var $productItem as Model_Invoice_Item */
						   foreach($this->invoice->getProductItems()->getRows() as $productItem) : ?>
							<tr>
								<td>
									<?= $productItem->getItem()->getNumber(); ?>
								</td>
								<td>
									<?= $productItem->getItem()->getName(); ?>
								</td>
								<td>
									<?= $productItem->getDescription(); ?>
								</td>
								<td class="center">
									<?= $productItem->getCount(); ?>
								</td>
								<td class="center">
									<?= Helper::ShowUnit($productItem->getItem()->getUnit()); ?>
								</td>
								<td class="right">
									<?= Helper::FormatMoney(($productItem->getPrice()-$productItem->getRebate()));?>
								</td>
								<td class="right">
									<?= Helper::FormatMoney($productItem->getTotal($this->company)); ?>
								</td>
							</tr>
						<? endforeach;?>
						</tbody>
					</table>
					<? endif; ?>
				</td>
			</tr>
			<tr>
				<td valign="top"></td>
				<td align="right" style="border-bottom:1px solid #CCC;">
					<table>
						<? if($this->invoice->getExcludeVat()) : ?>
						<tr>
							<td>
								<?= $this->_('Invoice/TotalOfWhichVAT', $this->company->getVatPercentage()); ?>
							</td>
							<td class="right">
								<?= Helper::FormatMoney($this->vatTotal); ?>
							</td>
						</tr>
						<tr class="bold">
							<td>
								<?= $this->_('Invoice/TotalWithVat', $this->company->getCurrency()); ?>
							</td>
							<td class="right">
								<?= Helper::FormatMoney($this->invoice->getTotal($this->company)); ?>
							</td>
						</tr>
						<? else: ?>
						<tr>
							<td>
								<?= $this->_('Invoice/TotalExVat')?>
							</td>
							<td class="right">
								<?= Helper::FormatMoney($this->invoice->getTotal($this->company,FALSE)); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?= $this->_('Invoice/TotalVat',$this->company->getVatPercentage()); ?>
							</td>
							<td class="right">
								<?= Helper::FormatMoney($this->invoice->getVatTotal($this->company)); ?>
							</td>
						</tr>
						<tr class="bold">
							<td>
								<?= $this->_('Invoice/TotalWithVat', $this->company->getCurrency()); ?>
							</td>
							<td class="right">
								<?= Helper::FormatMoney($this->invoice->getTotal($this->company)); ?>
							</td>
						</tr>
						<? endif; ?>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding-top:50px;padding-bottom:40px;" colspan="2">
					<?= $this->_('Invoice/View/InvoiceBankTransferReminder', $this->invoice->getInvoiceID())?>
				</td>
			</tr>
			<tr>
				<td style="padding-top:20px;font-size:12px;color:#999;">
					<?= $this->company->getName(); ?> - <?= $this->company->getAddress(); ?> - <?= $this->company->getPostalCode(); ?> 
					<?= $this->company->getCity(); ?> - 
					<?= $this->_('Invoice/View/VATNo'); ?> <?= $this->company->getVATNo();?> -
					<?= $this->_('Invoice/View/Phone'); ?> <?= $this->company->getPhone(); ?>
				</td>
			</tr>
		</table>
	</body>
</html>