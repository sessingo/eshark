<?php /* @var $this Widget_Site */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
		<script type="text/javascript">
			$(document).ready(function() {
				var g=new global();
				g.construct();
			});
		</script>
	</head>
	<body>
		<div class="header">
			<div class="wrapper">
				<div class="inner">
					<?= $this->userMenu; ?>
					<a href="<?= \Pecee\Router::GetRoute('', ''); ?>"><img src="/img/logo.png" alt="" /></a>
				</div>
			</div>
		</div>
		<div class="nav">
			<div class="wrapper">
				<div class="inner">
					<?= $this->topmenu; ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="content">
			<div class="wrapper">
				<div class="inner">
					<?= $this->getContentHtml(); ?>
				</div>
			</div>
		</div>
		<!-- BUILD: <?= \Pecee\Registry::GetInstance()->get('BuildVersion'); ?> -->
	</body>
</html>