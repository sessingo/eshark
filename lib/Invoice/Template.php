<?php
class Invoice_Template {
	protected static $instance;
	public static function GetInstance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}
	
	protected $templates;
	
	public function __construct() {
		$this->findTemplates();
	}
	
	protected function getDir() {
		$dir=explode(PATH_SEPARATOR, get_include_path());
		return $dir[1].'Template';
	}
	
	protected function findTemplates() {
		$this->templates=array();
		$handle=opendir($this->getDir());
		if($handle) {
			$excludeDir=array('.', '..', '.svn', 'Abstract', 'Invoice.php', 'Dialog.php', 'Error.php', 'Form.php', 'Mail.php');
			while (FALSE !== ($dirs = readdir($handle))) {
				if(!is_dir($this->getDir() . DIRECTORY_SEPARATOR . $dirs) && !in_array($dirs, $excludeDir)) {
					$this->templates[]=$dirs;
				}
			}
			closedir($handle);
		}
	}
	
	public function getTemplates() {
		return $this->templates;
	}
	
	public function exists($template) {
		return in_array($template, $this->templates);
	}
}