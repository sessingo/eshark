<?php
class UserType {
	public $types;
	public function __construct() {
		$this->types[Model_User::TYPE_EMPLOYEE] = \Pecee\Language::Instance()->_('User/UserTypes/Employee');
		$this->types[Model_User::TYPE_ADMIN] = \Pecee\Language::Instance()->_('User/UserTypes/Administrator');
	}
	public function getTypes() {
		asort($this->types);
		return $this->types;
	}
	public function getByType($type) {
		return isset($this->types[$type]) ? $this->types[$type] : NULL;
	}
}