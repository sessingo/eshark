<?php
class Helper {
	const LOCALE_DA_DK='da-DK';
	const LOCALE_EN_UK='en-UK';
	public static $LOCALES=array(self::LOCALE_EN_UK,self::LOCALE_DA_DK);
	const DEFAULT_TIMEOUT=20;
	const MONEY_FORMAT='%!2n';
	public static function FormatMoney($price) {
		if($price) {
			return number_format($price, 2, ',', '.');
		}
		return 0;
	}
	
	public static function FormatTotal($company, $price) {
		$price=number_format($price, 2, ',', '.');
		return $price.$company->getCurrency();
	}
	
	public static function FormatDateTime($dateTime) {
		return strftime('%c', strtotime($dateTime));
	}
	
	public static function FormatDate($date) {
		return strftime('%x', strtotime($date));
	}
	
	public static function MinutesToHour($minutes) {
		return sprintf("%02d:%02d", floor($minutes/60), $minutes%60);
	}
	
	public static function ShowUnit($index) {
		$unit=new Unit();
		return $unit->getByIndex($index);
	}
	
	public static function ShowType($type) {
		$types=new UserType();
		return $types->getByType($type);
	}
	
	public static function CalculateTotals(Model_Invoice $invoices, Model_Company $company, &$total, &$totalVat) {
		$total=0;
		$totalVat=0;
		if($invoices->hasRows()) {
			foreach($invoices->getRows() as $invoice) {
				$totalVat += $invoice->getVatTotal($company);
				if($invoice->getTotal($company) > 0) {
					$total += $invoice->getTotal($company);
				} else {
					$total = $invoice->getTotal($company)+$total;
				}
			}
		}
	}
}