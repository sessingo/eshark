<?php
class Dataset_Case_Subject extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$subjects=Model_Case_Subject::Get();
		if($subjects->hasRows()) {
			foreach($subjects->getRows() as $subject) {
				$this->createArray($subject->getCaseSubjectID(), $subject->getTitle());
			}
		}
	}
}