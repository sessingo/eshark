<?php
class Dataset_Invoice_Template extends \Pecee\Dataset {
	public function __construct() {
		$this->createArray('', $this->_('Invoice/DefaultTemplate'));
		
		$templates=Invoice_Template::GetInstance()->getTemplates();
		foreach($templates as $template) {
			$this->createArray($template, $template);
		}
	}
}