<?php
class Dataset_Invoice_Payment_List extends \Pecee\Dataset {
	public function __construct(Model_Company $company, $customerId, $choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$invoices=Model_Invoice::Get($customerId, Model_Invoice::TYPE_INVOICE, NULL, NULL, NULL, FALSE, Model_Invoice::ORDER_DUEDATE);
		if($invoices->hasRow()) {
			foreach($invoices->getRows() as $invoice){
				$this->createArray($invoice->getInvoiceID(), 
						$this->_('Invoice/ChooseInvoiceOption', $invoice->getInvoiceID(), 
									Helper::FormatTotal($company, $invoice->getTotal($company))));
			}
 		}
	}
}