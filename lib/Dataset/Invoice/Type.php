<?php
class Dataset_Invoice_Type extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		foreach(Model_Invoice::$TYPES as $type) {
			$this->createArray($type, $type);
		}
	}
}