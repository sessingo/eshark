<?php
class Dataset_Customer_Project extends \Pecee\Dataset {
	public function __construct($customerId=NULL, $choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		if($customerId) {
			$projects=Model_Project::GetByCustomerID($customerId);
			if($projects->hasRows()) {
				foreach($projects->getRows() as $project) {
					$this->createArray($project->getProjectID(), $project->getName());
				}
			}
		}
	}
}