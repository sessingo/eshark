<?php
class Dataset_Customer_Project_Activity extends \Pecee\Dataset {
	public function __construct($projectId=NULL, $choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		
		if($projectId) {
			$activities=Model_Project_Activity::GetByProjectId($projectId);
			if($activities->hasRows()) {
				foreach($activities->getRows() as $activity) {
					$this->createArray($activity->getProjectActivityID(), $activity->getName());
				}
			}
		}
	}
}