<?php
class Dataset_Customer extends \Pecee\Dataset {
	public function __construct($choose=NULL) {	
		$customers=Model_Customer::Get();
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		if($customers->hasRow()) {
			foreach($customers->getRows() as $customer)  {
				$this->createArray($customer->getCustomerID(), $customer->getName());
			}
		}
	}
}