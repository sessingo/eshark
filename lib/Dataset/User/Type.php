<?php
class Dataset_User_Type extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		
		$types=new UserType();
		foreach($types->getTypes() as $type=>$name) {
			$this->createArray($type, $name);
		}
	}
}