<?php
class Dataset_Account extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$accounts=Model_Account::Get();
		if($accounts->hasRows()) {
			foreach($accounts->getRows() as $account) {
				$this->createArray($account->getAccountID(), $account->getName());
			}
		}
	}
}