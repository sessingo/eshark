<?php
class Dataset_Country extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$countries=\Pecee\Currency::GetCountries();
		foreach($countries as $country) {
			$this->createArray($country, $country);
		}
	}
}