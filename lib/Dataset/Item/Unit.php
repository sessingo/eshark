<?php
class Dataset_Item_Unit extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$units=new Unit();
		$units=$units->getUnits();
		foreach($units as $key=>$unit) {
			$this->createArray($key,$unit);
		}
	}
}