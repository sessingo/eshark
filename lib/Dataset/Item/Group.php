<?php
class Dataset_Item_Group extends \Pecee\Dataset {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$groups=Model_Item_Group::Get();
		if($groups->hasRows()) {
			foreach($groups->getRows() as $group) {
				$this->createArray($group->getItemGroupID(),$group->getName());
			}
		}
	}
}