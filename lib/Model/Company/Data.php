<?php
class Model_Company_Data extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('CompanyData', array('CompanyID' => NULL, 'Key' => '', 'Value' => ''));
	}
	
	public function save() {
		if($this->exists()) {
			parent::update();
		} else {
			parent::save();
		}
	}
	
	public function exists() {
		return self::Scalar('SELECT `Key` FROM `CompanyData` WHERE `Key` = %s AND `CompanyID` = %s', $this->CompanyID, $this->Key);
	}
	
	/**
	 * Get by company id
	 * @param int $companyId
	 * @return Model_Company_Data
	 */
	public static function GetByCompanyId($companyId) {
		return self::FetchAll('SELECT * FROM `CompanyData` WHERE `CompanyID` = %s', $companyId);
	}
	
	public static function Clear($companyId) {
		self::NonQuery('DELETE FROM `CompanyData` WHERE `CompanyID` = %s', $companyId);
	}
}