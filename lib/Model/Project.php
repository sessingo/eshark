<?php
class Model_Project extends \Pecee\Model\Model {
	protected $customer;
	public function __construct() {
		parent::__construct('Project', array('ProjectID' => NULL, 'CustomerID' => NULL, 'UserID' => NULL, 'Name' => '', 'Description' => NULL, 'CreatedDate' => \Pecee\Date::ToDateTime(), 'Active' => TRUE));
	}
	
	/**
	 * Get customer
	 * @return Model_Customer
	 */
	public function getCustomer() {
		if(!$this->customer) {
			$this->customer = Model_Customer::GetById($this->CustomerID);
		}
		return $this->customer;
	}
	
	/**
	 * Get projects by project id
	 * @param int $projectId
	 * @return Model_Project
	 */
	public static function GetById($projectId) {
		return self::FetchOne('SELECT p.* FROM `Project` p  WHERE p.`ProjectID` = %s', $projectId);
	}
	
	/**
	 * Get projects by customer id
	 * @param int $customerId
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Project
	 */
	public static function GetByCustomerId($customerId, $active=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		if(!is_null($active)) {
			$where[]=\Pecee\DB\DB::FormatQuery('p.`Active` = %s', array($active));
		}
		return self::FetchPage('p.`ProjectID`', 'SELECT p.* FROM `Project` p WHERE '. join(' && ',  $where) .' && p.`CustomerID` = %s ORDER BY p.`Name` ASC', $rows, $page, $customerId);
	}
}