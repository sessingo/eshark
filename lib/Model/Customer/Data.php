<?php
class Model_Customer_Data extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('CustomerData', array('CustomerID' => NULL, 'Key' => '', 'Value' => ''));
	}
	
	public function save() {
		if($this->exists()) {
			parent::update();
		} else {
			parent::save();
		}
	}
	
	public function exists() {
		return self::Scalar('SELECT `Key` FROM `CustomerData` WHERE `Key` = %s AND `CustomerID` = %s', $this->CustomerID, $this->Key);
	}
	
	/**
	 * Get by customer id
	 * @param int $customerId
	 * @return Model_Company_Data
	 */
	public static function GetByCustomerId($customerId) {
		return self::FetchAll('SELECT * FROM `CustomerData` WHERE `CustomerID` = %s', $customerId);
	}
	
	public static function Clear($customerId) {
		self::NonQuery('DELETE FROM `CustomerData` WHERE `CustomerID` = %s', $customerId);
	}
}