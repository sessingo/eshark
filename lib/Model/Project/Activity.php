<?php
class Model_Project_Activity extends \Pecee\Model\Model {
	protected $project;
	public function __construct() {
		parent::__construct('ProjectActivity', array('ProjectActivityID' => NULL, 'ProjectID' => NULL, 'UserID' => NULL, 'Name' => '', 
														'Description' => NULL, 'MaxHours' => NULL, 'Price' => NULL, 
														'CreatedDate' => \Pecee\Date::ToDateTime(), 'Active' => TRUE));
	}
		
	/**
	 * Get project
	 * @return Model_Project
	 */
	public function getProject() {
		if(!$this->project) {
			$this->project = Model_Project::GetById($this->ProjectID, Model_User::Current()->getUserID());
		}
		return $this->project;
	}
	
	public static function GetById($projectActivityId) {
		return self::FetchOne('SELECT pa.* FROM `ProjectActivity` pa JOIN `Project` p ON(p.`ProjectID` = pa.`ProjectID`) WHERE pa.`ProjectActivityID` = %s', $projectActivityId);
	}
	
	/**
	 * Get activities by project id
	 * @param int $projectId
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Project_Activity
	 */
	public static function GetByProjectId($projectId, $rows=NULL, $page=NULL) {
		return self::FetchPage('pa.`ProjectActivityID`', 'SELECT pa.* FROM `ProjectActivity` pa JOIN `Project` p ON(p.`ProjectID` = pa.`ProjectID`) WHERE pa.`ProjectID` = %s ORDER BY pa.`Name` ASC', $rows, $page, $projectId);
	}
}