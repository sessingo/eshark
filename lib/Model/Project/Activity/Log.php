<?php
class Model_Project_Activity_Log extends \Pecee\Model\Model {
	const ORDER_LOGID_ASC='pal.`ProjectActivityLogID` ASC';
	const ORDER_LOGID_DESC='pal.`ProjectActivityLogID` DESC';
	const ORDER_DATE_ASC='pal.`Date` ASC';
	const ORDER_DATE_DESC='pal.`Date` DESC';
	
	public static $ORDERTYPES=array(self::ORDER_DATE_ASC,self::ORDER_DATE_DESC,self::ORDER_LOGID_ASC,self::ORDER_LOGID_DESC);
	
	protected $activity;
	protected $user;
	public function __construct() {
		parent::__construct('ProjectActivityLog', array('ProjectActivityLogID' => NULL, 'ProjectActivityID' => NULL, 'UserID' => NULL, 
															'Minutes' => '', 'Date' => \Pecee\Date::ToDate(), 'Description' => NULL));
	}
	
	/**
	 * Get user
	 * @return Model_User
	 */
	public function getUser() {
		if(!$this->user) {
			$this->user=Model_User::GetByUserID($this->UserID);
		}
		return $this->user;
	}
	
	public function getHoursLog() {
		return floor($this->getMinutes()/60);
	}
	
	public function getMinutesLog() {
		return $this->getMinutes()%60;
	}
	
	/**
	 * Get activity
	 * @return Model_Project_Activity
	 */
	public function getActivity() {
		if(!$this->activity) {
			$this->activity = Model_Project_Activity::GetById($this->ProjectActivityID, Model_User::Current()->getUserID());
		}
		return $this->activity;
	}
	
	public static function GetLatest($dateFrom=NULL, $dateTo=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$order=(!is_null($order) && in_array($order, self::$ORDERTYPES)) ? $order : self::ORDER_DATE_DESC;
		$where=array('1=1');
		if(!is_null($dateFrom)) {
			$where[]=\Pecee\DB\DB::FormatQuery('pal.`Date` >= DATE(%s)', array($dateFrom));
		}
		if(!is_null($dateTo)) {
			$where[]=\Pecee\DB\DB::FormatQuery('pal.`Date` <= DATE(%s)', array($dateTo));
		}
		return self::FetchPage('pal.`ProjectActivityLogID`', 'SELECT pal.* FROM `ProjectActivityLog` pal
				JOIN `ProjectActivity` pa ON(pa.`ProjectActivityID` = pal.`ProjectActivityID`)
				JOIN `Project` p ON(p.`ProjectID` = pa.`ProjectID`) WHERE '.join(' && ', $where).'
				 && p.`Active` = 1 && pa.`Active` = 1 GROUP BY p.`ProjectID` ORDER BY ' . $order,
				$rows, $page);
	}
	
	/**
	 * Get log by id
	 * @param int $activityLogId
	 * @return Model_Project_Activity_Log
	 */
	public static function GetById($activityLogId) {
		return self::FetchOne('SELECT pal.* FROM `ProjectActivityLog` pal 
				JOIN `ProjectActivity` pa ON(pa.`ProjectActivityID` = pal.`ProjectActivityID`) 
				JOIN `Project` p ON(p.`ProjectID` = pa.`ProjectID`) WHERE pal.`ProjectActivityLogID` = %s',  
				$activityLogId);
	}
	
	/**
	 * Get activity log by date
	 * @param int $date
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Project_Activity_Log
	 */
	public static function Get($dateFrom=NULL, $dateTo=NULL, $customerId=NULL, $projectId=NULL, $activityId=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$order=(!is_null($order) && in_array($order, self::$ORDERTYPES)) ? $order : self::ORDER_DATE_DESC;
		$where=array('1=1');
		if(!is_null($dateFrom)) {
			$where[]=\Pecee\DB\DB::FormatQuery('pal.`Date` >= DATE(%s)', array($dateFrom));
		}
		if(!is_null($dateTo)) {
			$where[]=\Pecee\DB\DB::FormatQuery('pal.`Date` <= DATE(%s)', array($dateTo));
		}
		if(!is_null($customerId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('p.`CustomerID` = %s', array($customerId));
		}
		if(!is_null($projectId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('p.`ProjectID` = %s', array($projectId));
		}
		if(!is_null($activityId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('pal.`ProjectActivityID` = %s', array($activityId));
		}
		return self::FetchPage('pal.`ProjectActivityLogID`', 'SELECT pal.* FROM `ProjectActivityLog` pal 
				JOIN `ProjectActivity` pa ON(pa.`ProjectActivityID` = pal.`ProjectActivityID`) 
				JOIN `Project` p ON(p.`ProjectID` = pa.`ProjectID`) WHERE '.join(' && ', $where).' 
				 && p.`Active` = 1 && pa.`Active` = 1 GROUP BY pal.`ProjectActivityLogID` ORDER BY '.$order, 
				$rows, $page);
	}
}