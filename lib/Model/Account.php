<?php
class Model_Account extends \Pecee\Model\Model {
	public function __construct(){
		parent::__construct('Account', array('AccountID' => NULL, 'UserID' => NULL, 'Name' => NULL, 'Bank' => '', 'RegistrationNumber' => '',
											'AccountNumber' => '', 'IBAN' => NULL, 'Swift' => NULL, 'PubDate' => \Pecee\Date::ToDateTime()));
	}
	
	public static function GetById($accountId) {
		return self::FetchOne('SELECT * FROM `Account` WHERE `AccountID` = %s', $accountId);
	}
	
	public static function Get($rows=NULL, $page=NULL) {
		return self::FetchPage('`AccountID`', 'SELECT * FROM `Account`', $rows, $page);
	}
}