<?php
class Model_User_Confirm extends \Pecee\Model\Model {
	const MAX_CONFIRMS_PER_HOUR=3;
	const MAX_CONFIRM_DAYS=31; // One month
	public function __construct() {
		parent::__construct('UserConfirm', array('UserConfirmID' => NULL, 'Email' => '', 'Token' => \Pecee\Guid::Create(TRUE), 'Activated' => FALSE, 'Date' => \Pecee\Date::ToDateTime(),
													'IP' => \Pecee\Server::GetRemoteAddr()));
	}
	
	public function save() {
		/* Check if we are allowed to send and if the user trying to share has access to the project. */
		if(!$this->exists()) {
			/* Send email */
			parent::save();
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * @return Model_User_Confirm
	 */
	public static function GetByToken($token) {
		return self::FetchOne('SELECT * FROM `UserConfirm` WHERE `Token` = %s && `Activated` = 0 && `Date` > DATE(%s)', $token, \Pecee\Date::ToDateTime((time()-(60*60*24*self::MAX_CONFIRM_DAYS))));
	}
	
	/**
	 * @return Model_User_Confirm
	 */
	public static function GetActivatedByEmail($email) {
		return self::FetchOne('SELECT * FROM `UserConfirm` WHERE `Email` = %s && `Activated` = 1', $email);
	}
	
	public function exists() {
		return self::Scalar('SELECT `Email` FROM `UserConfirm` WHERE `Email` = %s && `Activated` = 0 && `Date` > DATE(%s)', $this->Email, \Pecee\Date::ToDateTime((time()-(60*60*self::MAX_CONFIRMS_PER_HOUR))));
	}
}