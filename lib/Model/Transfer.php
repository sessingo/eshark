<?php
class Model_Transfer extends \Pecee\Model\Model {
	protected $account;
	protected $invoice;
	public function __construct() {
		parent::__construct('Transfer', array('TransferID' => NULL, 'AccountID' => NULL, 'InvoiceID' => NULL, 'UserID' => NULL, 'Text' => '',
												'Amount' => 0, 'Balance' => 0, 'Date' => \Pecee\Date::ToDate(), 'PubDate' => \Pecee\Date::ToDateTime(), 
												'Deleted' => FALSE));
	}
	
	public function getAmount() {
		if(!$this->invoice) {
			$this->invoice = Model_Invoice::GetById($this->InvoiceID);
		}
		if($this->invoice->hasRow()) {
			return $this->invoice->getTotalSimple();
		}
		return $this->Amount;
	}
	
	public function save() {
		if(!$this->TransferID) {
			$this->TransferID=parent::save()->getInsertId();
		} else {
			parent::update();
		}
	}
	
	public function delete() {
		$this->Deleted=TRUE;
		$this->update();
	}
	
	public function getAccount() {
		if(!$this->account) {
			$this->account=Model_Account::GetById($this->AccountID);
		}
		return $this->account;
	}
	
	public function updateBalance() {
		$accounts=Model_Account::Get();
		if($accounts->hasRows()) {
			/* @var $account Model_Account */
			foreach($accounts->getRows() as $account) {
				$where=array();
				$where[]=\Pecee\DB\DB::FormatQuery('(t.`AccountID` = %s || (SELECT TRUE FROM `InvoiceData` WHERE `Key` = \'accountid\' && `Value` = %s && `InvoiceID` = t.`InvoiceID` LIMIT 1))', 
							array($account->getAccountID(), $account->getAccountID()));
				$where[]='t.`Deleted` = 0';
				$transfers=self::FetchAll('SELECT t.* FROM `Transfer` t WHERE ' . join(' && ', $where) . ' ORDER BY t.`Date` ASC, t.`TransferID` ASC');
				$balance=0;
				if($transfers->hasRows()) {
					foreach($transfers->getRows() as $transfer) {
						$balance+=$transfer->getAmount();
						$transfer->setBalance(\Pecee\Float::ParseFloat($balance));
						$transfer->update();
					}
				}
			}
		}
	}
	
	/**
	 * Get by account id
	 * @param id|null $accountId
	 * @param bool|null $deleted
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Transfer
	 */
	public static function GetByAccountId($accountId, $deleted=FALSE, $fromDate = NULL, $toDate = NULL, $rows=NULL, $page=NULL) {
		$where=array();
		$where[]=\Pecee\DB\DB::FormatQuery('(t.`AccountID` = %s || (SELECT TRUE FROM `InvoiceData` WHERE `Key` = \'accountid\' && `Value` = %s && `InvoiceID` = t.`InvoiceID` LIMIT 1))', array($accountId, $accountId));
		if(!is_null($deleted)) {
			$deleted=($deleted) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('t.`Deleted` = %s', array($deleted));
		}
		if(!is_null($fromDate)) {
			$where[] = \Pecee\DB\DB::FormatQuery('t.`Date` >= %s', array(\Pecee\Date::ToDateTime(strtotime($fromDate))));
		}
		if(!is_null($toDate)) {
			$where[] = \Pecee\DB\DB::FormatQuery('t.`Date` <= %s', array(\Pecee\Date::ToDateTime(strtotime($toDate))));
		}
		return self::FetchPage('t.`TransferID`', 'SELECT t.*, 
									!IFNULL(t.`InvoiceID`, (SELECT `Value` FROM `InvoiceData` WHERE `Key` = \'accountid\' AND `Value` = t.`InvoiceID` LIMIT 1))
									FROM `Transfer` t WHERE ' . join(' && ', $where) . ' ORDER BY t.`Date` ASC, t.`TransferID` DESC', $rows, $page);
	}
	
	/**
	 * Get by id
	 * @param int $transferId
	 * @return Model_Transfer
	 */
	public static function GetById($transferId) {
		return self::FetchOne('SELECT t.* FROM `Transfer` t WHERE t.`TransferID` = %s ORDER BY t.`Date` DESC', $transferId);
	}
}