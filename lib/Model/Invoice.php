<?php
class Model_Invoice extends \Pecee\Model\ModelData {
	protected $customer;
	/* Orders */
	const ORDER_INVOICEID='i.`InvoiceID`';
	const ORDER_NAME='CHAR|name';
	const ORDER_DESCRIPTION='CHAR|description';
	const ORDER_DUEDATE='DATE|duedate';
	const ORDER_INVOICEDATE='DATE|invoicedate';
	const ORDER_TOTAL='SIGNED|total';
	const ORDER_INVOICENUMBER='SIGNED|invoicenumber';
	
	const ORDER_VOUCHER_PAYMENTDATE='DATE|paymentdate';
	const ORDER_PAYMENT_PAYEDINVOICEID='SIGNED|payedinvoiceid';
	
	const ORDER_TYPE_ASC='ASC';
	const ORDER_TYPE_DESC='DESC';
		
	public static $ORDER_TYPES=array(self::ORDER_TYPE_ASC, self::ORDER_TYPE_DESC);
	public static $ORDERS=array(self::ORDER_INVOICEID, self::ORDER_DESCRIPTION, self::ORDER_DUEDATE, self::ORDER_INVOICEDATE, 
								self::ORDER_NAME, self::ORDER_TOTAL, self::ORDER_INVOICENUMBER, self::ORDER_VOUCHER_PAYMENTDATE,
								self::ORDER_PAYMENT_PAYEDINVOICEID);
	
	const TYPE_VOUCHER='voucher';
	const TYPE_INVOICE='invoice';
	const TYPE_PAYMENT='payment';
	const TYPE_CREDITMEMO='creditmemo';
	
	public static $TYPES=array(self::TYPE_VOUCHER,self::TYPE_INVOICE,self::TYPE_PAYMENT,self::TYPE_CREDITMEMO);
	
	protected $account;
	protected $productItems;
	
	public function __construct() {
		parent::__construct('Invoice', array('InvoiceID' => NULL, 'UserID' => '', 'Type' => self::TYPE_INVOICE, 'CustomerID' => NULL, 
												'CreatedDate' => \Pecee\Date::ToDateTime(), 'Recorded' => FALSE, 'Deleted' => FALSE));
	}
	
	public function setRecorded($bool) {
		$this->Recorded = $bool;
	}
	
	public function setTemplate($template) {
		$this->data->template=$template;
	}
	
	public function getTemplate() {
		if($this->data->template && Invoice_Template::GetInstance()->exists($this->data->template)) {
			return $this->data->template;
		}
		return NULL;
	}
	
	public function setExcludeVat($bool) {
		$this->data->excludeVat=\Pecee\Bool::Parse($bool);
	}
	
	public function getExcludeVat() {
		return \Pecee\Bool::Parse($this->data->excludeVat);
	}
	
	public function getIcon() {
		$path='/img/ico/16x16/';
		switch($this->Type) {
			case self::TYPE_CREDITMEMO:
				return $path.'creditmemo.png';
				break;
			case self::TYPE_PAYMENT:
				return $path.'payment.png';
				break;
			case self::TYPE_VOUCHER;
				return $path.'voucher.png';
			break;
		}
		return $path.'invoice.png';
	}
	
	/**
	 * Get account
	 * @return Model_Account
	 */
	public function getAccount() {
		if(!$this->account) {
			$accountId=NULL;
			switch($this->Type) {
				default:
					$accountId=$this->getAccountID();
					break;
				case self::TYPE_PAYMENT:
					$invoice=self::GetById($this->getPayedInvoiceID());
					$accountId=$invoice->getAccountID();
					break;
				case self::TYPE_CREDITMEMO:
					$invoice=self::GetById($this->getCreditedInvoiceID());
					$accountId=$invoice->getAccountID();
					break;
			}
			$this->account=Model_Account::GetById($accountId);
		}
		return $this->account;
	}
	
	public function getTotal(Model_Company $company, $includeVat=TRUE) {
		$total=$this->getTotalSimple();
		if($includeVat && $total > 0 && !$this->getExcludeVat()) {
			$vat=sprintf('1.%s', $company->getVatPercentage());
			$total+=(floatval($total*$vat) - $total);
		}		
		return floatval($total);
	}
	
	public function getTotalSimple() {
		$total=0;
		if($this->getProductItems() && $this->getProductItems()->hasRows()) {
			/* @var $productItem as Model_Invoice_Item */
			foreach($this->getProductItems()->getRows() as $productItem) {
				$total+=floatval($productItem->getTotal());
			}
		}
		return $total;
	}
	
	/**
	 * Get customer
	 * @return Model_Customer
	 */
	public function getCustomer() {
		if(!$this->customer) {
			$this->customer=Model_Customer::GetById($this->CustomerID);
		}
		return $this->customer;
	}
	
	public function getVatTotal(Model_Company $company) {
		$vat=sprintf('0.%s', $company->getVatPercentage());
		return $this->getTotal($company,FALSE)*$vat;
	}
	
	public function getProductItems() {
		if(!$this->productItems) {
			if($this->InvoiceID) {
				$this->productItems = Model_Invoice_Item::GetByInvoiceId($this->InvoiceID);
			}
		}
		return $this->productItems;
	}
	
	public function updateData() {
		// Save payment as transfer
		if($this->Type==self::TYPE_PAYMENT) {
			if($this->data->transferId) {
				$transfer=Model_Transfer::GetById($this->data->transferId);
				$transfer=(!$transfer->hasRow()) ? new Model_Transfer() : $transfer;
			} else {
				$transfer=new Model_Transfer();
			}
			$amount=$this->getTotalSimple();
			if($amount<0) {
				$amount=$amount/-1;
			}
			$transfer->setAmount(\Pecee\Float::ParseFloat($amount));
			$transfer->setInvoiceID($this->getPayedInvoiceID());
			$transfer->setDate(\Pecee\Date::FromToDateTime($this->getPaymentDate()));
			$transfer->setUserID(Model_User::Current()->getUserID());
			$transfer->save();
			$transfer->updateBalance();
			$this->data->transferId=$transfer->getTransferID();
		}
		
		/* Remove all data */
		if($this->InvoiceID) {
			Model_Invoice_Data::Clear($this->InvoiceID);
			if($this->data) {
				foreach($this->data->getData() as $key=>$value) {
					$data=new Model_Invoice_Data();
					$data->setInvoiceID($this->InvoiceID);
					$data->setKey($key);
					$data->setValue($value);
					$data->save();
				}
			}
		}
	}
	
	public function save() {
		$this->InvoiceID=parent::save()->getInsertId();
		$this->updateData();
	}
	
	public function update() {
		$this->updateData();
		parent::update();
	}
	
	public function delete() {
		if($this->Type==self::TYPE_PAYMENT && $this->data->transferId) {
			$transfer=Model_Transfer::GetById($this->data->transferId);
			if($transfer->hasRow()) {
				$transfer->delete();
			}
		}
		
		Model_Invoice_Data::Clear($this->InvoiceID);
		parent::delete();
		//$this->Deleted=TRUE;
		//parent::update();
	}
	
	protected function fetchData($row) {
		$data=Model_Invoice_Data::GetByInvoiceId($row->InvoiceID);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}
	
	private static function FormatOrderQuery($type, $order) {
		$type=(in_array($type, self::$ORDERS)) ? $type : self::ORDER_INVOICEID;
		if($type==self::ORDER_INVOICEID) {
			return sprintf('%s %s', $type, $order);
		}
		$key=substr($type, strpos($type, '|')+1, strlen($type));
		$type=substr($type, 0, strpos($type, '|'));
		$value='`Value`';
		if($type=='DATE') {
			$value='STR_TO_DATE(Value,\'%d-%m-%Y\')';
		}
		return sprintf('CAST((SELECT %s FROM `InvoiceData` WHERE `Key` = \'%s\' AND `InvoiceID` = i.`InvoiceID` LIMIT 1) AS %s) %s', $value, $key, $type, $order);
	}
	
	protected static function GetOrder($types, $order) {		
		$query='';
		if(is_array($types)) {
			foreach($types as $i=>$type) {
				$query .= self::FormatOrderQuery($type,$order);
				if(($i+1) < count($types)) {
					$query .= ', ';
				}
			}
		} else {
			$query=self::FormatOrderQuery($types,$order);
		}
		return $query;
	}
	
	/**
	 * Get invoice by id
	 * @param int $invoiceId
	 * @return Model_Invoice
	 */
	public static function GetById($invoiceId) {
		return self::FetchOne('SELECT * FROM `Invoice` WHERE `InvoiceID` = %s', $invoiceId);
	}
	
	/**
	 * Get
	 * @param string|array|null $orderType
	 * @param string|null $order
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Invoice
	 */
	public static function Get($customerId=NULL, $type=NULL, $from=NULL, $to=NULL, $recorded=NULL, $deleted=FALSE, $orderType=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$order=(!is_null($order) && in_array($order, self::$ORDER_TYPES)) ? $order : self::ORDER_TYPE_DESC;
		$where=array('1=1');
		if(!is_null($customerId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`CustomerID` = %s', array($customerId));
		} else {
			$where[]='ISNULL(i.`CustomerID`)';
		}
		if(!is_null($type)) {
			if(is_array($type)) {
				$error=false;
				foreach($type as $t) {
					if(!in_array($t, self::$TYPES)) {
						$error=true;
					}
				}
				if(!$error) {
					$where[]='i.`Type` IN ('.\Pecee\DB\DB::JoinArray($type).')';
				}
			} else {
				if(in_array($type, self::$TYPES)) {
					$where[]=\Pecee\DB\DB::FormatQuery('i.`Type` = %s', array($type));
				}
			}
			
		}
		if(!is_null($recorded)) {
			$recorded=($recorded) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('i.`Recorded` = %s', array($recorded));
		}
		if(!is_null($deleted)) {
			$deleted=($deleted) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('i.`Deleted` = %s', array($deleted));
		}
		if(!is_null($from)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`InvoiceID` IN (SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'invoicedate\' AND STR_TO_DATE(Value,\'%%d-%%m-%%Y\') >= DATE(%s))', array($from));
		}
		if(!is_null($to)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`InvoiceID` IN (SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'invoicedate\' AND STR_TO_DATE(Value,\'%%d-%%m-%%Y\') <= DATE(%s))', array($to));
		}
		return self::FetchPage('i.`InvoiceID`', 'SELECT  i.* FROM `Invoice` i WHERE '. join(' && ', $where) .' ORDER BY ' . self::GetOrder($orderType, $order), $rows, $page);
	}
	
	public static function GetCustomerInvoice($customerId, $fromDate=NULL, $toDate=NULL, $hidePayment=FALSE, $recorded=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		$where[]=\Pecee\DB\DB::FormatQuery('i.`CustomerID` = %s', array($customerId));
		$types=($hidePayment) ? array(Model_Invoice::TYPE_INVOICE,Model_Invoice::TYPE_CREDITMEMO) : array(Model_Invoice::TYPE_INVOICE,Model_Invoice::TYPE_CREDITMEMO,Model_Invoice::TYPE_PAYMENT);
		$where[]='i.`Type` IN ('.\Pecee\DB\DB::JoinArray($types).')';
		$where[]=\Pecee\DB\DB::FormatQuery('i.`Deleted` = %s', array(FALSE));
		if(!is_null($recorded)) {
			$recorded=($recorded) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('i.`Recorded` = %s', array($recorded));
		}
		if(!is_null($fromDate)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`InvoiceID` IN (SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'invoicedate\' AND STR_TO_DATE(Value,\'%%d-%%m-%%Y\') >= DATE(%s))', array($fromDate));
		}
		if(!is_null($toDate)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`InvoiceID` IN (SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'invoicedate\' AND STR_TO_DATE(Value,\'%%d-%%m-%%Y\') <= DATE(%s))', array($toDate));
		}
		return self::FetchPage('i.`InvoiceID`', 'SELECT  i.* FROM `Invoice` i WHERE '. join(' && ', $where) .' ORDER BY 
				IFNULL(CAST((SELECT (`Value`+0.50) FROM `InvoiceData` WHERE `Key` IN (\'payedinvoiceid\',\'creditedinvoiceid\') AND `InvoiceID` = i.`InvoiceID` LIMIT 1) AS SIGNED), i.InvoiceID) DESC', $rows, $page);
	}
	
	/**
	 * Get invoices by search
	 * @param int|null $customerId
	 * @param int|null $invoiceId
	 * @param \Pecee\Util\Map|null $data
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Invoice
	 */
	public static function GetBySearch($customerId=NULL, $invoiceId=NULL, \Pecee\Util\Map $data=NULL, $orderType=NULL, $order=NULL, $rows=25, $page=0) {
		$order=(!is_null($order) && in_array($order, self::$ORDER_TYPES)) ? $order : self::ORDER_TYPE_DESC;
		$where=array('1=1');
		if(!is_null($customerId) && $customerId) {
			$where[]=sprintf('i.`CustomerID` LIKE \'%s\'', \Pecee\DB\DB::Escape($customerId));
		}
		if(!is_null($invoiceId) && $invoiceId) {
			$where[]=sprintf('i.`InvoiceID` LIKE \'%s\'', \Pecee\DB\DB::Escape($invoiceId));
		}
		if(!is_null($data) && $data) {
			$filter=array();
			foreach($data->getData() as $key=>$val) {
				if($val) {
					$filter[]=sprintf('IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'%s\' && `InvoiceID` = i.`InvoiceID` && `Value` LIKE \'%s\' LIMIT 1), FALSE)', \Pecee\DB\DB::Escape($key,FALSE), \Pecee\DB\DB::Escape($val,FALSE));
				}
			}
			if(count($filter) > 0) {
				$where[]=join(' && ', $filter);
			}
		}
		$where[]='i.`Type` IN('.\Pecee\DB\DB::JoinArray(array(self::TYPE_INVOICE,self::TYPE_CREDITMEMO)).')';
		return self::FetchPage('i.`InvoiceID`', 'SELECT * FROM `Invoice` i JOIN `InvoiceData` id ON(id.`InvoiceID` = i.`InvoiceID`) WHERE '.join(' && ', $where).' GROUP BY i.`InvoiceID` ORDER BY ' . self::GetOrder($orderType, $order));
	}
	
	public static function GetPayedInvoices(Model_Company $company, $fromDate=NULL, $toDate=NULL, $recorded=NULL, $orderType=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$order=(!is_null($order) && in_array($order, self::$ORDER_TYPES)) ? $order : self::ORDER_TYPE_DESC;
		$where=array('1=1');
		
		if(!is_null($fromDate)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`InvoiceID` IN (SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'invoicedate\' AND STR_TO_DATE(Value,\'%%d-%%m-%%Y\') >= DATE(%s))', array($fromDate));
		}
		if(!is_null($toDate)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`InvoiceID` IN (SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'invoicedate\' AND STR_TO_DATE(Value,\'%%d-%%m-%%Y\') <= DATE(%s))', array($toDate));
		}
		
		if(!is_null($recorded)) {
			$recorded=($recorded) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('i.`Recorded` = %s', array($recorded));
		}
		
		// TODO: Better handling of creditmemo types
		$where[]='i.`Type` IN('.\Pecee\DB\DB::JoinArray(array(self::TYPE_INVOICE)).')';
		$where[]=\Pecee\DB\DB::FormatQuery('((SELECT IFNULL((SELECT ABS(IFNULL(SUM(`Total`), (`Count` * Price - (`Count` * Price - (`Count` * Price * 1.'.$company->getVatPercentage().'))))) FROM InvoiceItem WHERE `InvoiceID` IN(SELECT pi.`InvoiceID` FROM `Invoice` pi JOIN `InvoiceData` pid
												ON(pid.`InvoiceID` = pi.`InvoiceID`) WHERE pi.`Type` IN ('.\Pecee\DB\DB::JoinArray(array(self::TYPE_PAYMENT)).')
												&& pid.`Key` IN (\'payedinvoiceid\', \'creditedinvoiceid\') && pid.`Value` = i.`InvoiceID`)),0)) >=
												(SELECT ABS(SUM(ROUND(IFNULL(`Total`, (`Count` * Price - (`Count` * Price - (`Count` * Price * 1.'.$company->getVatPercentage().'))))))) as `Total` FROM InvoiceItem WHERE InvoiceID = i.`InvoiceID`) 
												|| i.`Type` = \''.self::TYPE_CREDITMEMO.'\')');
		return self::FetchPage('i.`InvoiceID`', 'SELECT i.* FROM `Invoice` i WHERE '. join(' && ', $where) .' GROUP BY i.`InvoiceID` ORDER BY ' . self::GetOrder($orderType, $order), $rows, $page);
	}
	
	public static function GetLatestInvoices(Model_Company $company, $orderType=NULL, $order=NULL, $rows=NULL,$page=NULL) {
		$orderType = (!is_null($orderType) && in_array($orderType, self::$ORDER_TYPES)) ? $orderType : self::ORDER_DUEDATE;
		$order=(!is_null($order) && in_array($order, self::$ORDER_TYPES)) ? $order : self::ORDER_TYPE_ASC;
		$where=array('1=1');
		$where[]='i.`Type` IN('.\Pecee\DB\DB::JoinArray(array(self::TYPE_INVOICE)).')';
		$where[]=\Pecee\DB\DB::FormatQuery('((SELECT IFNULL((SELECT ABS(IFNULL(SUM(`Total`), (`Count` * Price - (`Count` * Price - (`Count` * Price * 1.'.$company->getVatPercentage().'))))) FROM InvoiceItem WHERE `InvoiceID` IN(SELECT pi.`InvoiceID` FROM `Invoice` pi JOIN `InvoiceData` pid
												ON(pid.`InvoiceID` = pi.`InvoiceID`) WHERE pi.`Type` IN ('.\Pecee\DB\DB::JoinArray(array(self::TYPE_PAYMENT,self::TYPE_CREDITMEMO)).') 
												&& pid.`Key` IN (\'payedinvoiceid\', \'creditedinvoiceid\') && pid.`Value` = i.`InvoiceID`)), 0)) 
												<
												(SELECT ABS(SUM(ROUND(IFNULL(`Total`, (`Count` * Price - (`Count` * Price - (`Count` * Price * 1.'.$company->getVatPercentage().'))))))) as `Total` FROM InvoiceItem WHERE InvoiceID = i.`InvoiceID`))');
		
		$where[]='( (IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'duedate\' && `InvoiceID` = i.`InvoiceID` && `Value` != \'\' && STR_TO_DATE(`Value`,\'%d-%m-%Y\') > NOW() LIMIT 1), FALSE)) || ' .
			'(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'duedate\' && `InvoiceID` = i.`InvoiceID` && `Value` != \'\' && STR_TO_DATE(`Value`,\'%d-%m-%Y\') < NOW() LIMIT 1), FALSE)) && ' .
			sprintf('(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'reminderdate\' && `InvoiceID` = i.`InvoiceID` && (`Value` = \'\' || `Value` != \'\' && STR_TO_DATE(`Value`,\'%%d-%%m-%%Y\') < NOW()) LIMIT 1), FALSE)) && ', $company->getReminderDays()) .
			sprintf('(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'firstreminderdate\' && `InvoiceID` = i.`InvoiceID` && (`Value` = \'\' || `Value` != \'\' && STR_TO_DATE(`Value`,\'%%d-%%m-%%Y\') < NOW()) LIMIT 1), FALSE)) && ', $company->getFirstReminderDays()) .
			sprintf('(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'secondreminderdate\' && `InvoiceID` = i.`InvoiceID` && (`Value` = \'\' || `Value` != \'\' && STR_TO_DATE(`Value`,\'%%d-%%m-%%Y\') < NOW()) LIMIT 1), FALSE)) ) ', $company->getSecondReminderDays());
		return self::FetchPage('i.`InvoiceID`', 'SELECT i.* FROM `Invoice` i WHERE '. join(' && ', $where) .' GROUP BY i.`InvoiceID` ORDER BY ' . self::GetOrder($orderType, $order), $rows, $page);
	}
	
	public static function GetOverdueInvoices(Model_Company $company, $orderType=NULL, $order=NULL, $rows=NULL,$page=NULL) {
		$order=(!is_null($order) && in_array($order, self::$ORDER_TYPES)) ? $order : self::ORDER_TYPE_DESC;
		$where=array('1=1');
		$where[]=\Pecee\DB\DB::FormatQuery('(SELECT IFNULL((SELECT ABS(IFNULL(SUM(`Total`), (`Count` * Price - (`Count` * Price - (`Count` * Price * 1.'.$company->getVatPercentage().'))))) FROM InvoiceItem WHERE `InvoiceID` IN(SELECT pi.`InvoiceID` FROM `Invoice` pi JOIN `InvoiceData` pid
												ON(pid.`InvoiceID` = pi.`InvoiceID`) WHERE pi.`Type` IN ('.\Pecee\DB\DB::JoinArray(array(self::TYPE_PAYMENT,self::TYPE_CREDITMEMO)).') 
												&& pid.`Key` IN (\'payedinvoiceid\', \'creditedinvoiceid\') && pid.`Value` = i.`InvoiceID`)),0)) <
												(SELECT ABS(SUM(ROUND(IFNULL(`Total`, (`Count` * Price - (`Count` * Price - (`Count` * Price * 1.'.$company->getVatPercentage().')))/-1)))) as `Total` FROM InvoiceItem WHERE InvoiceID = i.`InvoiceID`)');
		$where[]='(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'duedate\' && `InvoiceID` = i.`InvoiceID` && `Value` != \'\' && STR_TO_DATE(`Value`,\'%d-%m-%Y\') < NOW() LIMIT 1), FALSE))';
		$where[]=sprintf('(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'reminderdate\' && `InvoiceID` = i.`InvoiceID` && (`Value` = \'\' || `Value` != \'\' && DATE_ADD(STR_TO_DATE(`Value`,\'%%d-%%m-%%Y\'),INTERVAL %s DAY) < NOW()) LIMIT 1), FALSE))', $company->getReminderDays());
		$where[]=sprintf('(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'firstreminderdate\' && `InvoiceID` = i.`InvoiceID` && (`Value` = \'\' || `Value` != \'\' && DATE_ADD(STR_TO_DATE(`Value`,\'%%d-%%m-%%Y\'),INTERVAL %s DAY) < NOW()) LIMIT 1), FALSE))', $company->getFirstReminderDays());
		$where[]=sprintf('(IFNULL((SELECT `InvoiceID` FROM `InvoiceData` WHERE `Key` = \'secondreminderdate\' && `InvoiceID` = i.`InvoiceID` && (`Value` = \'\' || `Value` != \'\' && DATE_ADD(STR_TO_DATE(`Value`,\'%%d-%%m-%%Y\'),INTERVAL %s DAY) < NOW()) LIMIT 1), FALSE))', $company->getSecondReminderDays());
		return self::FetchPage('i.`InvoiceID`', 'SELECT i.* FROM `Invoice` i WHERE '. join(' && ', $where) .' GROUP BY i.`InvoiceID` ORDER BY ' . self::GetOrder($orderType, $order), $rows, $page);
	}
}