<?php
class Model_Case extends \Pecee\Model\Model {
	protected $childs=array();
	protected $subject;
	public function __construct() {
		parent::__construct('Case', array('CaseID' => NULL, 'ParentCaseID' => NULL, 'CaseSubjectID' => NULL, 'CustomerID' => NULL, 'UserID' => NULL, 'Text' => '', 
											'CreatedDate' => \Pecee\Date::ToDateTime(), 'Closed' => FALSE, 'Deleted' => FALSE));
	}
	
	/**
	 * Get subject
	 * @return Model_Case_Subject
	 */
	public function getSubject() {
		if(!$this->subject && $this->CaseSubjectID) {
			$this->subject=Model_Case_Subject::GetById($this->CaseSubjectID);
		}
		return $this->subject;
	}
	
	public function save() {
		$this->CaseID=parent::save()->getInsertId();
	}
	
	public function getChildren($rows=NULL, $page=NULL) {
		$key=sprintf('%s%s', $rows, $page);
		if(!isset($this->childs[$key])) {
			$this->childs[$key]=self::Get($this->CustomerID, $this->CaseID, NULL, FALSE, $rows, $page);
		}
		return $this->childs[$key];
	}
	
	/**
	 * Get by id
	 * @param int $caseId
	 * @return Model_Case
	 */
	public static function GetById($caseId) {
		return self::FetchOne('SELECT c.* FROM `Case` c WHERE c.`CaseID` = %s', $caseId);
	}
	
	/**
	 * Get by company list
	 * @param int $customerId
	 * @param int|null $closed
	 * @param int|null $deleted
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Case
	 */
	public static function Get($customerId, $parentCaseId=NULL, $closed=NULL, $deleted=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		if(!is_null($closed)) {
			$closed=($closed) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('c.`Closed` = %s', array($closed));
		}
		$deleted=(is_null($deleted)) ? FALSE : $deleted;
		if(!is_null($deleted)) {
			$deleted=($deleted) ? TRUE : FALSE;
			$where[]=\Pecee\DB\DB::FormatQuery('c.`Deleted` = %s', array($deleted));
		}
		if(!is_null($parentCaseId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('c.`ParentCaseID` = %s', array($parentCaseId));
		} else {
			$where[]='ISNULL(c.`ParentCaseID`)';
		}
		return self::FetchPage('c.`CaseID`', 'SELECT c.* FROM `Case` c WHERE c.`CustomerID` = %s && '.join(' && ', $where). ' ORDER BY c.`CreatedDate` DESC', $rows, $page, $customerId);
	}
}