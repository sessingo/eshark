<?php
class Model_User extends \Pecee\Model\User\ModelUser {
	const TYPE_ADMIN='admin';
	const TYPE_EMPLOYEE='employee';
	public static $TYPES=array(self::TYPE_ADMIN,self::TYPE_EMPLOYEE);
	
	public function delete() {
		parent::delete();
	}
			
	protected function signIn($cookieExp){
		$user = array($this->UserID, $this->Password, md5(microtime()), $this->Username, $this->AdminLevel, $this->data->Type);
		$ticket = \Pecee\Mcrypt::Encrypt(join('|',$user), self::GenerateLoginKey() );
		\Pecee\Cookie::Create('ticket', $ticket, $cookieExp);
	}
	
	/**
	 * Get current user
	 * @return \Pecee\Model\User\ModelUser
	 */
	public static function Current($setData=FALSE) {
		if(self::IsLoggedIn()){
			$ticket = \Pecee\Cookie::Get('ticket');
			if(trim($ticket) != ''){
				$ticket = \Pecee\Mcrypt::Decrypt($ticket, self::GenerateLoginKey() );
				$user = explode('|', $ticket);
				if(is_array($user)) {
					if($setData) {
						return self::GetByUserID($user[0]);
					} else {
						$caller=get_called_class();
						$obj=new $caller();
						$obj->setRow('UserID', $user[0]);
						$obj->setRow('Password', $user[1]);
						$obj->setRow('Username', $user[3]);
						$obj->setRow('AdminLevel', $user[4]);
						if(isset($user[5])) {
							$obj->setDataValue('type',trim($user[5]));
						}
						return $obj;
					}
				}
			}
		}
		return NULL;
	}
}