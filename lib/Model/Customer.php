<?php
class Model_Customer extends \Pecee\Model\ModelData {
	public function __construct() {
		parent::__construct('Customer', array('CustomerID' => NULL, 'UserID' => NULL, 'CreatedDate' => \Pecee\Date::ToDateTime()));
	}
	
	/**
	 * Get logo
	 * @return Image|NULL
	 */
	public function getLogo() {
		if($this->data->logoFileId) {
			return Model_Image::GetById($this->data->logoFileId);
		}
		return NULL;
	}
	
	public function save() {
		$this->CustomerID=parent::save()->getInsertId();
		$this->updateData();
	}
	
	public function updateData() {
		/* Remove all data */
		if($this->CustomerID) {
			Model_Customer_Data::Clear($this->CustomerID);
			if($this->data) {
				foreach($this->data->getData() as $key=>$value) {
					$data=new Model_Customer_Data();
					$data->setCustomerID($this->CustomerID);
					$data->setKey($key);
					$data->setValue($value);
					$data->save();
				}
			}
		}
	}
	
	public function delete() {
		Model_Customer_Data::Clear($this->CustomerID);
		parent::delete();
	}
	
	protected function fetchData($row) {
		$data=Model_Customer_Data::GetByCustomerId($row->CustomerID);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}
	
	/**
	 * Get by search
	 * @param int|null $customerId
	 * @param \Pecee\Util\Map|null $data
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Customer
	 */
	public static function GetBySearch($customerId = NULL, \Pecee\Util\Map $data=NULL, $rows=25, $page=NULL) {
		$where=array('1=1');
		if(!is_null($customerId) && $customerId) {
			$where[]=sprintf('c.`CustomerID` LIKE \'%s\'', \Pecee\DB\DB::Escape($customerId));
		}
		if(!is_null($data) && $data) {
			$filter=array();
			foreach($data->getData() as $key=>$val) {
				if($val) {
					$filter[]=sprintf('IFNULL((SELECT `CustomerID` FROM `CustomerData` WHERE `Key` = \'%s\' && `CustomerID` = c.`CustomerID` && `Value` LIKE \'%s\' LIMIT 1), FALSE)', \Pecee\DB\DB::Escape($key,FALSE), \Pecee\DB\DB::Escape($val,FALSE));
				}
			}
			if(count($filter) > 0) {
				$where[]=join(' && ', $filter);
			}
		}
		return self::FetchPage('c.`CustomerID`', 'SELECT c.* FROM `Customer` c JOIN `CustomerData` cd ON(cd.`CustomerID` = c.`CustomerID`) WHERE '.join(' && ', $where).' GROUP BY c.`CustomerID` ORDER BY c.`CustomerID` DESC');
	}
	
	/**
	 * Get customer by id
	 * @param int $customerId
	 * @return Model_Customer
	 */
	public static function GetById($customerId) {
		return self::FetchOne('SELECT * FROM `Customer` WHERE `CustomerID` = %s', $customerId);
	}
	
	/**
	 * Get list
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Customer
	 */
	public static function Get($rows=NULL, $page=NULL) {
		return self::FetchPage('c.`CustomerID`', 'SELECT c.* FROM `Customer` c ORDER BY CAST(COALESCE((SELECT `Value` FROM `CustomerData` WHERE `Key` = \'name\' && `CustomerID` = c.`CustomerID` LIMIT 1),0) AS SIGNED) ASC', $rows, $page);
	}
}