<?php
class Model_Item extends \Pecee\Model\ModelData {
	const ORDER_NAME_ASC='CAST(COALESCE((SELECT `Value` FROM `ItemData` WHERE `Key` = \'name\' && `ItemID` = i.`ItemID` LIMIT 1),0) AS SIGNED) ASC';
	public static $ORDERS=array(self::ORDER_NAME_ASC);
	public function __construct() {
		parent::__construct('Item', array('ItemID' => NULL, 'ItemGroupID' => NULL, 'UserID' => NULL, 'CreatedDate' => \Pecee\Date::ToDateTime(), 'AutoCreated' => NULL));
	}
	
	public function save() {
		if($this->ItemID) {
			$this->updateData();
			parent::update();
		} else {
			$this->ItemID=parent::save()->getInsertId();
			$this->updateData();
		}
	}
	
	public function updateData() {
		/* Remove all data */
		if($this->ItemID) {
			Model_Item_Data::Clear($this->ItemID);
			if($this->data) {
				foreach($this->data->getData() as $key=>$value) {
					$data=new Model_Item_Data();
					$data->setItemID($this->ItemID);
					$data->setKey($key);
					$data->setValue($value);
					$data->save();
				}
			}
		}
	}
	
	public function delete() {
		Model_Item_Data::Clear($this->ItemID);
		parent::delete();
	}
	
	protected function fetchData($row) {
		$data=Model_Item_Data::GetByItemId($row->ItemID);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}
	
	/**
	 * Get items
	 * @param int|null $itemGroupId
	 * @param string|null $order
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Customer
	 */
	public static function Get($itemGroupId=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		if(!is_null($itemGroupId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('i.`ItemGroupID` = %s', array($itemGroupId));
		}
		$order=(is_null($order) || !in_array($order, self::$ORDERS)) ? self::ORDER_NAME_ASC : $order;
		return self::FetchPage('i.`ItemID`', 'SELECT i.* FROM `Item` i WHERE ISNULL(i.`AutoCreated`) && '.join(' && ', $where) .' ORDER BY ' . $order, $rows, $page);
	}
	
	/**
	 * Get single item by id
	 * @param int $itemId
	 * @return Model_Item
	 */
	public static function GetById($itemId) {
		return self::FetchOne('SELECT * FROM `Item` WHERE `ItemID` = %s', $itemId);
	}
	
	/**
	 * Get single item by number
	 * @param string $number
	 * @return Model_Item
	 */
	public static function GetByNumber($number) {
		return self::FetchOne('SELECT i.* FROM `Item` i JOIN `ItemData` id ON(id.`ItemID` = i.`ItemID`) WHERE id.`Key` = \'number\' && id.`Value` = %s GROUP BY i.`ItemID`', $number);
	}
	
	/**
	 * Get by search
	 * @param int|null $itemId
	 * @param \Pecee\Util\Map|null $data
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Item
	 */
	public static function GetBySearch($itemId = NULL, \Pecee\Util\Map $data=NULL, $rows=15, $page=0) {
		$where=array('1=1');
		if(!is_null($itemId) && $itemId) {
			$where[]=sprintf('i.`ItemID` LIKE \'%s\'', \Pecee\DB\DB::Escape($itemId));
		}
		if(!is_null($data) && $data) {
			$filter=array();
			foreach($data->getData() as $key=>$val) {
				if($val) {
					$filter[]=sprintf('IFNULL((SELECT `ItemID` FROM `ItemData` WHERE ISNULL(`AutoCreated`) && `Key` = \'%s\' && `ItemID` = i.`ItemID` && `Value` LIKE \'%s\' LIMIT 1), FALSE)', \Pecee\DB\DB::Escape($key,FALSE), \Pecee\DB\DB::Escape($val,FALSE));
				}
			}
			if(count($filter) > 0) {
				$where[]=join(' || ', $filter);
			}
		}
		return self::FetchPage('i.`ItemID`', 'SELECT i.* FROM `Item` i JOIN `ItemData` id ON(id.`ItemID` = i.`ItemID`) WHERE ISNULL(i.`AutoCreated`) && '.join(' && ', $where).' GROUP BY i.`ItemID`');
	}
}