<?php
class Model_Invoice_Data extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('InvoiceData', array('InvoiceID' => NULL, 'Key' => '', 'Value' => ''));
	}
	
	public function save() {
		if($this->exists()) {
			parent::update();
		} else {
			parent::save();
		}
	}
	
	public function exists() {
		return self::Scalar('SELECT `Key` FROM `InvoiceData` WHERE `Key` = %s AND `InvoiceID` = %s', $this->InvoiceID, $this->Key);
	}
	
	/**
	 * Get by customer id
	 * @param int $invoiceId
	 * @return Model_Invoice_Data
	 */
	public static function GetByInvoiceId($invoiceId) {
		return self::FetchAll('SELECT * FROM `InvoiceData` WHERE `InvoiceID` = %s', $invoiceId);
	}
	
	public static function Clear($invoiceId) {
		self::NonQuery('DELETE FROM `InvoiceData` WHERE `InvoiceID` = %s', $invoiceId);
	}
}