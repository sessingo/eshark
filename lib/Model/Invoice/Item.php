<?php
class Model_Invoice_Item extends \Pecee\Model\Model {
	protected $item;
	protected $updateItem=FALSE;
	public function __construct() {
		parent::__construct('InvoiceItem', array('InvoiceItemID' => NULL, 'InvoiceID' => NULL, 
													'ItemID' => NULL, 'Description' => '', 
													'Price' => 0, 'Count' => 1, 
													'Rebate' => NULL, 'Total' => NULL));
	}
	
	public function setTotal($total) {
		$this->Total = ($total) ? $total : NULL;
	}
	
	public function getPrice() {
		if($this->Total) {
			return $this->getTotal();
		}
		return $this->Price;
	}
	
	public function getTotal() {
		if($this->Total) {
			return $this->Total;
		}
		$total=$this->getPrice()*$this->getCount();
		if($this->getRebate()) {
			$total=$total-$this->getRebate();
		}
		return $total;
	}
	
	public function save() {
		if($this->updateItem && $this->item) {
			$this->item->save();
		}
		$this->ItemID=$this->item->getItemID();
		$this->InvoiceItemID=parent::save()->getInsertId();
	}
	
	public function getItem() {
		return $this->item;
	}
	
	public function setItem(Model_Item $item) {
		$this->updateItem=TRUE;
		$this->item=$item;
	}
	
	protected static function FetchItems(Model_Invoice_Item $rows) {
		if($rows->hasRows()) {
			/* @var $row Model_Invoice_Item */
			foreach($rows->getRows() as $key=>$row) {
				if($row->getItemID() != null) {
					$row->setItem(Model_Item::GetById($row->ItemID));
				}
			}
		}
		return $rows;
	}
	
	public static function FetchAll($query, $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		return self::FetchItems(parent::FetchAll($query, $args));
	}
	
	public static function FetchPage($countField, $query, $rows = 10, $page = 0, $args=NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		return self::FetchItems(parent::FetchPage($countField, $query, $rows = 10, $page = 0, $args));
	}
	
	public static function Clear($invoiceId) {
		return self::NonQuery('DELETE FROM `InvoiceItem` WHERE `InvoiceID` = %s', $invoiceId);
	}
	
	/**
	 * Get product items by invoice id
	 * @param int $invoiceId
	 * @return Model_Invoice_Item
	 */
	public static function GetByInvoiceId($invoiceId) {
		return self::FetchAll('SELECT * FROM `InvoiceItem` WHERE `InvoiceID` = %s', $invoiceId);
	}
}