<?php
class Model_Case_Subject extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('CaseSubject', array('CaseSubjectID' => NULL, 'Title' => ''));
	}
	
	/**
	 * Get all
	 * @return Model_Case_Subject
	 */
	public static function Get($rows=NULL,$page=NULL) {
		return self::FetchPage('`CaseSubjectID`', 'SELECT * FROM `CaseSubject` ORDER BY `Title` ASC', $rows, $page);
	}
	
	/**
	 * Get by case subject id.
	 * @param int $caseSubjectId
	 * @return Model_Case_Subject
	 */
	public static function GetById($caseSubjectId) {
		return self::FetchOne('SELECT * FROM `CaseSubject` WHERE `CaseSubjectID` = %s', $caseSubjectId);
	}
}