<?php
class Model_Image extends \Pecee\Model\File\ModelFile {
	protected $image;
	
	public function __construct($filename='', $path='') {
		parent::__construct($filename, $path);
	}
	
	public function getFullPath() {
		return \Pecee\Registry::GetInstance()->get('StoragePath', '') . parent::getFullPath();
	}
	
	public function update() {
		$test=parent::update();
		print_r($test);
	}
	
	public function getImage() {
		if(!$this->image) {
			$this->image=new \Pecee\Image(file_get_contents($this->getFullPath()), $this->getType());
		}
		return $this->image;
	}
	
	public function getInternalUrl($width=NULL,$height=NULL) {
		return \Pecee\Router::GetRoute('image', 'show', array($this->FileID, $width, $height)) . 'logo.jpg';
	}
}