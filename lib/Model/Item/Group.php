<?php
class Model_Item_Group extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('ItemGroup', array('ItemGroupID' => NULL, 'Name' => ''));
	}
	
	/**
	 * Get groups
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Item_Group
	 */
	public static function Get($rows=NULL, $page=NULL) {
		return self::FetchPage('`ItemGroupID`', 'SELECT * FROM `ItemGroup` ORDER BY `Name` ASC', $rows, $page);
	}
	
	/**
	 * Get by id
	 * @param int $itemGroupId
	 * @return Model_Item_Group
	 */
	public static function GetById($itemGroupId) {
		return self::FetchOne('SELECT * FROM `ItemGroup` WHERE `ItemGroupID` = %s', $itemGroupId);
	}
}