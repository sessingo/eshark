<?php
class Model_Item_Data extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('ItemData', array('ItemID' => NULL, 'Key' => '', 'Value' => ''));
	}
	
	public function save() {
		if($this->exists()) {
			parent::update();
		} else {
			parent::save();
		}
	}
	
	public function exists() {
		return self::Scalar('SELECT `Key` FROM `ItemData` WHERE `Key` = %s AND `ItemID` = %s', $this->ItemID, $this->Key);
	}
	
	/**
	 * Get by item id
	 * @param int $itemId
	 * @return Model_Item_Data
	 */
	public static function GetByItemId($itemId) {
		return self::FetchAll('SELECT * FROM `ItemData` WHERE `ItemID` = %s', $itemId);
	}
	
	public static function Clear($itemId) {
		self::NonQuery('DELETE FROM `ItemData` WHERE `ItemID` = %s', $itemId);
	}
}