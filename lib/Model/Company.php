<?php
class Model_Company extends \Pecee\Model\ModelData {
	public function __construct() {
		parent::__construct('Company', array('CompanyID' => NULL, 'Name' => '', 'Country' => '', 'VatPercentage' => 0, 'CreatedDate' => \Pecee\Date::ToDateTime(), 'Main' => FALSE));
	}
	
	public function getWorkhours() {
		return (!is_null($this->data->Workhours)) ? $this->data->Workhours : 7.30;
	}
	
	public function setLogoFileID($fileId) {
		$this->data->logo=$fileId;
	}
	
	public function getCurrency() {
		return \Pecee\Currency::GetCurrencyByCountry($this->Country);
	}
	
	public function getReminderDays() {
		return \Pecee\String::GetFirstOrValue($this->data->reminderDays, 10);
	}
	
	public function getFirstReminderDays() {
		return \Pecee\String::GetFirstOrValue($this->data->firstReminderDays, 10);
	}
	
	public function getSecondReminderDays() {
		return \Pecee\String::GetFirstOrValue($this->data->secondReminderDays, 14);
	}
	
	public function getDebtCollectionDays() {
		return \Pecee\String::GetFirstOrValue($this->data->debtCollectionDays, 21);
	}
	
	public function getPaymentDays() {
		return \Pecee\String::GetFirstOrValue($this->data->paymentDays, 12);
	}
	
	/**
	 * Get logo
	 * @return Image|NULL
	 */
	public function getLogo() {
		if($this->data->logo) {
			return Model_Image::GetById($this->data->logo);
		}
		return NULL;
	}
	
	public function save() {
		$this->CompanyID=parent::save()->getInsertId();
		$this->updateData();
	}
	
	public function updateData() {
		/* Remove all data */
		if($this->CompanyID) {
			Model_Company_Data::Clear($this->CompanyID);
			if($this->data) {
				foreach($this->data->getData() as $key=>$value) {
					$data=new Model_Company_Data();
					$data->setCompanyID($this->CompanyID);
					$data->setKey($key);
					$data->setValue($value);
					$data->save();
				}
			}
		}
	}
	
	public function delete() {
		Model_Company_Data::Clear($this->CompanyID);
		parent::delete();
	}
	
	protected function fetchData($row) {
		$data=Model_Company_Data::GetByCompanyId($row->CompanyID);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}
	
	/**
	 * Get main company
	 * @return Model_Company
	 */
	public static function GetMain() {
		return self::FetchOne('SELECT c.* FROM `Company` c WHERE c.`Main` = 1');
	}
}