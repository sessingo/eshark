<?php
class Controller_User extends \Pecee\Controller\Controller {	
	public function editView() {
		echo new Widget_User_Edit();
	}
	
	public function signoutView() {
		\Pecee\Session::Instance()->destroy('UserID');
		if(Model_User::IsLoggedIn()) {
			Model_User::Current()->signOut();
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
	}
}