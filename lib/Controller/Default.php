<?php
class Controller_Default extends \Pecee\Controller\Controller {
	public function indexView() {
		if(Model_User::IsLoggedIn()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('customer',''));
		} else {
			echo new Widget_Home();
		}
	}
	
	public function langView() {
		$locale=$this->getParam('locale');
		if(in_array($locale, Helper::$LOCALES)) {
			\Pecee\Cookie::Create('Locale', $locale);
		}
		\Pecee\Router::GoBack();
	}
}