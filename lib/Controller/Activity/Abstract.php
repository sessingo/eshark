<?php
abstract class Controller_Activity_Abstract extends \Pecee\Controller\Controller {
	protected function setDefaultTopMenu($active) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Activity/LogHours'), \Pecee\Router::GetRoute(NULL, 'index'));
		$menu->addItem($this->_('Activity/Overview'), \Pecee\Router::GetRoute(NULL, 'overview'));
		$menu->getItem($active)->addClass('active');
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
}