<?php
class Controller_Customer extends Controller_Customer_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(0);
		echo new Widget_Customer_Home();
	}
	public function createView() {
		$this->setDefaultTopMenu(1);
		echo new Widget_Customer_Create();
	}	
	public function editView($customerId) {
		$this->setDetailTopMenu($customerId);
		echo new Widget_Customer_Edit($customerId);
	}
	public function deleteView($customerId) {
		echo new Widget_Customer_Delete($customerId);
	}
	public function detailsView($customerId) {
		$this->setDetailTopMenu($customerId, 0);
		echo new Widget_Customer_Detail($customerId);
	}
	public function updatenoteJson() {
		$customerId=$this->getParam('customerId',FALSE);
		$note=$this->getPost('note',FALSE);
		$results=array('error'=>FALSE,'msg'=>'');
		if(!$customerId) {
			$results['error']=TRUE;
			$results['msg']='Required variable(s) missing';
			$this->asJSON($results);
			return;
		}
		$customer=Model_Customer::GetById($customerId);
		if($customer->hasRow()) {
			$customer->data->notes=$note;
			$customer->update();
		}else{
			$results['error']=TRUE;
			$results['msg']='Customer not found';
		}
		$this->asJSON($results);
	}
}