<?php
class Controller_Ajax extends \Pecee\Controller\Controller {
	public function __construct() {
		header('Content-Type: text/html; charset=utf-8');
	}
	public function dialogView() {
		$args=func_get_args();
		if(count($args) > 0) {
			$args=array_map('ucfirst', $args);
			$widget=sprintf('Widget_Ajax_Dialog_%s', join('_', $args));
			if(@class_exists($widget)) {
				$class=\Pecee\Router::LoadClass($widget);
				echo $class;
			}
		}
	}
}