<?php
class Controller_Voucher extends Controller_Voucher_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(0);
		echo new Widget_Voucher_Home();
	}
	public function editView($invoiceId) {
		$this->setDefaultTopMenu(0);
		echo new Widget_Invoice_Edit($invoiceId);
	}
	public function createView() {
		$this->setDefaultTopMenu(0);
		echo new Widget_Invoice_Create(Model_Invoice::TYPE_VOUCHER);
	}
	public function deleteView($invoiceId) {
		echo new Widget_Invoice_Delete($invoiceId);
	}
	public function recordedView($invoiceId) {
		echo new Widget_Invoice_Recorded($invoiceId);
	}
}