<?php
abstract class Controller_Admin_Abstract extends \Pecee\Controller\Controller {
	protected function setDefaultTopMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Admin/Menu/Company'), \Pecee\Router::GetRoute('admin','index'));
		$menu->addItem($this->_('Admin/Menu/Users'), \Pecee\Router::GetRoute('adminuser', 'index'));
		$menu->addItem($this->_('Admin/Menu/Cases'), \Pecee\Router::GetRoute('admincase', 'index'));
		$menu->addItem($this->_('Admin/Menu/Items'), \Pecee\Router::GetRoute('adminitem', 'index'));
		$menu->addItem($this->_('Admin/Menu/Accounts'), \Pecee\Router::GetRoute('adminaccount', 'index'));
	
		if(!is_null($active)) {
			$menu->getItem($active)->addClass('active');
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
}