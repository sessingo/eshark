<?php
abstract class Controller_Customer_Abstract extends \Pecee\Controller\Controller  {
	protected function setDefaultTopMenu($active) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Customer/Menu/Search'), \Pecee\Router::GetRoute('customer', ''));
		$menu->addItem($this->_('Customer/Menu/CreateCustomer'), \Pecee\Router::GetRoute('customer', 'create'));
		$menu->getItem($active)->addClass('active');
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	protected function setDetailTopMenu($customerId, $active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Customer/Menu/Information'), \Pecee\Router::GetRoute('customer', 'details', array($customerId)));
		$menu->addItem($this->_('Customer/Menu/Cases'), \Pecee\Router::GetRoute('customercase','index',array($customerId)));
		$menu->addItem($this->_('Customer/Menu/Projects'), \Pecee\Router::GetRoute('customerproject','list',array($customerId)));
		$menu->addItem($this->_('Customer/Menu/Activities'), \Pecee\Router::GetRoute('customeractivity','list',array($customerId)));
		$menu->addItem($this->_('Customer/Menu/Invoices'), \Pecee\Router::GetRoute('customerinvoice','list',array($customerId)));
		
		if(!is_null($active)) {
			$menu->getItem($active)->addClass('active');
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
}