<?php
class Controller_Item extends \Pecee\Controller\Controller {
	public function indexView($itemGroupId=NULL) {
		$this->setDefaultTopMenu(0);
		echo new Widget_Item_Home($itemGroupId);
	}
	
	public function addView($itemGroupId=NULL) {
		$this->setDefaultTopMenu(0);
		echo new Widget_Item_Add($itemGroupId);
	}
	
	public function editView($itemId) {
		$this->setDefaultTopMenu(0);
		echo new Widget_Item_Edit($itemId);
	}
	
	public function deleteView($itemId) {
		echo new Widget_Item_Delete($itemId);
	}
	
	public function searchJson() {
		$q=$this->getParam('q',FALSE);
		$result=array('Error' => TRUE, 'Msg' => '');
		if(!$this->hasParam('q')) {
			$result['Msg']='Missing required parameter (q)';
			$this->asJSON($result);
		}
		
		$data=new \Pecee\Util\Map();
		$data->number=$q.'%';
		$data->name=$q.'%';
		
		$items=Model_Item::GetBySearch($this->getParam('itemId',NULL), $data, 10, 0);
		$result=array();
		if($items->hasRows()) {
			foreach($items->getRows() as $item) {
				$result[]=array('ItemID' => intval($item->getItemID()), 'Name' => $item->getName(), 'Number' => $item->getNumber(), 'Unit' => $item->getUnit(), 'SalePrice' => $item->getSaleprice());
			}
		}
		$this->asJSON($result);
	}
	
	private function setDefaultTopMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Item/Overview'), \Pecee\Router::GetRoute('item', ''));
		if(!is_null($active)) {
			$menu->getItem($active)->addClass('active');
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
}