<?php
class Controller_Admincase extends Controller_Admin_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(2);
		$this->setLeftMenu(0);
		echo new Widget_Admin_Case_Subject_Home();
	}
	public function editView($caseSubjectId) {
		$this->setDefaultTopMenu(2);
		$this->setLeftMenu(0);
		echo new Widget_Admin_Case_Subject_Edit($caseSubjectId);
	}
	public function deleteView($caseSubjectId) {
		echo new Widget_Admin_Case_Subject_Delete($caseSubjectId);
	}
	
	private function setLeftMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addAttribute('class', 'sub-nav');
		$menu->addItem($this->_('Admin/Case/Subjects'), \Pecee\Router::GetRoute(NULL, 'index'));
	
		if(!is_null($active)) {
			$item=$menu->getItem($active);
			if($item) {
				$item->addClass('active');
			}
		}
	
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_LEFT);
	}
}