<?php
abstract class Controller_Invoice_Abstract extends \Pecee\Controller\Controller {
	protected function setDefaultTopMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Invoice/Payments'), \Pecee\Router::GetRoute(NULL, 'index'));
		$menu->addItem($this->_('Invoice/Search'), \Pecee\Router::GetRoute(NULL,'search'));
		
		if(!is_null($active)) {
			$menu->getItem($active)->addClass('active');
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	protected function setDefaultLeftMenu($active) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addAttribute('class', 'sub-nav');
		$menu->addItem($this->_('Invoice/Comming'), \Pecee\Router::GetRoute(NULL, 'index'));
		$menu->addItem($this->_('Invoice/Overdue'), \Pecee\Router::GetRoute(NULL, 'overdue'));
		$menu->addItem($this->_('Invoice/Payed'), \Pecee\Router::GetRoute(NULL, 'payed'));
		
		if(!is_null($active)) {
			$item=$menu->getItem($active);
			if($item) {
				$item->addClass('active');
			}
		}
		
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_LEFT);
	}
}