<?php
class Controller_Adminitem extends Controller_Admin_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(3);
		$this->setLeftMenu(0);
		echo new Widget_Admin_Item_Group_Home();
	}
	
	public function editView($itemGroupId) {
		$this->setDefaultTopMenu(3);
		$this->setLeftMenu(0);
		echo new Widget_Admin_Item_Group_Edit($itemGroupId);
	}
	public function deleteView($itemGroupId) {
		echo new Widget_Admin_Item_Group_Delete($itemGroupId);
	}
	
	private function setLeftMenu($selectedIndex=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addAttribute('class', 'sub-nav');
		$menu->addItem($this->_('Admin/Item/Categories'), \Pecee\Router::GetRoute(NULL, 'index'));
	
		if(!is_null($selectedIndex)) {
			$item=$menu->getItem($selectedIndex);
			if($item) {
				$item->addClass('active');
			}
		}
	
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_LEFT);
	}
}