<?php
class Controller_CustomerCase extends Controller_Customer_Abstract {
	public function indexView($customerId) {
		$this->setDetailTopMenu($customerId, 1);
		echo new Widget_Customer_Case_Home($customerId);
	}
	public function viewView($customerId,$caseId) {
		$this->setDetailTopMenu($customerId, 1);
		echo new Widget_Customer_Case_View($customerId,$caseId);
	}
	public function createView($customerId, $parentCaseId=NULL) {
		$this->setDetailTopMenu($customerId, 1);
		echo new Widget_Customer_Case_Create($customerId, $parentCaseId);
	}
}