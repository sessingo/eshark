<?php
class Controller_Activity extends Controller_Activity_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(0);
		echo new Widget_Activity_Home();
	}
	
	public function overviewView() {
		$this->setDefaultTopMenu(1);
		echo new Widget_Activity_Overview();
	}
	
	public function dateView() {
		$logs=Model_Project_Activity_Log::Get($this->getParam('date'), Model_User::Current()->getUserID());
		$this->asJSON($logs->getAsJsonObject());
	}
	
	public function deleteView($activityLogId) {
		echo new Widget_Activity_Delete($activityLogId);
	}
	
	public function getJson() {
		$projectId=$this->requiredParam('projectId');
		$activities=Model_Project_Activity::GetByProjectId($projectId);
		$this->asJSON($activities->getAsJsonObject());
	}
	
	public function logMinutesJson() {
		$month=$this->requiredParam('month');
		$year=$this->getParam('year', date('Y'));
		
		$date=strtotime(sprintf('01-%s-%s', $month, $year));
		$from=date('Y-m-d', $date);
		$to = date('Y-m-d',strtotime('-1 second',strtotime('+1 month',strtotime(date('m', $date).'/01/'.date('Y', $date). ' 00:00:00'))));
				
		$stats=Model_Project_Activity_Log::Get($from, $to);
		$total=array();
		if($stats->hasRows()) {
			foreach($stats->getRows() as $log) {
				if(isset($total[date('j', strtotime($log->getDate()))])) {
					$total[date('j', strtotime($log->getDate()))]+=floatval($log->getMinutes());
				} else {
					$total[date('j', strtotime($log->getDate()))]=floatval($log->getMinutes());
				}
			}
		}
		return $this->asJSON($total);
	}
}