<?php
class Controller_CustomerProject extends Controller_Customer_Abstract {
	public function listView($customerId) {
		$this->setDetailTopMenu($customerId, 2);
		echo new Widget_Customer_Project_Home($customerId);
	}
	public function editView($customerId,$projectId) {
		$this->setDetailTopMenu($customerId, 2);
		echo new Widget_Customer_Project_Edit($customerId, $projectId);
	}
	public function deleteView($projectId) {
		echo new Widget_Customer_Project_Delete($projectId);
	}
}