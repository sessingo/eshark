<?php
abstract class Controller_Abstract extends \Pecee\Controller\Controller {
	public function requiredParam($name,$default=NULL) {
		if(!$this->hasParam($name)) {
			die(sprintf('The param "%s" doesn\'t exist and is required.', $name));
		}
		return \Pecee\String::GetFirstOrValue($this->getParam($name), $default);
	}
}