<?php
class Controller_CustomerActivity extends Controller_Customer_Abstract {
	public function listView($customerId,$projectId=NULL) {
		$this->setDetailTopMenu($customerId, 3);
		echo new Widget_Customer_Activity_Home($customerId,$projectId);
	}
	public function createView($customerId, $projectId) {
		$this->setDetailTopMenu($customerId, 3);
		echo new Widget_Customer_Activity_Create($customerId, $projectId);
	}
	public function editView($customerId, $activityId) {
		$this->setDetailTopMenu($customerId, 3);
		echo new Widget_Customer_Activity_Edit($customerId,$activityId);
	}
	public function deleteView($activityId) {
		echo new Widget_Customer_Activity_Delete($activityId);
	}
}