<?php
class Controller_Project extends Controller_Abstract {
	public function getJson() {
		$customerId=$this->requiredParam('customerId');
		$active=$this->getParam('active', TRUE);
		$projects=Model_Project::GetByCustomerId($customerId, $active);
		$this->asJSON($projects->getAsJsonObject());
	}
}