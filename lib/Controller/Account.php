<?php
class Controller_Account extends \Pecee\Controller\Controller {
	public function indexView($accountId=NULL) {
		$this->setDefaultTopMenu(0);
		echo new Widget_Account_Home($accountId);
	}
	
	private function setDefaultTopMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem('Oversigt', \Pecee\Router::GetRoute('account', ''));
		if(!is_null($active)) {
			$menu->getItem($active)->addClass('active');
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
}