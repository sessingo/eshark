<?php
class Controller_Adminuser extends Controller_Admin_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(1);
		$this->setLeftMenu(0);
		echo new Widget_Admin_User_Home();
	}
	
	public function createView() {
		$this->setDefaultTopMenu(1);
		$this->setLeftMenu(0);
		echo new Widget_Admin_User_Create();
	}
	
	public function editView($userId) {
		$this->setDefaultTopMenu(1);
		$this->setLeftMenu(0);
		echo new Widget_Admin_User_Edit($userId);
	}
	
	public function deleteView($userId) {
		echo new Widget_Admin_User_Delete($userId);
	}
	
	private function setLeftMenu($selectedIndex=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addAttribute('class', 'sub-nav');
		$menu->addItem($this->_('Admin/User/Users'), \Pecee\Router::GetRoute(NULL, 'index'));
	
		if(!is_null($selectedIndex)) {
			$item=$menu->getItem($selectedIndex);
			if($item) {
				$item->addClass('active');
			}
		}
	
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_LEFT);
	}
}