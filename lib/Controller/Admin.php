<?php
class Controller_Admin extends Controller_Admin_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(0);
		$this->setCompanyLeftMenu(0);
		echo new Widget_Admin_Company_Edit();
	}
	
	private function setCompanyLeftMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addAttribute('class', 'sub-nav');
		$menu->addItem('Rediger firma', \Pecee\Router::GetRoute(NULL, 'index'));
	
		if(!is_null($active)) {
			$item=$menu->getItem($active);
			if($item) {
				$item->addClass('active');
			}
		}
	
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_LEFT);
	}
}