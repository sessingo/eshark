<?php
class Controller_Adminaccount extends Controller_Admin_Abstract {
	public function indexView() {
		$this->setDefaultTopMenu(4);
		$this->setLeftMenu(0);
		echo new Widget_Admin_Account_Home();
	}
	
	public function deleteView($accountId) {
		echo new Widget_Account_Delete($accountId);
	}
	
	private function setLeftMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addAttribute('class', 'sub-nav');
		$menu->addItem('Oversigt', \Pecee\Router::GetRoute(NULL, 'index'));
	
		if(!is_null($active)) {
			$item=$menu->getItem($active);
			if($item) {
				$item->addClass('active');
			}
		}
	
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_LEFT);
	}
}