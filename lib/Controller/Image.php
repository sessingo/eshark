<?php
class Controller_Image extends \Pecee\Controller\Controller {
	public function showView($fileId, $width=NULL, $height=NULL) {
		$file=Model_Image::GetById($fileId);
		if($file->hasRow()) {
			header(sprintf('Content-type: %s', $file->getType()));
			echo file_get_contents($file->getFullPath());
		}
	}
}