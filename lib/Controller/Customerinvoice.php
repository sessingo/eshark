<?php
class Controller_CustomerInvoice extends Controller_Customer_Abstract {
	public function listView($customerId) {
		$this->setDetailTopMenu($customerId, 4);
		echo new Widget_Customer_Invoice_Home($customerId);
	}
	public function createView($customerId) {
		$this->setDetailTopMenu($customerId, 4);
		$widget= new Widget_Invoice_Create();
		$widget->setCustomerId($customerId);
		echo $widget;
	}
	public function createMemoView($customerId) {
		$this->setDetailTopMenu($customerId, 4);
		$widget= new Widget_Invoice_Create_CreditMemo(Model_Invoice::TYPE_CREDITMEMO);
		$widget->setCustomerId($customerId);
		echo $widget;
	}
	public function editCreditMemoView($invoiceId) {
		$this->setDetailTopMenu($this->getParam('customerId'), 4);
		echo new Widget_Invoice_Edit_CreditMemo($invoiceId);
	}
	public function editView($invoiceId) {
		$this->setDetailTopMenu($this->getParam('customerId'), 4);
		echo new Widget_Invoice_Edit($invoiceId);
	}
	public function deleteView($invoiceId) {
		echo new Widget_Invoice_Delete($invoiceId);
	}
	public function recordedView($invoiceId) {
		echo new Widget_Invoice_Recorded($invoiceId);
	}
}