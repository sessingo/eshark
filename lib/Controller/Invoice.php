<?php
class Controller_Invoice extends Controller_Invoice_Abstract {
	const TYPE_PDF='pdf';
	public function indexView() {
		$this->setDefaultTopMenu(0);
		$this->setDefaultLeftMenu(0);
		echo new Widget_Invoice_Home();
	}
	public function searchView() {
		$this->setDefaultTopMenu(1);
		echo new Widget_Invoice_Search();
	}
	public function payedView() {
		$this->setDefaultTopMenu(0);
		$this->setDefaultLeftMenu(2);
		echo new Widget_Invoice_Payed();
	}
	public function overdueView() {
		$this->setDefaultTopMenu(0);
		$this->setDefaultLeftMenu(1);
		echo new Widget_Invoice_Overdue();
	}
	public function editView($invoiceId) {
		$this->setDefaultTopMenu(0);
		echo new Widget_Invoice_Edit($invoiceId);
	}
	public function deleteView($invoiceId) {
		echo new Widget_Invoice_Delete($invoiceId);
	}
	public function recordedView($invoiceId) {
		echo new Widget_Invoice_Recorded($invoiceId);
	}
	public function viewView($invoiceId,$format=NULL) {
		switch($format) {
			default:
				echo new Widget_Invoice_View($invoiceId);
				break;
			case self::TYPE_PDF:
				echo new Widget_Invoice_View_Pdf($invoiceId);
				break;
	
		}
	}
}