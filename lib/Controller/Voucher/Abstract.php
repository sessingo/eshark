<?php
abstract class Controller_Voucher_Abstract extends \Pecee\Controller\Controller {
	protected function setDefaultTopMenu($active=NULL) {
		$menu=new \Pecee\UI\Menu\Menu();
		$menu->addItem($this->_('Voucher/Overview'), \Pecee\Router::GetRoute(NULL, 'index'));
	
		if(!is_null($active)) {
			$menu->getItem($active)->addClass('active');
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
}