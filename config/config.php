<?php
const LibraryPath = '/var/www/Pecee';
require_once LibraryPath . '/lib/Registry.php';
require_once LibraryPath . '/lib/Language.php';
require_once LibraryPath . '/lib/Locale.php';
require_once 'Helper.php';
$key = \Pecee\Registry::GetInstance();

/* ---------- Configuration start ---------- */ 
/* Database  */
$key->Set('DBUsername', 'Eshark');
$key->Set('DBPassword', 'MV5xMyHWwR4zahHT');
$key->Set('DBHost', '10.0.0.41');
$key->Set('DBDatabase', 'EShark');

/* Configuration */
$key->Set('BuildVersion', 'B0.100');
$key->Set('RequireSSL', TRUE);
$key->Set('Debug', FALSE);

/* Language */
\Pecee\Locale::Instance()->setLocale(Helper::LOCALE_DA_DK);
\Pecee\Language::Instance()->setType(\Pecee\Language::TYPE_XML); // USE XML-ENGINE

/* Storage */
$key->Set('StoragePath', '/storage/eshark');
/* ---------- Configuration end ---------- */
