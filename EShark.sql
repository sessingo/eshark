-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 18. 04 2014 kl. 21:25:52
-- Serverversion: 5.5.35
-- PHP-version: 5.4.4-14+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `EShark`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Account`
--

CREATE TABLE IF NOT EXISTS `Account` (
  `AccountID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Bank` varchar(255) NOT NULL,
  `RegistrationNumber` int(4) NOT NULL,
  `AccountNumber` varchar(255) NOT NULL,
  `IBAN` varchar(255) DEFAULT NULL,
  `Swift` varchar(255) DEFAULT NULL,
  `PubDate` datetime NOT NULL,
  PRIMARY KEY (`AccountID`),
  KEY `UserID` (`UserID`),
  KEY `PubDate` (`PubDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Cache`
--

CREATE TABLE IF NOT EXISTS `Cache` (
  `Key` varchar(400) NOT NULL,
  `Data` longtext NOT NULL,
  `ExpireDate` int(11) NOT NULL,
  PRIMARY KEY (`Key`),
  KEY `ExpireDate` (`ExpireDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Case`
--

CREATE TABLE IF NOT EXISTS `Case` (
  `CaseID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentCaseID` bigint(20) DEFAULT NULL,
  `CaseSubjectID` bigint(20) DEFAULT NULL,
  `CustomerID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Text` longtext,
  `CreatedDate` datetime NOT NULL,
  `Closed` tinyint(1) DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CaseID`),
  KEY `ParentCaseID` (`ParentCaseID`),
  KEY `CaseSubjectID` (`CaseSubjectID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Closed` (`Closed`),
  KEY `Deleted` (`Deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CaseSubject`
--

CREATE TABLE IF NOT EXISTS `CaseSubject` (
  `CaseSubjectID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Title` varchar(300) NOT NULL,
  PRIMARY KEY (`CaseSubjectID`),
  KEY `Title` (`Title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Company`
--

CREATE TABLE IF NOT EXISTS `Company` (
  `CompanyID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(400) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `VatPercentage` int(11) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Main` tinyint(1) NOT NULL,
  PRIMARY KEY (`CompanyID`),
  KEY `Name` (`Name`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Main` (`Main`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CompanyData`
--

CREATE TABLE IF NOT EXISTS `CompanyData` (
  `CompanyID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext,
  KEY `CompanyID` (`CompanyID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Customer`
--

CREATE TABLE IF NOT EXISTS `Customer` (
  `CustomerID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `CreatedDate` (`CreatedDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CustomerData`
--

CREATE TABLE IF NOT EXISTS `CustomerData` (
  `CustomerID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext,
  KEY `CustomerID` (`CustomerID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Entity`
--

CREATE TABLE IF NOT EXISTS `Entity` (
  `EntityID` varchar(13) NOT NULL,
  `EntityCategoryID` varchar(13) DEFAULT NULL,
  `Title` varchar(500) NOT NULL,
  `Content` longtext NOT NULL,
  `PubDate` datetime NOT NULL,
  `ActiveFrom` datetime DEFAULT NULL,
  `ActiveTo` datetime DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`EntityID`),
  KEY `EntityCategoryID` (`EntityCategoryID`,`Title`,`PubDate`,`ActiveFrom`,`ActiveTo`,`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `EntityCategory`
--

CREATE TABLE IF NOT EXISTS `EntityCategory` (
  `EntityCategoryID` varchar(13) NOT NULL,
  `ParentEntityCategoryID` varchar(13) DEFAULT NULL,
  `Path` varchar(500) DEFAULT NULL,
  `Title` varchar(300) NOT NULL,
  `Description` longtext NOT NULL,
  `Order` int(11) NOT NULL,
  `PubDate` datetime NOT NULL,
  PRIMARY KEY (`EntityCategoryID`),
  KEY `ParentEntityCategoryID` (`ParentEntityCategoryID`),
  KEY `Path` (`Path`),
  KEY `Title` (`Title`),
  KEY `Order` (`Order`),
  KEY `PubDate` (`PubDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `EntityField`
--

CREATE TABLE IF NOT EXISTS `EntityField` (
  `EntityID` varchar(13) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Value` longtext NOT NULL,
  KEY `EntityID` (`EntityID`,`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `File`
--

CREATE TABLE IF NOT EXISTS `File` (
  `FileID` varchar(32) NOT NULL,
  `Filename` varchar(255) NOT NULL,
  `OriginalFilename` varchar(300) DEFAULT NULL,
  `Path` varchar(400) DEFAULT NULL,
  `Type` varchar(255) NOT NULL,
  `Bytes` bigint(20) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`FileID`),
  KEY `Type` (`Type`),
  KEY `Bytes` (`Bytes`),
  KEY `CreatedDate` (`CreatedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `FileData`
--

CREATE TABLE IF NOT EXISTS `FileData` (
  `FileID` varchar(32) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext,
  KEY `FileID` (`FileID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Invoice`
--

CREATE TABLE IF NOT EXISTS `Invoice` (
  `InvoiceID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CustomerID` bigint(20) DEFAULT NULL,
  `UserID` bigint(20) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Recorded` tinyint(1) NOT NULL,
  `Deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`InvoiceID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `Type` (`Type`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Recorded` (`Recorded`),
  KEY `Deleted` (`Deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `InvoiceData`
--

CREATE TABLE IF NOT EXISTS `InvoiceData` (
  `InvoiceID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext NOT NULL,
  KEY `InvoiceID` (`InvoiceID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `InvoiceItem`
--

CREATE TABLE IF NOT EXISTS `InvoiceItem` (
  `InvoiceItemID` bigint(20) NOT NULL AUTO_INCREMENT,
  `InvoiceID` bigint(20) NOT NULL,
  `ItemID` bigint(20) DEFAULT NULL,
  `Description` text,
  `Count` float NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `Rebate` float DEFAULT NULL,
  `Total` float DEFAULT NULL,
  PRIMARY KEY (`InvoiceItemID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `ItemID` (`ItemID`),
  KEY `Count` (`Count`),
  KEY `Price` (`Price`),
  KEY `Rebate` (`Rebate`),
  KEY `Total` (`Total`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Item`
--

CREATE TABLE IF NOT EXISTS `Item` (
  `ItemID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ItemGroupID` bigint(20) DEFAULT NULL,
  `UserID` bigint(20) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `AutoCreated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ItemID`),
  KEY `ItemGroupID` (`ItemGroupID`),
  KEY `UserID` (`UserID`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `AutoCreated` (`AutoCreated`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ItemData`
--

CREATE TABLE IF NOT EXISTS `ItemData` (
  `ItemID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` text NOT NULL,
  KEY `ItemID` (`ItemID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ItemGroup`
--

CREATE TABLE IF NOT EXISTS `ItemGroup` (
  `ItemGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) NOT NULL,
  PRIMARY KEY (`ItemGroupID`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Language`
--

CREATE TABLE IF NOT EXISTS `Language` (
  `LanguageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OriginalText` longtext NOT NULL,
  `TranslatedText` longtext NOT NULL,
  `Locale` varchar(5) NOT NULL,
  `PageCode` varchar(32) DEFAULT NULL,
  `Path` varchar(350) NOT NULL,
  PRIMARY KEY (`LanguageID`),
  KEY `PageCode` (`PageCode`),
  KEY `Path` (`Path`),
  KEY `Locale` (`Locale`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Project`
--

CREATE TABLE IF NOT EXISTS `Project` (
  `ProjectID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CustomerID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `Description` longtext,
  `CreatedDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`ProjectID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ProjectActivity`
--

CREATE TABLE IF NOT EXISTS `ProjectActivity` (
  `ProjectActivityID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProjectID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `Description` longtext,
  `MaxHours` int(4) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`ProjectActivityID`),
  KEY `ProjectID` (`ProjectID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `MaxHours` (`MaxHours`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ProjectActivityLog`
--

CREATE TABLE IF NOT EXISTS `ProjectActivityLog` (
  `ProjectActivityLogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProjectActivityID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Minutes` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Description` longtext,
  PRIMARY KEY (`ProjectActivityLogID`),
  KEY `ProjectActivityID` (`ProjectActivityID`),
  KEY `UserID` (`UserID`),
  KEY `Minutes` (`Minutes`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Rewrite`
--

CREATE TABLE IF NOT EXISTS `Rewrite` (
  `OriginalPath` varchar(500) NOT NULL,
  `RewritePath` varchar(500) NOT NULL,
  PRIMARY KEY (`OriginalPath`),
  KEY `RewritePath` (`RewritePath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Transfer`
--

CREATE TABLE IF NOT EXISTS `Transfer` (
  `TransferID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AccountID` int(11) DEFAULT NULL,
  `UserID` bigint(20) NOT NULL,
  `InvoiceID` bigint(20) DEFAULT NULL,
  `Text` text,
  `Amount` float NOT NULL,
  `Balance` float NOT NULL,
  `Date` date NOT NULL,
  `PubDate` datetime NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`TransferID`),
  KEY `AccountID` (`AccountID`),
  KEY `UserID` (`UserID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `Amount` (`Amount`),
  KEY `Balance` (`Balance`),
  KEY `Date` (`Date`),
  KEY `PubDate` (`PubDate`),
  KEY `Deleted` (`Deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Username` varchar(300) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `LastActivity` datetime DEFAULT NULL,
  `AdminLevel` tinyint(1) NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `Username` (`Username`),
  KEY `Password` (`Password`),
  KEY `AdminLevel` (`AdminLevel`),
  KEY `Deleted` (`Deleted`),
  KEY `LastActivity` (`LastActivity`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserConfirm`
--

CREATE TABLE IF NOT EXISTS `UserConfirm` (
  `UserConfirmID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email` varchar(500) NOT NULL,
  `Token` varchar(36) NOT NULL,
  `Activated` tinyint(1) NOT NULL,
  `Date` datetime NOT NULL,
  `IP` varchar(36) NOT NULL,
  PRIMARY KEY (`UserConfirmID`),
  KEY `Email` (`Email`),
  KEY `Token` (`Token`),
  KEY `Accepted` (`Activated`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserData`
--

CREATE TABLE IF NOT EXISTS `UserData` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` text NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserIncorrectLogin`
--

CREATE TABLE IF NOT EXISTS `UserIncorrectLogin` (
  `Username` varchar(300) NOT NULL,
  `RequestTime` int(11) NOT NULL,
  `IPAddress` varchar(32) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `Username` (`Username`),
  KEY `RequestTime` (`RequestTime`),
  KEY `IPAddress` (`IPAddress`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserPasswordReset`
--

CREATE TABLE IF NOT EXISTS `UserPasswordReset` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(32) NOT NULL,
  `Date` int(11) NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
