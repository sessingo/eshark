<?php
require_once '../config/init.php';
try {
	
	$locale= \Pecee\Cookie::Get('Locale', FALSE);
	if($locale && in_array($locale, Helper::$LOCALES)) {
		\Pecee\Locale::Instance()->setLocale($locale);
	} elseif(in_array(\Pecee\Browser::GetLocale(), Helper::$LOCALES)) {
		\Pecee\Locale::Instance()->setLocale(\Pecee\Browser::GetLocale());
	}
	
	$auth = \Pecee\Auth::GetInstance();
	$auth->setAdminIP('127.0.0.1');
	$auth->setAdminIP('87.63.82.2');
	$auth->setAdminIP('89.150.71.196');
	$router = \Pecee\Router::GetInstance();
	$router->addAlias(new Router_Alias());
	$router->routeRequest();
} catch(\Pecee\Auth\AuthException $e) {
	$timeout=FALSE;
	if(\Pecee\Session::Instance()->exists('UserID')) {
		\Pecee\Session::Instance()->destroy('UserID');
		$timeout=TRUE;
	}
	echo new Widget_Page_Error_403($timeout);
} catch (\Pecee\Router\RouterException $e) {
	if($e->getCode()==404) {
		echo new Widget_Page_Error_404();
	} else {
		echo new Widget_Page_Error($e);
	}
} catch (Exception $e) {
	if(\Pecee\Registry::GetInstance()->get('Debug',FALSE)) {
		throw $e;
	}
	echo new Widget_Page_Error($e);
}