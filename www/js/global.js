function global() {};
global.prototype={
	options: {},
	construct: function(options) {
		$.extend(this.options, options);
		$('a[rel="dialog"]').live('click',function(e) {
			e.preventDefault();
			dialog.show({	
							url: $(this).attr('href'),
							type: dialog.dialogTypes.ajax
						});
		});
		$('a[rel="post-dialog"]').live('click',function(e) {
			e.preventDefault();
			var d=$(this).data();
			d.autoPostback=true;
			var href=$(this).attr('href');
			$.ajax({ 	
						url: href, 
						success: function(r) {
							dialog.show({	
											type: dialog.dialogTypes.html,
											html: r
										});
						},
						type: 'POST', 
						data: d
				   });
			
		});
		$('a[rel*=new]').live('click',function(e) {
			e.preventDefault();
			window.open($(this).attr('href'));
		});
		
		dialog.init();
		this.dialog.bindForm();
	},
	dialog: {
		o: { confirmCallback: null },
		confirm: function(msg, fn) {
			this.o.confirmCallback=fn;
			dialog.show({ 	url: '/ajax/dialog/confirm/?msg='+  encodeURIComponent(msg),
							type: dialog.dialogTypes.ajax
						});
		},
		bindForm: function() {
			$('.dialog:last form:first').live('submit', function(e) {
				if(e.isTrigger == null) {
					e.preventDefault();
					var action=$(this).attr('action');
					var c=$('#dialog_content');
					var d=$('.dialog:last');
					if(action) {
						var data=$(this).serialize();
						c.find('.inner').html('');
						d.addClass('loading');
						dialog.center();
						$.ajax({ url: action, cache: false, type: 'POST', data: data, success: function(r) {
							d.removeClass('loading');
							c.html(r);
							dialog.center();
						} });
					}
				}
			});
		}
	}
};