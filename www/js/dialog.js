var dialog = {
	dialogTypes: {
		ajax: 'ajax',
		html: 'html',
		iframe: 'iframe'
	},
	settings: {
		fadeInTimeout: 200,
		fadeOutTimeout: 110,
		type: null,
		html: '',
		url: '',
		title: null,
		text: {
			close: ''
		},
		onLoad: null,
		onClose: null,
		onClosed: null
	},
	init: function() {
		this.settings.type = this.dialogTypes.ajax;
		$(document).live('keydown',function(e) {
			var code=(e.which) ? e.which : e.keyCode;
			if(code != null && code==27) {
				dialog.close();
			}
		});
		$('.dialog .contents a[rel="dialog"]').live('click', function(e) {
			dialog.replace($(this).attr('title'), $(this).attr('href'));
			e.preventDefault();
		});
		$('.dialog .container div.close a').live('click', function(e) {
			dialog.close();
			e.preventDefault();
		});
		return this;
	},
	overlay: {
		close: function() {
			var s=dialog.settings;
			var onclose=s.onClose;
			if(onclose!=null) {
				onclose(dialog);
			}
			var o = $('body div.screenoverlay');
			o.fadeOut(s.fadeOutTimeout, function() {
				o.remove();
				var c=s.onClosed;
				if(c!=null) {
					c(dialog);
				}
			});
		},
		show: function(fn) {
			var o = $('body div.screenoverlay');
			if(o.length) {
				fn();
			} else {
				$('body').prepend('<div class="screenoverlay dialog-bg"></div>');
				$('body div.screenoverlay').fadeIn(dialog.settings.fadeInTimeout, function() {
					fn();
				});
			}
		}
	},
	show: function(settings) {
		$.extend(this.settings, settings);
		this.start();
	},
	replace: function(settings) {
		 $('.dialog').remove();
		 this.show(settings);
	},
	start: function() {
		var self=this;
		this.overlay.show(function() {
			var s=self.settings;
			$('body div.screenoverlay').prepend('<div class="dialog"><div class="container"></div></div>');
			var d = $('.dialog');
			if(s.title != null) {
				d.find('.container').append('<div class="header"><h3>'+s.title+'</h3> <div class="close"><a href="#">'+s.text.close+'</a></div></div>');
			}
			var c = self.getContent();
			d.find('.container').append('<div class="contents" id="dialog_content">'+c+'</div>');
			self.center();
			$(window).resize(function() {
				self.center();
			});
			d.fadeIn(s.fadeInTimeout);
			var load=s.onLoad;
			if(load!=null) {
				load(self);
			}
		});
	},
	close: function() {
		this.overlay.close();
		$('.dialog').remove();
	},
	center: function() {
		var e = $('.dialog');
		e.css({ 'position': 'absolute', 'left': ($(window).width()-e.outerWidth())/2+$(window).scrollLeft(), 'top': $(window).scrollTop() });
	    $('.screenoverlay').css({ height: $(document).height(), width: $(document).width() });
	},
	setUrl: function(url) {
		this.settings.url = url;
	},
	getContent: function() {
		var s=this.settings;
		var t=this.dialogTypes;
		switch(s.type) {
			default:
			case t.ajax: {
				var o = '';
				$.ajax({url: this.getUrl(), 
					async:false,
					success: function(r) {
						o=r;
					}, error: function(r,t,e) {
						o=e;
					}
				});
				return o;
				break;
			}
			case t.html:
				return s.html;
				break;
			case t.iframe: {
				return '<iframe src="'+this.getUrl()+'" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
				break;
			}
		}
	},
	getUrl: function() {
		return this.settings.url;
	},
	setHTML: function(html) {
		this.settings.html = html;
	}
};