var tooltip = {
	timer: null,
	o: null,
	init: function() {
		this.o={ el: null, text: '', customClass: '', previousClass: '', timeout: 2000, position: '' };
	},
	getPosition: function(el, tipWidth, elWidth) {
		var o=this.o;
		var offset=el.offset();
		var left=offset.left;
		switch(o.position)
        {
           case 'top':
              return (left - (tipWidth / 2) + (elWidth / 2));
           case 'left':
              return (left - tipWidth);
           case 'right':
              return (left + elWidth);
        };
        return (left - (tipWidth / 2) + (elWidth / 2)); 
	},
	bind: function(options) {
		this.init();
		var self=this;
		var o=$.extend(this.o, options);
		o.el.bind('mouseenter', function() {
			var timer=self.timer;
			clearTimeout(timer);
			var tip=$('.tooltip');
			if(tip.length==0) {
				tip=$('<div class="tooltip"></div>');
				$('body').prepend(tip);
			}
			if(o.previousClass != null) {
				tip.removeClass(o.previousClass);
			}
			if(o.customClass != null) {
				tip.addClass(o.customClass);
				o.previousClass=o.customClass;
			}
			
			tip.html('<div class="txt"><span></span>'+o.text+'</div>');
			var s=tip.find('span');
			s.css({ left: (tip.width()/2)-(s.outerWidth()/2) });
			
			var left=self.getPosition(o.el, tip.outerWidth(), o.el.outerWidth());
			var offset=o.el.offset();
			tip.css({ left: left, top: offset.top });
			tip.fadeIn(200);
		}).bind('mouseleave', function() {
			var tip=$('.tooltip');
			clearTimeout(self.timer);
			self.timer=setTimeout(function() {
				tip.fadeOut(150, function() {
					$(this).remove();
				});
			}, o.timeout);
		});
	},
	hide: function() {
		$('.tooltip').fadeOut(150, function() {
			$(this).remove();
		});
	}
};