function customer() { }

customer.prototype={
	options: {},
	widget: null,
	customerId: null,
	construct: function() {
		this.generel();
	},
	edit: function(customerId) {
		this.customerId=customerId;
		this.generel();
	},
	setMenu: function(index) {
		var menu=$('ul.sub-nav');
		menu.find('li.active').removeClass('active');
		menu.find('li:eq('+index+')').addClass('active');
	},
	generel: function() {
		var self=this;
		this.widget=new $p.Widget($.generel, $('#customer'));
		if(this.customerId != null) {
			$.getJSON('/customer/get/'+this.customerId+'.json', function(r) {
				if(r != null) {
					self.widget.setData(r);
					self.widget.render();
					self.setMenu(0);
				}
			});
		} else {
			this.widget.render();
			this.setMenu(0);
		}
	},
	projects: function() {
		var self=this;
		this.widget=new $p.Widget($.projects, $('#customer'));
		$.getJSON('/customer/projects/'+this.customerId+'.json', function(r) {
			self.widget.setData(r);
			self.widget.render();
			self.setMenu(1);
		});
		
	},
	activities: function() {
		this.widget=new $p.Widget($.activities, $('#customer'));
		this.widget.render();
		this.setMenu(2);
	},
	economy: function() {
		this.widget=new $p.Widget($.economy, $('#customer'));
		this.widget.render();
		this.setMenu(3);
	}
};