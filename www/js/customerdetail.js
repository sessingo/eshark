function customerdetail() { }
customerdetail.prototype = {
	settings: { customerId: null },
	construct: function(settings) {
		$.extend(this.settings,settings);
		var self=this;
		$('#notes').live('blur',function() {
			var note=$(this).val();
			$.ajax({ 	
						url: '/customer/updatenote.json/?customerId='+self.settings.customerId, 
						type: 'post',
						dataType: 'json',
						data: { 'note': note },
						success: function(res) {
							if(res != null && res.error) {
								alert('An error occured:\n'+res.msg);
							}
						}
					});
		});
	}	
};