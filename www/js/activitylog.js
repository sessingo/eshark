function activitylog() { }
activitylog.prototype = {
	logStats: [],
	options: {},
	construct: function(options) {
		$.extend(this.options, options);
		var self=this;
		
		$('ul.js-previousLog li a').live('click', function(e) {
			e.preventDefault();
			var val=$(this).data('val');
			if(val.length > 0) {
				$(this).parents('form:first').prepend('<input type="hidden" name="pCustomerId" value="'+val[0]+'" /><input type="hidden" name="pProjectId" value="'+val[1]+'" /><input type="hidden" name="pActivityId" value="'+val[2]+'" />');
				$p.doPostback(this);
			}
		});
		
		var parseDate=function(input) {
			var parts = input.match(/(\d+)/g);
			return new Date(parts[2], parts[1]-1, parts[0]); // months are 0-based
		};
		
		var today=parseDate(this.options.SelectedDate);
		$.ajax( { 
			url		: '/activity/logminutes.json?month='+(today.getMonth()+1)+'&year='+today.getFullYear(), 
			success	: function(r) {
				self.logStats=r;
				self.setDatePicker();
			},
			dataType: 'json',
			async: false
		});
	},
	setDatePicker: function() {
		var self=this;
		var dp=$('#datepicker');		
		$.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
		$.datepicker._updateDatepicker = function(inst) {
			$.datepicker._updateDatepicker_original(inst);
			var afterShow = this._get(inst, 'afterShow');
			if (afterShow)
				afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
		};
		
		var renderPercentage=function() {
			$('#datepicker').find('a.ui-state-default').each(function() {
				var minutes=(self.logStats[$(this).data('day')] != null) ? self.logStats[$(this).data('day')] : 0;
				var percentage=0;
				if(minutes > 0) {
					var time=self.options.WorkHours.split('.');
					var workMinutes=0;
					if(time.length > 0) {
						workMinutes=parseFloat(time[0])*60;
						if(time[1] != null) {
							var tmp=parseFloat(time[1]);
							workMinutes+=tmp;
						}
					}
					percentage=Math.floor(minutes/(workMinutes)*100);
				}
				var t=$(this).text();
				$(this).html('<span>'+t+'</span><div class="percentage">'+percentage+'%</div>');
			});
		};
		dp.datepicker({
			defaultDate: self.options.SelectedDate,
			dateFormat: 'dd-mm-yy',
			afterShow: function() {
				renderPercentage();
			},
			onSelect: function(date) {
				top.location='?date='+date;
			},
			onChangeMonthYear: function(year, month, inst) {
				$.ajax( { 
					url		: '/activity/logminutes.json?month='+month+'&year='+year, 
					success	: function(r) {
						self.logStats=r;
					},
					dataType: 'json',
					async	: false
				});
			}
		});
	}
};