-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 10. 10 2012 kl. 01:14:04
-- Serverversion: 5.1.63
-- PHP-version: 5.3.3-7+squeeze14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `EShark`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Account`
--

CREATE TABLE IF NOT EXISTS `Account` (
  `AccountID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Bank` varchar(255) NOT NULL,
  `RegistrationNumber` int(4) NOT NULL,
  `AccountNumber` varchar(255) NOT NULL,
  `IBAN` varchar(255) DEFAULT NULL,
  `Swift` varchar(255) DEFAULT NULL,
  `PubDate` datetime NOT NULL,
  PRIMARY KEY (`AccountID`),
  KEY `UserID` (`UserID`),
  KEY `PubDate` (`PubDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `Account`
--

INSERT INTO `Account` (`AccountID`, `UserID`, `Name`, `Bank`, `RegistrationNumber`, `AccountNumber`, `IBAN`, `Swift`, `PubDate`) VALUES
(1, 1, 'LÃ¸n konto', 'Nordea', 2410, '5908140453', 'DK3020005908140453', 'NDEADKKK', '2012-10-08 18:24:37'),
(2, 1, 'Erhvervskonto', 'Nordea', 2109, '6887155230', 'DK4720006887155230', 'NDEADKKK', '2012-10-08 18:44:07');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Cache`
--

CREATE TABLE IF NOT EXISTS `Cache` (
  `Key` varchar(400) NOT NULL,
  `Data` longtext NOT NULL,
  `ExpireDate` int(11) NOT NULL,
  PRIMARY KEY (`Key`),
  KEY `ExpireDate` (`ExpireDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `Cache`
--

INSERT INTO `Cache` (`Key`, `Data`, `ExpireDate`) VALUES
('LANG_01288be1a5dfd7e934006e79e794b36b', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:72:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:14:"Ny indbetaling";i:14;s:10:"Ny faktura";i:15;s:7:"Periode";i:16;s:3:"Fra";i:17;s:3:"Til";i:18;s:16:"Skjul betalinger";i:19;s:13:"Kun bogfÃ¸rte";i:20;s:7:"Opdater";i:21;s:11:"Faktura nr.";i:22;s:11:"Beskrivelse";i:23;s:12:"Faktura dato";i:24;s:14:"Betalings dato";i:25;s:4:"Moms";i:26;s:16:"BelÃ¸b ink. moms";i:27;s:17:"Ingen beskrivelse";i:28;s:51:"Er du sikker pÃ¥, at du vil bogfÃ¸re denne faktura?";i:29;s:7:"BogfÃ¸r";i:30;s:49:"Er du sikker pÃ¥, at du vil slette denne faktura?";i:31;s:4:"Slet";i:32;s:7:"Rediger";i:33;s:20:"Indbetaling faktura:";i:34;s:5:"Total";i:35;s:25:"Customer/Menu/Information";i:36;s:19:"Customer/Menu/Cases";i:37;s:22:"Customer/Menu/Projects";i:38;s:24:"Customer/Menu/Activities";i:39;s:22:"Customer/Menu/Invoices";i:40;s:14:"Menu/Customers";i:41;s:15:"Menu/Activities";i:42;s:13:"Menu/Vouchers";i:43;s:13:"Menu/Invoices";i:44;s:10:"Menu/Items";i:45;s:16:"Menu/EditCompany";i:46;s:19:"Menu/Administration";i:47;s:12:"Menu/Signout";i:48;s:13:"Ny kreditnota";i:49;s:18:"Invoice/NewPayment";i:50;s:21:"Invoice/NewCreditMemo";i:51;s:18:"Invoice/NewInvoice";i:52;s:14:"Invoice/Period";i:53;s:12:"Invoice/From";i:54;s:10:"Invoice/To";i:55;s:19:"Invoice/HidePayment";i:56;s:20:"Invoice/RecordedOnly";i:57;s:14:"Invoice/Update";i:58;s:17:"Invoice/InvoiceNo";i:59;s:19:"Invoice/Description";i:60;s:19:"Invoice/InvoiceDate";i:61;s:19:"Invoice/PaymentDate";i:62;s:11:"Invoice/Vat";i:63;s:21:"Invoice/AmountWithVat";i:64;s:21:"Invoice/NoDescription";i:65;s:21:"Invoice/ConfirmRecord";i:66;s:14:"Invoice/Record";i:67;s:21:"Invoice/ConfirmDelete";i:68;s:14:"Invoice/Delete";i:69;s:12:"Invoice/Edit";i:70;s:22:"Invoice/PaymentInvoice";i:71;s:13:"Invoice/Total";}s:10:"translated";a:72:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:14:"Ny indbetaling";i:14;s:10:"Ny faktura";i:15;s:7:"Periode";i:16;s:3:"Fra";i:17;s:3:"Til";i:18;s:16:"Skjul betalinger";i:19;s:13:"Kun bogfÃ¸rte";i:20;s:7:"Opdater";i:21;s:11:"Faktura nr.";i:22;s:11:"Beskrivelse";i:23;s:12:"Faktura dato";i:24;s:14:"Betalings dato";i:25;s:4:"Moms";i:26;s:16:"BelÃ¸b ink. moms";i:27;s:17:"Ingen beskrivelse";i:28;s:51:"Er du sikker pÃ¥, at du vil bogfÃ¸re denne faktura?";i:29;s:7:"BogfÃ¸r";i:30;s:49:"Er du sikker pÃ¥, at du vil slette denne faktura?";i:31;s:4:"Slet";i:32;s:7:"Rediger";i:33;s:20:"Indbetaling faktura:";i:34;s:5:"Total";i:35;s:25:"Customer/Menu/Information";i:36;s:19:"Customer/Menu/Cases";i:37;s:22:"Customer/Menu/Projects";i:38;s:24:"Customer/Menu/Activities";i:39;s:22:"Customer/Menu/Invoices";i:40;s:14:"Menu/Customers";i:41;s:15:"Menu/Activities";i:42;s:13:"Menu/Vouchers";i:43;s:13:"Menu/Invoices";i:44;s:10:"Menu/Items";i:45;s:16:"Menu/EditCompany";i:46;s:19:"Menu/Administration";i:47;s:12:"Menu/Signout";i:48;s:13:"Ny kreditnota";i:49;s:18:"Invoice/NewPayment";i:50;s:21:"Invoice/NewCreditMemo";i:51;s:18:"Invoice/NewInvoice";i:52;s:14:"Invoice/Period";i:53;s:12:"Invoice/From";i:54;s:10:"Invoice/To";i:55;s:19:"Invoice/HidePayment";i:56;s:20:"Invoice/RecordedOnly";i:57;s:14:"Invoice/Update";i:58;s:17:"Invoice/InvoiceNo";i:59;s:19:"Invoice/Description";i:60;s:19:"Invoice/InvoiceDate";i:61;s:19:"Invoice/PaymentDate";i:62;s:11:"Invoice/Vat";i:63;s:21:"Invoice/AmountWithVat";i:64;s:21:"Invoice/NoDescription";i:65;s:21:"Invoice/ConfirmRecord";i:66;s:14:"Invoice/Record";i:67;s:21:"Invoice/ConfirmDelete";i:68;s:14:"Invoice/Delete";i:69;s:12:"Invoice/Edit";i:70;s:22:"Invoice/PaymentInvoice";i:71;s:13:"Invoice/Total";}}', 1348675313),
('LANG_10af7aeafab3333da7ae3cd46350105c', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:28:{i:0;s:20:"Customer/Menu/Search";i:1;s:28:"Customer/Menu/CreateCustomer";i:2;s:14:"Menu/Customers";i:3;s:15:"Menu/Activities";i:4;s:13:"Menu/Vouchers";i:5;s:13:"Menu/Invoices";i:6;s:10:"Menu/Items";i:7;s:16:"Menu/EditCompany";i:8;s:19:"Menu/Administration";i:9;s:12:"Menu/Signout";i:10;s:13:"Customer/Name";i:11;s:16:"Customer/Address";i:12;s:19:"Customer/PostalCode";i:13;s:13:"Customer/City";i:14;s:16:"Customer/Country";i:15;s:22:"Customer/ChooseCountry";i:16;s:22:"Customer/ContactPerson";i:17;s:24:"Customer/DeliveryAddress";i:18;s:14:"Customer/Notes";i:19;s:14:"Customer/Phone";i:20;s:18:"Customer/Cellphone";i:21;s:14:"Customer/VATNo";i:22;s:16:"Customer/Website";i:23;s:13:"Customer/Bank";i:24;s:12:"Customer/EAN";i:25;s:24:"Customer/InternalAccount";i:26;s:13:"Customer/Logo";i:27;s:13:"Customer/Save";}s:10:"translated";a:28:{i:0;s:20:"Customer/Menu/Search";i:1;s:28:"Customer/Menu/CreateCustomer";i:2;s:14:"Menu/Customers";i:3;s:15:"Menu/Activities";i:4;s:13:"Menu/Vouchers";i:5;s:13:"Menu/Invoices";i:6;s:10:"Menu/Items";i:7;s:16:"Menu/EditCompany";i:8;s:19:"Menu/Administration";i:9;s:12:"Menu/Signout";i:10;s:13:"Customer/Name";i:11;s:16:"Customer/Address";i:12;s:19:"Customer/PostalCode";i:13;s:13:"Customer/City";i:14;s:16:"Customer/Country";i:15;s:22:"Customer/ChooseCountry";i:16;s:22:"Customer/ContactPerson";i:17;s:24:"Customer/DeliveryAddress";i:18;s:14:"Customer/Notes";i:19;s:14:"Customer/Phone";i:20;s:18:"Customer/Cellphone";i:21;s:14:"Customer/VATNo";i:22;s:16:"Customer/Website";i:23;s:13:"Customer/Bank";i:24;s:12:"Customer/EAN";i:25;s:24:"Customer/InternalAccount";i:26;s:13:"Customer/Logo";i:27;s:13:"Customer/Save";}}', 1348613404),
('LANG_13e986618aedd8d4bbde7de0d80dd0f6', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:14:{i:0;s:25:"Customer/Menu/Information";i:1;s:19:"Customer/Menu/Cases";i:2;s:22:"Customer/Menu/Projects";i:3;s:24:"Customer/Menu/Activities";i:4;s:22:"Customer/Menu/Invoices";i:5;s:14:"Menu/Customers";i:6;s:15:"Menu/Activities";i:7;s:13:"Menu/Vouchers";i:8;s:13:"Menu/Invoices";i:9;s:10:"Menu/Items";i:10;s:16:"Menu/EditCompany";i:11;s:19:"Menu/Administration";i:12;s:12:"Menu/Signout";i:13;s:30:"Customer/Project/ChooseProject";}s:10:"translated";a:14:{i:0;s:25:"Customer/Menu/Information";i:1;s:19:"Customer/Menu/Cases";i:2;s:22:"Customer/Menu/Projects";i:3;s:24:"Customer/Menu/Activities";i:4;s:22:"Customer/Menu/Invoices";i:5;s:14:"Menu/Customers";i:6;s:15:"Menu/Activities";i:7;s:13:"Menu/Vouchers";i:8;s:13:"Menu/Invoices";i:9;s:10:"Menu/Items";i:10;s:16:"Menu/EditCompany";i:11;s:19:"Menu/Administration";i:12;s:12:"Menu/Signout";i:13;s:30:"Customer/Project/ChooseProject";}}', 1348589562),
('LANG_1ac437800708d853dbec7be164b83a6c', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:88:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:15:"Rediger faktura";i:14;s:11:"Beskrivelse";i:15;s:12:"Faktura dato";i:16;s:13:"Forfalds dato";i:17;s:17:"PÃ¥mindelses dato";i:18;s:14:"1. rykker dato";i:19;s:14:"2. rykker dato";i:20;s:12:"Inkasso dato";i:21;s:8:"BogfÃ¸rt";i:22;s:7:"TilfÃ¸j";i:23;s:6:"Varenr";i:24;s:4:"Navn";i:25;s:5:"Antal";i:26;s:5:"Enhed";i:27;s:4:"Pris";i:28;s:5:"Rabat";i:29;s:6:"BelÃ¸b";i:30;s:3:"stk";i:31;s:5:"timer";i:32;s:3:"dag";i:33;s:3:"uge";i:34;s:2:"pk";i:35;s:7:"Rediger";i:36;s:5:"Fjern";i:37;s:3:"Gem";i:38;s:25:"Customer/Menu/Information";i:39;s:19:"Customer/Menu/Cases";i:40;s:22:"Customer/Menu/Projects";i:41;s:24:"Customer/Menu/Activities";i:42;s:22:"Customer/Menu/Invoices";i:43;s:14:"Menu/Customers";i:44;s:15:"Menu/Activities";i:45;s:13:"Menu/Vouchers";i:46;s:13:"Menu/Invoices";i:47;s:10:"Menu/Items";i:48;s:16:"Menu/EditCompany";i:49;s:19:"Menu/Administration";i:50;s:12:"Menu/Signout";i:51;s:28:"%s skal vÃ¦re en gyldig dato";i:52;s:14:"Betalings dato";i:53;s:16:"Påmindelses dato";i:54;s:7:"Bogført";i:55;s:6:"Tilføj";i:56;s:5:"Beløb";i:57;s:18:"Rediger kreditnota";i:58;s:13:"Ny kreditnota";i:59;s:10:"Ny faktura";i:60;s:18:"Invoice/NewInvoice";i:61;s:19:"Invoice/Description";i:62;s:19:"Invoice/InvoiceDate";i:63;s:19:"Invoice/PaymentDate";i:64;s:20:"Invoice/ReminderDate";i:65;s:25:"Invoice/FirstReminderDate";i:66;s:26:"Invoice/SecondReminderDate";i:67;s:26:"Invoice/DebtCollectionDate";i:68;s:14:"Invoice/Record";i:69;s:15:"Invoice/AddItem";i:70;s:13:"Invoice/Items";i:71;s:11:"Item/ItemNo";i:72;s:9:"Item/Name";i:73;s:16:"Item/Description";i:74;s:10:"Item/Count";i:75;s:9:"Item/Unit";i:76;s:10:"Item/Price";i:77;s:11:"Item/Rebate";i:78;s:11:"Item/Amount";i:79;s:16:"Item/Units/Piece";i:80;s:16:"Item/Units/Hours";i:81;s:14:"Item/Units/Day";i:82;s:15:"Item/Units/Week";i:83;s:18:"Item/Units/Package";i:84;s:9:"Item/Edit";i:85;s:11:"Item/Remove";i:86;s:19:"Invoice/SaveChanges";i:87;s:21:"Invoice/NewCreditMemo";}s:10:"translated";a:88:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:15:"Rediger faktura";i:14;s:11:"Beskrivelse";i:15;s:12:"Faktura dato";i:16;s:13:"Forfalds dato";i:17;s:17:"PÃ¥mindelses dato";i:18;s:14:"1. rykker dato";i:19;s:14:"2. rykker dato";i:20;s:12:"Inkasso dato";i:21;s:8:"BogfÃ¸rt";i:22;s:7:"TilfÃ¸j";i:23;s:6:"Varenr";i:24;s:4:"Navn";i:25;s:5:"Antal";i:26;s:5:"Enhed";i:27;s:4:"Pris";i:28;s:5:"Rabat";i:29;s:6:"BelÃ¸b";i:30;s:3:"stk";i:31;s:5:"timer";i:32;s:3:"dag";i:33;s:3:"uge";i:34;s:2:"pk";i:35;s:7:"Rediger";i:36;s:5:"Fjern";i:37;s:3:"Gem";i:38;s:25:"Customer/Menu/Information";i:39;s:19:"Customer/Menu/Cases";i:40;s:22:"Customer/Menu/Projects";i:41;s:24:"Customer/Menu/Activities";i:42;s:22:"Customer/Menu/Invoices";i:43;s:14:"Menu/Customers";i:44;s:15:"Menu/Activities";i:45;s:13:"Menu/Vouchers";i:46;s:13:"Menu/Invoices";i:47;s:10:"Menu/Items";i:48;s:16:"Menu/EditCompany";i:49;s:19:"Menu/Administration";i:50;s:12:"Menu/Signout";i:51;s:28:"%s skal vÃ¦re en gyldig dato";i:52;s:14:"Betalings dato";i:53;s:16:"Påmindelses dato";i:54;s:7:"Bogført";i:55;s:6:"Tilføj";i:56;s:5:"Beløb";i:57;s:18:"Rediger kreditnota";i:58;s:13:"Ny kreditnota";i:59;s:10:"Ny faktura";i:60;s:18:"Invoice/NewInvoice";i:61;s:19:"Invoice/Description";i:62;s:19:"Invoice/InvoiceDate";i:63;s:19:"Invoice/PaymentDate";i:64;s:20:"Invoice/ReminderDate";i:65;s:25:"Invoice/FirstReminderDate";i:66;s:26:"Invoice/SecondReminderDate";i:67;s:26:"Invoice/DebtCollectionDate";i:68;s:14:"Invoice/Record";i:69;s:15:"Invoice/AddItem";i:70;s:13:"Invoice/Items";i:71;s:11:"Item/ItemNo";i:72;s:9:"Item/Name";i:73;s:16:"Item/Description";i:74;s:10:"Item/Count";i:75;s:9:"Item/Unit";i:76;s:10:"Item/Price";i:77;s:11:"Item/Rebate";i:78;s:11:"Item/Amount";i:79;s:16:"Item/Units/Piece";i:80;s:16:"Item/Units/Hours";i:81;s:14:"Item/Units/Day";i:82;s:15:"Item/Units/Week";i:83;s:18:"Item/Units/Package";i:84;s:9:"Item/Edit";i:85;s:11:"Item/Remove";i:86;s:19:"Invoice/SaveChanges";i:87;s:21:"Invoice/NewCreditMemo";}}', 1348682214),
('LANG_2621e77865f273584092e1a73e4ef418', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:21:{i:0;s:6:"Kunder";i:1;s:9:"Aktivitet";i:2;s:5:"Bilag";i:3;s:7:"Faktura";i:4;s:5:"Varer";i:5;s:13:"Rediger firma";i:6;s:14:"Administration";i:7;s:6:"Log ud";i:8;s:27:"404: Siden blev ikke fundet";i:9;s:59:"Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.";i:10;s:17:"customer/customer";i:11;s:14:"Menu/Customers";i:12;s:15:"Menu/Activities";i:13;s:13:"Menu/Vouchers";i:14;s:13:"Menu/Invoices";i:15;s:10:"Menu/Items";i:16;s:16:"Menu/EditCompany";i:17;s:19:"Menu/Administration";i:18;s:12:"Menu/Signout";i:19;s:23:"Page/ErrorPage404/Title";i:20;s:29:"Page/ErrorPage404/Description";}s:10:"translated";a:21:{i:0;s:6:"Kunder";i:1;s:9:"Aktivitet";i:2;s:5:"Bilag";i:3;s:7:"Faktura";i:4;s:5:"Varer";i:5;s:13:"Rediger firma";i:6;s:14:"Administration";i:7;s:6:"Log ud";i:8;s:27:"404: Siden blev ikke fundet";i:9;s:59:"Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.";i:10;s:17:"customer/customer";i:11;s:14:"Menu/Customers";i:12;s:15:"Menu/Activities";i:13;s:13:"Menu/Vouchers";i:14;s:13:"Menu/Invoices";i:15;s:10:"Menu/Items";i:16;s:16:"Menu/EditCompany";i:17;s:19:"Menu/Administration";i:18;s:12:"Menu/Signout";i:19;s:23:"Page/ErrorPage404/Title";i:20;s:29:"Page/ErrorPage404/Description";}}', 1348679444),
('LANG_2e5f7477eaf65b8cee189b7de21febbb', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:21:{i:0;s:10:"Betalinger";i:1;s:4:"SÃ¸g";i:2;s:8:"Kommende";i:3;s:11:"Overskredet";i:4;s:7:"Betalte";i:5;s:14:"Menu/Customers";i:6;s:15:"Menu/Activities";i:7;s:13:"Menu/Vouchers";i:8;s:13:"Menu/Invoices";i:9;s:10:"Menu/Items";i:10;s:16:"Menu/EditCompany";i:11;s:19:"Menu/Administration";i:12;s:12:"Menu/Signout";i:13;s:26:"Betalingsfrist overskredet";i:14;s:18:"Invoice/NoInvoices";i:15;s:16:"Invoice/Payments";i:16;s:14:"Invoice/Search";i:17;s:15:"Invoice/Comming";i:18;s:15:"Invoice/Overdue";i:19;s:13:"Invoice/Payed";i:20;s:22:"Invoice/PaymentOverdue";}s:10:"translated";a:21:{i:0;s:10:"Betalinger";i:1;s:4:"SÃ¸g";i:2;s:8:"Kommende";i:3;s:11:"Overskredet";i:4;s:7:"Betalte";i:5;s:14:"Menu/Customers";i:6;s:15:"Menu/Activities";i:7;s:13:"Menu/Vouchers";i:8;s:13:"Menu/Invoices";i:9;s:10:"Menu/Items";i:10;s:16:"Menu/EditCompany";i:11;s:19:"Menu/Administration";i:12;s:12:"Menu/Signout";i:13;s:26:"Betalingsfrist overskredet";i:14;s:18:"Invoice/NoInvoices";i:15;s:16:"Invoice/Payments";i:16;s:14:"Invoice/Search";i:17;s:15:"Invoice/Comming";i:18;s:15:"Invoice/Overdue";i:19;s:13:"Invoice/Payed";i:20;s:22:"Invoice/PaymentOverdue";}}', 1348679461),
('LANG_3040a8ab4d5df87a178a90fca9f75577', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:23:{i:0;s:8:"Oversigt";i:1;s:7:"Brugere";i:2;s:5:"Sager";i:3;s:5:"Varer";i:4;s:6:"Kunder";i:5;s:9:"Aktivitet";i:6;s:5:"Bilag";i:7;s:7:"Faktura";i:8;s:13:"Rediger firma";i:9;s:14:"Administration";i:10;s:6:"Log ud";i:11;s:14:"Admin/Overview";i:12;s:11:"Admin/Users";i:13;s:11:"Admin/Cases";i:14;s:11:"Admin/Items";i:15;s:14:"Menu/Customers";i:16;s:15:"Menu/Activities";i:17;s:13:"Menu/Vouchers";i:18;s:13:"Menu/Invoices";i:19;s:10:"Menu/Items";i:20;s:16:"Menu/EditCompany";i:21;s:19:"Menu/Administration";i:22;s:12:"Menu/Signout";}s:10:"translated";a:23:{i:0;s:8:"Oversigt";i:1;s:7:"Brugere";i:2;s:5:"Sager";i:3;s:5:"Varer";i:4;s:6:"Kunder";i:5;s:9:"Aktivitet";i:6;s:5:"Bilag";i:7;s:7:"Faktura";i:8;s:13:"Rediger firma";i:9;s:14:"Administration";i:10;s:6:"Log ud";i:11;s:14:"Admin/Overview";i:12;s:11:"Admin/Users";i:13;s:11:"Admin/Cases";i:14;s:11:"Admin/Items";i:15;s:14:"Menu/Customers";i:16;s:15:"Menu/Activities";i:17;s:13:"Menu/Vouchers";i:18;s:13:"Menu/Invoices";i:19;s:10:"Menu/Items";i:20;s:16:"Menu/EditCompany";i:21;s:19:"Menu/Administration";i:22;s:12:"Menu/Signout";}}', 1348614166),
('LANG_3c3a00b86ff69df132509dc26044b4c9', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:87:{i:0;s:15:"Rediger logning";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:5:"Kunde";i:10;s:11:"-- VÃ¦lg --";i:11;s:7:"Projekt";i:12;s:5:"Timer";i:13;s:8:"Minutter";i:14;s:11:"Beskrivelse";i:15;s:9:"Log timer";i:16;s:11:"Opret bilag";i:17;s:4:"Navn";i:18;s:14:"Faktura-nummer";i:19;s:11:"Fakturadato";i:20;s:13:"Betalingsdato";i:21;s:6:"BelÃ¸b";i:22;s:8:"BogfÃ¸rt";i:23;s:5:"Opret";i:24;s:7:"Project";i:25;s:12:"TilfÃ¸j vare";i:26;s:17:"Find eksisterende";i:27;s:6:"SÃ¸g..";i:28;s:2:"Ny";i:29;s:7:"Item no";i:30;s:5:"Tekst";i:31;s:5:"Antal";i:32;s:17:"-- VÃ¦lg enhed --";i:33;s:3:"stk";i:34;s:5:"timer";i:35;s:3:"dag";i:36;s:3:"uge";i:37;s:2:"pk";i:38;s:4:"Pris";i:39;s:5:"Rabat";i:40;s:7:"TilfÃ¸j";i:41;s:12:"Rediger vare";i:42;s:14:"Menu/Customers";i:43;s:15:"Menu/Activities";i:44;s:13:"Menu/Vouchers";i:45;s:13:"Menu/Invoices";i:46;s:10:"Menu/Items";i:47;s:16:"Menu/EditCompany";i:48;s:19:"Menu/Administration";i:49;s:12:"Menu/Signout";i:50;s:18:"Rediger vare: "%s"";i:51;s:7:"Opdater";i:52;s:21:"%s mÃ¥ ikke vÃ¦re tom";i:53;s:21:"%s skal indeholde tal";i:54;s:19:"Rediger indbetaling";i:55;s:19:"-- VÃ¦lg faktura --";i:56;s:23:"Faktura: %s - Total: %s";i:57;s:7:"Rediger";i:58;s:13:"Rediger bilag";i:59;s:16:"Activity/LogEdit";i:60;s:17:"Activity/Customer";i:61;s:16:"Activity/Project";i:62;s:17:"Activity/Activity";i:63;s:14:"Activity/Hours";i:64;s:16:"Activity/Minutes";i:65;s:18:"Voucher/NewVoucher";i:66;s:12:"Voucher/Name";i:67;s:19:"Voucher/PaymentDate";i:68;s:14:"Voucher/Amount";i:69;s:13:"Item/EditItem";i:70;s:17:"Item/FindExisting";i:71;s:11:"Item/Search";i:72;s:8:"Item/New";i:73;s:11:"Item/ItemNo";i:74;s:9:"Item/Name";i:75;s:16:"Item/Description";i:76;s:10:"Item/Count";i:77;s:15:"Item/ChooseUnit";i:78;s:16:"Item/Units/Piece";i:79;s:16:"Item/Units/Hours";i:80;s:14:"Item/Units/Day";i:81;s:15:"Item/Units/Week";i:82;s:18:"Item/Units/Package";i:83;s:10:"Item/Price";i:84;s:11:"Item/Rebate";i:85;s:11:"Item/Amount";i:86;s:11:"Item/Update";}s:10:"translated";a:87:{i:0;s:15:"Rediger logning";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:5:"Kunde";i:10;s:11:"-- VÃ¦lg --";i:11;s:7:"Projekt";i:12;s:5:"Timer";i:13;s:8:"Minutter";i:14;s:11:"Beskrivelse";i:15;s:9:"Log timer";i:16;s:11:"Opret bilag";i:17;s:4:"Navn";i:18;s:14:"Faktura-nummer";i:19;s:11:"Fakturadato";i:20;s:13:"Betalingsdato";i:21;s:6:"BelÃ¸b";i:22;s:8:"BogfÃ¸rt";i:23;s:5:"Opret";i:24;s:7:"Project";i:25;s:12:"TilfÃ¸j vare";i:26;s:17:"Find eksisterende";i:27;s:6:"SÃ¸g..";i:28;s:2:"Ny";i:29;s:7:"Item no";i:30;s:5:"Tekst";i:31;s:5:"Antal";i:32;s:17:"-- VÃ¦lg enhed --";i:33;s:3:"stk";i:34;s:5:"timer";i:35;s:3:"dag";i:36;s:3:"uge";i:37;s:2:"pk";i:38;s:4:"Pris";i:39;s:5:"Rabat";i:40;s:7:"TilfÃ¸j";i:41;s:12:"Rediger vare";i:42;s:14:"Menu/Customers";i:43;s:15:"Menu/Activities";i:44;s:13:"Menu/Vouchers";i:45;s:13:"Menu/Invoices";i:46;s:10:"Menu/Items";i:47;s:16:"Menu/EditCompany";i:48;s:19:"Menu/Administration";i:49;s:12:"Menu/Signout";i:50;s:18:"Rediger vare: "%s"";i:51;s:7:"Opdater";i:52;s:21:"%s mÃ¥ ikke vÃ¦re tom";i:53;s:21:"%s skal indeholde tal";i:54;s:19:"Rediger indbetaling";i:55;s:19:"-- VÃ¦lg faktura --";i:56;s:23:"Faktura: %s - Total: %s";i:57;s:7:"Rediger";i:58;s:13:"Rediger bilag";i:59;s:16:"Activity/LogEdit";i:60;s:17:"Activity/Customer";i:61;s:16:"Activity/Project";i:62;s:17:"Activity/Activity";i:63;s:14:"Activity/Hours";i:64;s:16:"Activity/Minutes";i:65;s:18:"Voucher/NewVoucher";i:66;s:12:"Voucher/Name";i:67;s:19:"Voucher/PaymentDate";i:68;s:14:"Voucher/Amount";i:69;s:13:"Item/EditItem";i:70;s:17:"Item/FindExisting";i:71;s:11:"Item/Search";i:72;s:8:"Item/New";i:73;s:11:"Item/ItemNo";i:74;s:9:"Item/Name";i:75;s:16:"Item/Description";i:76;s:10:"Item/Count";i:77;s:15:"Item/ChooseUnit";i:78;s:16:"Item/Units/Piece";i:79;s:16:"Item/Units/Hours";i:80;s:14:"Item/Units/Day";i:81;s:15:"Item/Units/Week";i:82;s:18:"Item/Units/Package";i:83;s:10:"Item/Price";i:84;s:11:"Item/Rebate";i:85;s:11:"Item/Amount";i:86;s:11:"Item/Update";}}', 1348682217),
('LANG_3c966e5a230ab13fe02dbd3f954351a9', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:39:{i:0;s:25:"Customer/Menu/Information";i:1;s:19:"Customer/Menu/Cases";i:2;s:22:"Customer/Menu/Projects";i:3;s:24:"Customer/Menu/Activities";i:4;s:22:"Customer/Menu/Invoices";i:5;s:14:"Menu/Customers";i:6;s:15:"Menu/Activities";i:7;s:13:"Menu/Vouchers";i:8;s:13:"Menu/Invoices";i:9;s:10:"Menu/Items";i:10;s:16:"Menu/EditCompany";i:11;s:19:"Menu/Administration";i:12;s:12:"Menu/Signout";i:13;s:13:"Ny kreditnota";i:14;s:11:"Beskrivelse";i:15;s:12:"Faktura dato";i:16;s:13:"Forfalds dato";i:17;s:17:"PÃ¥mindelses dato";i:18;s:14:"1. rykker dato";i:19;s:14:"2. rykker dato";i:20;s:12:"Inkasso dato";i:21;s:8:"BogfÃ¸rt";i:22;s:7:"TilfÃ¸j";i:23;s:5:"Varer";i:24;s:6:"Varenr";i:25;s:4:"Navn";i:26;s:5:"Antal";i:27;s:5:"Enhed";i:28;s:4:"Pris";i:29;s:5:"Rabat";i:30;s:6:"BelÃ¸b";i:31;s:3:"Gem";i:32;s:3:"stk";i:33;s:5:"timer";i:34;s:3:"dag";i:35;s:3:"uge";i:36;s:2:"pk";i:37;s:7:"Rediger";i:38;s:5:"Fjern";}s:10:"translated";a:39:{i:0;s:25:"Customer/Menu/Information";i:1;s:19:"Customer/Menu/Cases";i:2;s:22:"Customer/Menu/Projects";i:3;s:24:"Customer/Menu/Activities";i:4;s:22:"Customer/Menu/Invoices";i:5;s:14:"Menu/Customers";i:6;s:15:"Menu/Activities";i:7;s:13:"Menu/Vouchers";i:8;s:13:"Menu/Invoices";i:9;s:10:"Menu/Items";i:10;s:16:"Menu/EditCompany";i:11;s:19:"Menu/Administration";i:12;s:12:"Menu/Signout";i:13;s:13:"Ny kreditnota";i:14;s:11:"Beskrivelse";i:15;s:12:"Faktura dato";i:16;s:13:"Forfalds dato";i:17;s:17:"PÃ¥mindelses dato";i:18;s:14:"1. rykker dato";i:19;s:14:"2. rykker dato";i:20;s:12:"Inkasso dato";i:21;s:8:"BogfÃ¸rt";i:22;s:7:"TilfÃ¸j";i:23;s:5:"Varer";i:24;s:6:"Varenr";i:25;s:4:"Navn";i:26;s:5:"Antal";i:27;s:5:"Enhed";i:28;s:4:"Pris";i:29;s:5:"Rabat";i:30;s:6:"BelÃ¸b";i:31;s:3:"Gem";i:32;s:3:"stk";i:33;s:5:"timer";i:34;s:3:"dag";i:35;s:3:"uge";i:36;s:2:"pk";i:37;s:7:"Rediger";i:38;s:5:"Fjern";}}', 1348600169),
('LANG_4d1a7a4750fd6dd70fe0798a8068f2f5', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:23:{i:0;s:8:"Oversigt";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:4:"Alle";i:10;s:8:"Kategory";i:11;s:11:"-- VÃ¦lg --";i:12;s:5:"Enhed";i:13;s:3:"stk";i:14;s:5:"timer";i:15;s:3:"dag";i:16;s:3:"uge";i:17;s:2:"pk";i:18;s:10:"Varenummer";i:19;s:4:"Navn";i:20;s:9:"KÃ¸bspris";i:21;s:9:"Salgspris";i:22;s:3:"Gem";}s:10:"translated";a:23:{i:0;s:8:"Oversigt";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:4:"Alle";i:10;s:8:"Kategory";i:11;s:11:"-- VÃ¦lg --";i:12;s:5:"Enhed";i:13;s:3:"stk";i:14;s:5:"timer";i:15;s:3:"dag";i:16;s:3:"uge";i:17;s:2:"pk";i:18;s:10:"Varenummer";i:19;s:4:"Navn";i:20;s:9:"KÃ¸bspris";i:21;s:9:"Salgspris";i:22;s:3:"Gem";}}', 1348460549),
('LANG_5651e7adff9bd76b32c555e448533987', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:35:{i:0;s:9:"Log timer";i:1;s:8:"Oversigt";i:2;s:6:"Kunder";i:3;s:9:"Aktivitet";i:4;s:5:"Bilag";i:5;s:7:"Faktura";i:6;s:5:"Varer";i:7;s:13:"Rediger firma";i:8;s:14:"Administration";i:9;s:6:"Log ud";i:10;s:7:"Periode";i:11;s:3:"Fra";i:12;s:3:"Til";i:13;s:7:"Opdater";i:14;s:5:"Kunde";i:15;s:11:"-- Kunde --";i:16;s:13:"-- Projekt --";i:17;s:15:"-- Aktivitet --";i:18;s:7:"Projekt";i:19;s:11:"Beskrivelse";i:20;s:6:"Bruger";i:21;s:4:"Dato";i:22;s:5:"Timer";i:23;s:4:"Pris";i:24;s:5:"Total";i:25;s:17:"Activity/LogHours";i:26;s:17:"Activity/Overview";i:27;s:14:"Menu/Customers";i:28;s:15:"Menu/Activities";i:29;s:13:"Menu/Vouchers";i:30;s:13:"Menu/Invoices";i:31;s:10:"Menu/Items";i:32;s:16:"Menu/EditCompany";i:33;s:19:"Menu/Administration";i:34;s:12:"Menu/Signout";}s:10:"translated";a:35:{i:0;s:9:"Log timer";i:1;s:8:"Oversigt";i:2;s:6:"Kunder";i:3;s:9:"Aktivitet";i:4;s:5:"Bilag";i:5;s:7:"Faktura";i:6;s:5:"Varer";i:7;s:13:"Rediger firma";i:8;s:14:"Administration";i:9;s:6:"Log ud";i:10;s:7:"Periode";i:11;s:3:"Fra";i:12;s:3:"Til";i:13;s:7:"Opdater";i:14;s:5:"Kunde";i:15;s:11:"-- Kunde --";i:16;s:13:"-- Projekt --";i:17;s:15:"-- Aktivitet --";i:18;s:7:"Projekt";i:19;s:11:"Beskrivelse";i:20;s:6:"Bruger";i:21;s:4:"Dato";i:22;s:5:"Timer";i:23;s:4:"Pris";i:24;s:5:"Total";i:25;s:17:"Activity/LogHours";i:26;s:17:"Activity/Overview";i:27;s:14:"Menu/Customers";i:28;s:15:"Menu/Activities";i:29;s:13:"Menu/Vouchers";i:30;s:13:"Menu/Invoices";i:31;s:10:"Menu/Items";i:32;s:16:"Menu/EditCompany";i:33;s:19:"Menu/Administration";i:34;s:12:"Menu/Signout";}}', 1348555272),
('LANG_5d09aaec3aaa5609d2317f6e57e88b56', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:32:{i:0;s:8:"Oversigt";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:4:"Alle";i:10;s:12:"TilfÃ¸j vare";i:11;s:6:"Varenr";i:12;s:4:"Navn";i:13;s:5:"Enhed";i:14;s:9:"KÃ¸bspris";i:15;s:9:"Salgspris";i:16;s:3:"stk";i:17;s:5:"timer";i:18;s:3:"dag";i:19;s:3:"uge";i:20;s:2:"pk";i:21;s:7:"Rediger";i:22;s:46:"Er du sikker pÃ¥, at du vil slette denne vare?";i:23;s:4:"Slet";i:24;s:14:"Menu/Customers";i:25;s:15:"Menu/Activities";i:26;s:13:"Menu/Vouchers";i:27;s:13:"Menu/Invoices";i:28;s:10:"Menu/Items";i:29;s:16:"Menu/EditCompany";i:30;s:19:"Menu/Administration";i:31;s:12:"Menu/Signout";}s:10:"translated";a:32:{i:0;s:8:"Oversigt";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:4:"Alle";i:10;s:12:"TilfÃ¸j vare";i:11;s:6:"Varenr";i:12;s:4:"Navn";i:13;s:5:"Enhed";i:14;s:9:"KÃ¸bspris";i:15;s:9:"Salgspris";i:16;s:3:"stk";i:17;s:5:"timer";i:18;s:3:"dag";i:19;s:3:"uge";i:20;s:2:"pk";i:21;s:7:"Rediger";i:22;s:46:"Er du sikker pÃ¥, at du vil slette denne vare?";i:23;s:4:"Slet";i:24;s:14:"Menu/Customers";i:25;s:15:"Menu/Activities";i:26;s:13:"Menu/Vouchers";i:27;s:13:"Menu/Invoices";i:28;s:10:"Menu/Items";i:29;s:16:"Menu/EditCompany";i:30;s:19:"Menu/Administration";i:31;s:12:"Menu/Signout";}}', 1348543226),
('LANG_5d3951cefd337459cc1dad9cca1ae2c0', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:44:{i:0;s:4:"SÃ¸g";i:1;s:8:"Ny kunde";i:2;s:6:"Kunder";i:3;s:9:"Aktivitet";i:4;s:5:"Bilag";i:5;s:7:"Faktura";i:6;s:5:"Varer";i:7;s:13:"Rediger firma";i:8;s:14:"Administration";i:9;s:6:"Log ud";i:10;s:10:"Find kunde";i:11;s:11:"Kundenummer";i:12;s:4:"Navn";i:13;s:7:"Adresse";i:14;s:6:"Postnr";i:15;s:2:"by";i:16;s:7:"Telefon";i:17;s:5:"Mobil";i:18;s:6:"E-mail";i:19;s:3:"CVR";i:20;s:17:"customer/customer";i:21;s:7:"Kundenr";i:22;s:8:"Addresse";i:23;s:14:"Postnummer, by";i:24;s:20:"Customer/Menu/Search";i:25;s:28:"Customer/Menu/CreateCustomer";i:26;s:14:"Menu/Customers";i:27;s:15:"Menu/Activities";i:28;s:13:"Menu/Vouchers";i:29;s:13:"Menu/Invoices";i:30;s:10:"Menu/Items";i:31;s:16:"Menu/EditCompany";i:32;s:19:"Menu/Administration";i:33;s:12:"Menu/Signout";i:34;s:21:"Customer/FindCustomer";i:35;s:23:"Customer/CustomerNumber";i:36;s:13:"Customer/Name";i:37;s:16:"Customer/Address";i:38;s:26:"Customer/PostalCodeAndCity";i:39;s:18:"Customer/Telephone";i:40;s:18:"Customer/Cellphone";i:41;s:14:"Customer/Email";i:42;s:14:"Customer/VATNo";i:43;s:19:"Customer/CustomerNo";}s:10:"translated";a:44:{i:0;s:4:"SÃ¸g";i:1;s:8:"Ny kunde";i:2;s:6:"Kunder";i:3;s:9:"Aktivitet";i:4;s:5:"Bilag";i:5;s:7:"Faktura";i:6;s:5:"Varer";i:7;s:13:"Rediger firma";i:8;s:14:"Administration";i:9;s:6:"Log ud";i:10;s:10:"Find kunde";i:11;s:11:"Kundenummer";i:12;s:4:"Navn";i:13;s:7:"Adresse";i:14;s:6:"Postnr";i:15;s:2:"by";i:16;s:7:"Telefon";i:17;s:5:"Mobil";i:18;s:6:"E-mail";i:19;s:3:"CVR";i:20;s:17:"customer/customer";i:21;s:7:"Kundenr";i:22;s:8:"Addresse";i:23;s:14:"Postnummer, by";i:24;s:20:"Customer/Menu/Search";i:25;s:28:"Customer/Menu/CreateCustomer";i:26;s:14:"Menu/Customers";i:27;s:15:"Menu/Activities";i:28;s:13:"Menu/Vouchers";i:29;s:13:"Menu/Invoices";i:30;s:10:"Menu/Items";i:31;s:16:"Menu/EditCompany";i:32;s:19:"Menu/Administration";i:33;s:12:"Menu/Signout";i:34;s:21:"Customer/FindCustomer";i:35;s:23:"Customer/CustomerNumber";i:36;s:13:"Customer/Name";i:37;s:16:"Customer/Address";i:38;s:26:"Customer/PostalCodeAndCity";i:39;s:18:"Customer/Telephone";i:40;s:18:"Customer/Cellphone";i:41;s:14:"Customer/Email";i:42;s:14:"Customer/VATNo";i:43;s:19:"Customer/CustomerNo";}}', 1348675230),
('LANG_5ff14dd791429c4a55c416d81874ce81', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:54:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:10:"Ny faktura";i:14;s:11:"Beskrivelse";i:15;s:12:"Faktura dato";i:16;s:13:"Forfalds dato";i:17;s:17:"PÃ¥mindelses dato";i:18;s:14:"1. rykker dato";i:19;s:14:"2. rykker dato";i:20;s:12:"Inkasso dato";i:21;s:8:"BogfÃ¸rt";i:22;s:7:"TilfÃ¸j";i:23;s:6:"Varenr";i:24;s:4:"Navn";i:25;s:5:"Antal";i:26;s:5:"Enhed";i:27;s:4:"Pris";i:28;s:5:"Rabat";i:29;s:6:"BelÃ¸b";i:30;s:3:"Gem";i:31;s:3:"stk";i:32;s:5:"timer";i:33;s:3:"dag";i:34;s:3:"uge";i:35;s:2:"pk";i:36;s:7:"Rediger";i:37;s:5:"Fjern";i:38;s:14:"Betalings dato";i:39;s:27:"404: Siden blev ikke fundet";i:40;s:59:"Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.";i:41;s:25:"Customer/Menu/Information";i:42;s:19:"Customer/Menu/Cases";i:43;s:22:"Customer/Menu/Projects";i:44;s:24:"Customer/Menu/Activities";i:45;s:22:"Customer/Menu/Invoices";i:46;s:14:"Menu/Customers";i:47;s:15:"Menu/Activities";i:48;s:13:"Menu/Vouchers";i:49;s:13:"Menu/Invoices";i:50;s:10:"Menu/Items";i:51;s:16:"Menu/EditCompany";i:52;s:19:"Menu/Administration";i:53;s:12:"Menu/Signout";}s:10:"translated";a:54:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:10:"Ny faktura";i:14;s:11:"Beskrivelse";i:15;s:12:"Faktura dato";i:16;s:13:"Forfalds dato";i:17;s:17:"PÃ¥mindelses dato";i:18;s:14:"1. rykker dato";i:19;s:14:"2. rykker dato";i:20;s:12:"Inkasso dato";i:21;s:8:"BogfÃ¸rt";i:22;s:7:"TilfÃ¸j";i:23;s:6:"Varenr";i:24;s:4:"Navn";i:25;s:5:"Antal";i:26;s:5:"Enhed";i:27;s:4:"Pris";i:28;s:5:"Rabat";i:29;s:6:"BelÃ¸b";i:30;s:3:"Gem";i:31;s:3:"stk";i:32;s:5:"timer";i:33;s:3:"dag";i:34;s:3:"uge";i:35;s:2:"pk";i:36;s:7:"Rediger";i:37;s:5:"Fjern";i:38;s:14:"Betalings dato";i:39;s:27:"404: Siden blev ikke fundet";i:40;s:59:"Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.";i:41;s:25:"Customer/Menu/Information";i:42;s:19:"Customer/Menu/Cases";i:43;s:22:"Customer/Menu/Projects";i:44;s:24:"Customer/Menu/Activities";i:45;s:22:"Customer/Menu/Invoices";i:46;s:14:"Menu/Customers";i:47;s:15:"Menu/Activities";i:48;s:13:"Menu/Vouchers";i:49;s:13:"Menu/Invoices";i:50;s:10:"Menu/Items";i:51;s:16:"Menu/EditCompany";i:52;s:19:"Menu/Administration";i:53;s:12:"Menu/Signout";}}', 1348589760),
('LANG_8725c184f895191a50abf5c67d18c4f8', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:8:{i:0;s:14:"Menu/Customers";i:1;s:15:"Menu/Activities";i:2;s:13:"Menu/Vouchers";i:3;s:13:"Menu/Invoices";i:4;s:10:"Menu/Items";i:5;s:16:"Menu/EditCompany";i:6;s:19:"Menu/Administration";i:7;s:12:"Menu/Signout";}s:10:"translated";a:8:{i:0;s:14:"Menu/Customers";i:1;s:15:"Menu/Activities";i:2;s:13:"Menu/Vouchers";i:3;s:13:"Menu/Invoices";i:4;s:10:"Menu/Items";i:5;s:16:"Menu/EditCompany";i:6;s:19:"Menu/Administration";i:7;s:12:"Menu/Signout";}}', 1348589653),
('LANG_8820d79dd45c5dbea23172d151b9883e', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:35:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:7:"Rediger";i:14;s:24:"Ingen Ã¥bne sager fundet";i:15;s:25:"Customer/Menu/Information";i:16;s:19:"Customer/Menu/Cases";i:17;s:22:"Customer/Menu/Projects";i:18;s:24:"Customer/Menu/Activities";i:19;s:22:"Customer/Menu/Invoices";i:20;s:14:"Menu/Customers";i:21;s:15:"Menu/Activities";i:22;s:13:"Menu/Vouchers";i:23;s:13:"Menu/Invoices";i:24;s:10:"Menu/Items";i:25;s:16:"Menu/EditCompany";i:26;s:19:"Menu/Administration";i:27;s:12:"Menu/Signout";i:28;s:13:"Customer/Edit";i:29;s:14:"Customer/Notes";i:30;s:24:"Customer/DeliveryAddress";i:31;s:21:"Customer/Cases/Latest";i:32;s:23:"Customer/Cases/NoneOpen";i:33;s:20:"Customer/Case/Latest";i:34;s:22:"Customer/Case/NoneOpen";}s:10:"translated";a:35:{i:0;s:11:"Information";i:1;s:5:"Sager";i:2;s:9:"Projekter";i:3;s:11:"Aktiviteter";i:4;s:9:"Fakturaer";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:7:"Rediger";i:14;s:24:"Ingen Ã¥bne sager fundet";i:15;s:25:"Customer/Menu/Information";i:16;s:19:"Customer/Menu/Cases";i:17;s:22:"Customer/Menu/Projects";i:18;s:24:"Customer/Menu/Activities";i:19;s:22:"Customer/Menu/Invoices";i:20;s:14:"Menu/Customers";i:21;s:15:"Menu/Activities";i:22;s:13:"Menu/Vouchers";i:23;s:13:"Menu/Invoices";i:24;s:10:"Menu/Items";i:25;s:16:"Menu/EditCompany";i:26;s:19:"Menu/Administration";i:27;s:12:"Menu/Signout";i:28;s:13:"Customer/Edit";i:29;s:14:"Customer/Notes";i:30;s:24:"Customer/DeliveryAddress";i:31;s:21:"Customer/Cases/Latest";i:32;s:23:"Customer/Cases/NoneOpen";i:33;s:20:"Customer/Case/Latest";i:34;s:22:"Customer/Case/NoneOpen";}}', 1348675238),
('LANG_b0b47d9fb6ee11349b592391ba7314d6', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:30:{i:0;s:10:"Betalinger";i:1;s:4:"SÃ¸g";i:2;s:8:"Kommende";i:3;s:11:"Overskredet";i:4;s:7:"Betalte";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:13:"Nye fakturaer";i:14;s:16:"Ingen fakturaer.";i:15;s:14:"Menu/Customers";i:16;s:15:"Menu/Activities";i:17;s:13:"Menu/Vouchers";i:18;s:13:"Menu/Invoices";i:19;s:10:"Menu/Items";i:20;s:16:"Menu/EditCompany";i:21;s:19:"Menu/Administration";i:22;s:12:"Menu/Signout";i:23;s:18:"Invoice/NoInvoices";i:24;s:16:"Invoice/Payments";i:25;s:14:"Invoice/Search";i:26;s:15:"Invoice/Comming";i:27;s:15:"Invoice/Overdue";i:28;s:13:"Invoice/Payed";i:29;s:19:"Invoice/NewInvoices";}s:10:"translated";a:30:{i:0;s:10:"Betalinger";i:1;s:4:"SÃ¸g";i:2;s:8:"Kommende";i:3;s:11:"Overskredet";i:4;s:7:"Betalte";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:13:"Nye fakturaer";i:14;s:16:"Ingen fakturaer.";i:15;s:14:"Menu/Customers";i:16;s:15:"Menu/Activities";i:17;s:13:"Menu/Vouchers";i:18;s:13:"Menu/Invoices";i:19;s:10:"Menu/Items";i:20;s:16:"Menu/EditCompany";i:21;s:19:"Menu/Administration";i:22;s:12:"Menu/Signout";i:23;s:18:"Invoice/NoInvoices";i:24;s:16:"Invoice/Payments";i:25;s:14:"Invoice/Search";i:26;s:15:"Invoice/Comming";i:27;s:15:"Invoice/Overdue";i:28;s:13:"Invoice/Payed";i:29;s:19:"Invoice/NewInvoices";}}', 1348679450),
('LANG_b460f82af7eba742d2e6d1ae8083b328', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:15:{i:0;s:6:"E-mail";i:1;s:11:"Adgangskode";i:2;s:7:"Log ind";i:3;s:35:"%s er ikke en gyldig e-mail-adresse";i:4;s:21:"%s mÃ¥ ikke vÃ¦re tom";i:5;s:18:"Forkert brugernavn";i:6;s:10:"Brugernavn";i:7;s:39:"Forkert brugernavn og/eller adgangskode";i:8;s:33:"%s indeholder ugyldige karakterer";i:9;s:41:"Ugyldigt brugernavn og/eller adgangskode.";i:10;s:18:"User/UsernameLabel";i:11;s:18:"User/PasswordLabel";i:12;s:16:"User/SubmitLabel";i:13;s:19:"Error/CannotBeEmpty";i:14;s:23:"Error/Username/TooShort";}s:10:"translated";a:15:{i:0;s:6:"E-mail";i:1;s:11:"Adgangskode";i:2;s:7:"Log ind";i:3;s:35:"%s er ikke en gyldig e-mail-adresse";i:4;s:21:"%s mÃ¥ ikke vÃ¦re tom";i:5;s:18:"Forkert brugernavn";i:6;s:10:"Brugernavn";i:7;s:39:"Forkert brugernavn og/eller adgangskode";i:8;s:33:"%s indeholder ugyldige karakterer";i:9;s:41:"Ugyldigt brugernavn og/eller adgangskode.";i:10;s:18:"User/UsernameLabel";i:11;s:18:"User/PasswordLabel";i:12;s:16:"User/SubmitLabel";i:13;s:19:"Error/CannotBeEmpty";i:14;s:23:"Error/Username/TooShort";}}', 1348679442),
('LANG_b5681892475216652be78c4b47606c0b', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:50:{i:0;s:9:"Log timer";i:1;s:8:"Oversigt";i:2;s:6:"Kunder";i:3;s:9:"Aktivitet";i:4;s:5:"Bilag";i:5;s:7:"Faktura";i:6;s:5:"Varer";i:7;s:13:"Rediger firma";i:8;s:14:"Administration";i:9;s:6:"Log ud";i:10;s:5:"Kunde";i:11;s:11:"-- VÃ¦lg --";i:12;s:7:"Projekt";i:13;s:5:"Timer";i:14;s:8:"Minutter";i:15;s:11:"Beskrivelse";i:16;s:19:"Tidligere logninger";i:17;s:16:"Logninger for %s";i:18;s:3:"T:M";i:19;s:49:"Er du sikker pÃ¥, at du vil slette denne logning?";i:20;s:4:"Slet";i:21;s:3:"Ret";i:22;s:17:"Activity/LogHours";i:23;s:17:"Activity/Overview";i:24;s:14:"Menu/Customers";i:25;s:15:"Menu/Activities";i:26;s:13:"Menu/Vouchers";i:27;s:13:"Menu/Invoices";i:28;s:10:"Menu/Items";i:29;s:16:"Menu/EditCompany";i:30;s:19:"Menu/Administration";i:31;s:12:"Menu/Signout";i:32;s:17:"Activity/Customer";i:33;s:15:"Activity/Choose";i:34;s:16:"Activity/Project";i:35;s:17:"Activity/Activity";i:36;s:14:"Activity/Hours";i:37;s:16:"Activity/Minutes";i:38;s:20:"Activity/Description";i:39;s:27:"Activity/PreviouslyActivity";i:40;s:7:"Project";i:41;s:23:"Activity/ActivityByDate";i:42;s:26:"Activity/HoursMinutesShort";i:43;s:15:"Activity/Delete";i:44;s:13:"Activity/Edit";i:45;s:26:"Customer/Activity/Customer";i:46;s:25:"Customer/Activity/Project";i:47;s:26:"Customer/Activity/Activity";i:48;s:23:"Customer/Activity/Hours";i:49;s:25:"Customer/Activity/Minutes";}s:10:"translated";a:50:{i:0;s:9:"Log timer";i:1;s:8:"Oversigt";i:2;s:6:"Kunder";i:3;s:9:"Aktivitet";i:4;s:5:"Bilag";i:5;s:7:"Faktura";i:6;s:5:"Varer";i:7;s:13:"Rediger firma";i:8;s:14:"Administration";i:9;s:6:"Log ud";i:10;s:5:"Kunde";i:11;s:11:"-- VÃ¦lg --";i:12;s:7:"Projekt";i:13;s:5:"Timer";i:14;s:8:"Minutter";i:15;s:11:"Beskrivelse";i:16;s:19:"Tidligere logninger";i:17;s:16:"Logninger for %s";i:18;s:3:"T:M";i:19;s:49:"Er du sikker pÃ¥, at du vil slette denne logning?";i:20;s:4:"Slet";i:21;s:3:"Ret";i:22;s:17:"Activity/LogHours";i:23;s:17:"Activity/Overview";i:24;s:14:"Menu/Customers";i:25;s:15:"Menu/Activities";i:26;s:13:"Menu/Vouchers";i:27;s:13:"Menu/Invoices";i:28;s:10:"Menu/Items";i:29;s:16:"Menu/EditCompany";i:30;s:19:"Menu/Administration";i:31;s:12:"Menu/Signout";i:32;s:17:"Activity/Customer";i:33;s:15:"Activity/Choose";i:34;s:16:"Activity/Project";i:35;s:17:"Activity/Activity";i:36;s:14:"Activity/Hours";i:37;s:16:"Activity/Minutes";i:38;s:20:"Activity/Description";i:39;s:27:"Activity/PreviouslyActivity";i:40;s:7:"Project";i:41;s:23:"Activity/ActivityByDate";i:42;s:26:"Activity/HoursMinutesShort";i:43;s:15:"Activity/Delete";i:44;s:13:"Activity/Edit";i:45;s:26:"Customer/Activity/Customer";i:46;s:25:"Customer/Activity/Project";i:47;s:26:"Customer/Activity/Activity";i:48;s:23:"Customer/Activity/Hours";i:49;s:25:"Customer/Activity/Minutes";}}', 1348613429),
('LANG_d49521d94d454a300fca5f1900d83e35', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:44:{i:0;s:6:"Kunder";i:1;s:9:"Aktivitet";i:2;s:5:"Bilag";i:3;s:7:"Faktura";i:4;s:5:"Varer";i:5;s:13:"Rediger firma";i:6;s:14:"Administration";i:7;s:6:"Log ud";i:8;s:4:"Navn";i:9;s:4:"Land";i:10;s:12:"Arbejdstimer";i:11;s:11:"PÃ¥mindelse";i:12;s:9:"1. rykker";i:13;s:9:"2. rykker";i:14;s:7:"Inkasso";i:15;s:8:"Generelt";i:16;s:11:"Momsprocent";i:17;s:4:"Logo";i:18;s:7:"Opdater";i:19;s:12:"Kontakt info";i:20;s:7:"Adresse";i:21;s:6:"Postnr";i:22;s:2:"By";i:23;s:16:"-- VÃ¦lg land --";i:24;s:7:"Telefon";i:25;s:12:"Mobiltelefon";i:26;s:3:"CVR";i:27;s:10:"Hjemmeside";i:28;s:4:"Bank";i:29;s:12:"Bank reg. nr";i:30;s:14:"Bank konto nr.";i:31;s:15:"EAN-lokationsnr";i:32;s:23:"dage efter forfaldsdato";i:33;s:22:"dage efter pÃ¥mindelse";i:34;s:20:"dage efter 1. rykker";i:35;s:20:"dage efter 2. rykker";i:36;s:14:"Menu/Customers";i:37;s:15:"Menu/Activities";i:38;s:13:"Menu/Vouchers";i:39;s:13:"Menu/Invoices";i:40;s:10:"Menu/Items";i:41;s:16:"Menu/EditCompany";i:42;s:19:"Menu/Administration";i:43;s:12:"Menu/Signout";}s:10:"translated";a:44:{i:0;s:6:"Kunder";i:1;s:9:"Aktivitet";i:2;s:5:"Bilag";i:3;s:7:"Faktura";i:4;s:5:"Varer";i:5;s:13:"Rediger firma";i:6;s:14:"Administration";i:7;s:6:"Log ud";i:8;s:4:"Navn";i:9;s:4:"Land";i:10;s:12:"Arbejdstimer";i:11;s:11:"PÃ¥mindelse";i:12;s:9:"1. rykker";i:13;s:9:"2. rykker";i:14;s:7:"Inkasso";i:15;s:8:"Generelt";i:16;s:11:"Momsprocent";i:17;s:4:"Logo";i:18;s:7:"Opdater";i:19;s:12:"Kontakt info";i:20;s:7:"Adresse";i:21;s:6:"Postnr";i:22;s:2:"By";i:23;s:16:"-- VÃ¦lg land --";i:24;s:7:"Telefon";i:25;s:12:"Mobiltelefon";i:26;s:3:"CVR";i:27;s:10:"Hjemmeside";i:28;s:4:"Bank";i:29;s:12:"Bank reg. nr";i:30;s:14:"Bank konto nr.";i:31;s:15:"EAN-lokationsnr";i:32;s:23:"dage efter forfaldsdato";i:33;s:22:"dage efter pÃ¥mindelse";i:34;s:20:"dage efter 1. rykker";i:35;s:20:"dage efter 2. rykker";i:36;s:14:"Menu/Customers";i:37;s:15:"Menu/Activities";i:38;s:13:"Menu/Vouchers";i:39;s:13:"Menu/Invoices";i:40;s:10:"Menu/Items";i:41;s:16:"Menu/EditCompany";i:42;s:19:"Menu/Administration";i:43;s:12:"Menu/Signout";}}', 1348524165),
('LANG_d7308db13418c03250372d66063582a1', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:54:{i:0;s:8:"Oversigt";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:9:"Nyt bilag";i:10;s:7:"Periode";i:11;s:3:"Fra";i:12;s:3:"Til";i:13;s:13:"Kun bogfÃ¸rte";i:14;s:7:"Opdater";i:15;s:4:"Navn";i:16;s:11:"Beskrivelse";i:17;s:11:"Fakturadato";i:18;s:13:"Betalingsdato";i:19;s:4:"Moms";i:20;s:16:"BelÃ¸b ink. moms";i:21;s:49:"Er du sikker pÃ¥, at du vil bogfÃ¸re dette bilag?";i:22;s:7:"BogfÃ¸r";i:23;s:47:"Er du sikker pÃ¥, at du vil slette dette bilag?";i:24;s:4:"Slet";i:25;s:7:"Rediger";i:26;s:5:"Total";i:27;s:14:"Menu/Customers";i:28;s:15:"Menu/Activities";i:29;s:13:"Menu/Vouchers";i:30;s:13:"Menu/Invoices";i:31;s:10:"Menu/Items";i:32;s:16:"Menu/EditCompany";i:33;s:19:"Menu/Administration";i:34;s:12:"Menu/Signout";i:35;s:16:"Voucher/Overview";i:36;s:18:"Voucher/NewVoucher";i:37;s:14:"Voucher/Period";i:38;s:12:"Voucher/From";i:39;s:10:"Voucher/To";i:40;s:20:"Voucher/RecordedOnly";i:41;s:14:"Voucher/Update";i:42;s:12:"Voucher/Name";i:43;s:19:"Voucher/Description";i:44;s:19:"Voucher/InvoiceDate";i:45;s:19:"Voucher/PaymentDate";i:46;s:11:"Voucher/Vat";i:47;s:21:"Voucher/AmountWithVat";i:48;s:21:"Voucher/ConfirmRecord";i:49;s:14:"Voucher/Record";i:50;s:21:"Voucher/ConfirmDelete";i:51;s:14:"Voucher/Delete";i:52;s:12:"Voucher/Edit";i:53;s:13:"Voucher/Total";}s:10:"translated";a:54:{i:0;s:8:"Oversigt";i:1;s:6:"Kunder";i:2;s:9:"Aktivitet";i:3;s:5:"Bilag";i:4;s:7:"Faktura";i:5;s:5:"Varer";i:6;s:13:"Rediger firma";i:7;s:14:"Administration";i:8;s:6:"Log ud";i:9;s:9:"Nyt bilag";i:10;s:7:"Periode";i:11;s:3:"Fra";i:12;s:3:"Til";i:13;s:13:"Kun bogfÃ¸rte";i:14;s:7:"Opdater";i:15;s:4:"Navn";i:16;s:11:"Beskrivelse";i:17;s:11:"Fakturadato";i:18;s:13:"Betalingsdato";i:19;s:4:"Moms";i:20;s:16:"BelÃ¸b ink. moms";i:21;s:49:"Er du sikker pÃ¥, at du vil bogfÃ¸re dette bilag?";i:22;s:7:"BogfÃ¸r";i:23;s:47:"Er du sikker pÃ¥, at du vil slette dette bilag?";i:24;s:4:"Slet";i:25;s:7:"Rediger";i:26;s:5:"Total";i:27;s:14:"Menu/Customers";i:28;s:15:"Menu/Activities";i:29;s:13:"Menu/Vouchers";i:30;s:13:"Menu/Invoices";i:31;s:10:"Menu/Items";i:32;s:16:"Menu/EditCompany";i:33;s:19:"Menu/Administration";i:34;s:12:"Menu/Signout";i:35;s:16:"Voucher/Overview";i:36;s:18:"Voucher/NewVoucher";i:37;s:14:"Voucher/Period";i:38;s:12:"Voucher/From";i:39;s:10:"Voucher/To";i:40;s:20:"Voucher/RecordedOnly";i:41;s:14:"Voucher/Update";i:42;s:12:"Voucher/Name";i:43;s:19:"Voucher/Description";i:44;s:19:"Voucher/InvoiceDate";i:45;s:19:"Voucher/PaymentDate";i:46;s:11:"Voucher/Vat";i:47;s:21:"Voucher/AmountWithVat";i:48;s:21:"Voucher/ConfirmRecord";i:49;s:14:"Voucher/Record";i:50;s:21:"Voucher/ConfirmDelete";i:51;s:14:"Voucher/Delete";i:52;s:12:"Voucher/Edit";i:53;s:13:"Voucher/Total";}}', 1348630014),
('LANG_f2367dd213123205f90d35d1ee9582f6', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:37:{i:0;s:10:"Betalinger";i:1;s:4:"SÃ¸g";i:2;s:8:"Kommende";i:3;s:11:"Overskredet";i:4;s:7:"Betalte";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:7:"Periode";i:14;s:3:"Fra";i:15;s:3:"Til";i:16;s:13:"Kun bogfÃ¸rte";i:17;s:7:"Opdater";i:18;s:14:"Menu/Customers";i:19;s:15:"Menu/Activities";i:20;s:13:"Menu/Vouchers";i:21;s:13:"Menu/Invoices";i:22;s:10:"Menu/Items";i:23;s:16:"Menu/EditCompany";i:24;s:19:"Menu/Administration";i:25;s:12:"Menu/Signout";i:26;s:18:"Invoice/NoInvoices";i:27;s:16:"Invoice/Payments";i:28;s:14:"Invoice/Search";i:29;s:15:"Invoice/Comming";i:30;s:15:"Invoice/Overdue";i:31;s:13:"Invoice/Payed";i:32;s:14:"Invoice/Period";i:33;s:12:"Invoice/From";i:34;s:10:"Invoice/To";i:35;s:20:"Invoice/RecordedOnly";i:36;s:14:"Invoice/Update";}s:10:"translated";a:37:{i:0;s:10:"Betalinger";i:1;s:4:"SÃ¸g";i:2;s:8:"Kommende";i:3;s:11:"Overskredet";i:4;s:7:"Betalte";i:5;s:6:"Kunder";i:6;s:9:"Aktivitet";i:7;s:5:"Bilag";i:8;s:7:"Faktura";i:9;s:5:"Varer";i:10;s:13:"Rediger firma";i:11;s:14:"Administration";i:12;s:6:"Log ud";i:13;s:7:"Periode";i:14;s:3:"Fra";i:15;s:3:"Til";i:16;s:13:"Kun bogfÃ¸rte";i:17;s:7:"Opdater";i:18;s:14:"Menu/Customers";i:19;s:15:"Menu/Activities";i:20;s:13:"Menu/Vouchers";i:21;s:13:"Menu/Invoices";i:22;s:10:"Menu/Items";i:23;s:16:"Menu/EditCompany";i:24;s:19:"Menu/Administration";i:25;s:12:"Menu/Signout";i:26;s:18:"Invoice/NoInvoices";i:27;s:16:"Invoice/Payments";i:28;s:14:"Invoice/Search";i:29;s:15:"Invoice/Comming";i:30;s:15:"Invoice/Overdue";i:31;s:13:"Invoice/Payed";i:32;s:14:"Invoice/Period";i:33;s:12:"Invoice/From";i:34;s:10:"Invoice/To";i:35;s:20:"Invoice/RecordedOnly";i:36;s:14:"Invoice/Update";}}', 1348679455),
('LANG_f36b9d8103959471a7034e66daaccf25', 'O:28:"Pecee_Model_Translate_Object":2:{s:8:"original";a:42:{i:0;s:6:"Kunder";i:1;s:9:"Aktivitet";i:2;s:5:"Bilag";i:3;s:7:"Faktura";i:4;s:5:"Varer";i:5;s:13:"Rediger firma";i:6;s:14:"Administration";i:7;s:6:"Log ud";i:8;s:10:"Faktura nr";i:9;s:8:"Kunde nr";i:10;s:11:"Fakturadato";i:11;s:13:"Betalingsdato";i:12;s:6:"Varenr";i:13;s:4:"Navn";i:14;s:11:"Beskrivelse";i:15;s:5:"Antal";i:16;s:5:"Enhed";i:17;s:4:"Pris";i:18;s:6:"BelÃ¸b";i:19;s:3:"stk";i:20;s:5:"timer";i:21;s:3:"dag";i:22;s:3:"uge";i:23;s:2:"pk";i:24;s:18:"Total (eksk. moms)";i:25;s:9:"%s%% moms";i:26;s:8:"Total %s";i:27;s:78:"Renter kan blive opkrÃ¦vet ved forsinket betaling i henhold til gÃ¦ldende lov.";i:28;s:14:"Menu/Customers";i:29;s:15:"Menu/Activities";i:30;s:13:"Menu/Vouchers";i:31;s:13:"Menu/Invoices";i:32;s:10:"Menu/Items";i:33;s:16:"Menu/EditCompany";i:34;s:19:"Menu/Administration";i:35;s:12:"Menu/Signout";i:36;s:10:"Kreditnota";i:37;s:16:"Item/Units/Piece";i:38;s:16:"Item/Units/Hours";i:39;s:14:"Item/Units/Day";i:40;s:15:"Item/Units/Week";i:41;s:18:"Item/Units/Package";}s:10:"translated";a:42:{i:0;s:6:"Kunder";i:1;s:9:"Aktivitet";i:2;s:5:"Bilag";i:3;s:7:"Faktura";i:4;s:5:"Varer";i:5;s:13:"Rediger firma";i:6;s:14:"Administration";i:7;s:6:"Log ud";i:8;s:10:"Faktura nr";i:9;s:8:"Kunde nr";i:10;s:11:"Fakturadato";i:11;s:13:"Betalingsdato";i:12;s:6:"Varenr";i:13;s:4:"Navn";i:14;s:11:"Beskrivelse";i:15;s:5:"Antal";i:16;s:5:"Enhed";i:17;s:4:"Pris";i:18;s:6:"BelÃ¸b";i:19;s:3:"stk";i:20;s:5:"timer";i:21;s:3:"dag";i:22;s:3:"uge";i:23;s:2:"pk";i:24;s:18:"Total (eksk. moms)";i:25;s:9:"%s%% moms";i:26;s:8:"Total %s";i:27;s:78:"Renter kan blive opkrÃ¦vet ved forsinket betaling i henhold til gÃ¦ldende lov.";i:28;s:14:"Menu/Customers";i:29;s:15:"Menu/Activities";i:30;s:13:"Menu/Vouchers";i:31;s:13:"Menu/Invoices";i:32;s:10:"Menu/Items";i:33;s:16:"Menu/EditCompany";i:34;s:19:"Menu/Administration";i:35;s:12:"Menu/Signout";i:36;s:10:"Kreditnota";i:37;s:16:"Item/Units/Piece";i:38;s:16:"Item/Units/Hours";i:39;s:14:"Item/Units/Day";i:40;s:15:"Item/Units/Week";i:41;s:18:"Item/Units/Package";}}', 1348679678);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Case`
--

CREATE TABLE IF NOT EXISTS `Case` (
  `CaseID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentCaseID` bigint(20) DEFAULT NULL,
  `CaseSubjectID` bigint(20) DEFAULT NULL,
  `CustomerID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Text` longtext,
  `CreatedDate` datetime NOT NULL,
  `Closed` tinyint(1) DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CaseID`),
  KEY `ParentCaseID` (`ParentCaseID`),
  KEY `CaseSubjectID` (`CaseSubjectID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Closed` (`Closed`),
  KEY `Deleted` (`Deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `Case`
--

INSERT INTO `Case` (`CaseID`, `ParentCaseID`, `CaseSubjectID`, `CustomerID`, `UserID`, `Text`, `CreatedDate`, `Closed`, `Deleted`) VALUES
(1, NULL, 1, 1, 1, 'Test', '2012-09-25 23:44:01', 0, 0);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CaseSubject`
--

CREATE TABLE IF NOT EXISTS `CaseSubject` (
  `CaseSubjectID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Title` varchar(300) NOT NULL,
  PRIMARY KEY (`CaseSubjectID`),
  KEY `Title` (`Title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `CaseSubject`
--

INSERT INTO `CaseSubject` (`CaseSubjectID`, `Title`) VALUES
(1, 'Diverse');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Company`
--

CREATE TABLE IF NOT EXISTS `Company` (
  `CompanyID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(400) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `VatPercentage` int(11) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Main` tinyint(1) NOT NULL,
  PRIMARY KEY (`CompanyID`),
  KEY `Name` (`Name`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Main` (`Main`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `Company`
--

INSERT INTO `Company` (`CompanyID`, `Name`, `Country`, `VatPercentage`, `CreatedDate`, `Main`) VALUES
(1, 'Pecee', 'Denmark', 25, '2012-07-06 14:51:09', 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CompanyData`
--

CREATE TABLE IF NOT EXISTS `CompanyData` (
  `CompanyID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext,
  KEY `CompanyID` (`CompanyID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `CompanyData`
--

INSERT INTO `CompanyData` (`CompanyID`, `Key`, `Value`) VALUES
(1, 'vatno', '33805675'),
(1, 'workhours', '7.30'),
(1, 'logo', '90FE51741ABC497B90630935ECADC518'),
(1, 'reminderdays', '0'),
(1, 'firstreminderdays', '10'),
(1, 'secondreminderdays', '10'),
(1, 'debtcollectiondays', '10'),
(1, 'address', 'Skaffervej 8, 21'),
(1, 'postalnumber', '2400'),
(1, 'city', 'KÃ¸benhavn NV'),
(1, 'country', 'Denmark'),
(1, 'contact', NULL),
(1, 'phone', '32207337'),
(1, 'cellphone', '60847679'),
(1, 'website', 'http://www.pecee.dk'),
(1, 'bank', 'Nordea'),
(1, 'ean', ''),
(1, 'internalaccount', NULL),
(1, 'bankregno', '2410'),
(1, 'bankaccount', '5908140453'),
(1, 'paymentdays', '12');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Customer`
--

CREATE TABLE IF NOT EXISTS `Customer` (
  `CustomerID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `CreatedDate` (`CreatedDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Data dump for tabellen `Customer`
--

INSERT INTO `Customer` (`CustomerID`, `UserID`, `Name`, `CreatedDate`) VALUES
(1, 1, 'Ebita', '2012-08-02 11:12:51'),
(2, 1, 'Pecee', '2012-08-02 11:14:09');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CustomerData`
--

CREATE TABLE IF NOT EXISTS `CustomerData` (
  `CustomerID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext,
  KEY `CustomerID` (`CustomerID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `CustomerData`
--

INSERT INTO `CustomerData` (`CustomerID`, `Key`, `Value`) VALUES
(2, 'name', 'Pecee'),
(2, 'address', 'Skaffervej 8, 21'),
(2, 'postalnumber', '2400'),
(2, 'city', 'KÃ¸benhavn NV'),
(2, 'country', 'Danmark'),
(2, 'contact', 'Simon SessingÃ¸'),
(2, 'deliveryaddress', ''),
(2, 'notes', ''),
(2, 'phone', '32207337'),
(2, 'cellphone', '53626843'),
(2, 'vat', '33805675'),
(2, 'bank', 'Nordea'),
(2, 'ean', ''),
(2, 'internalaccount', ''),
(1, 'name', 'Ebita'),
(1, 'address', 'Vesterbrogade 10, 4 .sal'),
(1, 'postalnumber', '1620'),
(1, 'city', 'KÃ¸benhavn V'),
(1, 'country', 'Danmark'),
(1, 'contact', 'Henrik DanbjÃ¸rg'),
(1, 'deliveryaddress', ''),
(1, 'notes', ''),
(1, 'phone', '33321317'),
(1, 'cellphone', ''),
(1, 'vat', '10105420'),
(1, 'bank', 'Jyske Bank'),
(1, 'ean', ''),
(1, 'internalaccount', ''),
(1, 'website', 'http://www.ebita.dk');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Entity`
--

CREATE TABLE IF NOT EXISTS `Entity` (
  `EntityID` varchar(13) NOT NULL,
  `EntityCategoryID` varchar(13) NOT NULL,
  `Title` varchar(500) NOT NULL,
  `Content` longtext NOT NULL,
  `PubDate` int(11) NOT NULL,
  `ActiveFrom` int(11) DEFAULT NULL,
  `ActiveTo` int(11) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`EntityID`),
  KEY `EntityCategoryID` (`EntityCategoryID`,`Title`,`PubDate`,`ActiveFrom`,`ActiveTo`,`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `Entity`
--


-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `EntityCategory`
--

CREATE TABLE IF NOT EXISTS `EntityCategory` (
  `EntityCategoryID` varchar(13) NOT NULL,
  `ParentEntityCategoryID` varchar(13) DEFAULT NULL,
  `Path` varchar(500) DEFAULT NULL,
  `Title` varchar(300) NOT NULL,
  `Description` longtext NOT NULL,
  `Order` int(11) NOT NULL,
  `PubDate` int(11) NOT NULL,
  PRIMARY KEY (`EntityCategoryID`),
  KEY `ParentEntityCategoryID` (`ParentEntityCategoryID`),
  KEY `Path` (`Path`),
  KEY `Title` (`Title`),
  KEY `Order` (`Order`),
  KEY `PubDate` (`PubDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `EntityCategory`
--


-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `EntityField`
--

CREATE TABLE IF NOT EXISTS `EntityField` (
  `EntityID` varchar(13) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Value` longtext NOT NULL,
  KEY `EntityID` (`EntityID`,`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `EntityField`
--


-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `File`
--

CREATE TABLE IF NOT EXISTS `File` (
  `FileID` varchar(32) NOT NULL,
  `Filename` varchar(255) NOT NULL,
  `OriginalFilename` varchar(300) DEFAULT NULL,
  `Path` varchar(400) DEFAULT NULL,
  `Type` varchar(255) NOT NULL,
  `Bytes` bigint(20) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`FileID`),
  KEY `Type` (`Type`),
  KEY `Bytes` (`Bytes`),
  KEY `CreatedDate` (`CreatedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `File`
--

INSERT INTO `File` (`FileID`, `Filename`, `OriginalFilename`, `Path`, `Type`, `Bytes`, `CreatedDate`) VALUES
('90FE51741ABC497B90630935ECADC518', 'pecee.png', 'logo.png', '', 'image/png', 14905, '2012-09-05 12:55:08');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Invoice`
--

CREATE TABLE IF NOT EXISTS `Invoice` (
  `InvoiceID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CustomerID` bigint(20) DEFAULT NULL,
  `UserID` bigint(20) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Recorded` tinyint(1) NOT NULL,
  `Deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`InvoiceID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `Type` (`Type`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Recorded` (`Recorded`),
  KEY `Deleted` (`Deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Data dump for tabellen `Invoice`
--

INSERT INTO `Invoice` (`InvoiceID`, `CustomerID`, `UserID`, `Type`, `CreatedDate`, `Recorded`, `Deleted`) VALUES
(1, 1, 1, 'invoice', '2012-09-05 22:11:02', 0, 0),
(2, 1, 1, 'payment', '2012-09-20 18:08:52', 0, 0),
(3, 1, 1, 'payment', '2012-09-21 09:16:21', 0, 0),
(4, NULL, 1, 'voucher', '2012-09-22 01:05:13', 0, 0),
(5, NULL, 1, 'voucher', '2012-09-22 01:08:31', 0, 0),
(6, NULL, 1, 'voucher', '2012-09-22 01:37:34', 0, 0),
(7, NULL, 1, 'voucher', '2012-09-22 02:17:25', 0, 0),
(8, NULL, 1, 'voucher', '2012-09-22 02:18:51', 0, 0),
(9, NULL, 1, 'voucher', '2012-09-22 02:19:53', 0, 0),
(10, NULL, 1, 'voucher', '2012-09-22 02:21:36', 0, 0),
(11, NULL, 1, 'voucher', '2012-09-22 02:23:10', 0, 0),
(12, NULL, 1, 'voucher', '2012-09-22 02:23:57', 0, 0),
(13, NULL, 1, 'voucher', '2012-09-22 02:24:58', 0, 0),
(14, NULL, 1, 'voucher', '2012-09-22 05:47:48', 0, 0),
(15, NULL, 1, 'voucher', '2012-09-22 05:53:14', 0, 0),
(16, NULL, 1, 'voucher', '2012-09-23 16:45:09', 0, 0),
(20, 1, 1, 'invoice', '2012-09-24 13:51:35', 0, 0),
(24, 1, 1, 'creditmemo', '2012-09-25 12:50:20', 0, 0),
(25, 1, 1, 'invoice', '2012-09-25 12:55:44', 0, 0),
(26, NULL, 1, 'voucher', '2012-09-25 21:06:51', 0, 0),
(27, NULL, 1, 'voucher', '2012-10-03 17:03:51', 0, 0),
(28, NULL, 1, 'voucher', '2012-10-03 17:05:27', 0, 0),
(29, 1, 1, 'payment', '2012-10-09 22:10:55', 0, 0),
(30, 1, 1, 'payment', '2012-10-09 22:21:09', 0, 0);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `InvoiceData`
--

CREATE TABLE IF NOT EXISTS `InvoiceData` (
  `InvoiceID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` longtext NOT NULL,
  KEY `InvoiceID` (`InvoiceID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `InvoiceData`
--

INSERT INTO `InvoiceData` (`InvoiceID`, `Key`, `Value`) VALUES
(4, 'name', 'Fullrate'),
(4, 'description', 'Internet'),
(4, 'invoicedate', '22-09-2012'),
(4, 'paymentdate', '10-09-2012'),
(4, 'invoiceno', '6354583'),
(5, 'name', 'Fullrate'),
(5, 'description', 'Internet'),
(5, 'invoicedate', '22-09-2012'),
(5, 'paymentdate', '06-08-2012'),
(5, 'invoiceno', ''),
(6, 'name', 'Fullrate'),
(6, 'description', 'Internet'),
(6, 'invoicedate', '22-09-2012'),
(6, 'paymentdate', '06-07-2012'),
(6, 'invoiceno', ''),
(8, 'name', 'Hetzner'),
(8, 'description', 'Server'),
(8, 'invoicedate', '22-09-2012'),
(8, 'paymentdate', '01-08-2012'),
(8, 'invoiceno', 'R0002232456'),
(9, 'name', 'Hetzner'),
(9, 'description', 'Server'),
(9, 'invoicedate', '22-09-2012'),
(9, 'paymentdate', '01-07-2012'),
(9, 'invoiceno', 'R0002171086'),
(10, 'name', 'RizRaz'),
(10, 'description', 'KundemÃ¸de'),
(10, 'invoicedate', '22-09-2012'),
(10, 'paymentdate', '21-09-2012'),
(10, 'invoiceno', '13893896'),
(11, 'name', 'Forlagshuset'),
(11, 'description', 'Husleje v. lokaler'),
(11, 'invoicedate', '22-09-2012'),
(11, 'paymentdate', '04-07-2012'),
(11, 'invoiceno', '110655'),
(12, 'name', 'Forlagshuset'),
(12, 'description', 'Husleje v. lokaler'),
(12, 'invoicedate', '22-09-2012'),
(12, 'paymentdate', '01-08-2012'),
(12, 'invoiceno', '110691'),
(14, 'name', 'DK-Hostmaster'),
(14, 'description', 'Betaling af domÃ¦net piratebay.dk'),
(14, 'invoicedate', '22-09-2012'),
(14, 'paymentdate', '02-09-2012'),
(14, 'invoiceno', '3575069'),
(15, 'name', 'Ad Libris'),
(15, 'description', 'PHP bog omhandlende social media programmering'),
(15, 'invoicedate', '22-09-2012'),
(15, 'paymentdate', '24-08-2012'),
(15, 'invoiceno', '27628848'),
(16, 'name', 'Elgiganten'),
(16, 'description', 'KÃ¸b af hÃ¸retelefoner og stÃ¸vsuger'),
(16, 'invoicedate', '23-09-2012'),
(16, 'paymentdate', '23-09-2012'),
(16, 'invoiceno', '1514594'),
(13, 'name', 'Forlagshuset'),
(13, 'description', 'Husleje v. lokaler'),
(13, 'invoicedate', '22-09-2012'),
(13, 'paymentdate', '25-09-2012'),
(13, 'invoiceno', ''),
(26, 'name', 'Nordvest CafÃ©'),
(26, 'description', 'CafÃ© med Nanna omhandlende kommende firma-aktiviteter'),
(26, 'invoicedate', '25-09-2012'),
(26, 'paymentdate', '25-09-2012'),
(26, 'invoiceno', '000086'),
(1, 'description', ''),
(1, 'duedate', '20-09-2012'),
(1, 'invoicedate', '05-09-2012'),
(1, 'reminderdate', ''),
(1, 'firstreminderdate', ''),
(1, 'secondreminderdate', ''),
(1, 'debtcollectiondate', ''),
(24, 'description', 'Krediteret pga. forkert momssats'),
(24, 'invoicedate', '25-09-2012'),
(24, 'creditedinvoiceid', '1'),
(27, 'name', 'Fullrate'),
(27, 'description', 'Internet'),
(27, 'invoicedate', '03-10-2012'),
(27, 'paymentdate', '06-10-2012'),
(27, 'invoiceno', '6389479'),
(7, 'name', 'Hetzner'),
(7, 'description', 'Server'),
(7, 'invoicedate', '22-08-2012'),
(7, 'paymentdate', '01-09-2012'),
(7, 'invoiceno', 'R0002295434'),
(28, 'name', 'Hetzner'),
(28, 'description', 'Server'),
(28, 'invoicedate', '28-09-2012'),
(28, 'paymentdate', '01-10-2012'),
(28, 'invoiceno', 'R0002360288'),
(25, 'description', 'Korrigeret faktura for fakturanr 1'),
(25, 'duedate', '07-10-2012'),
(25, 'invoicedate', '25-09-2012'),
(25, 'reminderdate', ''),
(25, 'firstreminderdate', ''),
(25, 'secondreminderdate', ''),
(25, 'debtcollectiondate', ''),
(25, 'accountid', '1'),
(29, 'description', ''),
(29, 'paymentdate', '09-10-2012'),
(29, 'payedinvoiceid', '25'),
(29, 'transferid', '1'),
(3, 'description', 'RestbelÃ¸b'),
(3, 'paymentdate', '21-09-2012'),
(3, 'payedinvoiceid', '25'),
(3, 'transferid', '2'),
(2, 'description', 'OverfÃ¸rt aconto belÃ¸b'),
(2, 'paymentdate', '03-09-2012'),
(2, 'payedinvoiceid', '25'),
(2, 'transferid', '3'),
(30, 'description', ''),
(30, 'paymentdate', '09-10-2012'),
(30, 'payedinvoiceid', '20'),
(30, 'transferid', '4'),
(20, 'description', ''),
(20, 'duedate', '06-10-2012'),
(20, 'invoicedate', '24-09-2012'),
(20, 'reminderdate', ''),
(20, 'firstreminderdate', ''),
(20, 'secondreminderdate', ''),
(20, 'debtcollectiondate', ''),
(20, 'accountid', '1');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `InvoiceItem`
--

CREATE TABLE IF NOT EXISTS `InvoiceItem` (
  `InvoiceItemID` bigint(20) NOT NULL AUTO_INCREMENT,
  `InvoiceID` bigint(20) NOT NULL,
  `ItemID` bigint(20) DEFAULT NULL,
  `Description` text,
  `Count` float NOT NULL,
  `Price` float NOT NULL,
  `Rebate` float DEFAULT NULL,
  PRIMARY KEY (`InvoiceItemID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `ItemID` (`ItemID`),
  KEY `Count` (`Count`),
  KEY `Price` (`Price`),
  KEY `Rebate` (`Rebate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Data dump for tabellen `InvoiceItem`
--

INSERT INTO `InvoiceItem` (`InvoiceItemID`, `InvoiceID`, `ItemID`, `Description`, `Count`, `Price`, `Rebate`) VALUES
(1, 1, 2, 'LÃ¸bende timer', 1, 32760, 0),
(2, 2, 3, '', 1, -15000, NULL),
(3, 3, 4, '', 1, -25950, NULL),
(4, 4, 5, '', 1, 207.75, NULL),
(5, 5, 6, '', 1, 207.01, NULL),
(6, 6, 7, '', 1, 224.97, NULL),
(7, 7, 8, '', 1, 695.83, NULL),
(8, 8, 9, '', 1, 695.83, NULL),
(9, 9, 10, '', 1, 695.83, NULL),
(10, 10, 11, '', 1, 416, NULL),
(11, 11, 12, '', 1, 3000, NULL),
(12, 12, 13, '', 1, 3000, NULL),
(13, 13, 14, '', 1, 3000, NULL),
(14, 14, 15, '', 1, 45, NULL),
(15, 15, 16, '', 1, 357, NULL),
(16, 16, 17, '', 1, 1148, NULL),
(20, 20, 21, '1-21. september', 95, 350, 0),
(21, 21, 22, '', 1, -40950, 0),
(22, 22, 23, 'LÃ¸bende timer', 1, 42656.2, 0),
(23, 22, 24, '', 1, -40950, 0),
(24, 21, 25, '1-31 august', 97.5, 350, 0),
(27, 23, 28, '1-31 august', 97.5, 350, 0),
(28, 23, 29, '', 1, -34125, 0),
(29, 23, 30, '1-31 august', 97.5, 350, 0),
(30, 24, 31, '', 1, -40950, 0),
(33, 25, 34, '1-31 august', 97.5, 350, 0),
(36, 26, 37, '', 1, 305, NULL),
(37, 27, 38, '', 1, 198.93, NULL),
(38, 28, 39, '', 1, 695.83, NULL),
(39, 29, 40, '', 1, -1706.25, NULL),
(40, 30, 41, '', 1, -41562.5, NULL);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Item`
--

CREATE TABLE IF NOT EXISTS `Item` (
  `ItemID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ItemGroupID` bigint(20) DEFAULT NULL,
  `UserID` bigint(20) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `AutoCreated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ItemID`),
  KEY `ItemGroupID` (`ItemGroupID`),
  KEY `UserID` (`UserID`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `AutoCreated` (`AutoCreated`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Data dump for tabellen `Item`
--

INSERT INTO `Item` (`ItemID`, `ItemGroupID`, `UserID`, `CreatedDate`, `AutoCreated`) VALUES
(1, 0, 1, '2012-09-05 22:08:25', NULL),
(2, NULL, 1, '2012-09-05 22:09:56', 1),
(3, NULL, 1, '2012-09-20 18:08:52', 1),
(4, NULL, 1, '2012-09-21 09:16:21', 1),
(5, NULL, 1, '2012-09-22 01:05:13', 1),
(6, NULL, 1, '2012-09-22 01:08:32', 1),
(7, NULL, 1, '2012-09-22 01:37:34', 1),
(8, NULL, 1, '2012-09-22 02:17:25', 1),
(9, NULL, 1, '2012-09-22 02:18:51', 1),
(10, NULL, 1, '2012-09-22 02:19:54', 1),
(11, NULL, 1, '2012-09-22 02:21:36', 1),
(12, NULL, 1, '2012-09-22 02:23:10', 1),
(13, NULL, 1, '2012-09-22 02:23:58', 1),
(14, NULL, 1, '2012-09-22 02:24:58', 1),
(15, NULL, 1, '2012-09-22 05:47:48', 1),
(16, NULL, 1, '2012-09-22 05:53:14', 1),
(17, NULL, 1, '2012-09-23 16:45:09', 1),
(21, NULL, 1, '2012-09-24 13:51:32', 1),
(22, NULL, 1, '2012-09-24 20:16:41', 1),
(23, NULL, 1, '2012-09-24 20:33:14', 1),
(24, NULL, 1, '2012-09-24 20:33:36', 1),
(25, NULL, 1, '2012-09-25 10:37:22', 1),
(26, NULL, 1, '2012-09-25 10:38:47', 1),
(27, NULL, 1, '2012-09-25 10:39:13', 1),
(28, NULL, 1, '2012-09-25 12:21:22', 1),
(29, NULL, 1, '2012-09-25 12:21:41', 1),
(30, NULL, 1, '2012-09-25 12:27:30', 1),
(31, NULL, 1, '2012-09-25 12:50:18', 1),
(32, NULL, 1, '2012-09-25 12:53:31', 1),
(33, NULL, 1, '2012-09-25 12:53:46', 1),
(34, NULL, 1, '2012-09-25 12:54:13', 1),
(35, NULL, 1, '2012-09-25 12:55:19', 1),
(36, NULL, 1, '2012-09-25 12:55:39', 1),
(37, NULL, 1, '2012-09-25 21:06:51', 1),
(38, NULL, 1, '2012-10-03 17:03:51', 1),
(39, NULL, 1, '2012-10-03 17:05:27', 1),
(40, NULL, 1, '2012-10-09 22:10:56', 1),
(41, NULL, 1, '2012-10-09 22:21:09', 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ItemData`
--

CREATE TABLE IF NOT EXISTS `ItemData` (
  `ItemID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` text NOT NULL,
  KEY `ItemID` (`ItemID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `ItemData`
--

INSERT INTO `ItemData` (`ItemID`, `Key`, `Value`) VALUES
(23, 'name', 'Konsulent arbejde'),
(23, 'number', ''),
(23, 'unit', ''),
(24, 'name', 'Indbetaling'),
(24, 'number', ''),
(24, 'unit', ''),
(22, 'name', 'Kreditnota'),
(22, 'number', ''),
(22, 'unit', ''),
(25, 'name', 'Konsulent arbejde'),
(25, 'number', ''),
(25, 'unit', '1'),
(26, 'name', 'Kreditnota forkert faktura'),
(26, 'number', ''),
(26, 'unit', ''),
(27, 'name', 'Konsulent arbejde'),
(27, 'number', ''),
(27, 'unit', '1'),
(28, 'name', 'Konsulent arbejde'),
(28, 'number', ''),
(28, 'unit', '1'),
(29, 'name', 'Kreditnota'),
(29, 'number', ''),
(29, 'unit', ''),
(30, 'name', 'Konsulent arbejde'),
(30, 'number', ''),
(30, 'unit', '1'),
(32, 'name', 'Konsulent arbejde'),
(32, 'number', ''),
(32, 'unit', ''),
(33, 'name', 'Kreditering'),
(33, 'number', ''),
(33, 'unit', ''),
(35, 'name', 'Indbetaling 3/9'),
(35, 'number', ''),
(35, 'unit', ''),
(36, 'name', 'Indbetaling 29/9'),
(36, 'number', ''),
(36, 'unit', ''),
(1, 'unit', '1'),
(1, 'number', ''),
(1, 'name', 'Konsulent arbejde'),
(1, 'buyprice', ''),
(1, 'saleprice', ''),
(2, 'name', 'Konsulent arbejde'),
(2, 'number', ''),
(2, 'unit', '1'),
(31, 'name', 'Kreditering fakturanr 1'),
(31, 'number', ''),
(31, 'unit', ''),
(34, 'name', 'Konsulent arbejde'),
(34, 'number', ''),
(34, 'unit', '1'),
(21, 'name', 'Konsulent arbejde'),
(21, 'number', ''),
(21, 'unit', '1');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ItemGroup`
--

CREATE TABLE IF NOT EXISTS `ItemGroup` (
  `ItemGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) NOT NULL,
  PRIMARY KEY (`ItemGroupID`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `ItemGroup`
--

INSERT INTO `ItemGroup` (`ItemGroupID`, `Name`) VALUES
(2, 'Test');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Language`
--

CREATE TABLE IF NOT EXISTS `Language` (
  `LanguageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OriginalText` longtext NOT NULL,
  `TranslatedText` longtext NOT NULL,
  `Locale` varchar(5) NOT NULL,
  `PageCode` varchar(32) DEFAULT NULL,
  `Path` varchar(350) NOT NULL,
  PRIMARY KEY (`LanguageID`),
  KEY `PageCode` (`PageCode`),
  KEY `Path` (`Path`),
  KEY `Locale` (`Locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=897 ;

--
-- Data dump for tabellen `Language`
--

INSERT INTO `Language` (`LanguageID`, `OriginalText`, `TranslatedText`, `Locale`, `PageCode`, `Path`) VALUES
(1, 'Information', 'Information', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(2, 'Sager', 'Sager', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(3, 'Projekter', 'Projekter', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(4, 'Aktiviteter', 'Aktiviteter', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(5, 'Fakturaer', 'Fakturaer', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(6, 'Kunder', 'Kunder', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(7, 'Aktivitet', 'Aktivitet', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(8, 'Bilag', 'Bilag', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(9, 'Faktura', 'Faktura', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(10, 'Varer', 'Varer', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(11, 'Rediger firma', 'Rediger firma', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(12, 'Administration', 'Administration', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(13, 'Log ud', 'Log ud', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(14, 'Ny indbetaling', 'Ny indbetaling', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(15, 'Ny faktura', 'Ny faktura', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(16, 'Periode', 'Periode', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(17, 'Fra', 'Fra', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(18, 'Til', 'Til', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(19, 'Skjul betalinger', 'Skjul betalinger', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(20, 'Kun bogfÃ¸rte', 'Kun bogfÃ¸rte', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(21, 'Opdater', 'Opdater', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(22, 'Faktura nr.', 'Faktura nr.', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(23, 'Beskrivelse', 'Beskrivelse', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(24, 'Faktura dato', 'Faktura dato', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(25, 'Betalings dato', 'Betalings dato', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(26, 'Moms', 'Moms', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(27, 'BelÃ¸b ink. moms', 'BelÃ¸b ink. moms', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(28, 'Ingen beskrivelse', 'Ingen beskrivelse', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(29, 'Er du sikker pÃ¥, at du vil bogfÃ¸re denne faktura?', 'Er du sikker pÃ¥, at du vil bogfÃ¸re denne faktura?', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(30, 'BogfÃ¸r', 'BogfÃ¸r', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(31, 'Er du sikker pÃ¥, at du vil slette denne faktura?', 'Er du sikker pÃ¥, at du vil slette denne faktura?', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(32, 'Slet', 'Slet', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(33, 'Rediger', 'Rediger', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(34, 'Indbetaling faktura:', 'Indbetaling faktura:', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(35, 'Total', 'Total', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(36, 'Kunder', 'Kunder', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(37, 'Aktivitet', 'Aktivitet', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(38, 'Bilag', 'Bilag', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(39, 'Faktura', 'Faktura', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(40, 'Varer', 'Varer', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(41, 'Rediger firma', 'Rediger firma', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(42, 'Administration', 'Administration', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(43, 'Log ud', 'Log ud', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(44, '404: Siden blev ikke fundet', '404: Siden blev ikke fundet', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(45, 'Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.', 'Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(46, 'Information', 'Information', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(47, 'Sager', 'Sager', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(48, 'Projekter', 'Projekter', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(49, 'Aktiviteter', 'Aktiviteter', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(50, 'Fakturaer', 'Fakturaer', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(51, 'Kunder', 'Kunder', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(52, 'Aktivitet', 'Aktivitet', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(53, 'Bilag', 'Bilag', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(54, 'Faktura', 'Faktura', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(55, 'Varer', 'Varer', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(56, 'Rediger firma', 'Rediger firma', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(57, 'Administration', 'Administration', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(58, 'Log ud', 'Log ud', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(59, 'Rediger', 'Rediger', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(60, 'Ingen Ã¥bne sager fundet', 'Ingen Ã¥bne sager fundet', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(61, 'SÃ¸g', 'SÃ¸g', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(62, 'Ny kunde', 'Ny kunde', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(63, 'Kunder', 'Kunder', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(64, 'Aktivitet', 'Aktivitet', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(65, 'Bilag', 'Bilag', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(66, 'Faktura', 'Faktura', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(67, 'Varer', 'Varer', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(68, 'Rediger firma', 'Rediger firma', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(69, 'Administration', 'Administration', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(70, 'Log ud', 'Log ud', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(71, 'Find kunde', 'Find kunde', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(72, 'Kundenummer', 'Kundenummer', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(73, 'Navn', 'Navn', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(74, 'Adresse', 'Adresse', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(75, 'Postnr', 'Postnr', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(76, 'by', 'by', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(77, 'Telefon', 'Telefon', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(78, 'Mobil', 'Mobil', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(79, 'E-mail', 'E-mail', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(80, 'CVR', 'CVR', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(81, 'Oversigt', 'Oversigt', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(82, 'Brugere', 'Brugere', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(83, 'Sager', 'Sager', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(84, 'Varer', 'Varer', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(85, 'Kunder', 'Kunder', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(86, 'Aktivitet', 'Aktivitet', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(87, 'Bilag', 'Bilag', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(88, 'Faktura', 'Faktura', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(89, 'Rediger firma', 'Rediger firma', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(90, 'Administration', 'Administration', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(91, 'Log ud', 'Log ud', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(92, 'Oversigt', 'Oversigt', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(93, 'Kunder', 'Kunder', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(94, 'Aktivitet', 'Aktivitet', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(95, 'Bilag', 'Bilag', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(96, 'Faktura', 'Faktura', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(97, 'Varer', 'Varer', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(98, 'Rediger firma', 'Rediger firma', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(99, 'Administration', 'Administration', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(100, 'Log ud', 'Log ud', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(101, 'Alle', 'Alle', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(102, 'TilfÃ¸j vare', 'TilfÃ¸j vare', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(103, 'Varenr', 'Varenr', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(104, 'Navn', 'Navn', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(105, 'Enhed', 'Enhed', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(106, 'KÃ¸bspris', 'KÃ¸bspris', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(107, 'Salgspris', 'Salgspris', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(108, 'stk', 'stk', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(109, 'timer', 'timer', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(110, 'dag', 'dag', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(111, 'uge', 'uge', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(112, 'pk', 'pk', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(113, 'Rediger', 'Rediger', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(114, 'Er du sikker pÃ¥, at du vil slette denne vare?', 'Er du sikker pÃ¥, at du vil slette denne vare?', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(115, 'Slet', 'Slet', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(116, 'Log timer', 'Log timer', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(117, 'Oversigt', 'Oversigt', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(118, 'Kunder', 'Kunder', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(119, 'Aktivitet', 'Aktivitet', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(120, 'Bilag', 'Bilag', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(121, 'Faktura', 'Faktura', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(122, 'Varer', 'Varer', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(123, 'Rediger firma', 'Rediger firma', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(124, 'Administration', 'Administration', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(125, 'Log ud', 'Log ud', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(126, 'Kunde', 'Kunde', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(127, '-- VÃ¦lg --', '-- VÃ¦lg --', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(128, 'Projekt', 'Projekt', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(129, 'Timer', 'Timer', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(130, 'Minutter', 'Minutter', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(131, 'Beskrivelse', 'Beskrivelse', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(132, 'Tidligere logninger', 'Tidligere logninger', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(133, 'Logninger for %s', 'Logninger for %s', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(134, 'T:M', 'T:M', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(135, 'Er du sikker pÃ¥, at du vil slette denne logning?', 'Er du sikker pÃ¥, at du vil slette denne logning?', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(136, 'Slet', 'Slet', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(137, 'Ret', 'Ret', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(138, 'Rediger logning', 'Rediger logning', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(139, 'Kunder', 'Kunder', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(140, 'Aktivitet', 'Aktivitet', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(141, 'Bilag', 'Bilag', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(142, 'Faktura', 'Faktura', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(143, 'Varer', 'Varer', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(144, 'Rediger firma', 'Rediger firma', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(145, 'Administration', 'Administration', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(146, 'Log ud', 'Log ud', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(147, 'Kunde', 'Kunde', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(148, '-- VÃ¦lg --', '-- VÃ¦lg --', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(149, 'Projekt', 'Projekt', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(150, 'Timer', 'Timer', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(151, 'Minutter', 'Minutter', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(152, 'Beskrivelse', 'Beskrivelse', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(153, 'Log timer', 'Log timer', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(154, 'Betalinger', 'Betalinger', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(155, 'SÃ¸g', 'SÃ¸g', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(156, 'Kommende', 'Kommende', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(157, 'Overskredet', 'Overskredet', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(158, 'Betalte', 'Betalte', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(159, 'Kunder', 'Kunder', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(160, 'Aktivitet', 'Aktivitet', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(161, 'Bilag', 'Bilag', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(162, 'Faktura', 'Faktura', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(163, 'Varer', 'Varer', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(164, 'Rediger firma', 'Rediger firma', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(165, 'Administration', 'Administration', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(166, 'Log ud', 'Log ud', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(167, 'Nye fakturaer', 'Nye fakturaer', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(168, 'Ingen fakturaer.', 'Ingen fakturaer.', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(169, 'customer/customer', 'customer/customer', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(170, 'customer/customer', 'customer/customer', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(171, 'Betalinger', 'Betalinger', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(172, 'SÃ¸g', 'SÃ¸g', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(173, 'Kommende', 'Kommende', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(174, 'Overskredet', 'Overskredet', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(175, 'Betalte', 'Betalte', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(176, 'Kunder', 'Kunder', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(177, 'Aktivitet', 'Aktivitet', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(178, 'Bilag', 'Bilag', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(179, 'Faktura', 'Faktura', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(180, 'Varer', 'Varer', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(181, 'Rediger firma', 'Rediger firma', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(182, 'Administration', 'Administration', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(183, 'Log ud', 'Log ud', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(184, 'Periode', 'Periode', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(185, 'Fra', 'Fra', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(186, 'Til', 'Til', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(187, 'Kun bogfÃ¸rte', 'Kun bogfÃ¸rte', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(188, 'Opdater', 'Opdater', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(189, 'E-mail', 'E-mail', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(190, 'Adgangskode', 'Adgangskode', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(191, 'Log ind', 'Log ind', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(192, '%s er ikke en gyldig e-mail-adresse', '%s er ikke en gyldig e-mail-adresse', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(193, '%s mÃ¥ ikke vÃ¦re tom', '%s mÃ¥ ikke vÃ¦re tom', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(194, 'Forkert brugernavn', 'Forkert brugernavn', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(195, 'Brugernavn', 'Brugernavn', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(196, 'Forkert brugernavn og/eller adgangskode', 'Forkert brugernavn og/eller adgangskode', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(197, '%s indeholder ugyldige karakterer', '%s indeholder ugyldige karakterer', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(198, 'Ugyldigt brugernavn og/eller adgangskode.', 'Ugyldigt brugernavn og/eller adgangskode.', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(199, 'Log timer', 'Log timer', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(200, 'Oversigt', 'Oversigt', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(201, 'Kunder', 'Kunder', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(202, 'Aktivitet', 'Aktivitet', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(203, 'Bilag', 'Bilag', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(204, 'Faktura', 'Faktura', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(205, 'Varer', 'Varer', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(206, 'Rediger firma', 'Rediger firma', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(207, 'Administration', 'Administration', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(208, 'Log ud', 'Log ud', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(209, 'Periode', 'Periode', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(210, 'Fra', 'Fra', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(211, 'Til', 'Til', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(212, 'Opdater', 'Opdater', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(213, 'Kunde', 'Kunde', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(214, '-- Kunde --', '-- Kunde --', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(215, '-- Projekt --', '-- Projekt --', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(216, '-- Aktivitet --', '-- Aktivitet --', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(217, 'Oversigt', 'Oversigt', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(218, 'Kunder', 'Kunder', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(219, 'Aktivitet', 'Aktivitet', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(220, 'Bilag', 'Bilag', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(221, 'Faktura', 'Faktura', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(222, 'Varer', 'Varer', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(223, 'Rediger firma', 'Rediger firma', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(224, 'Administration', 'Administration', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(225, 'Log ud', 'Log ud', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(226, 'Nyt bilag', 'Nyt bilag', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(227, 'Periode', 'Periode', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(228, 'Fra', 'Fra', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(229, 'Til', 'Til', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(230, 'Kun bogfÃ¸rte', 'Kun bogfÃ¸rte', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(231, 'Opdater', 'Opdater', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(232, 'Navn', 'Navn', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(233, 'Beskrivelse', 'Beskrivelse', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(234, 'Fakturadato', 'Fakturadato', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(235, 'Betalingsdato', 'Betalingsdato', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(236, 'Moms', 'Moms', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(237, 'BelÃ¸b ink. moms', 'BelÃ¸b ink. moms', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(238, 'Er du sikker pÃ¥, at du vil bogfÃ¸re dette bilag?', 'Er du sikker pÃ¥, at du vil bogfÃ¸re dette bilag?', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(239, 'BogfÃ¸r', 'BogfÃ¸r', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(240, 'Er du sikker pÃ¥, at du vil slette dette bilag?', 'Er du sikker pÃ¥, at du vil slette dette bilag?', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(241, 'Slet', 'Slet', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(242, 'Rediger', 'Rediger', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(243, 'Total', 'Total', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(244, 'Opret bilag', 'Opret bilag', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(245, 'Navn', 'Navn', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(246, 'Faktura-nummer', 'Faktura-nummer', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(247, 'Fakturadato', 'Fakturadato', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(248, 'Betalingsdato', 'Betalingsdato', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(249, 'BelÃ¸b', 'BelÃ¸b', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(250, 'BogfÃ¸rt', 'BogfÃ¸rt', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(251, 'Opret', 'Opret', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(252, 'Oversigt', 'Oversigt', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(253, 'Kunder', 'Kunder', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(254, 'Aktivitet', 'Aktivitet', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(255, 'Bilag', 'Bilag', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(256, 'Faktura', 'Faktura', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(257, 'Varer', 'Varer', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(258, 'Rediger firma', 'Rediger firma', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(259, 'Administration', 'Administration', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(260, 'Log ud', 'Log ud', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(261, 'Alle', 'Alle', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(262, 'Kategory', 'Kategory', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(263, '-- VÃ¦lg --', '-- VÃ¦lg --', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(264, 'Enhed', 'Enhed', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(265, 'stk', 'stk', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(266, 'timer', 'timer', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(267, 'dag', 'dag', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(268, 'uge', 'uge', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(269, 'pk', 'pk', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(270, 'Varenummer', 'Varenummer', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(271, 'Navn', 'Navn', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(272, 'KÃ¸bspris', 'KÃ¸bspris', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(273, 'Salgspris', 'Salgspris', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(274, 'Gem', 'Gem', 'en_UK', 'd28ec26362f8e78f1503d546558772f0', 'item_add'),
(275, 'Project', 'Project', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(276, 'Projekt', 'Projekt', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(277, 'Beskrivelse', 'Beskrivelse', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(278, 'Bruger', 'Bruger', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(279, 'Dato', 'Dato', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(280, 'Timer', 'Timer', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(281, 'Pris', 'Pris', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(282, 'Total', 'Total', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(283, 'Kundenr', 'Kundenr', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(284, 'Addresse', 'Addresse', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(285, 'Postnummer, by', 'Postnummer, by', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(286, 'Information', 'Information', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(287, 'Sager', 'Sager', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(288, 'Projekter', 'Projekter', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(289, 'Aktiviteter', 'Aktiviteter', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(290, 'Fakturaer', 'Fakturaer', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(291, 'Kunder', 'Kunder', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(292, 'Aktivitet', 'Aktivitet', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(293, 'Bilag', 'Bilag', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(294, 'Faktura', 'Faktura', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(295, 'Varer', 'Varer', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(296, 'Rediger firma', 'Rediger firma', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(297, 'Administration', 'Administration', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(298, 'Log ud', 'Log ud', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(299, 'Ny faktura', 'Ny faktura', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(300, 'Beskrivelse', 'Beskrivelse', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(301, 'Faktura dato', 'Faktura dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(302, 'Forfalds dato', 'Forfalds dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(303, 'PÃ¥mindelses dato', 'PÃ¥mindelses dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(304, '1. rykker dato', '1. rykker dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(305, '2. rykker dato', '2. rykker dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(306, 'Inkasso dato', 'Inkasso dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(307, 'BogfÃ¸rt', 'BogfÃ¸rt', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(308, 'TilfÃ¸j', 'TilfÃ¸j', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(309, 'Varenr', 'Varenr', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(310, 'Navn', 'Navn', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(311, 'Antal', 'Antal', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(312, 'Enhed', 'Enhed', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(313, 'Pris', 'Pris', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(314, 'Rabat', 'Rabat', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(315, 'BelÃ¸b', 'BelÃ¸b', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(316, 'Gem', 'Gem', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(317, 'TilfÃ¸j vare', 'TilfÃ¸j vare', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(318, 'Find eksisterende', 'Find eksisterende', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(319, 'SÃ¸g..', 'SÃ¸g..', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(320, 'Ny', 'Ny', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(321, 'Item no', 'Item no', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(322, 'Tekst', 'Tekst', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(323, 'Antal', 'Antal', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(324, '-- VÃ¦lg enhed --', '-- VÃ¦lg enhed --', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(325, 'stk', 'stk', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(326, 'timer', 'timer', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(327, 'dag', 'dag', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(328, 'uge', 'uge', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(329, 'pk', 'pk', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(330, 'Pris', 'Pris', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(331, 'Rabat', 'Rabat', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(332, 'TilfÃ¸j', 'TilfÃ¸j', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(333, 'stk', 'stk', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(334, 'timer', 'timer', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(335, 'dag', 'dag', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(336, 'uge', 'uge', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(337, 'pk', 'pk', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(338, 'Rediger', 'Rediger', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(339, 'Fjern', 'Fjern', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(340, 'Betalings dato', 'Betalings dato', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(341, '404: Siden blev ikke fundet', '404: Siden blev ikke fundet', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(342, 'Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.', 'Den efterspurgte side %s blev ikke fundet pÃ¥ denne server.', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(343, 'Kunder', 'Kunder', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(344, 'Aktivitet', 'Aktivitet', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(345, 'Bilag', 'Bilag', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(346, 'Faktura', 'Faktura', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(347, 'Varer', 'Varer', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(348, 'Rediger firma', 'Rediger firma', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(349, 'Administration', 'Administration', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(350, 'Log ud', 'Log ud', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(351, 'Navn', 'Navn', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(352, 'Land', 'Land', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(353, 'Arbejdstimer', 'Arbejdstimer', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(354, 'PÃ¥mindelse', 'PÃ¥mindelse', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(355, '1. rykker', '1. rykker', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(356, '2. rykker', '2. rykker', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(357, 'Inkasso', 'Inkasso', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(358, 'Generelt', 'Generelt', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(359, 'Momsprocent', 'Momsprocent', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(360, 'Logo', 'Logo', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(361, 'Opdater', 'Opdater', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(362, 'Kontakt info', 'Kontakt info', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(363, 'Adresse', 'Adresse', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(364, 'Postnr', 'Postnr', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(365, 'By', 'By', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(366, '-- VÃ¦lg land --', '-- VÃ¦lg land --', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(367, 'Telefon', 'Telefon', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(368, 'Mobiltelefon', 'Mobiltelefon', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(369, 'CVR', 'CVR', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(370, 'Hjemmeside', 'Hjemmeside', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(371, 'Bank', 'Bank', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(372, 'Bank reg. nr', 'Bank reg. nr', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(373, 'Bank konto nr.', 'Bank konto nr.', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(374, 'EAN-lokationsnr', 'EAN-lokationsnr', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(375, 'dage efter forfaldsdato', 'dage efter forfaldsdato', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(376, 'dage efter pÃ¥mindelse', 'dage efter pÃ¥mindelse', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(377, 'dage efter 1. rykker', 'dage efter 1. rykker', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(378, 'dage efter 2. rykker', 'dage efter 2. rykker', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(379, 'Kunder', 'Kunder', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(380, 'Aktivitet', 'Aktivitet', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(381, 'Bilag', 'Bilag', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(382, 'Faktura', 'Faktura', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(383, 'Varer', 'Varer', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(384, 'Rediger firma', 'Rediger firma', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(385, 'Administration', 'Administration', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(386, 'Log ud', 'Log ud', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(387, 'Faktura nr', 'Faktura nr', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(388, 'Kunde nr', 'Kunde nr', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(389, 'Fakturadato', 'Fakturadato', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(390, 'Betalingsdato', 'Betalingsdato', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(391, 'Varenr', 'Varenr', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(392, 'Navn', 'Navn', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(393, 'Beskrivelse', 'Beskrivelse', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(394, 'Antal', 'Antal', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(395, 'Enhed', 'Enhed', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(396, 'Pris', 'Pris', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(397, 'BelÃ¸b', 'BelÃ¸b', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(398, 'stk', 'stk', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(399, 'timer', 'timer', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(400, 'dag', 'dag', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(401, 'uge', 'uge', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(402, 'pk', 'pk', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(403, 'Total (eksk. moms)', 'Total (eksk. moms)', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(404, '%s%% moms', '%s%% moms', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(405, 'Total %s', 'Total %s', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(406, 'Renter kan blive opkrÃ¦vet ved forsinket betaling i henhold til gÃ¦ldende lov.', 'Renter kan blive opkrÃ¦vet ved forsinket betaling i henhold til gÃ¦ldende lov.', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(407, 'Information', 'Information', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(408, 'Sager', 'Sager', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(409, 'Projekter', 'Projekter', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(410, 'Aktiviteter', 'Aktiviteter', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(411, 'Fakturaer', 'Fakturaer', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(412, 'Kunder', 'Kunder', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(413, 'Aktivitet', 'Aktivitet', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(414, 'Bilag', 'Bilag', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(415, 'Faktura', 'Faktura', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(416, 'Varer', 'Varer', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(417, 'Rediger firma', 'Rediger firma', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(418, 'Administration', 'Administration', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(419, 'Log ud', 'Log ud', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(420, 'Rediger faktura', 'Rediger faktura', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(421, 'Beskrivelse', 'Beskrivelse', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(422, 'Faktura dato', 'Faktura dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(423, 'Forfalds dato', 'Forfalds dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(424, 'PÃ¥mindelses dato', 'PÃ¥mindelses dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(425, '1. rykker dato', '1. rykker dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(426, '2. rykker dato', '2. rykker dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(427, 'Inkasso dato', 'Inkasso dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(428, 'BogfÃ¸rt', 'BogfÃ¸rt', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(429, 'TilfÃ¸j', 'TilfÃ¸j', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(430, 'Varenr', 'Varenr', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(431, 'Navn', 'Navn', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(432, 'Antal', 'Antal', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(433, 'Enhed', 'Enhed', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(434, 'Pris', 'Pris', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(435, 'Rabat', 'Rabat', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(436, 'BelÃ¸b', 'BelÃ¸b', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(437, 'stk', 'stk', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(438, 'timer', 'timer', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(439, 'dag', 'dag', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(440, 'uge', 'uge', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(441, 'pk', 'pk', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(442, 'Rediger', 'Rediger', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(443, 'Fjern', 'Fjern', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(444, 'Gem', 'Gem', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(445, 'Rediger vare', 'Rediger vare', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(446, 'Menu/Customers', 'Menu/Customers', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(447, 'Menu/Activities', 'Menu/Activities', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(448, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(449, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(450, 'Menu/Items', 'Menu/Items', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(451, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(452, 'Menu/Administration', 'Menu/Administration', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(453, 'Menu/Signout', 'Menu/Signout', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(454, 'Rediger vare: "%s"', 'Rediger vare: "%s"', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(455, 'Opdater', 'Opdater', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(456, 'Menu/Customers', 'Menu/Customers', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(457, 'Menu/Activities', 'Menu/Activities', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(458, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(459, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(460, 'Menu/Items', 'Menu/Items', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(461, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(462, 'Menu/Administration', 'Menu/Administration', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(463, 'Menu/Signout', 'Menu/Signout', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(464, 'Customer/Menu/Information', 'Customer/Menu/Information', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(465, 'Customer/Menu/Cases', 'Customer/Menu/Cases', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(466, 'Customer/Menu/Projects', 'Customer/Menu/Projects', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(467, 'Customer/Menu/Activities', 'Customer/Menu/Activities', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(468, 'Customer/Menu/Invoices', 'Customer/Menu/Invoices', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(469, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(470, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(471, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(472, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(473, 'Menu/Items', 'Menu/Items', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(474, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(475, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(476, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(477, 'Customer/Menu/Information', 'Customer/Menu/Information', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(478, 'Customer/Menu/Cases', 'Customer/Menu/Cases', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(479, 'Customer/Menu/Projects', 'Customer/Menu/Projects', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(480, 'Customer/Menu/Activities', 'Customer/Menu/Activities', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(481, 'Customer/Menu/Invoices', 'Customer/Menu/Invoices', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(482, 'Menu/Customers', 'Menu/Customers', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(483, 'Menu/Activities', 'Menu/Activities', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(484, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(485, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(486, 'Menu/Items', 'Menu/Items', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(487, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(488, 'Menu/Administration', 'Menu/Administration', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(489, 'Menu/Signout', 'Menu/Signout', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(490, '%s mÃ¥ ikke vÃ¦re tom', '%s mÃ¥ ikke vÃ¦re tom', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(491, '%s skal indeholde tal', '%s skal indeholde tal', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(492, '%s skal vÃ¦re en gyldig dato', '%s skal vÃ¦re en gyldig dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(493, 'Betalings dato', 'Betalings dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(494, 'Rediger indbetaling', 'Rediger indbetaling', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(495, '-- VÃ¦lg faktura --', '-- VÃ¦lg faktura --', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(496, 'Faktura: %s - Total: %s', 'Faktura: %s - Total: %s', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(497, 'Rediger', 'Rediger', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(498, 'Påmindelses dato', 'Påmindelses dato', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(499, 'Bogført', 'Bogført', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(500, 'Tilføj', 'Tilføj', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(501, 'Beløb', 'Beløb', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(502, 'Customer/Menu/Search', 'Customer/Menu/Search', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(503, 'Customer/Menu/CreateCustomer', 'Customer/Menu/CreateCustomer', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(504, 'Menu/Customers', 'Menu/Customers', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(505, 'Menu/Activities', 'Menu/Activities', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(506, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(507, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(508, 'Menu/Items', 'Menu/Items', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(509, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(510, 'Menu/Administration', 'Menu/Administration', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(511, 'Menu/Signout', 'Menu/Signout', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(512, 'Customer/FindCustomer', 'Customer/FindCustomer', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index');
INSERT INTO `Language` (`LanguageID`, `OriginalText`, `TranslatedText`, `Locale`, `PageCode`, `Path`) VALUES
(513, 'Customer/CustomerNumber', 'Customer/CustomerNumber', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(514, 'Customer/Name', 'Customer/Name', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(515, 'Customer/Address', 'Customer/Address', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(516, 'Customer/PostalCodeAndCity', 'Customer/PostalCodeAndCity', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(517, 'Customer/Telephone', 'Customer/Telephone', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(518, 'Customer/Cellphone', 'Customer/Cellphone', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(519, 'Customer/Email', 'Customer/Email', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(520, 'Customer/VATNo', 'Customer/VATNo', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(521, 'Customer/CustomerNo', 'Customer/CustomerNo', 'en_UK', '126ffaa7fa80d9e2ec8b275b3937d397', 'customer_index'),
(522, 'Customer/Menu/Information', 'Customer/Menu/Information', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(523, 'Customer/Menu/Cases', 'Customer/Menu/Cases', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(524, 'Customer/Menu/Projects', 'Customer/Menu/Projects', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(525, 'Customer/Menu/Activities', 'Customer/Menu/Activities', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(526, 'Customer/Menu/Invoices', 'Customer/Menu/Invoices', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(527, 'Menu/Customers', 'Menu/Customers', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(528, 'Menu/Activities', 'Menu/Activities', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(529, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(530, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(531, 'Menu/Items', 'Menu/Items', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(532, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(533, 'Menu/Administration', 'Menu/Administration', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(534, 'Menu/Signout', 'Menu/Signout', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(535, 'Customer/Edit', 'Customer/Edit', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(536, 'Customer/Notes', 'Customer/Notes', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(537, 'Customer/DeliveryAddress', 'Customer/DeliveryAddress', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(538, 'Customer/Cases/Latest', 'Customer/Cases/Latest', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(539, 'Customer/Cases/NoneOpen', 'Customer/Cases/NoneOpen', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(540, 'Customer/Menu/Information', 'Customer/Menu/Information', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(541, 'Customer/Menu/Cases', 'Customer/Menu/Cases', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(542, 'Customer/Menu/Projects', 'Customer/Menu/Projects', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(543, 'Customer/Menu/Activities', 'Customer/Menu/Activities', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(544, 'Customer/Menu/Invoices', 'Customer/Menu/Invoices', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(545, 'Menu/Customers', 'Menu/Customers', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(546, 'Menu/Activities', 'Menu/Activities', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(547, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(548, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(549, 'Menu/Items', 'Menu/Items', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(550, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(551, 'Menu/Administration', 'Menu/Administration', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(552, 'Menu/Signout', 'Menu/Signout', 'en_UK', '5e9ab8451fa0f68f125252d8aaeba701', 'customerinvoice_create'),
(553, 'User/UsernameLabel', 'User/UsernameLabel', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(554, 'User/PasswordLabel', 'User/PasswordLabel', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(555, 'User/SubmitLabel', 'User/SubmitLabel', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(556, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(557, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(558, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(559, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(560, 'Menu/Items', 'Menu/Items', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(561, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(562, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(563, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(564, 'Activity/LogHours', 'Activity/LogHours', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(565, 'Activity/Overview', 'Activity/Overview', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(566, 'Menu/Customers', 'Menu/Customers', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(567, 'Menu/Activities', 'Menu/Activities', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(568, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(569, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(570, 'Menu/Items', 'Menu/Items', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(571, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(572, 'Menu/Administration', 'Menu/Administration', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(573, 'Menu/Signout', 'Menu/Signout', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(574, 'Activity/Customer', 'Activity/Customer', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(575, 'Activity/Choose', 'Activity/Choose', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(576, 'Activity/Project', 'Activity/Project', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(577, 'Activity/Activity', 'Activity/Activity', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(578, 'Activity/Hours', 'Activity/Hours', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(579, 'Activity/Minutes', 'Activity/Minutes', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(580, 'Activity/Description', 'Activity/Description', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(581, 'Activity/PreviouslyActivity', 'Activity/PreviouslyActivity', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(582, 'Activity/LogHours', 'Activity/LogHours', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(583, 'Activity/Overview', 'Activity/Overview', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(584, 'Menu/Customers', 'Menu/Customers', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(585, 'Menu/Activities', 'Menu/Activities', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(586, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(587, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(588, 'Menu/Items', 'Menu/Items', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(589, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(590, 'Menu/Administration', 'Menu/Administration', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(591, 'Menu/Signout', 'Menu/Signout', 'en_UK', '3600357016a1438b67602eb2ca482f72', 'activity_overview'),
(592, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(593, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(594, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(595, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(596, 'Menu/Items', 'Menu/Items', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(597, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(598, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(599, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'df24090fccdd21786a5f6300e6a9981f', 'company_edit'),
(600, 'Ny kreditnota', 'Ny kreditnota', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(601, 'Customer/Menu/Information', 'Customer/Menu/Information', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(602, 'Customer/Menu/Cases', 'Customer/Menu/Cases', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(603, 'Customer/Menu/Projects', 'Customer/Menu/Projects', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(604, 'Customer/Menu/Activities', 'Customer/Menu/Activities', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(605, 'Customer/Menu/Invoices', 'Customer/Menu/Invoices', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(606, 'Menu/Customers', 'Menu/Customers', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(607, 'Menu/Activities', 'Menu/Activities', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(608, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(609, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(610, 'Menu/Items', 'Menu/Items', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(611, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(612, 'Menu/Administration', 'Menu/Administration', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(613, 'Menu/Signout', 'Menu/Signout', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(614, 'Ny kreditnota', 'Ny kreditnota', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(615, 'Beskrivelse', 'Beskrivelse', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(616, 'Faktura dato', 'Faktura dato', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(617, 'Forfalds dato', 'Forfalds dato', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(618, 'PÃ¥mindelses dato', 'PÃ¥mindelses dato', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(619, '1. rykker dato', '1. rykker dato', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(620, '2. rykker dato', '2. rykker dato', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(621, 'Inkasso dato', 'Inkasso dato', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(622, 'BogfÃ¸rt', 'BogfÃ¸rt', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(623, 'TilfÃ¸j', 'TilfÃ¸j', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(624, 'Varer', 'Varer', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(625, 'Varenr', 'Varenr', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(626, 'Navn', 'Navn', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(627, 'Antal', 'Antal', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(628, 'Enhed', 'Enhed', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(629, 'Pris', 'Pris', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(630, 'Rabat', 'Rabat', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(631, 'BelÃ¸b', 'BelÃ¸b', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(632, 'Gem', 'Gem', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(633, 'stk', 'stk', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(634, 'timer', 'timer', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(635, 'dag', 'dag', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(636, 'uge', 'uge', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(637, 'pk', 'pk', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(638, 'Rediger', 'Rediger', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(639, 'Fjern', 'Fjern', 'en_UK', '2a078715cf87a58257656e89b78d677b', 'customerinvoice_creatememo'),
(640, 'Kreditnota', 'Kreditnota', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(641, 'Project', 'Project', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(642, 'Activity/ActivityByDate', 'Activity/ActivityByDate', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(643, 'Activity/HoursMinutesShort', 'Activity/HoursMinutesShort', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(644, 'Activity/Delete', 'Activity/Delete', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(645, 'Activity/Edit', 'Activity/Edit', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(646, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(647, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(648, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(649, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(650, 'Menu/Items', 'Menu/Items', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(651, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(652, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(653, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(654, 'Menu/Customers', 'Menu/Customers', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(655, 'Menu/Activities', 'Menu/Activities', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(656, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(657, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(658, 'Menu/Items', 'Menu/Items', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(659, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(660, 'Menu/Administration', 'Menu/Administration', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(661, 'Menu/Signout', 'Menu/Signout', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(662, 'Menu/Customers', 'Menu/Customers', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(663, 'Menu/Activities', 'Menu/Activities', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(664, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(665, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(666, 'Menu/Items', 'Menu/Items', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(667, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(668, 'Menu/Administration', 'Menu/Administration', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(669, 'Menu/Signout', 'Menu/Signout', 'en_UK', '1b8146337037507754585c8de24d21d6', 'item_index'),
(670, 'Error/CannotBeEmpty', 'Error/CannotBeEmpty', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(671, 'Error/Username/TooShort', 'Error/Username/TooShort', 'en_UK', '71fa4edc941c9c396c99f2ab164f438d', 'default_index'),
(672, 'Customer/Case/Latest', 'Customer/Case/Latest', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(673, 'Customer/Case/NoneOpen', 'Customer/Case/NoneOpen', 'en_UK', '28a21f23981839ce7d77be4bb1a9ab24', 'customer_details'),
(674, 'Invoice/NoInvoices', 'Invoice/NoInvoices', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(675, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(676, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(677, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(678, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(679, 'Menu/Items', 'Menu/Items', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(680, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(681, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(682, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(683, 'Invoice/NoInvoices', 'Invoice/NoInvoices', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(684, 'Betalinger', 'Betalinger', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(685, 'SÃ¸g', 'SÃ¸g', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(686, 'Kommende', 'Kommende', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(687, 'Overskredet', 'Overskredet', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(688, 'Betalte', 'Betalte', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(689, 'Menu/Customers', 'Menu/Customers', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(690, 'Menu/Activities', 'Menu/Activities', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(691, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(692, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(693, 'Menu/Items', 'Menu/Items', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(694, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(695, 'Menu/Administration', 'Menu/Administration', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(696, 'Menu/Signout', 'Menu/Signout', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(697, 'Betalingsfrist overskredet', 'Betalingsfrist overskredet', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(698, 'Invoice/NoInvoices', 'Invoice/NoInvoices', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(699, 'Invoice/NewPayment', 'Invoice/NewPayment', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(700, 'Invoice/NewCreditMemo', 'Invoice/NewCreditMemo', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(701, 'Invoice/NewInvoice', 'Invoice/NewInvoice', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(702, 'Invoice/Period', 'Invoice/Period', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(703, 'Invoice/From', 'Invoice/From', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(704, 'Invoice/To', 'Invoice/To', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(705, 'Invoice/HidePayment', 'Invoice/HidePayment', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(706, 'Invoice/RecordedOnly', 'Invoice/RecordedOnly', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(707, 'Invoice/Update', 'Invoice/Update', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(708, 'Invoice/InvoiceNo', 'Invoice/InvoiceNo', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(709, 'Invoice/Description', 'Invoice/Description', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(710, 'Invoice/InvoiceDate', 'Invoice/InvoiceDate', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(711, 'Invoice/PaymentDate', 'Invoice/PaymentDate', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(712, 'Invoice/Vat', 'Invoice/Vat', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(713, 'Invoice/AmountWithVat', 'Invoice/AmountWithVat', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(714, 'Invoice/NoDescription', 'Invoice/NoDescription', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(715, 'Invoice/ConfirmRecord', 'Invoice/ConfirmRecord', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(716, 'Invoice/Record', 'Invoice/Record', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(717, 'Invoice/ConfirmDelete', 'Invoice/ConfirmDelete', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(718, 'Invoice/Delete', 'Invoice/Delete', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(719, 'Invoice/Edit', 'Invoice/Edit', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(720, 'Invoice/PaymentInvoice', 'Invoice/PaymentInvoice', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(721, 'Invoice/Total', 'Invoice/Total', 'en_UK', 'b54ee3837697333850b54b028de8876f', 'customerinvoice_list'),
(722, 'Rediger bilag', 'Rediger bilag', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(723, 'Customer/Menu/Information', 'Customer/Menu/Information', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(724, 'Customer/Menu/Cases', 'Customer/Menu/Cases', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(725, 'Customer/Menu/Projects', 'Customer/Menu/Projects', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(726, 'Customer/Menu/Activities', 'Customer/Menu/Activities', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(727, 'Customer/Menu/Invoices', 'Customer/Menu/Invoices', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(728, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(729, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(730, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(731, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(732, 'Menu/Items', 'Menu/Items', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(733, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(734, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(735, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(736, 'Customer/Project/ChooseProject', 'Customer/Project/ChooseProject', 'en_UK', 'e1e9dfe41cbe402c93bc10bceb7a547e', 'customeractivity_list'),
(737, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(738, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(739, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(740, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(741, 'Menu/Items', 'Menu/Items', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(742, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(743, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(744, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'cb699bcecdb452d96b546cafc887f2d3', 'customerinvoice_delete'),
(745, 'Rediger kreditnota', 'Rediger kreditnota', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(746, 'Ny kreditnota', 'Ny kreditnota', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(747, 'Ny faktura', 'Ny faktura', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(748, 'Invoice/Payments', 'Invoice/Payments', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(749, 'Invoice/Search', 'Invoice/Search', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(750, 'Invoice/Comming', 'Invoice/Comming', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(751, 'Invoice/Overdue', 'Invoice/Overdue', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(752, 'Invoice/Payed', 'Invoice/Payed', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(753, 'Invoice/NewInvoices', 'Invoice/NewInvoices', 'en_UK', '2ab2c1e8f4d2e6890b6f187cce3b0a6f', 'invoice_index'),
(754, 'Customer/Menu/Search', 'Customer/Menu/Search', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(755, 'Customer/Menu/CreateCustomer', 'Customer/Menu/CreateCustomer', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(756, 'Menu/Customers', 'Menu/Customers', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(757, 'Menu/Activities', 'Menu/Activities', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(758, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(759, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(760, 'Menu/Items', 'Menu/Items', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(761, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(762, 'Menu/Administration', 'Menu/Administration', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(763, 'Menu/Signout', 'Menu/Signout', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(764, 'Customer/Name', 'Customer/Name', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(765, 'Customer/Address', 'Customer/Address', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(766, 'Customer/PostalCode', 'Customer/PostalCode', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(767, 'Customer/City', 'Customer/City', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(768, 'Customer/Country', 'Customer/Country', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(769, 'Customer/ChooseCountry', 'Customer/ChooseCountry', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(770, 'Customer/ContactPerson', 'Customer/ContactPerson', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(771, 'Customer/DeliveryAddress', 'Customer/DeliveryAddress', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(772, 'Customer/Notes', 'Customer/Notes', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(773, 'Customer/Phone', 'Customer/Phone', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(774, 'Customer/Cellphone', 'Customer/Cellphone', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(775, 'Customer/VATNo', 'Customer/VATNo', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(776, 'Customer/Website', 'Customer/Website', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(777, 'Customer/Bank', 'Customer/Bank', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(778, 'Customer/EAN', 'Customer/EAN', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(779, 'Customer/InternalAccount', 'Customer/InternalAccount', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(780, 'Customer/Logo', 'Customer/Logo', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(781, 'Customer/Save', 'Customer/Save', 'en_UK', '236a82f1c8ff7af070f74e57eee64677', 'customer_create'),
(782, 'Customer/Activity/Customer', 'Customer/Activity/Customer', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(783, 'Customer/Activity/Project', 'Customer/Activity/Project', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(784, 'Customer/Activity/Activity', 'Customer/Activity/Activity', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(785, 'Customer/Activity/Hours', 'Customer/Activity/Hours', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(786, 'Customer/Activity/Minutes', 'Customer/Activity/Minutes', 'en_UK', '163a836d338d3b185bb400efe9794132', 'activity_index'),
(787, 'Voucher/Overview', 'Voucher/Overview', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(788, 'Voucher/NewVoucher', 'Voucher/NewVoucher', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(789, 'Voucher/Period', 'Voucher/Period', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(790, 'Voucher/From', 'Voucher/From', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(791, 'Voucher/To', 'Voucher/To', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(792, 'Voucher/RecordedOnly', 'Voucher/RecordedOnly', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(793, 'Voucher/Update', 'Voucher/Update', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(794, 'Admin/Overview', 'Admin/Overview', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(795, 'Admin/Users', 'Admin/Users', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(796, 'Admin/Cases', 'Admin/Cases', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(797, 'Admin/Items', 'Admin/Items', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(798, 'Menu/Customers', 'Menu/Customers', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(799, 'Menu/Activities', 'Menu/Activities', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(800, 'Menu/Vouchers', 'Menu/Vouchers', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(801, 'Menu/Invoices', 'Menu/Invoices', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(802, 'Menu/Items', 'Menu/Items', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(803, 'Menu/EditCompany', 'Menu/EditCompany', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(804, 'Menu/Administration', 'Menu/Administration', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(805, 'Menu/Signout', 'Menu/Signout', 'en_UK', 'fbd3cac35dbe56e88480edda84109e7f', 'admin_index'),
(806, 'Activity/LogEdit', 'Activity/LogEdit', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(807, 'Activity/Customer', 'Activity/Customer', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(808, 'Activity/Project', 'Activity/Project', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(809, 'Activity/Activity', 'Activity/Activity', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(810, 'Activity/Hours', 'Activity/Hours', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(811, 'Activity/Minutes', 'Activity/Minutes', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(812, 'Voucher/NewVoucher', 'Voucher/NewVoucher', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(813, 'Voucher/Name', 'Voucher/Name', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(814, 'Voucher/PaymentDate', 'Voucher/PaymentDate', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(815, 'Voucher/Amount', 'Voucher/Amount', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(816, 'Voucher/Name', 'Voucher/Name', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(817, 'Voucher/Description', 'Voucher/Description', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(818, 'Voucher/InvoiceDate', 'Voucher/InvoiceDate', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(819, 'Voucher/PaymentDate', 'Voucher/PaymentDate', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(820, 'Voucher/Vat', 'Voucher/Vat', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(821, 'Voucher/AmountWithVat', 'Voucher/AmountWithVat', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(822, 'Voucher/ConfirmRecord', 'Voucher/ConfirmRecord', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(823, 'Voucher/Record', 'Voucher/Record', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(824, 'Voucher/ConfirmDelete', 'Voucher/ConfirmDelete', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(825, 'Voucher/Delete', 'Voucher/Delete', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(826, 'Voucher/Edit', 'Voucher/Edit', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(827, 'Voucher/Total', 'Voucher/Total', 'en_UK', 'd8671551c7f766153ae4035a1fcedbe7', 'voucher_index'),
(828, 'Page/ErrorPage404/Title', 'Page/ErrorPage404/Title', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(829, 'Page/ErrorPage404/Description', 'Page/ErrorPage404/Description', 'en_UK', '1917704cf4013ea231623c72afd3bbac', 'err_404'),
(830, 'Invoice/Payments', 'Invoice/Payments', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(831, 'Invoice/Search', 'Invoice/Search', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(832, 'Invoice/Comming', 'Invoice/Comming', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(833, 'Invoice/Overdue', 'Invoice/Overdue', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(834, 'Invoice/Payed', 'Invoice/Payed', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(835, 'Invoice/Period', 'Invoice/Period', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(836, 'Invoice/From', 'Invoice/From', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(837, 'Invoice/To', 'Invoice/To', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(838, 'Invoice/RecordedOnly', 'Invoice/RecordedOnly', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(839, 'Invoice/Update', 'Invoice/Update', 'en_UK', 'cab91ad3e1f7c23f1b7baf2040109185', 'invoice_payed'),
(840, 'Invoice/Payments', 'Invoice/Payments', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(841, 'Invoice/Search', 'Invoice/Search', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(842, 'Invoice/Comming', 'Invoice/Comming', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(843, 'Invoice/Overdue', 'Invoice/Overdue', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(844, 'Invoice/Payed', 'Invoice/Payed', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(845, 'Invoice/PaymentOverdue', 'Invoice/PaymentOverdue', 'en_UK', '6c5aaf53b4f09d49b6bf78a7c3d10ff0', 'invoice_overdue'),
(846, 'Item/Units/Piece', 'Item/Units/Piece', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(847, 'Item/Units/Hours', 'Item/Units/Hours', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(848, 'Item/Units/Day', 'Item/Units/Day', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(849, 'Item/Units/Week', 'Item/Units/Week', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(850, 'Item/Units/Package', 'Item/Units/Package', 'en_UK', 'e7c68a4b03dff5ddbcec9b2e11fcbf8f', 'invoice_view'),
(851, 'Invoice/NewInvoice', 'Invoice/NewInvoice', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(852, 'Invoice/Description', 'Invoice/Description', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(853, 'Invoice/InvoiceDate', 'Invoice/InvoiceDate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(854, 'Invoice/PaymentDate', 'Invoice/PaymentDate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(855, 'Invoice/ReminderDate', 'Invoice/ReminderDate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(856, 'Invoice/FirstReminderDate', 'Invoice/FirstReminderDate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(857, 'Invoice/SecondReminderDate', 'Invoice/SecondReminderDate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(858, 'Invoice/DebtCollectionDate', 'Invoice/DebtCollectionDate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(859, 'Invoice/Record', 'Invoice/Record', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(860, 'Invoice/AddItem', 'Invoice/AddItem', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(861, 'Invoice/Items', 'Invoice/Items', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(862, 'Item/ItemNo', 'Item/ItemNo', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(863, 'Item/Name', 'Item/Name', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(864, 'Item/Description', 'Item/Description', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(865, 'Item/Count', 'Item/Count', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(866, 'Item/Unit', 'Item/Unit', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(867, 'Item/Price', 'Item/Price', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(868, 'Item/Rebate', 'Item/Rebate', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(869, 'Item/Amount', 'Item/Amount', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(870, 'Item/Units/Piece', 'Item/Units/Piece', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(871, 'Item/Units/Hours', 'Item/Units/Hours', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(872, 'Item/Units/Day', 'Item/Units/Day', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(873, 'Item/Units/Week', 'Item/Units/Week', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(874, 'Item/Units/Package', 'Item/Units/Package', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(875, 'Item/Edit', 'Item/Edit', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(876, 'Item/Remove', 'Item/Remove', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(877, 'Invoice/SaveChanges', 'Invoice/SaveChanges', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(878, 'Invoice/NewCreditMemo', 'Invoice/NewCreditMemo', 'en_UK', '6e5900a56b0d62fbb711e7dbd8ca1caa', 'customerinvoice_edit'),
(879, 'Item/EditItem', 'Item/EditItem', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(880, 'Item/FindExisting', 'Item/FindExisting', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(881, 'Item/Search', 'Item/Search', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(882, 'Item/New', 'Item/New', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(883, 'Item/ItemNo', 'Item/ItemNo', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(884, 'Item/Name', 'Item/Name', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(885, 'Item/Description', 'Item/Description', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(886, 'Item/Count', 'Item/Count', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(887, 'Item/ChooseUnit', 'Item/ChooseUnit', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(888, 'Item/Units/Piece', 'Item/Units/Piece', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(889, 'Item/Units/Hours', 'Item/Units/Hours', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(890, 'Item/Units/Day', 'Item/Units/Day', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(891, 'Item/Units/Week', 'Item/Units/Week', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(892, 'Item/Units/Package', 'Item/Units/Package', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(893, 'Item/Price', 'Item/Price', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(894, 'Item/Rebate', 'Item/Rebate', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(895, 'Item/Amount', 'Item/Amount', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog'),
(896, 'Item/Update', 'Item/Update', 'en_UK', '582b36a5ff040d1cd49369f3f7b4b3d9', 'ajax_dialog');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Project`
--

CREATE TABLE IF NOT EXISTS `Project` (
  `ProjectID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CustomerID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `Description` longtext,
  `CreatedDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`ProjectID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Data dump for tabellen `Project`
--

INSERT INTO `Project` (`ProjectID`, `CustomerID`, `UserID`, `Name`, `Description`, `CreatedDate`, `Active`) VALUES
(1, 1, 1, 'Diverse', '', '2012-08-02 11:13:09', 1),
(2, 2, 1, 'Sygdom', '', '2012-08-02 11:15:36', 1),
(3, 2, 1, 'Internt', '', '2012-08-02 11:16:21', 1),
(4, 2, 1, 'Ferie', '', '2012-08-02 11:44:23', 1),
(5, 2, 1, 'Diverse', '', '2012-08-06 11:44:49', 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ProjectActivity`
--

CREATE TABLE IF NOT EXISTS `ProjectActivity` (
  `ProjectActivityID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProjectID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `Description` longtext,
  `MaxHours` int(4) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`ProjectActivityID`),
  KEY `ProjectID` (`ProjectID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `MaxHours` (`MaxHours`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Data dump for tabellen `ProjectActivity`
--

INSERT INTO `ProjectActivity` (`ProjectActivityID`, `ProjectID`, `UserID`, `Name`, `Description`, `MaxHours`, `Price`, `CreatedDate`, `Active`) VALUES
(1, 1, 1, 'LÃ¸bende timer', '', 0, 350, '2012-08-10 13:42:44', 1),
(2, 2, 1, 'Syg', 'Angiv sygdommens art', 0, 0, '2012-08-02 11:15:50', 1),
(3, 3, 1, 'MÃ¸der', '', 0, 0, '2012-08-02 11:16:27', 1),
(4, 4, 1, 'Ferie', '', 0, 0, '2012-08-02 11:44:34', 1),
(5, 5, 1, 'Diverse', '', 0, 0, '2012-08-06 11:44:59', 1),
(9, 3, 1, 'Ã˜konomi', '', 0, 0, '2012-08-22 18:30:58', 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ProjectActivityLog`
--

CREATE TABLE IF NOT EXISTS `ProjectActivityLog` (
  `ProjectActivityLogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProjectActivityID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Minutes` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Description` longtext,
  PRIMARY KEY (`ProjectActivityLogID`),
  KEY `ProjectActivityID` (`ProjectActivityID`),
  KEY `UserID` (`UserID`),
  KEY `Minutes` (`Minutes`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Data dump for tabellen `ProjectActivityLog`
--

INSERT INTO `ProjectActivityLog` (`ProjectActivityLogID`, `ProjectActivityID`, `UserID`, `Minutes`, `Date`, `Description`) VALUES
(1, 2, 1, 450, '2012-07-30', ''),
(2, 1, 1, 450, '2012-07-31', 'Ebita, sÃ¸ndagsavisen / opsÃ¦tning - SÃ¸ndagsavisen dating eksport webservice.'),
(3, 1, 1, 480, '2012-08-01', 'SmÃ¥justeringer tilrettelser pÃ¥ sÃ¸ndagsavisen dating eksport webservice.\r\n\r\nTilrettet iframe pÃ¥ Scandlines claimsmangement system'),
(4, 1, 1, 450, '2012-08-02', 'Scandlines Claimsmanagement system. Rettet diverse smÃ¥ting pÃ¥ vegne af kunden.\r\n\r\nScandlines Bedre Billet: overblik over de sidste Ã¦ndringer og gennemgang samt gennemgang med Mojo.\r\n\r\nEbita intern IT: opsÃ¦tning af intern kontor VPN til QNAP.'),
(5, 1, 1, 300, '2012-08-03', ''),
(6, 5, 1, 30, '2012-08-06', 'LÃ¦ge + apotek'),
(7, 1, 1, 450, '2012-08-06', 'SÃ¸ndagsavisen: lagt webservice online\r\nEbita: opsÃ¦tning af VPN\r\nDiverse andet.'),
(8, 1, 1, 240, '2012-08-07', 'SA: Rettet kritisk fejl pÃ¥ SÃ¸ndagsavisen pga forkert udruldning.\r\n\r\nEbita: opsÃ¦tning af VPN hos Morten, samt tilfÃ¸jelse af ny bruger.'),
(9, 4, 1, 210, '2012-08-07', 'Slap af dag.'),
(10, 2, 1, 450, '2012-08-08', 'Roskilde syge'),
(11, 1, 1, 450, '2012-08-09', 'SÃ¸ndagsavisen: \r\n- Redirect til godpris.nu\r\n- Integration af redirect url''s i web.config, sÃ¥ det kan Ã¦ndres uden at publicere sitet til drift; test og lancering.\r\n\r\nInstallation af Visual Studio 2010 Ultimate for at arbejde pÃ¥ Scandlines ClaimsManagement system.'),
(12, 1, 1, 450, '2012-08-10', 'Scandlines:\r\n- Fikset SQL adgang til ClaimsManagement.\r\n- Opdateret ClaimsManagement til seneste version.\r\n\r\nSÃ¸ndagsavisen/forum: \r\n- Rettet fejl med at bruger ikke kunne logge ind.\r\n- Rettet kritisk fejl med SletteFÃ¦tteren som foresagede at slettede brugere ikke kunne logge ind.\r\n- Opdateret adgangskode pÃ¥ P-Generator med seneste adgangskode, sÃ¥ SletteFÃ¦tteren kÃ¸rer igen.\r\n- Opdateret SletteFÃ¦tter med seneste version pÃ¥ P-generator.\r\n\r\nEbita: \r\n- Opdateret QNAP firmware\r\n- Oprettet _Temp drev pÃ¥ K-drevet efter gentagne request fra Henrik'),
(13, 4, 1, 450, '2012-08-13', ''),
(14, 4, 1, 450, '2012-08-14', ''),
(15, 4, 1, 450, '2012-08-15', ''),
(16, 4, 1, 450, '2012-08-16', ''),
(17, 4, 1, 450, '2012-08-17', ''),
(18, 4, 1, 450, '2012-08-19', ''),
(19, 4, 1, 450, '2012-08-20', ''),
(20, 1, 1, 300, '2012-08-21', '12:00-12:30 - EBITA MÃ˜DE:\r\nSamtale vedr. nye og kommende opgaver heriblandt kommunikation vedrÃ¸rende Euroinvestor/Lifestyle caching problemer.\r\n\r\n12:30-13:00-  EUROINVESTOR:\r\nOpsÃ¦tning af Euroinvestor Lifestyle site samt webservice der automatisk kan flushe caching.\r\n\r\n13:00-15:00 - SCANDLINES CLAIMSMANAGEMENT:\r\nFikset diverse smÃ¥fejl pÃ¥ CM systemet, heriblandt:\r\nKorrektur Ã¦ndret i e-mail + fjernet diverse ligegyldig info. \r\nEmail Ã¦ndret til html-email for at lave bedre styling.\r\nTilfÃ¸jet ekstra elementer til dropdown menuen (emne).\r\nFjernet loader popup, nÃ¥r form-submittes.\r\n\r\n15:00-17:00: Euroinvestor. Gennemgang af Euroinvestor caching problem.'),
(21, 4, 1, 150, '2012-08-21', ''),
(22, 1, 1, 480, '2012-08-22', 'Euroinvestor lifestyle: \r\n- Opsat lokalt arbejdsmiljÃ¸ pÃ¥ hjemmekontoret for Lifestyle projekter.\r\n- Opsat Lifestyle projekter pÃ¥ 3 webservere pÃ¥ Hetzner og fÃ¥et cachen til at rydde pÃ¥ alle tre servere, nÃ¥r en artikel publiceres fra staging serveren.\r\n- Publiceret Ã¦ndringer til overstÃ¥ende servere og testet igennem.\r\n- Oprettet patch som skal tilfÃ¸jes pÃ¥ de pÃ¥virkede maskiner\r\n- Skrevet dokumentation som skal fÃ¸lges nÃ¸je, for at fÃ¥ load-balancing til at fungere.'),
(23, 9, 1, 120, '2012-08-22', 'Registreret regnskab og indrapporteret moms'),
(24, 1, 1, 450, '2012-08-23', 'Ebita (30 min):\r\n- Internt mÃ¸de, diskuteret kommende projekter som status pÃ¥ nuvÃ¦rende.\r\n\r\nEuroinvestor:\r\n- Fikset sidste del af caching problemer.\r\n- Tilrettet dokumentation.\r\n- Optimeret EuroinvestorDalPatch.zip\r\n- Testet at patch og dokumentation virker i nyopsatte test-miljÃ¸.\r\n\r\nScandlines ClaimsManagementSystem:\r\n- Talt om systemet og brugt tid pÃ¥ at undersÃ¸ge hvad kunden mente der skulle rettes samt kommunikation med kunden omkring dette.\r\n- Fjernet ugyldige data fra claims_and_damage side.\r\n- Skiftet signatur for brugeren Christian Liebetanz\r\n- Gjort sÃ¥ listen "invoice payed by" under oprettelse og opdatering af en Claim, viser listen over mulige payers (fÃ¸rhen var den hardcoded).'),
(26, 1, 1, 420, '2012-08-24', 'Scandlines claimsmanagement:\r\n- Rettet diverse smÃ¥ting, heriblandt signaturen som ikke var opdateret korrekt for visse brugere.\r\n\r\nEuroinvestor DAL caching issue:\r\n- Gensendt vejledning via Gmail og lagt versioner op pÃ¥ K drevet pga. interne problemer med vores mailsystem.\r\n\r\nEuroinvestor FileSync:\r\n- Oprettet projekt og fÃ¥et den til at sykronisere filer og mapper mellem flere destinationer. \r\n- Uploadet seneste version pÃ¥ K-drevet'),
(27, 4, 1, 60, '2012-08-24', 'Med Aija pÃ¥ privathospital for at hÃ¸re nÃ¦rmere om plastikoperationer samt cafÃ©.'),
(28, 4, 1, 450, '2012-08-27', ''),
(29, 4, 1, 450, '2012-08-28', ''),
(30, 1, 1, 450, '2012-08-29', 'SÃ¸ndagsavisen:\r\n- Fikset diverse tickets i Gemius\r\n- Rettet diverse andre fejl og mangler\r\n\r\nScandlines Claimsmanagement:\r\n- Fikset at "other" emnet ikke blev vist.\r\n- Publiceret til live\r\n'),
(31, 9, 1, 120, '2012-08-25', ''),
(32, 1, 1, 480, '2012-08-30', 'Euroinvestor Bullstreet:\r\n- MÃ¸de omhandlede Facebook integration samt diverse addons..\r\n\r\nSÃ¸ndagsavisen:\r\n- SÃ¸ndagsavisen serverflyt\r\n- Download + installation af SQL Server\r\n- OpsÃ¦tning af database + website\r\n- SÃ¸ndagsavisen Gemini tickets\r\n- SÃ¸ndagsavisen Ã¦ndret tekst pÃ¥ forsiden af mit.forum.dk\r\n- TilfÃ¸jet ip-adresser til dating XMLExport pÃ¥ vegne af kunden, sÃ¥ sÃ¸ndagsavisen har adgang til servicen fra deres interne systemer.\r\n- Fjernet redirect til gopris.nu pÃ¥ vegne af Ask\r\n\r\nScandlines Claimsmanagement:\r\n- Opdateret brugere/password adgang til systemet'),
(33, 1, 1, 450, '2012-08-31', 'Scandlines Claimsmanagement:\r\n- Ã†ndret alt fra claim til complaint pÃ¥ alle sider, minus db\r\n- Fikset emne-dropdown under Edit ikke viser alle emner\r\n- Fikset sÃ¥ datoen ikke indeholder bindestreg for at undgÃ¥ datetime-exception\r\n- Rettet databasen for iframe-claim, sÃ¥ man kan tilfÃ¸je uendelige karakterer\r\n- Rettet sÃ¥ e-mail igen er pÃ¥krÃ¦vet\r\n- Fjernet et par comments pÃ¥ edit-siden sÃ¥ der nu kun er 5 statiske i stedet for 10\r\n- Knapper i bunden pÃ¥ redigeringssiden er positioneret fixed.\r\n- Diverse Ã¦ndre minor-changes.\r\n- Publiceret seneste version til drift\r\n\r\nEbita FileSync:\r\n- Added gracefully stop, so that files being copied are not broken\r\n- Added thead-sleep to avoid "File is in use"-message\r\n- Rettet sÃ¥ Settings-dialog altid har fokus.\r\n- Rettet et par kritiske exceptions i Settings-dialogen\r\n- Rettet at man ikke kan tilfÃ¸je samme destination flere gange\r\n- TilfÃ¸jet About-dialog\r\n- Settings-dialog er ikke lÃ¦ngere resizable\r\n- Diverse stabilitetsÃ¦ndringer\r\n\r\nEuroinvestor Bullstreet:\r\n- Installation af SQL Toolbelt\r\n- Konventeret database fra 2008 til 2005\r\n- Gennemgang og emailkorrespondance med Allan vedr. opsÃ¦tning\r\n- Pakket konsol-applikation som opretter dummy-data og sendt til Allan'),
(34, 1, 1, 450, '2012-09-03', 'Scandlines Claimsmanagement\r\n- Rettet dato parsing fejl\r\n- Indsat nye tekster i email og administration\r\n- Publiceret til drift\r\n\r\nEbita FileSync \r\n- Rettet at annuller knappen gemmer i Settings-dialogen\r\n\r\nSÃ¸ndagsavisen - drift:\r\n- Rettet diverse tickets fra Gemini (og der ER mange)'),
(35, 1, 1, 270, '2012-09-04', '11:30 ClaimsManagement\r\n- Fikset at nyoprettede brugere ikke har fÃ¥et permissions til at logge ind\r\n\r\n12:00 SÃ¸ndagsavisen\r\n- Fikset tickets i Gemini\r\n- Smidt ofir-banner i bunden af SÃ¸ndagsavisen projektet samt Ã¦ndret styling\r\n- TilfÃ¸jet html-bullets til forum oprettelsessiden.'),
(36, 1, 1, 450, '2012-09-05', 'Euroinvestor Bullstreet\r\n- Supporteret Allan med at sÃ¦tte server op\r\n- Integeret Facebook-SDK i Bullstreet projektet\r\n- Lavet minor improvements i kildekoden\r\n- Kopieret seneste version af databasen til Euroinvestor dev server\r\n\r\nScandlines Claimsmanagement\r\n- Tjekket at der kan logges ind med bruger, da kunden siger at han stadig ikke kan logge ind'),
(37, 4, 1, 450, '2012-09-07', ''),
(38, 4, 1, 450, '2012-09-10', ''),
(39, 1, 1, 450, '2012-09-11', 'Euroinvestor Bullstreet:\r\n- Gennemgang af Signe''s oplÃ¦g\r\n- MÃ¸de vedrÃ¸rende Facebook integration\r\n- Sendt kildekode til Ebita Filesync\r\n- Lave Facebook integration til Bullstreet\r\n- Sendt skÃ¦rmdump af Facebook aktiviteter\r\n- PÃ¥begyndt Facebook friend selector\r\n\r\nScandlines Claimsmanagement:\r\n- Rettet login.aspx sÃ¥ den altid redirecter til claimslistall.aspx\r\n- TilfÃ¸jet logout-knap pÃ¥ liste-siden\r\n- Publiceret til drift-miljÃ¸\r\n\r\nScandlines:\r\n- Rettet noget dato fra Bordershop site\r\n\r\nEbita Intern:\r\n- Talt om Facebook-applikation til Tribeca (mulig kunde)'),
(40, 1, 1, 450, '2012-09-12', 'Kofoedsskole\r\n- KÃ¸bt Umbraco Contour, afventer pakke fra Umbraco.\r\n- SparebÃ¸ssen er erstattet med nyt design\r\n- TilfÃ¸jet datovÃ¦lger under "Indhold"-fanen pÃ¥ alle nyheder.\r\n- Lagt Ã¦ndringerne online\r\n\r\nEuroinvestor Bullstreet\r\n- Sendt seneste versioner af databaser til Allan\r\n- Lavet globalt Facebook javascript namespace\r\n- PÃ¥begyndt Facebook friend selector\r\n- Sendt opdaterede database scripts\r\n- Hjulpet med at fejlsÃ¸ge "Server Error in ''/'' Application."-fejl'),
(43, 1, 1, 450, '2012-09-13', 'Euroinvestor Bullstreet\r\n- Kort gennemgang af Facebook integration\r\n- UndersÃ¸gt fejl pÃ¥ Bullstreet nÃ¥r man inviterer gamle spillere\r\n- Lavet patch som retter diverse Object reference null fejl\r\n- Uploadet til Dropbox og sendt besked til Allan og Morten\r\n- Lagt egne Ã¦ndringer seperat og committet Ã¦ndringer til subversion\r\n- Gendannet Facebook content'),
(44, 4, 1, 180, '2012-09-04', ''),
(45, 1, 1, 450, '2012-09-14', 'Euroinvestor Bullstreet\r\n- FÃ¦rdiggjort Facebook friend-selector\r\n- Oprettet test bruger pÃ¥ Facebook til QA\r\n- Hjulpet Morten med haste opsÃ¦tning af test-bruger til test\r\n- Opsat lokalt udviklingsmiljÃ¸ af databaser pga. test\r\n- PÃ¥begyndt arbejde pÃ¥ Scandlines konkurrence\r\n- Fortsat arbejde pÃ¥ Facebook integration (primÃ¦rt friends selector)'),
(46, 1, 1, 450, '2012-09-06', '- Kofoedsskole diverse smÃ¥ting\r\n- Arbejdet pÃ¥ Ebita Filesync applikation\r\n- OpsÃ¦tning af VPN-forbindelse + test til Euroinvestor'),
(47, 1, 1, 450, '2012-09-17', 'SÃ¸ndagsavisen\r\n- Talt med Susanne vdr. kritisk fejl pÃ¥ SÃ¸ndagsavisen\r\n- Rettet kritisk fejl som gÃ¸r, at brugere ikke kan logge ind\r\n- Ringet til Susanne og givet status vdr. kritisk fejl\r\n- TilfÃ¸jet jura-info pÃ¥ mail oprettelsesside\r\n- Opgraderet 3 brugere til 100MB e-mail.\r\n- Rykket Morten for brugertilladelser til at rette header pÃ¥ mail\r\n- Publiceret seneste version til drift\r\n\r\nEuroinvestor Bullstreet\r\n- MÃ¸de vedrÃ¸rende Bullstreet status/Facebook integrationer\r\n- Rettet fejl med at visse brugere ikke kunne poste pÃ¥ vens vÃ¦g pga. permissions\r\n- Integreret Facebook UI-deling pÃ¥ NetvÃ¦rk > Grupper (kun pÃ¥ mine grupper)\r\n- Integreret Facebook UI-deling pÃ¥ NetvÃ¦rk > Dine challenges (kun pÃ¥ vundet, tabt)\r\n- Integreret Facebook UI-deling pÃ¥ Profil-side. \r\n- Test / QA'),
(48, 1, 1, 450, '2012-09-18', 'Euroinvestor Bullstreet\r\n- Smidt diverse Facebook konfiguration i web.config\r\n- TilfÃ¸jet Facebook stuff til Umbraco sÃ¥ kunden f.eks. kan Ã¦ndre share-billede\r\n- Sendt custom fejl-besked til grafik hos Signe\r\n- PÃ¥begyndt implementering af custom fejlhÃ¥ndtering\r\n- Optimeret og rettet diverse smÃ¥fejl pÃ¥ Bullstreet\r\n- Publiceret seneste release til dev.ebita.dk\r\n- Gennemgang Ã¦ndringer med Mojo\r\n- TilfÃ¸jet tekster der bruges af Facebook dialog til Umbraco (multisprog)\r\n- Test/QA'),
(49, 1, 1, 450, '2012-09-19', 'Ebita\r\n- Afventer opgaver (forsÃ¸gt kontakt t. Morten - syg)\r\n- Bugfixing af Bullstreet\r\n- Optimeret Ebita Filesync\r\n\r\nEuroinvestor Bullstreet\r\n- TilfÃ¸jet Facebook dialog/overlay pÃ¥ wizard-side\r\n- Integreret Facebook init-stuff.\r\n- Facebook-share pÃ¥ sidste side\r\n- TilfÃ¸jet oversatte tekster til Facebook dokumenttype\r\n- TilfÃ¸jet oversatte tekster (knap) til Localization dokumenttype\r\n- QA / Test\r\n- Lagt seneste version pÃ¥ dev'),
(50, 1, 1, 480, '2012-09-20', 'Scandlines ClaimsManagement\r\n- Tjekket at tekster var korrekt oversat\r\n- Rettet logout-knap sÃ¥ den peger pÃ¥ /pages/login.aspx i stedet for /\r\n- TilfÃ¸jet complaintsmanagement.scandlines.dk til host-header i IIS\r\n\r\nEuroinvestor Bullstreet\r\n- Lille mÃ¸de omhandlende kommende Ã¦ndringer\r\n- Merget seneste Umbraco konfigurationer ind i live-sitet.\r\n- Kommunikation med Allan vedr. lancering af seneste version\r\n- Optimeret fbmodal.js en del og gjort at den kan minificeres korrekt\r\n- Viser fejlbesked hvis man forsÃ¸ger at Ã¥bne Friend-selector uden popup\r\n- Lavet Facebook inviter knap om, sÃ¥ den ligner Facebook mere\r\n- Oversat tekst pÃ¥ Facebook inviter-venner knap\r\n- Lagt seneste version af Bullstreet pÃ¥ dev\r\n- Lagt seneste localization dokumenttype pÃ¥ Euroinvestor umbraco\r\n- Lagt seneste version af Bullstreet pÃ¥ Euroinvestor server\r\n- Bugfix: Rettet sÃ¥ kun dokumenttyper med pÃ¥krÃ¦vede properties vises i menuen\r\n- TilfÃ¸jet inviter-venner knap til "Favoritlisten" og "Dine challenges" siderne\r\n\r\nKofoedsskole\r\n- ForsÃ¸gt at installere Contour\r\n- Installeret Contour-pakke manuelt\r\n- Opsat licensnÃ¸gle'),
(51, 1, 1, 450, '2012-09-21', 'Euroinvestor Bullstreet\r\n- PÃ¥begyndt flytning af Facebook-venner side sÃ¥ tekster ligger som node under "Tekster"-siden\r\n- Rettet fejl med usercontrol pÃ¥ Wizard siden\r\n- Diverse bugfixes\r\n- Optimeret Facebook integration\r\n\r\nSÃ¸ndagsavisen\r\n- Rettet diverse tickets i Gemini\r\n- UndersÃ¸gt hvilket CUID der bliver brugt pÃ¥ chat pga. forkert placering af banner (chat.forum.dk/search.aspx)\r\n- Fundet CUID 14075 - afventer korrekte CUID fra kunde for erstatning\r\n'),
(52, 1, 1, 450, '2012-09-24', 'Euroinvestor Bullstreet\r\n- Internt statusmÃ¸de samt gennemgang af seneste Ã¦ndringer\r\n- Internt statusmÃ¸de omkring kommende nye features\r\n\r\nScandlines Claimsmanagement\r\n- TilfÃ¸jet ekstra linje/break pÃ¥ kundemail\r\n- Ã†ndret designet pÃ¥ kvitteringsside sÃ¥ den matcher kundemail\r\n- Lagt seneste version pÃ¥ drift\r\n\r\nSÃ¸ndagsavisen/Forum\r\n- Fikset diverse smÃ¥ tickets\r\n- Ledt efter informationer pÃ¥ 2 mailadresser til Politi-efterforskning\r\n- Opgraderet bruger til 100mb\r\n- Fikset jura information pÃ¥ brugeroprettelse\r\n- Lagt seneste version online\r\n- Aktiveret MailMover program for at flytte mail\r\n- Flyttet 4263 imap-brugere'),
(53, 1, 1, 450, '2012-09-25', 'Euroinvestor Bullstreet\r\n- Fikset forkerte bullet-style i Chrome/Firefox\r\n- Fikset manglende knapper pÃ¥ step2 i login-wizard i Chrome/Firefox\r\n- Fikset alignment pÃ¥ overskrifter sÃ¥ de aligner med content\r\n- Fjernet gennemsigtighed i boks pÃ¥ hjÃ¦lpeside\r\n- Fikset at logout genererer fejlside pÃ¥ live (lavet udruldnign)\r\n- Fjernet gruppedeling fra FacebookShare usercontrol\r\n- TilfÃ¸jet gruppeinformation (navn, beskrivelse) til Facebook deling pÃ¥ gruppe-side\r\n- Fjernet Facebook gruppe properties (Name, Description) fra Umbraco\r\n- Ã†ndret "5 trades in one day" til "1 trade per day"\r\n- Fikset at admin-sider i Umbraco ikke opdaterer (grundet caching)\r\n- Upload kommaseperaret for at opdatere flere spillere ad gangen via Umbraco\r\n- Lavet sÃ¥ InGame export indeholder GameID\r\n- Oprettet InGame usercontrol+datatype\r\n\r\nKofoedsskole\r\n- Dialog med Thomas omhandlende Ã¸nsker omkring tilrettelser af forsiden.\r\n- Diskuteret mulige lÃ¸sninger. Thomas vender tilbage med konklusion senere.\r\n\r\nEbita Intern\r\n- Uden internetforbindelse 30 min.\r\n\r\nSÃ¸ndagsavisen\r\n- Talt med Susanne vedr. bruger der ikke kunne logge ind\r\n- ForsÃ¸gt at finde brugeren via Surgemail administration pÃ¥ alle servere (uden held)'),
(54, 1, 1, 480, '2012-09-26', 'Kofoedsskole (1 time)\r\n- Integeret Facebook like-box i hÃ¸jre kolonne nederst pÃ¥ alle side\r\n- Flyttet dato i nyhederne pÃ¥ forsiden ud sÃ¥ den ligger i listen\r\n- Talt med kunden vdr. seneste Ã¦ndringer per telefon\r\n- Talt med Maria fra Kofoedsskole - alt ok\r\n\r\nEuroinvestor Bullstreet\r\n- FÃ¦rdiggjort InGame test\r\n- Lavet InGame konkurrence med multiple choice i backenden\r\n- Rettet frontenden sÃ¥ den understÃ¸tter multiple choice\r\n- Generel test / QA af ny funktionalitet\r\n- Kommunikation med Euroinvestor mht. lancering\r\n- Rettet sÃ¥ der altid tages den nyeste ingame quiz, fremfor den fÃ¸rste\r\n- Lavet pakke klar til publicering i morgen\r\n\r\nSÃ¸ndagsavisen Forum\r\n- Opdateret aktive sager med seneste status i Gemini\r\n- Fikset fejl med at bruger ikke kan logge ind og opdateret Gemini'),
(55, 1, 1, 450, '2012-09-27', 'Kofoedsskole\r\n- Rettet raslebÃ¸ssen sÃ¥ den ikke lÃ¦ngere er uskarp\r\n- Publiceret seneste version til drift\r\n\r\nEuroinvestor Bullstreet\r\n- Indsat navigation pÃ¥ public-sider\r\n- Rettet navigation sÃ¥ den virker med Article side-typer\r\n- Optimeret UmbracoAdminRepository\r\n- Bedt Signe om at lave en Start-trading knap med gennemsigtig baggrund\r\n- Rettet "Start trading" knap sÃ¥ den altid er gennemsigtig\r\n- Ã†ndret StartTrading usercontrol, sÃ¥ den understÃ¸tter flere knap-typer\r\n- Lagt "Join trading" knap ind i Buttons.ascx usercontrol\r\n- Lavet lancerings vejledning:\r\n	Publicering Euroinvestor:\r\n		+ KÃ¸r Boyan''s SQL-scripts pÃ¥ databasen (2 stks)\r\n		- Importer nye dokumenttyper (Locale, Facebook, Ingame)\r\n		- Rename StartTrading macro to Buttons and replace the usercontrol with the new one\r\n		- Replace all occurences in nodes:\r\n		- da\r\n		- en\r\n		- Profil\r\n		- Afdeling\r\n		- NetvÃ¦rk\r\n		- Premier\r\n		- Markedet\r\n		- HjÃ¦lp\r\n		- Om Bullstreet\r\n		- Ghost Page\r\n		- Vis profil\r\n		- Public pages + subpages\r\n- Lagt seneste version pÃ¥ Euroinvestor drift\r\n- Lagt seneste version pÃ¥ dev.ebita.dk'),
(56, 4, 1, 450, '2012-09-28', 'Diverset ticket pÃ¥ SÃ¸ndagsavisen samt optimeringer pÃ¥ Euroinvestor Bullstreet'),
(57, 2, 1, 450, '2012-10-01', ''),
(58, 2, 1, 450, '2012-10-02', ''),
(59, 1, 3, 480, '2012-10-03', 'Ebita intern\r\n- Internt mÃ¸de omhandlende kommende konsulentarbejde for Pharmaplan\r\n\r\nEuroinvestor Bullstreet\r\n- StatusmÃ¸de omhandlende seneste samt nye bugs/Ã¦ndringer\r\n- Rettet Facebook integration sÃ¥ den spÃ¸rger om permission hvis brugeren mangler extended permissions\r\n- Rettet Facebook friend-selector sÃ¥ den ligger sig i et overlay i stedet for seperat popup\r\n- Rettet styling (knapper) sÃ¥ de ikke lÃ¦ngere har en hvid linje i toppen og bunden\r\n- Rettet styling (inputfelter) sÃ¥ knapper og tekstbokse aligner i alle browsere\r\n- Rettet profilsiden sÃ¥ navne ikke kan Ã¸delÃ¦gge designet (tilfÃ¸jet overflow)\r\n- Klippet baggrund og indsat ny styling sÃ¥ baggrunden justerer sig efter sidens hÃ¸jde\r\n- Oprettet ny Facebook-side ved navn Bullstreet Inc.\r\n- Inviteret Signe Moe og Morten JÃ¸rgensen til den nye Facebook side\r\n- Fjernet footer fra alle public pages\r\n- Alignet menu sÃ¥ den altid er i bunden af headeren (public pages + login pages)\r\n- Rettet baggrund pÃ¥ logud side, sÃ¥ den viser hele baggrundsbilledet (min-hÃ¸jde)\r\n- Gennemgang test/QA af Ã¦ndringer i forskellige browsere (IE, Firefox, Chrome)\r\n- Merget Boyans og mine Ã¦ndringer til subversion\r\n- Publiceret seneste version til dev.ebita.dk'),
(60, 1, 1, 510, '2012-10-04', 'Euroinvestor Bullstreet\r\n- Rettet styling fejl med seneste baggrundsÃ¦ndringer\r\n- Rettet stylingfejl pÃ¥ tabel/profilside\r\n- TilfÃ¸jet ekstra linje pÃ¥ 2 column public pages\r\n- Indsat clearline class pÃ¥ 2 column public pages\r\n- Opdateret baggrundbilleder med nye sÃ¥ repeation ikke er sÃ¥ tydelig\r\n- Diverse smÃ¥justeringer i design/kildekode\r\n- Lancering af Bullstreet projektet til drift \r\n- TÃ¸mt Euroinvestor Maintenance mail for tidligere e-mails sÃ¥ der ikke kommer dobbeltdata\r\n- Lancering af Maintenance til drift\r\n- ForsÃ¸gt at kontakte Allan vdr. bruger til Task Scheduler pÃ¥ bullstreet.euroinvestor.nu\r\n- LÃ¸bende kontakt med kunden (Allan) vdr. lancering til produktion hjemmefra\r\n\r\nEbita Intern IT\r\n- Talt med Robert omkring fodele ulemper ved Umbraco samt "gennemgang" af systemet\r\n- Fortalt Signe omkring tekniske specifikationer for statisk baggrundsbillede til Folkeferie'),
(61, 1, 1, 450, '2012-10-05', 'Euroinvestor Bullstreet\r\n- Rettet fejl fra gÃ¥rsdagens kommunikation sÃ¥ projektet virker med SQL 2005\r\n- Lavet nyt deployment med nye Ã¦ndringer og bugfixes\r\n- Testet / QA af ny deployment pakke\r\n- Sendt deployment og dokumentation til kunden\r\n\r\nForum / SÃ¸ndagsavisen\r\n- Fikset diverse tickets i Gemini heriblandt:\r\n- KÃ¸rt mailmove program sÃ¥ diverse brugere bliver flyttet til nye mailservere\r\n- Opgraderet diverse brugere\r\n- Bedt Morten om kontaktinformationer til at fÃ¥ liv i p-generator'),
(62, 1, 1, 450, '2012-10-08', 'Folkeferie\r\n- Internt mÃ¸de omhandlende opstart af Folkeferie\r\n\r\nSÃ¸ndagsavisen / Forum\r\n- Fikset diverse tickets\r\n\r\nKofoedsskole\r\n- Fikset betalingsmodul pÃ¥ Kofoedsskole\r\n- Fikset at diverse lister ikke viser noget indhold');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Rewrite`
--

CREATE TABLE IF NOT EXISTS `Rewrite` (
  `OriginalPath` varchar(500) NOT NULL,
  `RewritePath` varchar(500) NOT NULL,
  PRIMARY KEY (`OriginalPath`),
  KEY `RewritePath` (`RewritePath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `Rewrite`
--


-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Transfer`
--

CREATE TABLE IF NOT EXISTS `Transfer` (
  `TransferID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AccountID` int(11) DEFAULT NULL,
  `UserID` bigint(20) NOT NULL,
  `InvoiceID` bigint(20) DEFAULT NULL,
  `Text` text,
  `Amount` float NOT NULL,
  `Balance` float NOT NULL,
  `Date` date NOT NULL,
  `PubDate` datetime NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`TransferID`),
  KEY `AccountID` (`AccountID`),
  KEY `UserID` (`UserID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `Amount` (`Amount`),
  KEY `Balance` (`Balance`),
  KEY `Date` (`Date`),
  KEY `PubDate` (`PubDate`),
  KEY `Deleted` (`Deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Data dump for tabellen `Transfer`
--

INSERT INTO `Transfer` (`TransferID`, `AccountID`, `UserID`, `InvoiceID`, `Text`, `Amount`, `Balance`, `Date`, `PubDate`, `Deleted`) VALUES
(1, NULL, 1, 25, '', 1706.25, 42656.2, '2012-10-09', '2012-10-09 22:30:58', 0),
(2, NULL, 1, 25, '', 25950, 40950, '2012-09-21', '2012-10-09 22:39:40', 0),
(3, NULL, 1, 25, '', 15000, 15000, '2012-09-03', '2012-10-09 22:39:55', 0),
(4, NULL, 1, 20, '', 41562.5, 84218.8, '2012-10-09', '2012-10-09 22:45:49', 0),
(5, 2, 1, NULL, 'Overf. SKAT Â§38/6191 MODERNISERINGSSTYREL', 1953, 1953, '2012-10-02', '2012-10-10 00:05:16', 0),
(6, 2, 1, NULL, '33805675 NEGATIV MOM SKAT, BETALINGSCENTR', 2938, 4891, '2012-10-09', '2012-10-10 00:06:01', 0);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Username` varchar(300) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Email` varchar(300) DEFAULT NULL,
  `LastActivity` datetime DEFAULT NULL,
  `AdminLevel` tinyint(1) NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `Username` (`Username`),
  KEY `Password` (`Password`),
  KEY `Email` (`Email`),
  KEY `AdminLevel` (`AdminLevel`),
  KEY `Deleted` (`Deleted`),
  KEY `LastActivity` (`LastActivity`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Data dump for tabellen `User`
--

INSERT INTO `User` (`UserID`, `Username`, `Password`, `Email`, `LastActivity`, `AdminLevel`, `Deleted`) VALUES
(1, 'ss', '5fec3c7eb77e065368d5dfb0113d5d85', NULL, '2012-10-10 01:13:48', 0, 0),
(3, 'test', '5fec3c7eb77e065368d5dfb0113d5d85', 'ss@pecee.dk', '2012-10-04 19:58:11', 0, 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserConfirm`
--

CREATE TABLE IF NOT EXISTS `UserConfirm` (
  `UserConfirmID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email` varchar(500) NOT NULL,
  `Token` varchar(36) NOT NULL,
  `Activated` tinyint(1) NOT NULL,
  `Date` datetime NOT NULL,
  `IP` varchar(36) NOT NULL,
  PRIMARY KEY (`UserConfirmID`),
  KEY `Email` (`Email`),
  KEY `Token` (`Token`),
  KEY `Accepted` (`Activated`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Data dump for tabellen `UserConfirm`
--


-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserData`
--

CREATE TABLE IF NOT EXISTS `UserData` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` text NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `UserData`
--

INSERT INTO `UserData` (`UserID`, `Key`, `Value`) VALUES
(1, 'type', 'admin'),
(3, 'name', 'Simon SessingÃ¸'),
(3, 'jobtitle', ''),
(3, 'type', 'admin');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserIncorrectLogin`
--

CREATE TABLE IF NOT EXISTS `UserIncorrectLogin` (
  `Username` varchar(300) NOT NULL,
  `RequestTime` int(11) NOT NULL,
  `IPAddress` varchar(32) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `Username` (`Username`),
  KEY `RequestTime` (`RequestTime`),
  KEY `IPAddress` (`IPAddress`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `UserIncorrectLogin`
--

INSERT INTO `UserIncorrectLogin` (`Username`, `RequestTime`, `IPAddress`, `Active`) VALUES
('ss', 1349748499, '78.46.253.84', 0);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserPasswordReset`
--

CREATE TABLE IF NOT EXISTS `UserPasswordReset` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(32) NOT NULL,
  `Date` int(11) NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `UserPasswordReset`
--

